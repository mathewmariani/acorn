R"squirrel"#(

# NOTE: here we can create default objects and store them directly in squirrel.

print("acorn::grapghics has been initialized\n")

local vertex = @"
#version 330 core
layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec2 VertexTexCoord;
layout(location = 2) in vec4 VertexColor;
uniform mat4 TransformMatrix;
uniform mat4 ProjectionMatrix;
out vec3 VertPosition;
out vec2 VertTexCoord;
out vec4 VertColor;
void main() {
	VertPosition = VertexPosition;
	VertTexCoord = VertexTexCoord;
	VertColor = VertexColor;
	gl_Position = ProjectionMatrix * TransformMatrix * vec4(VertexPosition.xyz, 1.0);
}
"

local fragment = @"
#version 330 core
#define texture2D texture
in vec3 VertPosition;
in vec2 VertTexCoord;
in vec4 VertColor;
uniform sampler2D uTexture;
out vec4 FragColor;
void main() {
	FragColor = texture2D(uTexture, VertTexCoord) * VertColor;
}
"

#)squirrel"#"
