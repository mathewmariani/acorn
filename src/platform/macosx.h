#ifndef ACORN_PLATFORM_MACOSX_H
#define ACORN_PLATFORM_MACOSX_H

// acorn
#include "common/platform.h"

#if defined(OS_MACOS)

// STL
#include <string>

namespace acorn {
namespace macos {

void postNotification(const std::string &title, const std::string &message);
void postNotification(const std::string &title, const std::string &subtitle, const std::string &message);

// NSApplication

void requestUserAttention();
void systemBeep();

// NSDockTile
void showApplicationBadge(bool show);
void setBadgeCount(int count);

// NSBundle

// use this function to get the full pathname of the receiver's executable file.
const std::string getExecutablePath();

// use this function to get the full pathname of the receiver’s bundle directory.
const std::string getBundlePath();

// use this function to get the full pathname of the bundle’s subdirectory containing resources.
const std::string getResourcePath();

const std::string getApplicationSupportDirectory();

}   // macos
}   // acorn

#endif	// OS_MACOS

#endif	// ACORN_PLATFORM_MACOSX_H
