#ifndef ACORN_SQ_GRAPHICS_H
#define ACORN_SQ_GRAPHICS_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace graphics {

extern "C" int sq_acorn_graphics(SQVM *v);

}	// graphics
}	// acorn

#endif	// ACORN_SQ_GRAPHICS_H
