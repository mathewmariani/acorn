// STL
#include <stdexcept>
#include <array>

// acorn
#include "common/exception.h"
#include "image/imagedata.h"

// stb
#include "libraries/stb/stb_image.h"

namespace acorn {
namespace image {

acorn::Type ImageData::type = std::type_index(typeid(image::ImageData));

ImageData::ImageData(std::shared_ptr<acorn::Data> data) {
	// FIXME: I think stb has a better example than this
	auto decode = [&](void *data, size_t size) {
		if (!data) {
			throw acorn::Exception("could not create image");
		}
		stbi_set_flip_vertically_on_load(true);
		this->data = stbi_load_from_memory(
			(stbi_uc*) data, size, &width, &height, &channels, 0
		);
		if (!this->data) {
			throw acorn::Exception("could not load image from memory");
		}
		printf("image loaded successfully (%ix%i)\n", getWidth(), getHeight());
	};

	decode(data->getData(), data->getSize());
}

ImageData::ImageData(int width, int height) {
	// sdl mutex since we deal with memory directly
	mutex = SDL_CreateMutex();
	if (!mutex) {
		printf("Could not create mutex\n");
		return;
	}

	this->width = width;
	this->height = height;

	try {
		size_t size = width * height * 8;
		data = new unsigned char[size];

		// clear memory block
		memset(data, 0, getSize());
	} catch (std::bad_alloc &) {
		throw acorn::Exception("Out of memory");
	}
}

ImageData::~ImageData() {
	//stbi_image_free(data);
	delete[] data;
	SDL_DestroyMutex(mutex);
}

int ImageData::getWidth() const noexcept {
	return width;
}

int ImageData::getHeight() const noexcept {
	return height;
}

void ImageData::getPixel(int x, int y, std::array<uint8_t, 4> &pixel) {
	// bounds checking
	if (!(x >= 0 && x < getWidth() && y >= 0 && y < getHeight())) {
		throw acorn::Exception("out-of-range");
	}

	// NOTE: reading from memory, use a mutex
	if (SDL_LockMutex(mutex) == 0) {
		//memcpy(&pixel, data + ((y * width + x) * 8), 8);
		memcpy(&pixel, data + ((y * width + x) * 8), sizeof(pixel));
		SDL_UnlockMutex(mutex);
	} else {
		printf("Couldn't lock mutex\n");
	}
}

void ImageData::setPixel(int x, int y, std::array<uint8_t, 4> &pixel) {
	// bounds checking
	if (!(x >= 0 && x < getWidth() && y >= 0 && y < getHeight())) {
		throw acorn::Exception("out-of-range");
	}

	unsigned char *pixeldata = data + ((y * width + x) * sizeof(pixel));

	// NOTE: writting to memory, use a mutex
	if (SDL_LockMutex(mutex) == 0) {
		memcpy(pixeldata, &pixel, sizeof(pixel));
		SDL_UnlockMutex(mutex);
	} else {
		printf("Could not lock mutex\n");
	}
}

void *ImageData::getData() const {
	return data;
}

size_t ImageData::getSize() const {
	// RGBA8 = 2^8 = 256 -> [0, 255]
	return size_t(getWidth() * getHeight()) * 8;
}

}	// image
}	// acorn
