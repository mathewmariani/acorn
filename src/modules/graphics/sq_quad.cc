// acorn
#include "graphics/graphics.h"
#include "graphics/quad.h"
#include "graphics/sq_quad.h"

namespace acorn {
namespace graphics {

#define instance() (Module::getInstance<graphics::Graphics>())

static SQInteger wrap_constructor(SQVM *v) {
	float x = sqx_getnumber(v, 2, 0.0f);
	float y = sqx_getnumber(v, 3, 0.0f);
	float w = sqx_getnumber(v, 4, 0.0f);
	float h = sqx_getnumber(v, 5, 0.0f);
	float sw = sqx_getnumber(v, 6, 0.0f);
	float sh = sqx_getnumber(v, 7, 0.0f);

	auto quad = instance()->newQuad(x, y, w, h, sw, sh);
	sqx_pushinstance(v, Quad::type, quad);
	return 1;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 7, "xnnnnnn", false),
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_quad(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Quad";
	reg.functions = functions;

	return sqx_registerclass(v, Quad::type, reg);
}

}	// graphics
}	// acorn
