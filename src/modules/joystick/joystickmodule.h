#ifndef ACORN_JOYSTICK_MODULE_H
#define ACORN_JOYSTICK_MODULE_H

// STL
#include <memory>
#include <string>
#include <vector>

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "joystick/joystick.h"

// SDL
#include <SDL.h>

namespace acorn {
namespace joystick {

class JoystickModule : public acorn::Module {
public:
	static acorn::Type type;

public:
	enum class HatState {
		centered,
		up,
		right,
		down,
		left,
		right_up,
		right_down,
		left_up,
		left_down,

		// always last
		hat_max_enum
	};

public:
	JoystickModule();
	virtual ~JoystickModule();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:
	std::shared_ptr<joystick::Joystick> addJoystick(int index);
	std::shared_ptr<joystick::Joystick> removeJoystick(int index);

	void removeJoystick(std::shared_ptr<joystick::Joystick> joystick);

	std::shared_ptr<joystick::Joystick> getJoystick(int index) const noexcept;

	// use this function to count the number of joysticks attached to the system.
	int getJoystickCount() const noexcept;

	// use this function to get the implementation dependent name of a joystick.
	std::string getDeviceName(int index) const noexcept;

	void purge() {
		printf("%s : %d\n", __FUNCTION__, joysticks.size());
		for (const auto &itr : joysticks) {
			std::cout << __FUNCTION__ << "\n"
				<< "  instance.get() = " << itr.get()
				<< "  instance.use_count() = " << itr.use_count()
				<< std::endl;
		}

		joysticks.clear();
	}

public:
	bool isValidDeviceIndex(int index) const noexcept;

private:
	std::vector<std::shared_ptr<joystick::Joystick>> joysticks;

};	// joystickmodule

}	// joystick
}	// acorn

#endif	// ACORN_JOYSTICK_MODULE_H
