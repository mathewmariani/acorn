#ifndef ACORN_SQ_TIMER_H
#define ACORN_SQ_TIMER_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace timer {

extern "C" int sq_acorn_timer(SQVM *v);

}	// timer
}	// acorn

#endif	// ACORN_SQ_TIMER_H
