#ifndef ACORN_LINEARALGEBRA_H
#define ACORN_LINEARALGEBRA_H

namespace acorn {

class Vector4f {
public:
	Vector4f();
	~Vector4f() = default;

public:
	void setZero();

// static functions
public:
	static float angle(const Vector4f &a, const Vector4f &b);
	static float distance(const Vector4f &a, const Vector4f &b);
	static float dot(const Vector4f &a, const Vector4f &b);
	static float magnitude(const Vector4f &v);
	static Vector4f max(const Vector4f &a, const Vector4f &b);
	static Vector4f min(const Vector4f &a, const Vector4f &b);
	static Vector4f normalize(const Vector4f &v);

// operators
public:
	operator float* ();
	operator const float* () const;

	float& operator [] (int i);
	const float& operator [] (int i) const;

	Vector4f& operator = (const Vector4f &v);

	Vector4f operator + (const Vector4f &v) const;
	void operator += (const Vector4f &v);

	Vector4f operator - (const Vector4f &v) const;
	void operator -= (const Vector4f &v);

	Vector4f operator * (const Vector4f &v) const;
	void operator *= (const Vector4f &v);

	Vector4f operator / (const Vector4f &v) const;
	void operator /= (const Vector4f &v);

	Vector4f operator * (const float &s) const;
	void operator *= (const float &s);

	Vector4f operator / (const float &s) const;
	void operator /= (const float &s);

protected:
	float e[4];
};	// vector4f

struct Vector2f {
	float x, y;
};	// vector2f

class Matrix4 {
public:
	Matrix4();
	~Matrix4() = default;

// static functions
public:
	static Matrix4 ortho(float left, float right, float bottom, float top, float near, float far);
	// static Matrix4 indetity();

public:
	void setIndentity();
	void setTranslation(float x, float y);
	void setRotation(float angle);
	void setScale(float x, float y);

	void translate(float x, float y);
	void rotate(float angle);
	void scale(float sx, float sy);

// operators
public:
	operator float* ();
	operator const float* () const;

	Vector4f& operator [] (int i);
	const Vector4f& operator [] (int i) const;

	Matrix4 operator * (const Matrix4 &m) const;
	void operator *= (const Matrix4 &m);

private:
	Vector4f v[16];
};	// matrix4

}	// acorn

#endif	// ACORN_LINEARALGEBRA_H
