// acorn
#include "common/log.h"
#include "common/platform.h"
#include "filesystem/filesystem.h"

#if defined (OS_WINDOWS)
	#include "platform/windows.h"
#elif defined (OS_MACOS)
	#include "platform/macosx.h"
#endif

// PhysFS
#include <physfs.h>

// SDL
#include <SDL.h>

namespace acorn {
namespace filesystem {

acorn::Type Filesystem::type = std::type_index(typeid(filesystem::Filesystem));

Filesystem::Filesystem() {
	log_debug("Filesystem module initialized.");
}

Filesystem::~Filesystem() {
	log_debug("Filesystem module deinitialized.");

	if (PHYSFS_isInit()) {
		PHYSFS_deinit();
	}
}

void Filesystem::init(const char *arg0) {
	if (!PHYSFS_init(arg0)) {
		log_error("%s", PHYSFS_getLastError());
		return;
	}
}

bool Filesystem::setIdentity(const std::string &org, const std::string &app) noexcept {
	printf("organization: %s\napplication: %s\n", org.c_str(), app.c_str());
	return false;
}

bool Filesystem::setSaveIdentity(const std::string &identity) noexcept {
	if (!PHYSFS_isInit()) {
		return false;
	}

	// FIXME:
	// Get the user-and-app-specific path where files can be written.
	// PHYSFS_getPrefDir()

	//printf("PHYSFS_getPrefDir() : %s\n", PHYSFS_getPrefDir("", ""));	// invalid.
	//printf("PHYSFS_getPrefDir() : %s\n", PHYSFS_getPrefDir("acorn2d", ""));	// invalid.
	//printf("PHYSFS_getPrefDir() : %s\n", PHYSFS_getPrefDir("", "mygame"));	// invalid.
	//printf("PHYSFS_getPrefDir() : %s\n", PHYSFS_getPrefDir("acorn2d", "mygame"));	// valid.

	printf("SDL_GetBasePath() : %s\n", SDL_GetBasePath());
	printf("SDL_GetPrefPath() : %s\n", SDL_GetPrefPath("acorn2d", "mygame"));
	//printf("PHYSFS_getPrefDir() : %s\n", PHYSFS_getPrefDir("acorn2d", "mygame"));


	saveidentity = identity;

	std::string path = std::string(
		getAppdataDirectory() + std::string(ACORN_PATH_SEPARATOR)
	);

	printf("%s\n", path.c_str());
	if (!PHYSFS_setWriteDir(path.c_str())) {
		printf("Unable to create write directory '%s': %s\n", path.c_str(), PHYSFS_getLastError());
		return false;
	}

	if (PHYSFS_getWriteDir() == 0) {
		return false;
	}

	if (!makeDirectory(identity)) {
		return false;
	}

	path += identity;
	printf("%s\n", path.c_str());
	if (!PHYSFS_setWriteDir(path.c_str())) {
		printf("Unable to create write directory '%s': %s\n", path.c_str(), PHYSFS_getLastError());
		return false;
	}

	return true;
}

const std::string Filesystem::getRealDirectory(const std::string &filename) const {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return "";
	}

	return PHYSFS_getRealDir(filename.c_str());
}

const std::string Filesystem::getWriteDirectory() const {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return "";
	}

	const char *dir = PHYSFS_getWriteDir();
	if (dir == nullptr) {
		return "";
	}

	return std::string(dir);
}

// NOTE: this may not be useful...
const std::string Filesystem::getAppdataDirectory() noexcept {
	if (appdatadir.empty()) {
#if defined (OS_WINDOWS)
		wchar_t *w_appdata = _wgetenv(L"APPDATA");
		std::wstring ws(w_appdata);
		appdatadir = std::string(ws.begin(), ws.end());
#elif defined (OS_MACOS)
		appdatadir = acorn::macos::getApplicationSupportDirectory();
#else
#error Missing implementation for Filesystem::getAppdataDirectory
#endif
	}

	return appdatadir;
}

// NOTE: while this directory might be writable, or even parents of the returned path, it isn't where you should be writing things.
const std::string Filesystem::getBaseDirectory() noexcept {
	if (basedir.empty()) {
		basedir = PHYSFS_getBaseDir();
	}

	return basedir;
}

const std::string Filesystem::getCurrentWorkingDirectory() noexcept {
	if (currentdir.empty()) {
#if defined (OS_WINDOWS)
		return acorn::windows::getCurrentWorkingDirectory();
#elif defined (OS_MACOS)
		return acorn::macos::getBundlePath();
#else
#error Missing implementation for Filesystem::getExecutablePath
#endif
	}

	return currentdir;
}

const std::string Filesystem::getExecutablePath() noexcept {
	if (exepath.empty()) {
#if defined (OS_WINDOWS)
		exepath = acorn::windows::getExecutablePath();
#elif defined (OS_MACOS)
		exepath = acorn::macos::getExecutablePath();
#else
#error Missing implementation for Filesystem::getExecutablePath
#endif
	}

	return exepath;
}

const std::string Filesystem::getPrefDirectory() noexcept {
    if (prefdir.empty()) {
    #if defined(OS_MACOS)
        prefdir = std::string(SDL_GetPrefPath("acorn", "mygame"));
    #else
        prefdir = std::string(PHYSFS_getPrefDir("acorn", "mygame"));
    #endif
    }

    return prefdir;
}

// NOTE: while this directory might be writable, or even parents of the returned path, it isn't where you should be writing things.
const std::string Filesystem::getUserDirectory() const {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return "";
	}

	const char *dir = PHYSFS_getUserDir();
	if (dir == nullptr) {
		return "";
	}

	return std::string(dir);
}


bool Filesystem::exists(const std::string &path) const noexcept {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return false;
	}

	return PHYSFS_exists(path.c_str());
}

// NOTE: may not be useful
bool Filesystem::setSource(const std::string &path) const noexcept {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return false;
	}

	if (!PHYSFS_mount(path.c_str(), nullptr, 1)) {
		return false;
	}

	return true;
}

bool Filesystem::isDirectory(const std::string &filename) const noexcept {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return false;
	}

	return PHYSFS_isDirectory(filename.c_str());
}

bool Filesystem::isFile(const std::string &filename) const noexcept {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return false;
	}

	STUBBED("PHYSFS_isFile isn't a thing.");
	return (!PHYSFS_isDirectory(filename.c_str()) && !PHYSFS_isSymbolicLink(filename.c_str()));
}

bool Filesystem::getStat(const std::string &path, Stat &info) const noexcept {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return false;
	}

#if !defined(OS_MACOS)
	PHYSFS_Stat stat = {};
	if (!PHYSFS_stat(path.c_str(), &stat)) {
		return false;
	}

	// size in bytes, -1 for non-files and unknown
	info.filesize = (int64) stat.filesize;

	// last modification time
	info.modtime = (int64) stat.modtime;

	// like modtime, but for file creation time
	info.createtime = (int64) stat.createtime;

	// type of a file
	switch (stat.filetype) {
	case PHYSFS_FILETYPE_REGULAR:
		// a normal file
		info.type = FileType::FILE;
		break;
	case PHYSFS_FILETYPE_DIRECTORY:
		// a directory
		info.type = FileType::DIRECTORY;
		break;
	case PHYSFS_FILETYPE_SYMLINK:
		// a symlink
		info.type = FileType::SYMLINK;
		break;
	case PHYSFS_FILETYPE_OTHER:
	default:
		// something completely different like a device
		info.type = FileType::OTHER;
		break;
	}
#endif

	return true;
}

bool Filesystem::isSymbolicLink(const std::string &filename) const noexcept {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return false;
	}

	return PHYSFS_isSymbolicLink(filename.c_str());
}

bool Filesystem::mount(const std::string &path) const noexcept {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return false;
	}

	return PHYSFS_mount(path.c_str(), nullptr, true);
}

bool Filesystem::unmount(const std::string &path) const noexcept {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return false;
	}

#if defined(OS_MACOS)
	return PHYSFS_removeFromSearchPath(path.c_str());
#else
	return PHYSFS_unmount(path.c_str());
#endif
}

void Filesystem::getSearchPath(std::vector<std::string> &list) {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return;
	}

	char **rc = PHYSFS_getSearchPath();
	if (rc == nullptr) {
		return;
	}

	for (char **i = rc; *i != 0; ++i) {
		list.push_back(*i);
	}

	// deallocate resources of lists returned by PhysicsFS.
	PHYSFS_freeList(rc);
}

bool Filesystem::makeDirectory(const std::string &dirpath) const noexcept {
	if (!PHYSFS_isInit()) {
		return false;
	}

	if (PHYSFS_getWriteDir() == 0) {
		return false;
	}

	if (!PHYSFS_mkdir(dirpath.c_str())) {
		return false;
	}

	return true;
}

bool Filesystem::remove(const std::string &filename) const noexcept {
	if (!PHYSFS_isInit()) {
		return false;
	}

	if (PHYSFS_getWriteDir() == 0) {
		return false;
	}

	if (!PHYSFS_delete(filename.c_str())) {
		return false;
	}

	return true;
}

void Filesystem::enumerateFiles(const std::string &path, std::vector<std::string> &list) {
	if (!PHYSFS_isInit()) {
		log_error("PhysFS is not initialized.");
		return;
	}

	char **rc = PHYSFS_enumerateFiles(path.c_str());
	if (rc == nullptr) {
		return;
	}

	for (char **i = rc; *i != 0; ++i) {
		list.push_back(*i);
	}

	// deallocate resources of lists returned by PhysicsFS.
	PHYSFS_freeList(rc);
}

std::shared_ptr<File> Filesystem::read(const std::string &filename, int64 size) const {
	//File file(filename);
	//file.open();
	//return file.read(size);

	STUBBED("file systems are hard...");

	return nullptr;
}

std::shared_ptr<File> Filesystem::newFile() const {
	STUBBED("factory function for Filesystem::File");
	return nullptr;
}

std::shared_ptr<FileData> Filesystem::newFileData() const {
	STUBBED("factory function for Filesystem::FileData");
	return nullptr;
}

// FIXME: I don't know if this is needed
//std::unordered_map<std::string, Filesystem::FileType> Filesystem::filetypeTable = {
//	{ "File", Filesystem::FileType::FILE },
//	{ "Directory", Filesystem::FileType::DIRECTORY },
//	{ "Symlink", Filesystem::FileType::SYMLINK },
//	{ "Other", Filesystem::FileType::OTHER },
//};

}	// filesystem
}	// acorn
