#ifndef ACORN_MATH_TRANSFORM_H
#define ACORN_MATH_TRANSFORM_H

// acorn
#include "common/object.h"

// GLM
#include <glm/glm.hpp>

namespace acorn {
namespace math {

class Transform : public acorn::Object {
public:
	static acorn::Type type;

public:
	Transform();
	Transform(float x, float y, float a, float sx, float sy, float ox, float oy);
	virtual ~Transform();

	void translate(float x, float y);
	void rotate(float angle);
	void scale(float x, float y);

	void setIndetity();
	void setTransformation(float x, float y, float a, float sx, float sy, float ox, float oy);

	const glm::mat4 &getMatrix() const;
	void setMatrix(const glm::mat4 &m);

private:
	glm::mat4 matrix;
};	// transform

}	// math
}	// acorn

#endif	// ACORN_MATH_TRANSFORM_H
