#ifndef ACORN_RESOURCE_FONTDATA_H
#define ACORN_RESOURCE_FONTDATA_H

// C/C++
#include <memory>

// acorn
#include "common/data.h"
#include "common/object.h"
#include "resource/glyphdata.h"

// freetype2
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

// stb
#include "libraries/stb/stb_truetype.h"

// NOTE: stb_truetype doesnt seem to want to work for me...
// NOTE: here we do all the rasterization; the font module handles the FreeType2 library
#define FREETYPE_IMPL 1

namespace acorn {
namespace resource {

class FontData : public acorn::Object {
public:
	static acorn::Type type;

public:
	struct FontMetrics {
		int advance;
		int ascent;
		int descent;
		int height;
	};	// fontmetrics

public:
	FontData(FT_Library library, std::shared_ptr<acorn::Data> data, int ptsize);
	virtual ~FontData();

public:
	// FIXME: const int ... const?
	int getAdvance() const;
	int getAscent() const;
	int getDescent() const;
	int getGlyphCount() const;
	int getHeight() const;

	bool hasGlyph(int codepoint) const;
	GlyphData *getGlyphData(uint32_t codepoint);

private:
#if (FREETYPE_IMPL)
	FT_Face	face;
#else
	stbtt_fontinfo font;
	float scale;
#endif

	FontMetrics metrics;
	std::shared_ptr<acorn::Data> data;
};	// fontdata

}	// resource
}	// acorn

#endif	// ACORN_RESOURCE_FONTDATA_H
