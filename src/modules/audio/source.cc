// acorn
#include "audio/source.h"

namespace acorn {
namespace audio {

acorn::Type Source::type = std::type_index(typeid(audio::Source));

Source::Source(std::shared_ptr<resource::SoundData> sounddata) :
 	sounddata(sounddata) {

	// create an audio source
	ALuint source;
	alGenSources(1, &source);

	alSourcef(source, AL_PITCH, 1.0f);
	alSourcef(source, AL_GAIN, 1.0f);
	alSource3f(source, AL_POSITION, 0.0f, 0.0f, 0.0f);
	alSource3f(source, AL_VELOCITY, 0.0f, 0.0f, 0.0f);
	alSourcei(source, AL_LOOPING, AL_FALSE);

	// convert data to openal buffer
	alGenBuffers(1, &buffer);
	unsigned int dataSize = ((sounddata->getSampleCount() * sounddata->getChannels() * sounddata->getSampleSize()) / 8);

	// upload sound data to buffer
	// alBufferData(buffer, format, sound->data, dataSize, data->sampleRate);

	// attach sound buffer to source
	alSourcei(source, AL_BUFFER, buffer);
}

Source::~Source() {
	if (source != 0) {
		alDeleteSources(1, &source);
		source = 0;
	}
	if (buffer != 0) {
		alDeleteBuffers(1, &buffer);
		buffer = 0;
	}
}

bool Source::create() {
	return (alGetError() == AL_NO_ERROR);
}

ALenum Source::getFormat() {
	ALenum format = 0;
	auto stereo = (sounddata->getChannels() > 1);
	switch (sounddata->getSampleSize()) {
	case 8: format = (stereo) ? AL_FORMAT_STEREO8 : AL_FORMAT_MONO8; break;
	case 16: format = (stereo) ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16; break;
	default: printf("Sound sample size not supported: %i\n", sounddata->getSampleSize()); break;
	}
	return format;
}

}	// audio
}	// acorn
