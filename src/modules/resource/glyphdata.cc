// acorn
#include "resource/fontdata.h"
#include "resource/glyphdata.h"

namespace acorn {
namespace resource {

acorn::Type GlyphData::type = std::type_index(typeid(resource::GlyphData));

GlyphData::GlyphData(int codepoint, GlyphMetrics metrics) :
 	codepoint(codepoint),
	metrics(metrics) {

}

GlyphData::~GlyphData() {

}

int GlyphData::getCodepoint() const {
	return codepoint;
}

int GlyphData::getAdvance() const {
	return metrics.advance;
}

int GlyphData::getBearingX() const {
 return metrics.bearingX;
}

int GlyphData::getBearingY() const {
 return metrics.bearingY;
}

int GlyphData::getHeight() const {
	return metrics.height;
}

int GlyphData::getWidth() const {
	return metrics.width;
}

}	// resource
}	// acorn
