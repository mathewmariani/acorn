// acorn
#include "common/exception.h"
#include "graphics/graphics.h"
#include "graphics/framebuffer.h"

#include "graphics/opengl/opengl.h"

namespace acorn {
namespace graphics {

acorn::Type Framebuffer::type = std::type_index(typeid(graphics::Framebuffer));

Framebuffer::Framebuffer(Texture::TextureType type, int width, int height) :
	acorn::graphics::Texture(type),
	fbo(0), texture(0), rbo(0) {

	this->width = width;
	this->height = height;

	initializeQuad();

	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// color attachment
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

	// depth/stenchil attachment
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE) {
		printf("failed to make complete framebuffer object %x", status);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glCheckError();
}

Framebuffer::~Framebuffer() {
	if (fbo != 0) {
		glDeleteFramebuffers(1, &fbo);
	}

	if (rbo != 0) {
		glDeleteRenderbuffers(1, &rbo);
	}

	if (texture != 0) {
		glDeleteTextures(1, &texture);
	}

	fbo = 0;
	rbo = 0;
	texture = 0;
}

void Framebuffer::bind() {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glCheckError();
}

void Framebuffer::unbind() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDisable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT);
	glCheckError();
}

void Framebuffer::draw(std::shared_ptr<Graphics> gfx, const glm::mat4 &m) {
	Texture::draw(gfx, quad, m);
}

const void *Framebuffer::getHandle() const {
	return &texture;
}

}	// graphics
}	// acorn
