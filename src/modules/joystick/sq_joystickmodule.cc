// acorn
#include "joystick/sq_joystick.h"
#include "joystick/sq_joystickmodule.h"
#include "joystick/joystickmodule.h"

namespace acorn {
namespace joystick {

// NOTE: types for acorn.joystick
const static SQFunction classes[] = {
	sq_acorn_joystick,
	nullptr
};

extern "C" int sq_acorn_joystickmodule(SQVM *v) {
	auto instance = std::make_shared<joystick::JoystickModule>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize joystick module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "joystick";
	reg.classes = classes;

	return acorn::sqx_registermodule_ext(v, reg);
}

}	// joystick
}	// acorn
