#ifndef ACORN_SQ_DISPLAY_H
#define ACORN_SQ_DISPLAY_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace window {

extern "C" int sq_register_display(SQVM *v);

}	// window
}	// acorn

#endif	// ACORN_SQ_DISPLAY_H
