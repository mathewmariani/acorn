#ifndef ACORN_RESOURCE_H
#define ACORN_RESOURCE_H

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"

namespace acorn {
namespace resource {

class Resource : public acorn::Module {
public:
	static acorn::Type type;

public:
	Resource();
	virtual ~Resource();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }
};	// resource

}	// resource
}	// acorn

#endif	// ACORN_RESOURCE_H
