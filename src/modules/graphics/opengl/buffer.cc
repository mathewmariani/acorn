// acorn
#include "common/exception.h"
#include "graphics/opengl/buffer.h"

namespace acorn {
namespace graphics {
namespace opengl {

Buffer::Buffer(size_t size, BufferType type, BufferUsage usage) :
	acorn::graphics::Buffer(size, type, usage),
	vbo(0),
	offset(0),
	memory(nullptr) {

	switch (type) {
	case Buffer::BufferType::Vertex: target = GL_ARRAY_BUFFER; break;
	case Buffer::BufferType::Index: target = GL_ELEMENT_ARRAY_BUFFER; break;
	}

	try {
		memory = new char[size];
	} catch (std::bad_alloc&) {
		throw acorn::Exception("out of memory");
	}

	if (!create()) {
		delete[] memory;
		throw acorn::Exception("could not create vertex buffer");
	}
}

Buffer::~Buffer() {
	if (vbo != 0) {
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}
	delete[] memory;
}

void *Buffer::bind() {
	// FIXME: how much memory are we binding, and how much has already been bound?
	return memory + offset;
}

size_t Buffer::unbind(size_t usedsize) {

	// FIXME:
	// we error here because every draw command we overwrite the same part of the buffer
	// we need a way to use the entire buffer

	// glMapBufferRange can, as its name suggests, map only specific subsets of
	// the buffer. If only a portion of the buffer changes, there is no need to reupload
	// it completely

	// NOTE: more updated version
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	float *vertices = (float *) (glMapBufferRange(
		GL_ARRAY_BUFFER, offset, usedsize,
		(GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT)
	));

	if (vertices == nullptr) {
		throw std::runtime_error("Failed to map stream buffer.");
	}

	memcpy(vertices, memory + offset, usedsize);

	// FIXME: need to flush buffer
	// flush buffer range
	//glFlushMappedBufferRange(target, offset, size);

	// unmap buffer
	glUnmapBuffer(target);
	glBindBuffer(target, 0);

	// increment offset
	offset = offset + usedsize;

	glCheckError();

	return offset;
}

bool Buffer::create() {
	GLenum glusage = 0;
	switch (usage) {
	case Buffer::BufferUsage::Stream: glusage = GL_STREAM_DRAW; break;
	case Buffer::BufferUsage::Dynamic: glusage = GL_DYNAMIC_DRAW; break;
	case Buffer::BufferUsage::Static: glusage = GL_STATIC_DRAW; break;
	}

	glGenBuffers(1, &vbo);
	glBindBuffer(target, vbo);

	// reserve memory
	glBufferData(target, (GLsizeiptr) size, nullptr, glusage);

	// FIXME: I don't know if this is the appropriate place for this...
	// NOTE: buffer's shouldn't really care about how the data is stored
	// NOTE: keep for now.

	// position
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void *) 0);
	glEnableVertexAttribArray(0);

	// texcoord
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*) (2 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return (glGetError() == GL_NO_ERROR);
}

void Buffer::nextFrame() {
	offset = 0;
}

const void *Buffer::getHandle() const {
	return &vbo;
}

}	// opengl
}	// graphics
}	// acorn
