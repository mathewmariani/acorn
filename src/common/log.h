#ifndef ACORN_LOG_H
#define ACORN_LOG_H

namespace acorn {

enum { LOG_TRACE, LOG_INFO, LOG_ERR, LOG_WARN, LOG_DEBUG };

#define log_trace(...)  _log(LOG_TRACE, __FILE__, __LINE__, __VA_ARGS__)
#define log_info(...)  _log(LOG_INFO, __FILE__, __LINE__, __VA_ARGS__)
#define log_error(...) _log(LOG_ERR, __FILE__, __LINE__, __VA_ARGS__)
#define log_warn(...)  _log(LOG_WARN, __FILE__, __LINE__, __VA_ARGS__)
#define log_debug(...) _log(LOG_DEBUG, __FILE__, __LINE__, __VA_ARGS__)

void _log(int level, const char *file, int line, const char *str, ...);

}	// acorn

#endif	// ACORN_LOG_H
