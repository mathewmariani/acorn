// acorn
#include "platform/windows.h"

#if defined(OS_WINDOWS)

// windows
#include <windows.h>
#include <direct.h>

namespace acorn {
namespace windows {

void requestUserAttention() {
	// https://msdn.microsoft.com/en-us/library/ms679348(v=VS.85).aspx
	// NOTE: needs access to SDL window info
	// SDL_SysWMinfo.info.win.window;

	/*
	 * FLASHW_ALL			Flash both the window caption and taskbar button.
	 * FLASHW_CAPTION		Flash the window caption.
	 * FLASHW_STOP			Stop flashing.
	 * FLASHW_TIMER			Flash continuously, untill stop flag is set.
	 * FLASHW_TIMERNOFG		Flash continuously until the window comes to the foreground.
	 * FLASHW_TRAY			Flash the taskbar button.
	 */

	//FLASHWINFO flashinfo = {};
	//flashinfo.cbSize = sizeof(FLASHWINFO);
	//flashinfo.hwnd = nullptr; // SDL_SysWMinfo.info.win.window
	//flashinfo.uCount = 1;
	//flashinfo.dwFlags = FLASHW_ALL;

	//FlashWindowEx(&flashinfo);
}

void systemBeep() {
	/*
	 * MB_ICONASTERISK
	 * MB_ICONEXCLAMATION
	 * MB_ICONERROR
	 * MB_ICONHAND
	 * MB_ICONINFORMATION
	 * MB_ICONQUESTION
	 * MB_ICONSTOP
	 * MB_ICONWARNING
	 * MB_OK
	 */
	MessageBeep(MB_OK);
}

void postNotification() {
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ee330740(v=vs.85).aspx#display_notification
}

const std::string getAppdataDirectory() {
	wchar_t *w_appdata = _wgetenv(L"APPDATA");
	std::wstring ws(w_appdata);
	return std::string(ws.begin(), ws.end());
}

const std::string getCurrentWorkingDirectory() {
	char c_cwd[MAX_PATH];
	_getcwd(c_cwd, MAX_PATH);
	return std::string(c_cwd);
}

const std::string getExecutablePath() {
	wchar_t buffer[MAX_PATH];
	if (GetModuleFileNameW(nullptr, buffer, MAX_PATH) == 0) {
		return "";
	}
	std::wstring ws(buffer);
	return std::string(ws.begin(), ws.end());
}

}	// windows
}	// acorn

#endif	// OS_WINDOWS
