// acorn
#include "filesystem/sq_file.h"
#include "filesystem/file.h"
#include "filesystem/filesystem.h"

namespace acorn {
namespace filesystem {

#define instance() (Module::getInstance<filesystem::Filesystem>())

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "File cannot be constructed directly.");
}

static SQInteger wrap_exists(SQVM *v) {
	const char *path;
	sq_getstring(v, 2, &path);
	sq_pushbool(v, instance()->exists(path));
	return 1;
}

static SQInteger wrap_read(SQVM *v) {

	int bytes;
	const char *path;

	sq_getstring(v, 3, &path);
	sq_getinteger(v, 2, (SQInteger *) &bytes);

	//instance()->read(path, bytes);

	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),

	// static
	_DECL_FUNC(exists, 1, "y", true),
	_DECL_FUNC(read, 1, "ysn", true),


	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_file(SQVM *v) {
	ClassRegistry reg;
	reg.name = "File";
	reg.functions = functions;
	return sqx_registerclass(v, File::type, reg);
}

}	// filesystem
}	// acorn
