// acorn
#include "image/sq_imagedata.h"
#include "image/sq_image.h"
#include "image/image.h"

namespace acorn {
namespace image {

#define instance() (sqx_getmodule<Image>(v, "image"))

// NOTE: functions for acorn.system
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.system
const static SQFunction classes[] = {
	sq_register_imagedata,
	nullptr
};

extern "C" int sq_acorn_image(SQVM *v) {
	auto instance = std::make_shared<image::Image>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize image module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "image";
	reg.classes = classes;

	return acorn::sqx_registermodule_ext(v, reg);
}

}	// image
}	// acorn
