#ifndef ACORN_GRAPHICS_OPENGL_H
#define ACORN_GRAPHICS_OPENGL_H

// C/C++
#include <stack>

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"

#include "graphics/framebuffer.h"
#include "graphics/image.h"
#include "graphics/quad.h"
#include "graphics/texture.h"

// FIXME: these need to be moved to graphics::opengl::Graphics
#include "graphics/opengl/buffer.h"
#include "graphics/opengl/shaderstage.h"
#include "graphics/opengl/shader.h"

// gl3w
#include "libraries/gl3w/gl3w.h"

// glm
#include <glm/glm.hpp>

namespace acorn {

// forward declaration
namespace window { class Window; }

namespace graphics {

class Graphics : public acorn::Module, public std::enable_shared_from_this<Graphics> {
public:
	static acorn::Type type;

public:
	struct RendererInfo {
		std::string name;
		std::string version;
		std::string vendor;
		std::string device;
	};	// rendererinfo

	struct DrawCommand {
		GLenum mode = GL_TRIANGLES;
		GLint first = 0;
		GLsizei count = 0;
		Texture *texture = nullptr;
	};	// drawcommand

	struct DrawIndexedCommand {
		GLenum mode = GL_TRIANGLES;
		GLint first = 0;
		GLsizei count = 0;
		Resource *indices = nullptr;
		Texture *texture = nullptr;
	};	// drawindexedcommand

protected:
	struct MatrixStack {
		std::stack<glm::mat4> projection;
		std::stack<glm::mat4> transform;
	};	// matrixstack

public:
	Graphics();
	virtual ~Graphics();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

	RendererInfo getRendererInfo() const;

	int getWidth() const noexcept;
	int getHeight() const noexcept;

// state
public:
	bool setMode(int width, int height);

	void clear(float r, float g, float b, float a);
	void present();
	void setColor(float r, float g, float b, float a);

	void prepareDraw();

	void setPointSize(float size);

	// polygon mode
	//glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

// shader
public:
	void setShader(acorn::graphics::Shader *shader = nullptr);

// matrix
public:
	void push();
	void push(const glm::mat4 &m);
	void pop();
	void identity();
	void translate(float x, float y);
	void rotate(float angle);
	void scale(float x, float y);

	const glm::mat4 &getTransform() const;
	const glm::mat4 &getProjection() const;

// framebuffer
public:
	void bindFramebuffer(std::shared_ptr<Framebuffer> framebuffer);

// drawing
public:
	void points(std::vector<float> &points);
	void triangle(std::vector<float> &points);
	void polygon(std::vector<float> &points);

	void draw(std::shared_ptr<Texture> texture, const glm::mat4 &m);
	void draw(GLenum mode, GLsizei count, GLenum type, const GLvoid *indices, const void *ptr);
	
	// NOTE: newest
	void flush();
	void flush(const DrawCommand &cmd);
	void draw(const DrawCommand &cmd);
	void draw(const DrawIndexedCommand &cmd);

	// NOTE: draw pipeline
	Buffer *ext_requestDraw(const DrawCommand &cmd);
	void ext_flushDraw();
	void ext_draw(const DrawCommand &cmd);
	

	// FIXME: not final
	void drawArrays(GLenum mode, GLint first, GLsizei count);
	void drawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid *indices);

public:
	void flushBuffers();
	//void draw(Texture *texture, Buffer *buffer, GLenum mode, int first, int count);
	//void drawIndexed(Texture *texture, Buffer *buffer, GLenum mode, int count, GLenum type, const GLvoid *indices);
	//void draw(Image *texture, Buffer *buffer, GLenum mode, int first, int count);

// factory
public:
	std::shared_ptr<Framebuffer> newFramebuffer(int width, int height);
	std::shared_ptr<Quad> newQuad(int x, int y, int w, int h, int sw, int sh);
	std::shared_ptr<opengl::ShaderStage> newShaderStage(ShaderStage::StageType glstage, const std::string &source);
	std::shared_ptr<opengl::Shader> newShader(const std::string &vertex, const std::string &fragment);

private:
	int width;
	int height;

	GLuint vao;
	MatrixStack matrices;
	Colorf color;

	// NOTE: not final
	opengl::Buffer *buffer;

	// state
	Texture *state_texture;
	GLenum state_mode = GL_TRIANGLE_STRIP;
	int state_start = 0;
	int state_count = 0;
	

	// NOTE: not final
	GLuint points_vbo = 0;
	GLuint triangle_vbo = 0;
	GLuint polygon_vbo = 0;
	GLuint image_vbo = 0;
	GLuint image_ebo = 0;

// buffer
private:

};	// graphics

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_OPENGL_H
