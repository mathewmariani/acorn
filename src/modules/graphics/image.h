#ifndef ACORN_GRAPHICS_IMAGE_H
#define ACORN_GRAPHICS_IMAGE_H

// C/C++
#include <memory>

// acorn
#include "graphics/texture.h"
#include "image/imagedata.h"

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace acorn {
namespace graphics {

// forward declaration
class Graphics;

class Image : public graphics::Texture {
public:
	static acorn::Type type;

public:
	Image(TextureType type, std::shared_ptr<image::ImageData> imagedata);
	virtual ~Image();

private:
	// strong reference
	std::shared_ptr<image::ImageData> imagedata;
};	// image

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_IMAGE_H
