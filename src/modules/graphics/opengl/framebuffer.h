#ifndef ACORN_GRAPHICS_OPENGL_FRAMEBUFFER_H
#define ACORN_GRAPHICS_OPENGL_FRAMEBUFFER_H

// acorn
#include "graphics/opengl/opengl.h"
#include "graphics/framebuffer.h"

namespace acorn {
namespace graphics {
namespace opengl {

class Framebuffer : public acorn::graphics::Framebuffer {
public:
	Framebuffer(Texture::TextureType type, int width, int height);
	virtual ~Framebuffer();

public:
	// managed opengl resource
	const void *getHandle() const override;

private:
	GLuint fbo;
	GLuint texture;
	GLuint rbo;
};	// framebuffer

}	// opengl
}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_OPENGL_FRAMEBUFFER_H
