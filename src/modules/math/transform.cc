// acorn
#include "math/transform.h"

// glm
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace acorn {
namespace math {

acorn::Type Transform::type = std::type_index(typeid(math::Transform));

Transform::Transform() : matrix(1.0f) {

}

Transform::Transform(float x, float y, float a, float sx, float sy, float ox, float oy) : matrix(1.0f) {
	setTransformation(x, y, a, sx, sy, ox, oy);
}

Transform::~Transform() {

}

void Transform::translate(float x, float y) {
	matrix = glm::translate(matrix, glm::vec3(x, y, 0.0f));
}

void Transform::rotate(float angle) {
	matrix = glm::rotate(matrix, angle, glm::vec3(0.0, 0.0, 1.0f));
}

void Transform::scale(float x, float y) {
	matrix = glm::scale(matrix, glm::vec3(x, y, 1.0f));
}

void Transform::setIndetity() {
	matrix = glm::mat4(1.0f);
}

void Transform::setTransformation(float x, float y, float a, float sx, float sy, float ox, float oy) {
	matrix = glm::translate(matrix, glm::vec3(ox, oy, 0.0f));
	matrix = glm::scale(matrix, glm::vec3(sx, sy, 0.0f));
	matrix = glm::rotate(matrix, a, glm::vec3(0.0, 0.0, 1.0f));
	matrix = glm::translate(matrix, glm::vec3(x, y, 0.0f));
}

const glm::mat4 &Transform::getMatrix() const {
	return matrix;
}

void Transform::setMatrix(const glm::mat4 &m) {
	matrix = m;
}

}	// math
}	// acorn
