#ifndef ACORN_SQ_IMAGE_H
#define ACORN_SQ_IMAGE_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace image {

extern "C" int sq_acorn_image(SQVM *v);

}	// image
}	// acorn

#endif	// ACORN_SQ_IMAGE_H
