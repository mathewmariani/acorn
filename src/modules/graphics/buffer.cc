// acorn
#include "common/exception.h"
#include "graphics/buffer.h"

namespace acorn {
namespace graphics {

Buffer::Buffer(size_t size, BufferType type, BufferUsage usage) :
	size(size),
	type(type),
	usage(usage) {

}

Buffer::~Buffer() {

}

size_t Buffer::getSize() const noexcept {
	return size;
}

Buffer::BufferType Buffer::getType() const noexcept {
	return type;
}

Buffer::BufferUsage Buffer::getUsage() const noexcept {
	return usage;
}

}	// graphics
}	// acorn
