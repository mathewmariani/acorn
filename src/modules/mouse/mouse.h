#ifndef ACORN_MOUSE_H
#define ACORN_MOUSE_H

// STL
#include <map>

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"
#include "mouse/cursor.h"

// SDL
#include <SDL.h>

namespace acorn {
namespace mouse {

class Mouse : public acorn::Module {
public:
	static acorn::Type type;

public:
	enum class Button : uint32 {
		left = SDL_BUTTON_LEFT,
		middle = SDL_BUTTON_MIDDLE,
		right = SDL_BUTTON_RIGHT,
		x1 = SDL_BUTTON_X1,
		x2 = SDL_BUTTON_X2,
	};

public:
	Mouse();
	virtual ~Mouse();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:
	// use this function to retrieve the current state of the mouse; specifically the position.
	int getX() const noexcept;

	// use this function to retrieve the current state of the mouse; specifically the position.
	int getY() const noexcept;

	// use this function to retrieve the current state of the mouse; specifically the position.
	void getPosition(int &x, int &y) const noexcept;

	// use this function to move the mouse to the given position in global screen space.
	void setPosition(int x, int y) noexcept;

	// use this function to move the mouse to the given position within the window.
	void setPositionInWindow(int x, int y) noexcept;

	// use this function to query whether or not the cursor is shown.
	bool getVisible() const noexcept;

	// use this function to set whether or not the cursor is shown.
	void setVisible(bool visible) noexcept;

	// use this function to query whether relative mouse mode is enabled.
	bool getRelativeMode() const noexcept;

	// use this function to set whether relative mouse mode is enabled.
	bool setRelativeMode(bool relative) noexcept;

	// use this function to retrieve the current state of the mouse; specifically the current button state.
	bool isDown(int index) const noexcept;

	// use this function to retrieve the current state of the mouse; specifically the current button state.
	bool isDown(const std::vector<int> &index) const noexcept;

public:
	// use this function to create a system cursor.
	std::shared_ptr<Cursor> getSystemCursor(Cursor::SystemCursorType type) noexcept;

	// use this function to set the active cursor.
	void setSystemCursor(std::shared_ptr<mouse::Cursor> cursor) noexcept;

private:
	std::map<Cursor::SystemCursorType, std::shared_ptr<Cursor>> systemCursors;
};	// mouse

}	// mouse
}	// acorn

#endif	// ACORN_MOUSE_H
