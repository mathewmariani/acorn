// acorn
#include "common/log.h"
#include "common/platform.h"
#include "font/font.h"

namespace acorn {
namespace font {

acorn::Type Font::type = std::type_index(typeid(font::Font));


Font::Font() {
	log_debug("Font module initialized.");

	if (FT_Init_FreeType(&library)) {
		log_error("Could not init freetype library.");
    // FIXME: should throw error
		return;
	}
}

Font::~Font() {
	log_debug("Font module deinitialized.");
	FT_Done_FreeType(library);
}

const void *Font::getHandle() const noexcept {
	return &library;
}

}	// font
}	// acorn
