#ifndef ACORN_SQ_EVENT_H
#define ACORN_SQ_EVENT_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace event {

extern "C" int sq_acorn_event(SQVM *v);

}	// event
}	// acorn

#endif	// ACORN_SQ_EVENT_H
