#ifndef ACORN_SQ_GRAPHICS_QUAD_H
#define ACORN_SQ_GRAPHICS_QUAD_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace graphics {

extern "C" int sq_register_quad(SQVM *v);

}	// graphics
}	// acorn

#endif	// ACORN_SQ_GRAPHICS_QUAD_H
