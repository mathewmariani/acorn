// STL
#include <iostream>

// acorn
#include "graphics/particlesystem.h"

namespace acorn {
namespace graphics {

acorn::Type ParticleSystem::type = std::type_index(typeid(graphics::ParticleSystem));

const int MaxParticles = 100000;

ParticleSystem::ParticleSystem() {
	static const GLfloat g_vertex_buffer_data[] = {
		-0.5f, -0.5f, 0.0f,
		 0.5f, -0.5f, 0.0f,
		-0.5f,  0.5f, 0.0f,
		 0.5f,  0.5f, 0.0f,
	};

	// the VBO containing the vertices of the particles
	GLuint particles_vertex_buffer;
	glGenBuffers(1, &particles_vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, particles_vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	// the VBO containing the positions and sizes of the particles
	GLuint particles_position_buffer;
	glGenBuffers(1, &particles_position_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, particles_position_buffer);
	glBufferData(GL_ARRAY_BUFFER, MaxParticles * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);

	// the VBO containing the colors of the particles
	GLuint particles_color_buffer;
	glGenBuffers(1, &particles_color_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, particles_color_buffer);
	glBufferData(GL_ARRAY_BUFFER, MaxParticles * 4 * sizeof(GLubyte), NULL, GL_STREAM_DRAW);
}

ParticleSystem::~ParticleSystem() {

}

void ParticleSystem::draw(std::shared_ptr<Graphics> gfx, const glm::mat4 &m) {
	printf("attemping to draw ParticleSystem\n");
}

}	// graphics
}	// acorn
