// acorn
#include "resource/sq_fontdata.h"
#include "resource/sq_glyphdata.h"
#include "resource/sq_resource.h"
#include "resource/sq_sounddata.h"
#include "resource/resource.h"

namespace acorn {
namespace resource {

#define instance() (sqx_getmodule<Resource>(v, "resource"))

// NOTE: functions for acorn.system
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.system
const static SQFunction classes[] = {
	// sq_register_fontdata,
	// sq_register_glyphdata,
	// sq_register_imagedata,
	// sq_register_sounddata,
	nullptr
};

extern "C" int sq_acorn_resource(SQVM *v) {
	auto instance = std::make_shared<resource::Resource>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize resource module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "resource";
	reg.classes = classes;
	
	return acorn::sqx_registermodule_ext(v, reg);
}

}	// resource
}	// acorn
