// acorn
#include "graphics/opengl/graphics.h"

namespace acorn {
namespace graphics {
namespace opengl {

Graphics::Graphics() {

}

Graphics::~Graphics() {

}

const char *Graphics::getName() const {
	return type.name();
}

//std::shared_ptr<Framebuffer> Graphics::newFramebuffer() noexcept {
//	return nullptr;
//}

std::shared_ptr<Image> Graphics::newImage() noexcept {
	return nullptr;
}

std::shared_ptr<Shader> Graphics::newShader() noexcept {
	return nullptr;
}

std::shared_ptr<ShaderStage> Graphics::newShaderStage() noexcept {
	return nullptr;
}

}	// opengl
}	// graphics
}	// acorn
