#ifndef ACORN_GRAPHICS_METAL_BUFFER_H
#define ACORN_GRAPHICS_METAL_BUFFER_H

// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// acorn
#include "graphics/metal/metal.h"
#include "graphics/buffer.h"

namespace acorn {
namespace graphics {
namespace metal {

class Buffer final : public acorn::graphics::Buffer {
public:
	Buffer(size_t size, BufferType type, BufferUsage usage);
	virtual ~Buffer();

    void *bind() override;
    void unbind(size_t usedsize) override;
    void nextFrame() override;

public:
	const void *getHandle() const override;

private:
	bool create();

private:
	char *memory;
};	// buffer

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
#endif	// ACORN_GRAPHICS_METAL_BUFFER_H
