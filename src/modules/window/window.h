#ifndef ACORN_WINDOW_MODULE_H
#define ACORN_WINDOW_MODULE_H

// STL
#include <list>
#include <memory>
#include <string>
#include <vector>

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"

// SDL
#include <SDL.h>

namespace acorn {

// forward declaration
namespace graphics { class Graphics; }

namespace window {

// NOTE: more general video/display stuff
class Window : public acorn::Module {
public:
	static acorn::Type type;

public:
	enum FullscreenMode {
		FULLSCREEN,
		DESKTOP,

		// always last
		FULLSCREENMODE_MAX_ENUM
	};	// fullscreenMode

	struct Settings {
		const char* title;
		int width = 600;
		int height = 360;
		int minwidth = 1;
		int minheight = 1;
		bool fullscreen = false;
		bool borderless = false;
		bool resizable = false;
		bool highdpi = false;
		bool centered = false;
		bool vsync = true;
		FullscreenMode fmode = FullscreenMode::DESKTOP;
	};	// settings

	struct DisplayMode {
		int width;
		int height;
		int refresh_rate;
	};	// displaymode

public:
	Window();
	virtual ~Window();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:
	void setGraphicsDevice(std::shared_ptr<graphics::Graphics> gfx);

	// use this function to create a window with the specified position, dimensions, and flags.
	bool setWindow(int width, int height, Settings *settings);

private:
	// use this function to create an OpenGL context for use with an OpenGL window, and make it current.
	bool createWindowAndContext(int x, int y, int width, int height, uint32_t flags);

public:

	// use this function to get the dots/pixels-per-inch for a display.
	// diagonal, horizontal, vertical DPI of the display.
	bool getDisplayDPI(int index, float *ddpi, float *hdpi, float *vdpi) const noexcept;

	// use this function to get the index of the display associated with a window.
	int getDisplayCount() const noexcept;

	// use this function to get the number of available display modes.
	int	countDisplayModes(int index) const noexcept;

	// use this function to get information about the desktop display mode.
	void getDesktopDimensions(int index, int &width, int &height) const noexcept;

	// use this function to get the name of a display in UTF-8 encoding.
	std::string getDisplayName(int index) const noexcept;

	// use this function to get a list of the available display modes.
	void enumerateDisplayModes(int index, std::vector<DisplayMode> &list) const noexcept;

public:
	// use this function to get the index of the display associated with a window.
	int getDisplayIndex() const noexcept;

	// use this function to get the numeric ID of a window, for logging purposes.
	uint32 getId() const noexcept;

	// use this function to set the position of a window.
	void setPosition(int x, int y);

	// use this function to get the position of a window.
	void getPosition(int &x, int &y) const noexcept;

	// use this function to set the size of a window's client area.
	void setDimensions(int width, int height);

	// use this function to get the size of a window's client area.
	void getDimensions(int &width, int &height) const noexcept;
	int getWidth() const;
	int getHeight() const;

	// use this function to get the size of a window's underlying drawable in pixels (for use with glViewport).
	void getDrawableDimensions(int &width, int &height) const noexcept;
	int getDrawableWidth() const;
	int getDrawableHeight() const;

	// use this function to set the title of a window.
	void setTitle(const std::string &title) noexcept;

	// use this function to get the title of a window.
	const std::string &getTitle() const noexcept;

	// use this function to allow/prevent the screen to be blanked by a screen saver.
	void setScreenSaverEnabled(bool enable) const noexcept;

	// use this function to check whether the screensaver is currently enabled.
	bool isScreenSaverEnabled() const noexcept;

	// use this function to get the window flags.
	bool isMaximized() const noexcept;
	bool isMinimized() const noexcept;
	bool isShown() const noexcept;
	bool isHidden() const noexcept;
	bool isFullscreen() const noexcept;
	bool isFullscreenDesktop() const noexcept;

	bool setFullscreen(bool fullscreen, FullscreenMode mode);

	// use this function to get the window which currently has keyboard focus.
	bool hasKeyboardFocus() const noexcept;

	// use this function to get the window which currently has mouse focus.
	bool hasMouseFocus() const noexcept;

	// use this function to make a window as large as possible.
	void maximize() const noexcept;

	// use this function to minimize a window to an iconic representation.
	void minimize() const noexcept;

	// use this function to hide a window.
	void hide() const noexcept;

	// use this function to restore the size and position of a minimized or maximized window.
	void restore() const noexcept;

	// use this function to raise a window above other windows and set the input focus.
	void raise() const noexcept;

	// use this function to show a window.
	void show() const noexcept;

	// use this function to destroy a window and delete the OpenGL context.
	void close() noexcept;

	// use this function to update a window with OpenGL rendering.
	void swapBuffers() const noexcept;

	// use this function internally.
	void onResize();

public:
	// use this function to get the pointer to the underlying window object.
	// NOTE: cast as SDL_Window*
	const void *getHandle() const noexcept;

private:
	bool isValidDisplayIndex(int index) const noexcept;

private:
	std::shared_ptr<graphics::Graphics> graphics;

private:
	std::string title;
	int windowWidth = 512;
	int windowHeight = 448;
	int pixelWidth;
	int pixelHeight;

private:
	// managed sdl resources
	SDL_Window *window;
	SDL_GLContext context;
};	// window

}	// window
}	// acorn

#endif	// ACORN_WINDOW_H
