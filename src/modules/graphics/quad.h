#ifndef ACORN_GRAPHICS_QUAD_H
#define ACORN_GRAPHICS_QUAD_H

// acorn
#include "common/object.h"
#include "graphics/vertex.h"

// GLM
#include <glm/glm.hpp>

namespace acorn {
namespace graphics {

class Quad : public acorn::Object {
public:
	static acorn::Type type;

public:
	Quad(float x, float y, float w, float h, float sw, float sh);
	~Quad();

public:
	const float getWidth() const noexcept;
	const float getHeight() const noexcept;

	const float getTextureWidth() const noexcept;
	const float getTextureHeight() const noexcept;

	// FIXME: unsure of implementation
	
	// NOTE: custom vertex implementation
	const Vertex *getVertices() const noexcept;

	// NOTE: use glm since matrices are implemented that way
	const glm::vec2 *getVertexPositions() const noexcept;
	const glm::vec2 *getVertexTexCoords() const noexcept;

private:
	float width;
	float height;

	float textureWidth;
	float textureHeight;

	// FIXME: unsure of implementation
	// NOTE: use glm since matrices are implemented that way
	Vertex vertices[4];
	glm::vec2 vertexPositions[4];
	glm::vec2 vertexTexCoords[4];
};	// quad

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_QUAD_H
