#ifndef ACORN_OBJECT_H
#define ACORN_OBJECT_H

// acorn
#include "common/type.h"

namespace acorn {

class Object {
public:
	static acorn::Type type;

public:
	virtual ~Object() = 0;
};	// object

}	// acorn

#endif	// ACORN_OBJECT_H
