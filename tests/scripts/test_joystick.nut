R"squirrel"#(

# joystick
describe("acorn.joystick", function() {
	# typeof
	it("is a typeof of table", function() {
		assert.is_typeof(acorn.joystick, "table")
	})

	# functions
	it("contains a function `getDevice()`", function() {
		assert.is_in("getDevice", Joystick)
		assert.is_typeof(Joystick.getDevice, "function")
	})
	it("contains a function `getDevices()`", function() {
		assert.is_in("getDevices", Joystick)
		assert.is_typeof(Joystick.getDevices, "function")
	})
	it("contains a function `getJoystickCount()`", function() {
		assert.is_in("getJoystickCount", acorn.joystick)
		assert.is_typeof(Joystick.getJoystickCount, "function")
	})

	# classes
	it("contains a class `Joystick`", function() {
		assert.is_in("Joystick", acorn.joystick)
		assert.is_typeof(Joystick, "class")
	})

	# joystick
	describe("Joystick", function() {
		# typeof
		it("is a class of acorn.joystick", function() {
			assert.is_in("Joystick", acorn.joystick)
			assert.is_typeof(Joystick, "class")
		})

		# functions
		it("contains a function `constructor()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.constructor, "function")
		})
		it("contains a function `getName()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.getName, "function")
		})
		it("contains a function `getGUID()`", function() {
			assert.is_in("getGUID", Joystick)
			assert.is_typeof(Joystick.getGUID, "function")
		})
		it("contains a function `getInstanceID()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.getInstanceID, "function")
		})
		it("contains a function `isAttached()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.isAttached, "function")
		})
		it("contains a function `isGameController()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.isGameController, "function")
		})
		it("contains a function `getAxisCount()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.getAxisCount, "function")
		})
		it("contains a function `getBallCount()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.getBallCount, "function")
		})
		it("contains a function `getHatCount()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.getHatCount, "function")
		})
		it("contains a function `getButtonCount()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.getButtonCount, "function")
		})
		it("contains a function `getPowerState()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.getPowerState, "function")
		})
		it("contains a function `getHat()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.getHat, "function")
		})
		it("contains a function `getButton()`", function() {
			assert.is_in("constructor", Joystick)
			assert.is_typeof(Joystick.getButton, "function")
		})
	})
})

#)squirrel"#"
