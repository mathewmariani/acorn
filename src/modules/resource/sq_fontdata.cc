// C/C++
#include <iostream>
#include <algorithm>
#include <memory>

// acorn
#include "filesystem/sq_filesystem.h"
#include "resource/sq_fontdata.h"
#include "font/font.h"
#include "resource/fontdata.h"

namespace acorn {
namespace resource {

static SQInteger wrap_constructor(SQVM *v) {
	std::shared_ptr<filesystem::FileData> filedata = filesystem::sqx_getfiledata(v, 2);

	int size = sqx_getnumber(v, 3, 0);

	auto font = sqx_getmodule<font::Font>(v);
	auto instance = std::make_shared<FontData>(*(FT_Library *) font->getHandle(), filedata, size);
	sqx_pushinstance(v, FontData::type, instance);
	return 1;
}

static SQInteger wrap_getMetrics(SQVM *v) {
	std::weak_ptr<FontData> instance(sqx_checkinstance<FontData>(v, 1));
	if (auto spt = instance.lock()) {
		// FIXME: make something in common/runtime to avoid lots of code like this
		sq_newtableex(v, 5);

		sq_pushstring(v, "advance", -1);
		sq_pushinteger(v, (SQInteger)spt->getAdvance());
		sq_newslot(v, -3, SQFalse);

		sq_pushstring(v, "ascent", -1);
		sq_pushinteger(v, (SQInteger)spt->getAscent());
		sq_newslot(v, -3, SQFalse);

		sq_pushstring(v, "descent", -1);
		sq_pushinteger(v, (SQInteger)spt->getDescent());
		sq_newslot(v, -3, SQFalse);

		sq_pushstring(v, "glyphCount", -1);
		sq_pushinteger(v, (SQInteger)spt->getGlyphCount());
		sq_newslot(v, -3, SQFalse);

		sq_pushstring(v, "height", -1);
		sq_pushinteger(v, (SQInteger)spt->getHeight());
		sq_newslot(v, -3, SQFalse);

		sq_newslot(v, -3, SQFalse);

		return 1;
	}

	return 0;
}

static SQInteger wrap_getAdvance(SQVM *v) {
	std::weak_ptr<FontData> instance(sqx_checkinstance<FontData>(v, 1));
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getAdvance());
		return 1;
	}
	return 0;
}

static SQInteger wrap_getAscent(SQVM *v) {
	std::weak_ptr<FontData> instance(sqx_checkinstance<FontData>(v, 1));
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getAscent());
		return 1;
	}
	return 0;
}

static SQInteger wrap_getDescent(SQVM *v) {
	std::weak_ptr<FontData> instance(sqx_checkinstance<FontData>(v, 1));
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getDescent());
		return 1;
	}
	return 0;
}

static SQInteger wrap_getGlyphCount(SQVM *v) {
	std::weak_ptr<FontData> instance(sqx_checkinstance<FontData>(v, 1));
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getGlyphCount());
		return 1;
	}
	return 0;
}

static SQInteger wrap_getHeight(SQVM *v) {
	std::weak_ptr<FontData> instance(sqx_checkinstance<FontData>(v, 1));
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getHeight());
		return 1;
	}
	return 0;
}

// static SQInteger wrap_getLineHeight(SQVM *v) {
// 	FontData *self = nullptr;
// 	if (SQ_FAILED(sq_getinstanceup(v, 1, (SQUserPointer *)&self, nullptr))) {
// 		return sq_throwerror(v, "FontData expected");
// 	}
//
// 	sq_pushinteger(v, (SQInteger)self->getLineHeight());
// 	return 1;
// }

static SQInteger wrap_hasGlyph(SQVM *v) {
  SQInteger codepoint;
  sq_getinteger(v, 2, &codepoint);

	std::weak_ptr<FontData> instance(sqx_checkinstance<FontData>(v, 1));
	if (auto spt = instance.lock()) {
		sq_pushbool(v, (SQBool)spt->hasGlyph((int)codepoint));
		return 1;
	}
	return 0;
}

static SQInteger wrap_getGlyphData(SQVM *v) {
	int codepoint = sqx_getnumber(v, 1, 0);
	std::weak_ptr<FontData> instance(sqx_checkinstance<FontData>(v, 1));
	if (auto spt = instance.lock()) {
		spt->getGlyphData(codepoint);
	}
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 3, "xsn", false),
	_DECL_FUNC(getMetrics, 1, "x", false),

	// metrics
	_DECL_FUNC(getAdvance, 1, "x", false),
	_DECL_FUNC(getAscent, 1, "x", false),
	_DECL_FUNC(getDescent, 1, "x", false),
	_DECL_FUNC(getGlyphCount, 1, "x", false),
	_DECL_FUNC(getHeight, 1, "x", false),
	// _DECL_FUNC(getLineHeight, 1, "x", false),

	// glyphs
	_DECL_FUNC(hasGlyph, 2, "xi", false),
	_DECL_FUNC(getGlyphData, 2, "xi", false),

	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_fontdata(SQVM *v) {
	ClassRegistry reg;
	reg.name = "FontData";
	reg.functions = functions;

	return sqx_registerclass(v, FontData::type, reg);
}

}	// resource
}	// acorn
