#ifndef ACORN_IMAGE_IMAGEDATA_H
#define ACORN_IMAGE_IMAGEDATA_H

// STL
#include <array>
#include <memory>

// acorn
#include "common/data.h"

// SDL2
#include <SDL_mutex.h>

namespace acorn {
namespace image {

class ImageData : public acorn::Data {
public:
	static acorn::Type type;

public:
	ImageData(std::shared_ptr<acorn::Data> data);
	ImageData(int width, int height);
	virtual ~ImageData();

public:
	int getWidth() const noexcept;
	int getHeight() const noexcept;

	void getPixel(int x, int y, std::array<uint8_t, 4> &pixel);
	void setPixel(int x, int y, std::array<uint8_t, 4> &pixel);

public:
	void *getData() const override;
	size_t getSize() const override;

private:
	unsigned char *data = nullptr;
	int channels;
	int width;
	int height;

	SDL_mutex *mutex;
};	// imagedata

}	// image
}	// acorn

#endif	// ACORN_IMAGE_IMAGEDATA_H
