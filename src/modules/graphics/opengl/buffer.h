#ifndef ACORN_GRAPHICS_OPENGL_BUFFER_H
#define ACORN_GRAPHICS_OPENGL_BUFFER_H

// acorn
#include "graphics/opengl/opengl.h"
#include "graphics/buffer.h"

namespace acorn {
namespace graphics {
namespace opengl {

class Buffer final : public acorn::graphics::Buffer {
public:
	Buffer(size_t size, BufferType type, BufferUsage usage);
	virtual ~Buffer();

	void *bind() override;
	size_t unbind(size_t usedsize) override;
	void nextFrame() override;

public:
	const void *getHandle() const override;

private:
	bool create();

private:
	GLuint vbo;
	GLenum target;
	size_t offset;
	char *memory;
};	// buffer

}	// opengl
}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_OPENGL_BUFFER_H
