#ifndef ACORN_SQ_GRAPHICS_SPRITEBATCH_H
#define ACORN_SQ_GRAPHICS_SPRITEBATCH_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"
namespace acorn {
namespace graphics {

extern "C" int sq_register_spritebatch(SQVM *v);

}	// graphics
}	// acorn

#endif	// ACORN_SQ_GRAPHICS_SPRITEBATCH_H
