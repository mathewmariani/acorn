#ifndef ACORN_SQ_FILESYSTEM_H
#define ACORN_SQ_FILESYSTEM_H

// FIXME: cleaner header

// STL
#include <memory>

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

#include "filesystem/file.h"
#include "filesystem/filedata.h"

namespace acorn {
namespace filesystem {

std::shared_ptr<File> sqx_getfile(SQVM *v, int idx);
std::shared_ptr<FileData> sqx_getfiledata(SQVM *v, int idx);

extern "C" int sq_acorn_filesystem(SQVM *v);

}	// filesystem
}	// acorn

#endif	// ACORN_SQ_FILESYSTEM_H
