#ifndef ACORN_KEYVALUES_H
#define ACORN_KEYVALUES_H

// STL
#include <string>
#include <unordered_map>

// acorn
#include "common/squirrel.h"
#include "common/variant.h"

namespace acorn {

class KeyValues {
public:
	KeyValues();
	KeyValues(const std::string &name);
	~KeyValues();

	const std::string &getName() const noexcept;

	template <typename T>
	void setValue(const std::string &key, const T value) {
		map.insert(std::pair<std::string, Variant>(key, Variant(value)));
	}

public:
	int toSquirrel(SQVM *v) const noexcept;

private:
	std::string name;
	std::unordered_map<std::string, Variant> map;
};	// keyvalues

}	// acorn

#endif	// ACORN_KEYVALUES_H
