#ifndef ACORN_GRAPHICS_OPENGL_GRAPHICS_H
#define ACORN_GRAPHICS_OPENGL_GRAPHICS_H

// C/C++
#include <memory>

// acorn
#include "graphics/graphics.h"

#include "graphics/opengl/framebuffer.h"
#include "graphics/opengl/image.h"
#include "graphics/opengl/shader.h"
#include "graphics/opengl/shaderstage.h"

namespace acorn {
namespace graphics {
namespace opengl {

class Graphics final : public acorn::graphics::Graphics {
public:
	Graphics();
	virtual ~Graphics();

	// implement module
	const char *getName() const override;

public:
	//std::shared_ptr<Framebuffer> newFramebuffer() noexcept;
	std::shared_ptr<Image> newImage() noexcept;
	std::shared_ptr<Shader> newShader() noexcept;
	std::shared_ptr<ShaderStage> newShaderStage() noexcept;
};	// graphics

}	// opengl
}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_OPENGL_GRAPHICS_H
