// acorn
#include "common/exception.h"
#include "graphics/opengl/framebuffer.h"

namespace acorn {
namespace graphics {
namespace opengl {

Framebuffer::Framebuffer(TextureType type, int width, int height) :
	acorn::graphics::Framebuffer(type, width, height) {

}

Framebuffer::~Framebuffer() {

}

const void *Framebuffer::getHandle() const {
	return &fbo;
}

}	// opengl
}	// graphics
}	// acorn
