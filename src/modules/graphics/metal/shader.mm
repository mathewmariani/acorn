// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// acorn
#include "common/exception.h"
#include "graphics/metal/shader.h"

namespace acorn {
namespace graphics {
namespace metal {

Shader::Shader(std::shared_ptr<acorn::graphics::ShaderStage> vertex, std::shared_ptr<acorn::graphics::ShaderStage> fragment) :
	acorn::graphics::Shader(vertex, fragment) {
	createShader();
}

Shader::~Shader() {

}

bool Shader::createShader() {
	return true;
}

// FIXME:
void Shader::bind() {

}

// FIXME:
void Shader::unbind() {

}

const int Shader::getAttributeLocation(const std::string &name) {
	auto itr = attributes.find(name);
	if (itr != attributes.end()) {
		return itr->second;
	}

	return -1;
}

const int Shader::getGetUniformLocation(const std::string &name) {
	auto itr = uniforms.find(name);
	if (itr != uniforms.end()) {
		return itr->second;
	}

	return -1;
}

// FIXME:
void Shader::sendFloats(int location, int count, const float *value) {

}

// FIXME:
void Shader::sendInts(int location, int count, const int *value) {

}

// FIXME:
void Shader::sendMatrices(int location, int count, const float *value) {

}

const void *Shader::getHandle() const {
	return nullptr;
}

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
