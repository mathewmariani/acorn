#ifndef ACORN_SQ_FILESYSTEM_FILEDATA_H
#define ACORN_SQ_FILESYSTEM_FILEDATA_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace filesystem {

extern "C" int sq_register_filedata(SQVM *v);

}	// filesystem
}	// acorn

#endif	// ACORN_SQ_FILESYSTEM_FILEDATA_H
