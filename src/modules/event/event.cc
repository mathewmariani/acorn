// acorn
#include "common/log.h"
#include "event/event.h"

#include "joystick/joystickmodule.h"

namespace acorn {
namespace event {

acorn::Type Event::type = std::type_index(typeid(event::Event));

Event::Event() {
	log_debug("Event module initialized.");

	// initialize sdl subsystem
	if (SDL_InitSubSystem(SDL_INIT_EVENTS) < 0) {
		log_error("Could not initialize SDL subsystem: (%s)\n", SDL_GetError());
	}
}

Event::~Event() {
	log_debug("Event module deinitialized.");

	// deinitialize subsystem
	SDL_QuitSubSystem(SDL_INIT_EVENTS);
}

void Event::push(std::shared_ptr<acorn::List> event) noexcept {
	std::lock_guard<std::mutex> lock(mutex);
	queue.push(event);
}

void Event::pump() noexcept {
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		auto event = convert(e);
		if (event.get()) {
			push(event);
			event.reset();
		}
	}
}

bool Event::poll(std::shared_ptr<acorn::List> &event) noexcept {
	std::lock_guard<std::mutex> lock(mutex);
	if (queue.empty()) {
		return false;
	}

	event = queue.front();
	queue.pop();

	return true;
}

std::shared_ptr<acorn::List> Event::convert(const SDL_Event &e) {
	std::shared_ptr<acorn::List> event = nullptr;

	switch (e.type) {
		// keyboard
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		event = handleKeyboardEvent(e);
		break;
		// text
	case SDL_TEXTINPUT:
	case SDL_TEXTEDITING:
		// NOTE: this is a super annoying stub.
		//STUBBED("SDL_Event for [TEXTINPUT, TEXTEDITING]");
		break;
		// mouse
	case SDL_MOUSEMOTION:
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
	case SDL_MOUSEWHEEL:
		event = handleMouseEvent(e);
		break;
		// joystick
	case SDL_JOYBUTTONDOWN:
	case SDL_JOYBUTTONUP:
	case SDL_JOYAXISMOTION:
	case SDL_JOYBALLMOTION:
	case SDL_JOYHATMOTION:
	case SDL_JOYDEVICEADDED:
	case SDL_JOYDEVICEREMOVED:
	case SDL_CONTROLLERBUTTONDOWN:
	case SDL_CONTROLLERBUTTONUP:
	case SDL_CONTROLLERAXISMOTION:
		event = handleJoystickEvent(e);
		break;
		// finger
	case SDL_FINGERDOWN:
	case SDL_FINGERUP:
	case SDL_FINGERMOTION:
		event = handleTouchEvent(e);
		break;
		// window
	case SDL_WINDOWEVENT:
		event = handleWindowEvent(e);
		break;
		// system
	case SDL_DROPFILE:
		STUBBED("SDL_Event for dropfile events");
		break;
	case SDL_QUIT:
	case SDL_APP_TERMINATING:
		event = std::make_shared<acorn::List>("quit");
		break;
	case SDL_APP_LOWMEMORY:
		event = std::make_shared<acorn::List>("lowmemory");
		break;
		// default
	default:
		break;
	}

	return event;
}

std::shared_ptr<acorn::List> Event::handleKeyboardEvent(const SDL_Event &e) const noexcept {
	std::shared_ptr<acorn::List> event = nullptr;
	std::vector<Variant> vargs;

	switch (e.type) {
	case SDL_KEYDOWN:
		vargs.emplace_back(e.key.keysym.scancode);
		vargs.emplace_back(e.key.repeat != 0);
		event = std::make_shared<acorn::List>("keypressed", vargs);
		break;
	case SDL_KEYUP:
		vargs.emplace_back(e.key.keysym.scancode);
		event = std::make_shared<acorn::List>("keyreleased", vargs);
		break;
	}
	return event;
}

std::shared_ptr<acorn::List> Event::handleMouseEvent(const SDL_Event &e) const noexcept {
	std::shared_ptr<acorn::List> event = nullptr;
	std::vector<Variant> vargs;

	switch (e.type) {
	case SDL_MOUSEMOTION:
		vargs.emplace_back(e.motion.x);
		vargs.emplace_back(e.motion.y);
		vargs.emplace_back(e.motion.xrel);
		vargs.emplace_back(e.motion.yrel);
		event = std::make_shared<acorn::List>("mousemotion", vargs);
		break;
	case SDL_MOUSEBUTTONDOWN:
		vargs.emplace_back(e.button.x);
		vargs.emplace_back(e.button.y);
		vargs.emplace_back(e.button.button);
		//vargs.emplace_back(e.button.clicks);
		event = std::make_shared<acorn::List>("mousepressed", vargs);
		break;
	case SDL_MOUSEBUTTONUP:
		vargs.emplace_back(e.button.x);
		vargs.emplace_back(e.button.y);
		vargs.emplace_back(e.button.button);
		//vargs.emplace_back(e.button.clicks);
		event = std::make_shared<acorn::List>("mousereleased", vargs);
		break;
	case SDL_MOUSEWHEEL:
		vargs.emplace_back(e.wheel.x);
		vargs.emplace_back(e.wheel.y);
		event = std::make_shared<acorn::List>("mousewheel", vargs);
		break;
	}
	return event;
}

std::shared_ptr<List> Event::handleJoystickEvent(const SDL_Event &e) const noexcept {
	auto joymodule = Module::getInstance<joystick::JoystickModule>();
	if (joymodule.get() == nullptr) {
		// return early since we didn't find the joystick module.
		return nullptr;
	}

	std::shared_ptr<acorn::List> event = nullptr;
	std::vector<Variant> vargs;

	switch (e.type) {
	case SDL_JOYBUTTONDOWN:
		// joystick button pressed
		vargs.emplace_back(e.jbutton.which);
		vargs.emplace_back(e.jbutton.button);
		event = std::make_shared<acorn::List>("joystickbuttonpressed", vargs);
		break;
	case SDL_JOYBUTTONUP:
		// joystick button released
		vargs.emplace_back(e.jbutton.which);
		vargs.emplace_back(e.jbutton.button);
		event = std::make_shared<acorn::List>("joystickbuttonreleased", vargs);
		break;
	case SDL_JOYAXISMOTION: {
		// joystick axis motion
		vargs.emplace_back(e.jaxis.which);
		vargs.emplace_back(e.jaxis.axis);
		float value = joystick::Joystick::clampval(e.jaxis.value / 32768.0f);
		vargs.emplace_back(value);
		event = std::make_shared<acorn::List>("joystickaxis", vargs);
		break;
	}
	case SDL_JOYBALLMOTION:
		// joystick trackball motion
		break;
	case SDL_JOYHATMOTION:
		// joystick hat position change
		vargs.emplace_back(e.jhat.which);
		vargs.emplace_back(e.jhat.hat);
		vargs.emplace_back(e.jhat.value);
		event = std::make_shared<acorn::List>("joystickhat", vargs);
		break;
	case SDL_JOYDEVICEADDED: {
		// joystick connected
		auto joy = joymodule->addJoystick(e.jdevice.which);

		vargs.emplace_back(e.jdevice.which);
		event = std::make_shared<acorn::List>("joystickadded", vargs);
		break;
	}
	case SDL_JOYDEVICEREMOVED:
		// joystick disconnected
		joymodule->removeJoystick(e.jdevice.which);

		vargs.emplace_back(e.jdevice.which);
		event = std::make_shared<acorn::List>("joystickremoved", vargs);
		break;
		// controller
	case SDL_CONTROLLERBUTTONDOWN:
		vargs.emplace_back(e.cbutton.which);
		vargs.emplace_back(e.cbutton.button);
		event = std::make_shared<acorn::List>("controllerbuttondown", vargs);
		break;
	case SDL_CONTROLLERBUTTONUP:
		vargs.emplace_back(e.cbutton.which);
		vargs.emplace_back(e.cbutton.button);
		event = std::make_shared<acorn::List>("controllerbuttonup", vargs);
		break;
	case SDL_CONTROLLERAXISMOTION: {
		vargs.emplace_back(e.caxis.which);
		vargs.emplace_back(e.caxis.axis);
		float value = joystick::Joystick::clampval(e.caxis.value / 32768.0f);
		vargs.emplace_back(value);
		event = std::make_shared<acorn::List>("controlleraxis", vargs);
		break;

	}
	default:
		STUBBED("SDL_Event for default joystick events");
		break;
	}
	return event;
}

std::shared_ptr<acorn::List> Event::handleTouchEvent(const SDL_Event &e) const noexcept {
	std::shared_ptr<acorn::List> event = nullptr;
	switch (e.type) {
	case SDL_FINGERDOWN:
	case SDL_FINGERUP:
	case SDL_FINGERMOTION:
	default:
#if !defined(OS_MACOS)
		// for events that were generated by a touch input device, and not a real mouse.
		STUBBED("SDL_[FINGERDOWN, FINGERUP, FINGERMOTION]");
#endif
		break;
	}
	return event;
}

std::shared_ptr<acorn::List> Event::handleWindowEvent(const SDL_Event &e) const noexcept {
	std::shared_ptr<acorn::List> event = nullptr;
	std::vector<Variant> vargs;

	switch (e.window.event) {
	case SDL_WINDOWEVENT_SHOWN:
	case SDL_WINDOWEVENT_HIDDEN:
		// window has been shown, or hidden
		vargs.emplace_back((e.window.event == SDL_WINDOWEVENT_SHOWN));
		event = std::make_shared<acorn::List>("visible", vargs);
		break;
	case SDL_WINDOWEVENT_MOVED:
		// window has been moved to data1, data2
		vargs.emplace_back((int) e.window.data1);
		vargs.emplace_back((int) e.window.data2);
		event = std::make_shared<acorn::List>("moved", vargs);
		break;
	case SDL_WINDOWEVENT_RESIZED:
		// window has been resized to (data1 x data2); preceds SDL_WINDOWEVENT_SIZE_CHANGED
		vargs.emplace_back((int) e.window.data1);
		vargs.emplace_back((int) e.window.data2);
		event = std::make_shared<acorn::List>("resize", vargs);
	case SDL_WINDOWEVENT_SIZE_CHANGED:
		// window size has changed, either as a result of an API call or through the system or user changing the window size
		break;
	case SDL_WINDOWEVENT_MINIMIZED:
		// window has been minimized
		event = std::make_shared<acorn::List>("minimized");
		break;
	case SDL_WINDOWEVENT_MAXIMIZED:
		// window has been maximized
		event = std::make_shared<acorn::List>("maximized", vargs);
		break;
	case SDL_WINDOWEVENT_RESTORED:
		// window has been restored to normal size and position
		printf("Window %d restored\n", e.window.windowID);
		STUBBED("SDL_WINDOWEVENT_RESTORED");
		break;
	case SDL_WINDOWEVENT_ENTER:
	case SDL_WINDOWEVENT_LEAVE:
		// window has gained, or lost mouse focus
		vargs.emplace_back((e.window.event == SDL_WINDOWEVENT_ENTER));
		event = std::make_shared<acorn::List>("mousefocus", vargs);
		break;
	case SDL_WINDOWEVENT_FOCUS_GAINED:
	case SDL_WINDOWEVENT_FOCUS_LOST:
		// window has gained, or lost keyboard focus
		vargs.emplace_back((e.window.event == SDL_WINDOWEVENT_FOCUS_GAINED));
		event = std::make_shared<acorn::List>("keyboardfocus", vargs);
		break;
	default:
		STUBBED("SDL_WINDOWEVENT_[EXPOSED, CLOSE, TAKE_FOCUS, HIT_TEST]");
		break;
	}
	return event;
}

}	// event
}	// acorn
