#ifndef ACORN_GRAPHICS_METAL_GRAPHICS_H
#define ACORN_GRAPHICS_METAL_GRAPHICS_H

// acorn
#include "common/platform.h"

#if defined(OS_MACOS)

// STL
#include <memory>

// acorn
#include "graphics/graphics.h"
#include "graphics/metal/image.h"
#include "graphics/metal/shader.h"

namespace acorn {
namespace graphics {
namespace metal {

class Graphics final : public acorn::graphics::Graphics {
public:
	Graphics();
	virtual ~Graphics();

	// implement module
	const char *getName() const override;

public:
	std::shared_ptr<Image> newImage() noexcept;
	std::shared_ptr<Shader> newShader() noexcept;
};	// graphics

}	// metal
}	// graphics
}	// acorn

#endif  // OS_MACOS

#endif	// ACORN_GRAPHICS_METAL_GRAPHICS_H
