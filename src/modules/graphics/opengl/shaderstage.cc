// acorn
#include "common/exception.h"
#include "graphics/opengl/shaderstage.h"

namespace acorn {
namespace graphics {
namespace opengl {

ShaderStage::ShaderStage(ShaderStage::StageType glstage, const std::string &glsl) :
	acorn::graphics::ShaderStage(glstage, glsl),
	glshader(0) {
	compileShader();
}

ShaderStage::~ShaderStage() {
	if (glshader != 0) {
		glDeleteShader(glshader);
		glshader = 0;
	}
}

bool ShaderStage::compileShader() {
	if (glshader != 0) {
		return true;
	}

	if (stageType != ShaderStage::StageType::Vertex && stageType != ShaderStage::StageType::Fragment) {
		throw acorn::Exception("unknown glstage.");
	}

	GLenum stage;
	switch (stageType) {
	case ShaderStage::StageType::Vertex: stage = GL_VERTEX_SHADER; break;
	case ShaderStage::StageType::Fragment: stage = GL_FRAGMENT_SHADER; break;
	}

	glshader = glCreateShader(stage);
	if (glshader == 0) {
		throw acorn::Exception("Cannot create OpenGL shader program.");
	}

	const char *cstr = source.c_str();
	GLint len = (GLint) source.length();

	glShaderSource(glshader, 1, (const GLchar **) &cstr, &len);
	glCompileShader(glshader);

	GLint loglen;
	glGetShaderiv(glshader, GL_INFO_LOG_LENGTH, &loglen);

	// Get any warnings the shader compiler may have produced.
	if (loglen > 0) {
		GLchar *infolog = new GLchar[len];
		glGetShaderInfoLog(glshader, len, nullptr, infolog);
		printf("Shader Log: \n%s\n", infolog);
		delete[] infolog;
	}

	GLint status;
	glGetShaderiv(glshader, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {
		glDeleteShader(glshader);
		throw acorn::Exception("Cannot compile shader program.");
	}

	glCheckError();

	return true;
}

const void *ShaderStage::getHandle() const {
	return &glshader;
}

}	// opengl
}	// graphics
}	// acorn
