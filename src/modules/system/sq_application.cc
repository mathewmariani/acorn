// acorn
#include "system/sq_application.h"
#include "system/system.h"

namespace acorn {
namespace system {

#define instance() (Module::getInstance<system::System>())

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Application cannot be constructed directly.");
}

static SQInteger wrap_postNotification(SQVM *v) {
	System::Notification notification = {};
	notification.title = "title";
	notification.subtitle = "subtitle";
	notification.message = "message";
	instance()->postNotification(notification);
	return 0;
}

static SQInteger wrap_requestUserAttention(SQVM *v) {
    instance()->requestUserAttention();
    return 0;
}

static SQInteger wrap_beep(SQVM *v) {
    instance()->beep();
    return 0;
}

static SQInteger wrap_showBadge(SQVM *v) {
    bool show = false;
    sq_getbool(v, -1, (SQBool *)&show);
    instance()->showApplicationBadge(show);
    return 0;
}

static SQInteger wrap_setBadgeCount(SQVM *v) {
    int count;
    sq_getinteger(v, -1, (SQInteger *)&count);
    instance()->setBadgeCount(count);
    return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),
	_DECL_FUNC(postNotification, 2, "yt", true),
	_DECL_FUNC(requestUserAttention, 1, "y", true),
	_DECL_FUNC(beep, 1, "y", true),
	_DECL_FUNC(showBadge, 2, "yb", true),
	_DECL_FUNC(setBadgeCount, 2, "yn", true),
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_acorn_application(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Application";
	reg.functions = functions;

	return sqx_registerclass(v, System::type, reg);
}

}	// system
}	// acorn
