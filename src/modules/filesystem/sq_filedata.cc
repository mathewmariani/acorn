// acorn
#include "filesystem/sq_filedata.h"
#include "filesystem/filedata.h"

namespace acorn {
namespace filesystem {

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "FileData cannot be constructed directly.");
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	// functions
	_DECL_FUNC(constructor, NULL, NULL, false),

	// delimeter
	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_filedata(SQVM *v) {
	ClassRegistry reg;
	reg.name = "FileData";
	reg.functions = functions;

	return sqx_registerclass(v, FileData::type, reg);
}

}	// filesystem
}	// acorn
