R"squirrel"#(

class Game {
	Window = null
	# GraphicsDevice = null

	image = null
	framebuffer = null

	constructor()	{ /* ... */ }

	# general
	function init(args) {
		print(format("Game::init()\n"))
		for (local i = 0; i < args.len(); i++) {
			print("args["+i+"] = "+args[i]+"\n")
		}

		Window.setTitle("Acorn2D")
		/* ... */
	}

	function update(dt)	{
		#print(format("Game::update(%f)\n", dt))
		/* ... */
	}

	function draw()	{
		#print(format("Game::draw()\n"))
		/* ... */
	}

	# mouse callbacks
	function mousepressed(x, y, b) {
		# print(format("Game::mousepressed(%d, %d, %d)\n", x, y, b))
		/* ... */
	}

	function mousereleased(x, y, b) {
		# print(format("Game::mousereleased(%d, %d, %d)\n", x, y, b))
		/* ... */
	}

	function mousemotion(x, y, xr, yr) {
		# print(format("Game::mousemotion(%d, %d, %d, %d)\n", x, y, xr, yr))
		/* ... */
	}

	function mousewheel(x, y) {
		# print(format("Game::mousewheel(%d, %d)\n", x, y))
		/* ... */
	}

	# keyboard callbacks
	fullscreen = false
	function keypressed(scancode, repeat) {
		# print(format("Game::keypressed(%d, %s)\n", scancode, (repeat ? "true" : "false")))
		/* ... */
		# if (scancode == Scancode.escape) {
		# 	fullscreen = !fullscreen;
		# 	Window.setFullscreen(fullscreen)
		# }
	}

	function keyreleased(scancode) {
		# print(format("Game::keypressed(%d)\n", scancode))
		/* ... */
	}

	# joystick callbacks
	function joystickadded(which) {
		# print(format("Game::joystickadded(%d)\n", which))
		# local device = Joystick.getDevice(which)
		# print(format("getName() : %s\n", device.getName()))
		# print(format("isGameController() : %s\n", device.isGameController() ? "true" : "false"))
		# print(format("getAxisCount() : %d\n", device.getAxisCount()))
		# print(format("getBallCount() : %d\n", device.getBallCount()))
		# print(format("getHatCount() : %d\n", device.getHatCount()))
		# print(format("getButtonCount() : %d\n", device.getButtonCount()))
		# print(format("getPowerState() : %d\n", device.getPowerState()))
		/* ... */
	}

	function joystickremoved(which) {
		# print(format("Game::joystickremoved(%d)\n", which))
		/* ... */
	}

	function joystickpressed(which, button) {
		# print(format("Game::joystickpressed(%d, %d)\n", which, button))
		/* ... */
	}

	function joystickreleased(which, button) {
		# print(format("Game::joystickreleased(%d)\n", which, button))
		/* ... */
	}

	function joystickhat(which, hat, direction) {
		# print(format("Game::joystickhat(%d, %d, %d)\n", which, hat, direction))
		/* ... */
	}

	function joystickaxis(which, axis, direction) {
		# print(format("Game::joystickaxis(%d, %d, %d)\n", which, axis, direction))
		/* ... */
	}

	# controller callbacks
	function controlleraxis(which, axis, value) {
		# print(format("Game::controlleraxis(%d, %d, %f)\n", which, axis, value))
		/* ... */
	}

	function controllerpressed(which, button) {
		# print(format("Game::controllerreleased(%d)\n", which, button))
		/* ... */
	}

	function controllerreleased(which, button) {
		# print(format("Game::controllerreleased(%d)\n", which, button))
		/* ... */
	}

	# window callbacks
	function keyboardfocus(focus) {
		# print(format("Game::keyboardfocus(%s)\n", (focus ? "true" : "false")))
		/* ... */
	}

	function mousefocus(focus) {
		# print(format("Game::mousefocus(%s)\n", (focus ? "true" : "false")))
		/* ... */
	}

	function resize(width, height) {
		# print(format("Game::resize(%d, %d)\n", width, height))
		/* ... */
	}

	function moved(x, y) {
		# print(format("Game::moved(%d, %d)\n", x, y))
		/* ... */
	}

	function visible(visible) {
		# print(format("Game::visible(%s)\n", (visible ? "true" : "false")))
		/* ... */
	}

	# app callbacks
	function quit()	{ print("Game::quit()\n")/* ... */ }
	function lowmemory()	{ print("Game::lowmemory()\n")/* ... */ }

	# functions
	function load() { /* ... */ }
	function run() {
		local dt = 0.0
		while (true) {
			if ("event" in acorn) {
		    acorn.event.pump()
		    local vargs = acorn.event.poll()
				if (vargs != null) {
					local name = vargs[0]
					vargs[0] = this
					acorn.handlers[name].acall(vargs)
				}
		  }

			if ("timer" in acorn) {
				dt = acorn.timer.step()
			}

			if ("update" in this) {
				this.update(dt)
			}

			# if ("graphics" in acorn) {
				GraphicsDevice.clear(130, 58, 97, 255)
				if ("draw" in this) { this.draw() }
				GraphicsDevice.present()
			# }

			if ("timer" in acorn) {
				acorn.timer.sleep(1)
			}
		}
	}
}

local vertex = @"
#version 330 core
layout(location = 0) in vec2 VertexPosition;
layout(location = 1) in vec2 VertexTexCoord;
layout(location = 2) in vec4 VertexColor;
layout(location = 3) in vec4 ConstantColor;

uniform mat4 ProjectionMatrix;
uniform mat4 TransformMatrix;
uniform mat4 ProjTransMatrix;

out vec2 VertPosition;
out vec2 VertTexCoord;
out vec4 VertColor;
out vec4 ConstColor;

vec4 position(mat4 matrix, vec4 localPosition) {
	return matrix * localPosition;
}

void main() {
	VertPosition = VertexPosition;
	VertTexCoord = VertexTexCoord;
	VertColor = VertexColor;
	ConstColor = ConstantColor;
	//gl_Position = vec4(VertexPosition.xy, 0.0, 1.0);
	gl_Position = ProjTransMatrix * vec4(VertexPosition.xy, 0.0, 1.0);
}
"

local fragment = @"
#version 330 core
in vec2 VertPosition;
in vec2 VertTexCoord;
in vec4 VertColor;
in vec4 ConstColor;

uniform sampler2D uTexture;

out vec4 FragColor;

void main() {
	//FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	FragColor = texture2D(uTexture, VertTexCoord);
}
"

# this game class should be provided through main.nut
class Game1 extends Game {

	defaultShader = null
	image_1 = null
	image_2 = null

	constructor()  {
		/* ... */
	}

	function init(args) {
		local info = GraphicsDevice.getInfo()
		print(info.name + "\n")
		print(info.version + "\n")
		print(info.vendor + "\n")
		print(info.device + "\n")

		defaultShader = Shader(vertex, fragment)
		GraphicsDevice.setShader(defaultShader)
		GraphicsDevice.setPointSize(10)

		local data = ImageData("mario.png")
		image_1 = Image(data)
		image_2 = Image(data)
	}

	function update(dt) {

	}

	function draw() {
		// x, y, a, sx, sy, ox, oy
		image_1.draw(0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0)
		image_1.draw(50.0, 50.0, 0.0, 1.0, 1.0, 0.0, 0.0)
	}
}

::game <- null

function createHandlers() {
  acorn.handlers <- {
		# mouse
		mousepressed = function(x, y, b) {
			if ("mousepressed" in game) game.mousepressed(x, y, b)
		},
		mousereleased = function(x, y, b) {
			if ("mousereleased" in game) game.mousereleased(x, y, b)
		},
		mousemotion = function(x, y, xr, yr) {
			if ("mousemotion" in game) game.mousemotion(x, y, xr, yr)
		},
		mousewheel = function(x, y) {
			if ("mousewheel" in game) game.mousewheel(x, y)
		},

		# keyboard
		keypressed = function(s, r) {
			if ("keypressed" in game) game.keypressed(s, r)
		},
		keyreleased = function(k) {
			if ("keyreleased" in game) game.keyreleased(k)
		},

		# joystick
		mousefocus = function(f) {
			if ("mousefocus" in game) game.mousefocus(f)
		},
		keyboardfocus = function(f) {
			if ("keyboardfocus" in game) game.keyboardfocus(f)
		},
		visible = function(v) {
			if ("visible" in game) game.visible(v)
		},
		resize = function(w, h) {
			if ("resize" in game) game.resize(w, h)
		},
		moved = function(x, y) {
			if ("moved" in game) game.moved(x, y)
		},

		# controller
		controlleraxis = function(w, a, v) {
			if ("controlleraxis" in game) game.controlleraxis(w, a, v)
		},
		controllerbuttonup = function(w, b) {
			if ("controllerpressed" in game) game.controllerpressed(w, b)
		},
		controllerbuttondown = function(w, b) {
			if ("controllerreleased" in game) game.controllerreleased(w, b)
		},

		# window
		joystickadded = function(w) {
			if ("joystickadded" in game) game.joystickadded(w)
		},
		joystickremoved = function(w) {
			if ("joystickremoved" in game) game.joystickremoved(w)
		},
		joystickbuttonpressed = function(w, b) {
			if ("joystickpressed" in game) game.joystickpressed(w, b)
		},
		joystickbuttonreleased = function(w, b) {
			if ("joystickreleased" in game) game.joystickreleased(w, b)
		},
		joystickhat = function(w, h, d) {
			if ("joystickhat" in game) game.joystickhat(w, h, d)
		},
		joystickaxis = function(w, a, d) {
			if ("joystickaxis" in game) game.joystickaxis(w, a, d)
		},

		# app callbacks
		quit = function() {
			if ("quit" in game) game.quit()
		},
		lowmemory = function() {
			if ("lowmemory" in game) game.lowmemory()
		},
	}
}

function boot() {
	print("acorn.boot()\n")

	local arg0 = acorn.args[0]
	Directory.init(arg0)

	local exepath = Directory.getExecutable()
	if (exepath.len() == 0) {
		exepath = arg0
	}

	local is_bundle = Directory.setSource(exepath)
	print(format("is_bundle: %s\n", (is_bundle ? "true" : "false")))

	print("== acorn.boot() ===========================\n")
	print(format("%s\n", arg0))
	print("===========================================\n")

	print("== acorn.filesystem.Directory() ===========\n")
	Directory.setSource(exepath)

	# SDL_GetBasePath() : C:\Users\Mat Mariani\Desktop\acorn\bin\Debug\
	# SDL_GetPrefPath() : C:\Users\Mat Mariani\AppData\Roaming\acorn2d\mygame\
	# PHYSFS_getPrefDir() : C:\Users\Mat Mariani\AppData\Roaming\acorn2d\mygame\

	# love2d paths/directories
	# love.filesystem.getSourceBaseDirectory			.. C:/Users/Mat Mariani/Desktop/love-0.10.2-win64
	# love.filesystem.getIdentity									.. mygame
	# love.filesystem.getAppdataDirectory					X. C:/Users/Mat Mariani/AppData/Roaming
	# love.filesystem.getSource										X. C:/Users/Mat Mariani/Desktop/love-0.10.2-win64/fs
	# love.filesystem.getUserDirectory						X. C:\Users\Mat Mariani\
	# love.filesystem.getWorkingDirectory					X. C:/Users/Mat Mariani/Desktop/love-0.10.2-win64
	# love.filesystem.getExecutablePath						X. C:\Users\Mat Mariani\Desktop\love-0.10.2-win64\love.exe
	# love.filesystem.getSaveDirectory						X. C:/Users/Mat Mariani/AppData/Roaming/LOVE/mygame

	# NOTE: taken from running debugger
	# getBase: C:\Users\Mat Mariani\Desktop\acorn\bin\Debug\
	# getAppdata: C:\Users\Mat Mariani\AppData\Roaming
	# getPref: C:\Users\Mat Mariani\AppData\Roaming\acorn\mygame\
	# getUser: C:\Users\Mat Mariani\
	# getExecutable: C:\Users\Mat Mariani\Desktop\acorn\bin\Debug\acorn.exe
	# getCurrent: C:\Users\Mat Mariani\Desktop\acorn\build

	# NOTE: taken from running exe
	# getBase: C:\Users\Mat Mariani\Desktop\acorn\bin\Debug\
	# getAppdata: C:\Users\Mat Mariani\AppData\Roaming
	# getPref: C:\Users\Mat Mariani\AppData\Roaming\acorn\mygame\
	# getUser: C:\Users\Mat Mariani\
	# getExecutable: C:\Users\Mat Mariani\Desktop\acorn\bin\Debug\acorn.exe
	# getCurrent: C:\Users\Mat Mariani\Desktop\acorn\bin\Debug

	print(format("getAppdata: %s\n", Directory.getAppdata()))
	print(format("getBase: %s\n", Directory.getBase()))
	print(format("getCurrent: %s\n", Directory.getCurrent()))
	print(format("getExecutable: %s\n", Directory.getExecutable()))
	print(format("getPref: %s\n", Directory.getPref()))
	print(format("getUser: %s\n", Directory.getUser()))

	local path = Directory.getCurrent()
	Directory.mount(path)

	local list = Directory.list("tests")
	for (local i = 0; i < list.len(); i++) {
		print(format("list[%d] = %s\n", i, list[i]))
	}


	local path = Directory.getSearchPath()
	for (local i = 0; i < path.len(); i++) {
		print(format("path[%d] = %s\n", i, path[i]))
	}

	Directory.setSaveIdentity("acorn2d")
	Directory.make("mygame/settings")
	Directory.make("mygame/settings")
	Directory.remove("mygame/settings")
	print("===========================================\n")

	# print("== acorn.joystick.Joystick() ==============\n")
	# {
	# 	local devices = Joystick.getDevices()
	# 	print(format("count %d\n",Joystick.getJoystickCount()))
	# 	for (local i = 0; i < devices.len(); i++) {
	# 		print("getName : " + devices[i].getName() + "\n")
	# 	}
	# }
	# print("===========================================\n")

	print("== acorn.mouse.Cursor() ===================\n")
	{
		local cursor = acorn.mouse.getSystemCursor(CursorType.Hand);
	}
	print("===========================================\n")

	Application.beep()
	# check for game.nut
}

function init() {
	local config = {
		fullscreen = false,
		borderless = false,
		resizable = true,
		highdpi = true,
		centered = true,
		vsync = false
	}

	game <- Game1()

	if (!Window.setWindow(512, 448, config)) {
		throw "could not create window."
	} else {
		game.Window = Window
	}

	# create event handlers
	createHandlers();

	if ("init" in game) {
		game.init(acorn.args)
	}

	# this is the boot
	game.run()
}

local retval = function() {
	local result = 0
	boot(); init();
	return result
}

return retval()

#)squirrel"#"
