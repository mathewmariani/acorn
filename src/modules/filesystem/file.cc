// acorn
#include "filesystem/file.h"

namespace acorn {
namespace filesystem {

acorn::Type File::type = std::type_index(typeid(filesystem::File));

File::File(const std::string &filename) :
	filename(filename),
	file(nullptr) {

}

File::~File() {
	printf("File (%s) was released\n", filename.c_str());
	close();
}

bool File::open() {
	if (!PHYSFS_isInit()) {
		throw std::runtime_error("PhysFS is not initialized!");
	}

	if (!PHYSFS_exists(filename.c_str())) {
		throw std::runtime_error("file does not exist!");
	}

	if (file != nullptr) {
		return false;
	}

	PHYSFS_File *handle = nullptr;
	handle = PHYSFS_openRead(filename.c_str());

	if (handle == nullptr) {
		throw std::runtime_error("could not open file");
	}

	file = handle;
	return (file != nullptr);
}

bool File::close() {
	if (file == nullptr || !PHYSFS_close(file)) {
		return false;
	}
	file = nullptr;
	return true;
}

size_t File::read(void *dst) {
	if (file == nullptr) {
		throw std::runtime_error("file is not opened");
	}

	size_t length_read = (size_t)PHYSFS_read(file, dst, 1, PHYSFS_fileLength(file));
	return length_read;
}

std::shared_ptr<FileData> File::read() {
	// open the file
	open();

	std::shared_ptr<FileData> filedata = std::make_shared<FileData>(getSize());
	size_t bytes = read(filedata->getData());

	if (bytes <= 0) {
		throw std::runtime_error("Could not read from file.");
	}

	printf("read %d bytes from %s\n", bytes, getFilename().c_str());

	// close the file
	close();

	return filedata;
}

size_t File::getSize() {
	if (file == nullptr) {
		throw std::runtime_error("file is not opened");
	}
	return (size_t) PHYSFS_fileLength(file);
}

const std::string &File::getFilename() const {
	return filename;
}

}	// filesystem
}	// acorn
