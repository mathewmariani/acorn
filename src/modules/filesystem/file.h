#ifndef ACORN_FILESYSTEM_FILE_H
#define ACORN_FILESYSTEM_FILE_H

// C/C++
#include <memory>
#include <string>

// acorn
#include "common/object.h"
#include "filesystem/filedata.h"

// PhysFS
#include <physfs.h>

namespace acorn {
namespace filesystem {

class File : public acorn::Object {
public:
	static acorn::Type type;

public:
	File(const std::string &filename);
	virtual ~File();

public:
	bool open();
	bool close();

	size_t read(void *dst);
	std::shared_ptr<FileData> read();

	size_t getSize();
	const std::string &getFilename() const;

private:
	// managed physfs resources
	PHYSFS_File *file;

	std::string filename;
};	// file

}	// filesystem
}	// acorn

#endif	// ACORN_FILESYSTEM_FILE_H
