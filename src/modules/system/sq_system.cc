// acorn
#include "system/sq_application.h"
#include "system/sq_clipboard.h"
#include "system/sq_powerstate.h"
#include "system/sq_platform.h"
#include "system/sq_system.h"
#include "system/system.h"

namespace acorn {
namespace system {

#define instance() (Module::getInstance<system::System>())

// NOTE: types for acorn.system
const static SQFunction classes[] = {
	sq_acorn_application,
	sq_acorn_clipboard,
	sq_acorn_platform,
	sq_acorn_powerstate,
	nullptr
};

extern "C" int sq_acorn_system(SQVM *v) {
	auto instance = std::make_shared<system::System>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize system module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "system";
	reg.classes = classes;

	return acorn::sqx_registermodule_ext(v, reg);
}

}	// timer
}	// acorn
