#ifndef ACORN_SQ_KEYBOARD_H
#define ACORN_SQ_KEYBOARD_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace keyboard {

extern "C" int sq_acorn_keyboard(SQVM *v);

}	// keyboard
}	// acorn

#endif	// ACORN_SQ_KEYBOARD_H
