// acorn
#include "filesystem/sq_directory.h"
#include "filesystem/filesystem.h"

namespace acorn {
namespace filesystem {

#define instance() (Module::getInstance<filesystem::Filesystem>())

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Directory cannot be constructed directly.");
}

static SQInteger wrap_init(SQVM *v) {
	const char *arg0;
	sq_getstring(v, 2, &arg0);
	instance()->init(arg0);
	return 0;
}

static SQInteger wrap_setSource(SQVM *v) {
	const char *filepath;
	sq_getstring(v, 2, &filepath);
	sq_pushbool(v, instance()->setSource(filepath));
	return 1;
}

static SQInteger wrap_setSaveIdentity(SQVM *v) {
	const char *name;
	sq_getstring(v, 2, &name);
	sq_pushbool(v, instance()->setSaveIdentity(name));
	return 1;
}

static SQInteger wrap_getSearchPath(SQVM *v) {
	std::vector<std::string> list;
	instance()->getSearchPath(list);

	sq_newtableex(v, list.size());
	for (int i = 0; i < (int) list.size(); ++i) {
		sq_pushinteger(v, i);
		sq_pushstring(v, list[i].c_str(), -1);
		sq_newslot(v, -3, SQFalse);
	}

	return 1;
}

static SQInteger wrap_mount(SQVM *v) {
	const char *path;
	sq_getstring(v, 2, &path);
	sq_pushbool(v, instance()->mount(path));
	return 1;
}

static SQInteger wrap_unmount(SQVM *v) {
	const char *path;
	sq_getstring(v, 2, &path);
	sq_pushbool(v, instance()->unmount(path));
	return 1;
}

static SQInteger wrap_make(SQVM *v) {
	const char *dirname;
	sq_getstring(v, 2, &dirname);
	sq_pushbool(v, instance()->makeDirectory(dirname));
	return 1;
}

static SQInteger wrap_remove(SQVM *v) {
	const char *filename;
	sq_getstring(v, 2, &filename);
	sq_pushbool(v, instance()->remove(filename));
	return 1;
}

static SQInteger wrap_exists(SQVM *v) {
	const char *filepath;
	sq_getstring(v, 2, &filepath);
	sq_pushbool(v, instance()->exists(filepath));
	return 1;
}

static SQInteger wrap_list(SQVM *v) {
	const char *path;
	sq_getstring(v, 2, &path);

	std::vector<std::string> list;
	instance()->enumerateFiles(path, list);

	sq_newtableex(v, list.size());
	for (int i = 0; i < (int) list.size(); ++i) {
		sq_pushinteger(v, i);
		sq_pushstring(v, list[i].c_str(), -1);
		sq_newslot(v, -3, SQFalse);
	}

	return 1;
}

static SQInteger wrap_getBase(SQVM *v) {
	sq_pushstring(v, instance()->getBaseDirectory().c_str(), -1);
	return 1;
}

static SQInteger wrap_getAppdata(SQVM *v) {
	sq_pushstring(v, instance()->getAppdataDirectory().c_str(), -1);
	return 1;
}

static SQInteger wrap_getPref(SQVM *v) {
	sq_pushstring(v, instance()->getPrefDirectory().c_str(), -1);
	return 1;
}

static SQInteger wrap_getUser(SQVM *v) {
	sq_pushstring(v, instance()->getUserDirectory().c_str(), -1);
	return 1;
}

static SQInteger wrap_getExecutable(SQVM *v) {
	sq_pushstring(v, instance()->getExecutablePath().c_str(), -1);
	return 1;
}

static SQInteger wrap_getCurrent(SQVM *v) {
	sq_pushstring(v, instance()->getCurrentWorkingDirectory().c_str(), -1);
	return 1;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),

	_DECL_FUNC(init, 2, "ys", true),
	_DECL_FUNC(setSource, 2, "ys", true),

	_DECL_FUNC(setSaveIdentity, 2, "ys", true),

	_DECL_FUNC(getSearchPath, 1, "y", true),

	_DECL_FUNC(mount, 2, "ys", true),
	_DECL_FUNC(unmount, 2, "ys", true),

	_DECL_FUNC(make, 2, "ys", true),
	_DECL_FUNC(remove, 2, "ys", true),

	_DECL_FUNC(exists, 1, "y", true),
	_DECL_FUNC(list, 2, "ys", true),

	// extensions
	_DECL_FUNC(getBase, 1, "y", true),
	_DECL_FUNC(getAppdata, 1, "y", true),
	_DECL_FUNC(getPref, 1, "y", true),
	_DECL_FUNC(getUser, 1, "y", true),
	_DECL_FUNC(getExecutable, 1, "y", true),
	_DECL_FUNC(getCurrent, 1, "y", true),
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_directory(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Directory";
	reg.functions = functions;
	return sqx_registerclass(v, Filesystem::type, reg);
}

}	// filesystem
}	// acorn
