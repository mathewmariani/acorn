// STL
#include <iostream>

// squirrel
#include <squirrel.h>
#include <sqstdio.h>

// acorn
#include "common/import.h"

SQRESULT acorn_import(SQVM *v) {
    const SQChar* moduleName;
    HSQOBJECT table;

    sq_getstring(v, -2, &moduleName);
    sq_getstackobj(v, -1, &table);
    sq_addref(v, &table);

    sq_settop(v, 0); // Clear Stack
    sq_pushobject(v, table); // Push the target table onto the stack

	if(SQ_FAILED(sqstd_loadfile(v, moduleName, true))) {
		printf("File [%s] does not exist!", moduleName);
		return SQ_ERROR;
    }

    sq_push(v, -2);
    sq_call(v, 1, false, true);

    sq_settop(v, 0); // Clean up the stack (just in case the module load leaves it messy)
    sq_pushobject(v, table); // return the target table
    sq_release(v, &table);

    return SQ_OK;
}

static SQInteger sqx_import(SQVM *v) {
	sq_pushroottable(v);
	acorn_import(v);
	return SQ_OK;
}

int sqx_register_import(SQVM *v) {
	sq_pushroottable(v);
	sq_pushstring(v, _SC("import"), -1);
	sq_newclosure(v, &sqx_import, 0);
	sq_newslot(v, -3, 0);
	sq_pop(v, 1);
	return SQ_OK;
}
