// acorn
#include "window/window.h"
#include "window/sq_display.h"
#include "window/sq_window.h"
#include "window/sq_windowclass.h"

namespace acorn {
namespace window {

// NOTE: types for acorn.window
const static SQFunction classes[] = {
	sq_register_display,
	sq_register_window,
	nullptr
};

extern "C" int sq_acorn_window(SQVM *v) {
	auto instance = std::make_shared<window::Window>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize window module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "window";
	reg.classes = classes;

	return acorn::sqx_registermodule_ext(v, reg);
}

}	// window
}	// acorn
