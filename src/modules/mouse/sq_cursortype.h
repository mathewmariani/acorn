#ifndef ACORN_SQ_MOUSE_CURSORTYPE_H
#define ACORN_SQ_MOUSE_CURSORTYPE_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace mouse {

extern "C" int sq_acorn_cursortype(SQVM *v);

}	// mouse
}	// acorn

#endif	// ACORN_SQ_MOUSE_CURSORTYPE_H
