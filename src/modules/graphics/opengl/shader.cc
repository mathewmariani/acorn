// acorn
#include "common/exception.h"
#include "graphics/opengl/shader.h"

namespace acorn {
namespace graphics {
namespace opengl {

Shader::Shader(std::shared_ptr<acorn::graphics::ShaderStage> vertex, std::shared_ptr<acorn::graphics::ShaderStage> fragment) :
	acorn::graphics::Shader(vertex, fragment),
	program(0) {
	createShader();
}

Shader::~Shader() {
	if (program != 0) {
		if (current.get() == this) {
			glUseProgram(0);
		}
		glDeleteProgram(program);
		program = 0;
	}
}

bool Shader::createShader() {
	// create shader program
	program = glCreateProgram();
	if (program == 0) {
		throw acorn::Exception("Cannot created shader program object.");
	}

	// link all stages to program
	glAttachShader(program, *(GLuint *) vertexStage->getHandle());
	glAttachShader(program, *(GLuint *) fragmentStage->getHandle());

	// bind vertex attributes

	// link shader program
	glLinkProgram(program);

	GLint status;
	glGetProgramiv(program, GL_LINK_STATUS, &status);

	if (status == GL_FALSE) {
		// get program warnings
		GLint size, nullpos;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &size);
		if (size > 0) {
			char *temp = new char[size];
			memset(temp, '\0', size);
			glGetProgramInfoLog(program, size, &nullpos, temp);
			temp[nullpos] = '\0';

			printf("%s\n", temp);

			delete[] temp;
		}

		glDeleteProgram(program);
		program = 0;
		throw acorn::Exception("Cannot link shader program object.");
	}

	return true;
}

// FIXME:
void Shader::bind() {
	glUseProgram(program);
	current.reset(this);
}

// FIXME:
void Shader::unbind() {

}

const int Shader::getAttributeLocation(const std::string &name) {
	auto itr = attributes.find(name);
	if (itr != attributes.end()) {
		return itr->second;
	}

	GLint location = glGetAttribLocation(program, name.c_str());
	attributes[name] = location;
	return location;
}

const int Shader::getGetUniformLocation(const std::string &name) {
	auto itr = uniforms.find(name);
	if (itr != uniforms.end()) {
		return itr->second;
	}

	GLint location = glGetUniformLocation(program, name.c_str());
	uniforms[name] = location;
	return location;
}

// FIXME:
void Shader::sendFloats(int location, int count, const float *value) {
	switch (count) {
	case 4: glUniform4fv(location, count, value); break;
	case 3: glUniform3fv(location, count, value); break;
	case 2: glUniform2fv(location, count, value); break;
	case 1: default: glUniform1fv(location, count, value); break;
	}
}

// FIXME:
void Shader::sendInts(int location, int count, const int *value) {
	switch (count) {
	case 4: glUniform4iv(location, count, value); break;
	case 3: glUniform3iv(location, count, value); break;
	case 2: glUniform2iv(location, count, value); break;
	case 1: default: glUniform1iv(location, count, value); break;
	}
}

// FIXME:
void Shader::sendMatrices(int location, int count, const float *value) {
	switch (count) {
	case 4: glUniformMatrix4fv(location, 1, GL_FALSE, value); break;
	case 3: glUniformMatrix3fv(location, 1, GL_FALSE, value); break;
	case 2: default: glUniformMatrix2fv(location, 1, GL_FALSE, value); break;
	}
}

const void *Shader::getHandle() const {
	return &program;
}

}	// opengl
}	// graphics
}	// acorn
