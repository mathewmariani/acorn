// acorn
#include "common/log.h"
#include "common/platform.h"
#include "system/system.h"

#if defined (OS_WINDOWS)
	#include "platform/windows.h"
#elif defined (OS_MACOS)
	#include "platform/macosx.h"
#endif

// SDL
#include <SDL.h>

namespace acorn {
namespace system {

acorn::Type System::type = std::type_index(typeid(system::System));

System::System() {
	log_debug("System module initialized.");
}

System::~System() {
	log_debug("System module deinitialized.");
}

std::string System::getOS() const noexcept {
#if defined (OS_WINDOWS)
	return "Windows";
#elif defined (OS_MACOS)
	return "macOS";
#elif defined (OS_LINUX)
	return "Linux";
#else
	return "Unknown";
#endif
}

bool System::isWindows() const noexcept {
#if defined (OS_WINDOWS)
	return true;
#else
	return false;
#endif
}

bool System::isMacOS() const noexcept {
#if defined (OS_MACOS)
	return true;
#else
	return false;
#endif
}

bool System::isLinux() const noexcept {
#if defined (OS_LINUX)
	return true;
#else
	return false;
#endif
}

int System::getCores() const noexcept {
	return SDL_GetCPUCount();
}

int System::getMemory() const noexcept {
	return SDL_GetSystemRAM();
}

System::PowerState System::getPowerInfo(int *secs, int *pct) const noexcept {
	return (System::PowerState) SDL_GetPowerInfo(secs, pct);
}

void System::postNotification(Notification &notification) {
#if defined (OS_WINDOWS)
  STUBBED("Windows implementation");
#elif defined (OS_MACOS)
	acorn::macos::postNotification(notification.title, notification.message);
#elif defined (OS_LINUX)
  STUBBED("Linux implementation");
#else
#error Missing implementation for System::postNotification()
#endif
}

void System::requestUserAttention() const noexcept {
#if defined (OS_WINDOWS)
	STUBBED("Windows implementation");
#elif defined (OS_MACOS)
	acorn::macos::requestUserAttention();
#elif defined (OS_LINUX)
	STUBBED("Linux implementation");
#else
#error Missing implementation for System::requestUserAttention()
#endif
}

void System::beep() const noexcept {
#if defined (OS_WINDOWS)
	acorn::windows::systemBeep();
#elif defined (OS_MACOS)
	acorn::macos::systemBeep();
#elif defined (OS_LINUX)
	STUBBED("Linux implementation");
#else
#error Missing implementation for System::beep()
#endif
}


void System::showApplicationBadge(bool show) const noexcept {
#if defined (OS_MACOS)
  acorn::macos::showApplicationBadge(show);
#endif
}

void System::setBadgeCount(int count) const noexcept {
#if defined (OS_MACOS)
  acorn::macos::setBadgeCount(count);
#endif
}

bool System::setClipboardText(const std::string &text) const noexcept {
	return (SDL_SetClipboardText(text.c_str()) != 0);
}

std::string System::getClipboardText() const noexcept {
	std::string text("");

	// must be released using SDL_free
	char* data = SDL_GetClipboardText();
	if (data) {
		text = std::string(data);
		SDL_free(data);
	}

	return std::string(SDL_GetClipboardText());
}

bool System::hasClipboardText() const noexcept {
	return (SDL_HasClipboardText() == SDL_TRUE);
}

// FIXME: I don't know if this is needed
//std::unordered_map<std::string, System::PowerState> System::powerstateTable = {
//	{ "unknown", System::PowerState::UKNOWN },
//	{ "on_battery", System::PowerState::ON_BATTERY },
//	{ "no_battery", System::PowerState::NO_BATTERY },
//	{ "charging", System::PowerState::CHARGING },
//	{ "charged", System::PowerState::CHARGED }
//};

}	// system
}	// acorn
