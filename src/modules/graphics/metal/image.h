#ifndef ACORN_GRAPHICS_METAL_IMAGE_H
#define ACORN_GRAPHICS_METAL_IMAGE_H

// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// acorn
#include "graphics/metal/metal.h"
#include "graphics/image.h"

namespace acorn {
namespace graphics {
namespace metal {

// forward declaration
class Graphics;

class Image final : public acorn::graphics::Image {
public:
  Image(std::shared_ptr<image::ImageData> imagedata);
	virtual ~Image();

public:
	void setFilter(const Texture::FilterMode mode) noexcept override;
	void setWrap(const Texture::WrapMode mode) noexcept override;

public:
	// managed opengl resource
	const void *getHandle() const override;

private:
	// metal texture identifier

};	// image

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
#endif	// ACORN_GRAPHICS_METAL_IMAGE_H
