// acorn
#include "common/log.h"
#include "mouse/mouse.h"
#include "window/window.h"

namespace acorn {
namespace mouse {

acorn::Type Mouse::type = std::type_index(typeid(mouse::Mouse));

Mouse::Mouse() :
	systemCursors({}) {
	log_debug("Mouse module initialized.");
}

Mouse::~Mouse() {
	// release all system cursors
	systemCursors.clear();

	log_debug("Mouse module deinitialized.");
}

int Mouse::getX() const noexcept {
	int x;
	SDL_GetMouseState(&x, nullptr);
	return x;
}

int Mouse::getY() const noexcept {
	int y;
	SDL_GetMouseState(nullptr, &y);
	return y;
}

void Mouse::getPosition(int &x, int &y) const noexcept {
	SDL_GetMouseState(&x, &y);
}

void Mouse::setPosition(int x, int y) noexcept {
	SDL_WarpMouseGlobal(x, y);
}

void Mouse::setPositionInWindow(int x, int y) noexcept {
	SDL_Window *handle = nullptr;

	auto window = Module::getInstance<window::Window>();
	if (window) {
		handle = (SDL_Window *) window->getHandle();
	}

	SDL_WarpMouseInWindow(handle, (int) x, (int) y);
}

bool Mouse::getVisible() const noexcept {
	return SDL_ShowCursor(SDL_QUERY) == SDL_ENABLE;
}

void Mouse::setVisible(bool visible) noexcept {
	SDL_ShowCursor(visible ? SDL_ENABLE : SDL_DISABLE);
}

bool Mouse::getRelativeMode() const noexcept {
	return (SDL_GetRelativeMouseMode() == SDL_TRUE);
}

bool Mouse::setRelativeMode(bool relative) noexcept {
	return (SDL_SetRelativeMouseMode(relative ? SDL_TRUE : SDL_FALSE) == 0);
}

bool Mouse::isDown(int index) const noexcept {
	uint32 buttonstate = SDL_GetMouseState(nullptr, nullptr);
	return (buttonstate & SDL_BUTTON(index));
}

bool Mouse::isDown(const std::vector<int> &index) const noexcept {
	return false;
}

std::shared_ptr<mouse::Cursor> Mouse::getSystemCursor(Cursor::SystemCursorType type) noexcept {
	std::shared_ptr<mouse::Cursor> cursor = nullptr;

	auto itr = systemCursors.find(type);
	if (itr != systemCursors.end()) {
		cursor = itr->second;
	} else {
		cursor = std::make_shared<mouse::Cursor>(type);
		systemCursors[type] = cursor;
	}

	return cursor;
}

void Mouse::setSystemCursor(std::shared_ptr<mouse::Cursor> cursor) noexcept {
	SDL_SetCursor((SDL_Cursor *) cursor->getHandle());
}

}	// mouse
}	// acorn
