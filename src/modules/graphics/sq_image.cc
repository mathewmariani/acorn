// acorn
#include "graphics/opengl/image.h"
#include "graphics/quad.h"
#include "graphics/graphics.h"
#include "image/imagedata.h"

#include "graphics/sq_image.h"
#include "image/sq_imagedata.h"

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace acorn {
namespace graphics {

// FIXME:
// [-] : clean up #include
// [-] : use `factory` pattern to remove graphics::opengl::Image which causes cluttering

#define instance() (Module::getInstance<graphics::Graphics>())

static SQInteger wrap_constructor(SQVM *v) {
	std::shared_ptr<image::ImageData> imagedata{
		sqx_checkinstance<image::ImageData>(v, 2)
	};
	std::shared_ptr<graphics::opengl::Image> image{
		std::make_shared<graphics::opengl::Image>(imagedata)
	};
	sqx_pushinstance(v, Image::type, image);
	return 1;
}

static SQInteger wrap_getWidth(SQVM *v) {
	std::weak_ptr<graphics::opengl::Image> image{
		sqx_checkinstance<graphics::opengl::Image>(v, 1)
	};
	if (auto spt = image.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getWidth());
		return 1;
	}
	return 0;
}

static SQInteger wrap_getHeight(SQVM *v) {
	std::weak_ptr<graphics::opengl::Image> image{
		sqx_checkinstance<graphics::opengl::Image>(v, 1)
	};
	if (auto spt = image.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getHeight());
		return 1;
	}
	return 0;
}

static SQInteger wrap_draw(SQVM *v) {
	std::weak_ptr<graphics::opengl::Image> image{
		sqx_checkinstance<graphics::opengl::Image>(v, 1)
	};

	float x, y, a, sx, sy, ox, oy;
	sq_getfloat(v, 2, (SQFloat *) &x);
	sq_getfloat(v, 3, (SQFloat *) &y);
	sq_getfloat(v, 4, (SQFloat *) &a);
	sq_getfloat(v, 5, (SQFloat *) &sx);
	sq_getfloat(v, 6, (SQFloat *) &sy);
	sq_getfloat(v, 7, (SQFloat *) &ox);
	sq_getfloat(v, 8, (SQFloat *) &oy);

	glm::mat4 t(1.0f);
	t = glm::translate(t, glm::vec3(ox, oy, 0.0f));
	t = glm::scale(t, glm::vec3(sx, sy, 0.0f));
	t = glm::rotate(t, a, glm::vec3(0.0, 0.0, 1.0f));
	t = glm::translate(t, glm::vec3(x, y, 0.0f));

	if (auto spt = image.lock()) {
		spt->draw(instance(), t);
	}
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, -1, "x", false),
	_DECL_FUNC(getWidth, 1, "x", false),
	_DECL_FUNC(getHeight, 1, "x", false),

	// FIXME: not final
	_DECL_FUNC(draw, 8, "x nn n nn nn", false),

	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_image(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Image";
	reg.functions = functions;

	return sqx_registerclass(v, Image::type, reg);
}

}	// graphics
}	// acorn
