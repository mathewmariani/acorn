---
layout: module
title: acorn
---

{% include incomplete.html %}

Acorn provides support for controller, joystick, keyboard, mouse, touch, time, window events and much more through callback functions. These callbacks can be used to for initialization, content loading, managing the game's state and rendering.


```
function acorn::load() {
  # initialization and content loading
}

function acorn::update(dt) {
  # manage your game's state here.
}

function acorn::draw() {
  # render the game's state to the screen.
}
```
