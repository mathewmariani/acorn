#ifndef ACORN_GRAPHICS_PARTICLESYSTEM_H
#define ACORN_GRAPHICS_PARTICLESYSTEM_H

// STL
#include <memory>

// acorn
#include "common/object.h"

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// FIXME: should be implemented without using OpenGL functions
// NOTE: but for now its fine.
// gl3w
#include "libraries/gl3w/gl3w.h"

namespace acorn {
namespace graphics {

// forward declaration
class Graphics;

class ParticleSystem : public acorn::Object {
public:
	static acorn::Type type;

public:
	struct Particle {
		glm::vec2 pos;
		glm::vec2 speed;
		unsigned char r;
		unsigned char g;
		unsigned char b;
		unsigned char a;
		float size;
		float angle;
		float weight;
		float life;
	};

public:
	ParticleSystem();
	virtual ~ParticleSystem();

public:
	void draw(std::shared_ptr<Graphics> gfx, const glm::mat4 &m);

private:
	GLuint particles_vertex_buffer;
	GLuint particles_position_buffer;
	GLuint particles_color_buffer;
};	// particlesystem

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_PARTICLESYSTEM_H
