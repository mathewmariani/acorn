// acorn
#include "common/exception.h"
#include "graphics/graphics.h"
#include "graphics/shader.h"

// glm
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace acorn {
namespace graphics {

acorn::Type Shader::type = std::type_index(typeid(graphics::Shader));

// strong reference
std::shared_ptr<Shader> Shader::current{ nullptr };

Shader::Shader(std::shared_ptr<ShaderStage> vertex, std::shared_ptr<ShaderStage> fragment) :
	vertexStage(vertex),
	fragmentStage(fragment) {
}

Shader::~Shader() {

}

// void Shader::updateUniforms() {
// 	auto gfx = Module::getInstance<graphics::Graphics>();
// 	const auto &projection = gfx->getProjection();
// 	const auto &transform = gfx->getTransform();
//
// 	{	// projection
// 		GLint location = getGetUniformLocation("ProjectionMatrix");
// 		if (location >= 0) {
// 			glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(projection));
// 		}
// 	}
// 	{	// transform
// 		GLint location = getGetUniformLocation("TransformMatrix");
// 		if (location >= 0) {
// 			glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(transform));
// 		}
// 	}
// }

}	// graphics
}	// acorn
