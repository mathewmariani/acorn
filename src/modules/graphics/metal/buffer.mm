// acorn
#include "common/platform.h"

#if defined(OS_MACOS)

// acorn
#include "common/exception.h"
#include "graphics/metal/buffer.h"

namespace acorn {
namespace graphics {
namespace metal {

Buffer::Buffer(size_t size, BufferType type, BufferUsage usage) :
	acorn::graphics::Buffer(size, type, usage),
	memory(nullptr) {

	try {
		memory = new char[size];
	} catch (std::bad_alloc&) {
		throw acorn::Exception("out of memory");
	}

	if (!create()) {
		delete[] memory;
		throw acorn::Exception("could not create vertex buffer");
	}
}

Buffer::~Buffer() {
	delete[] memory;
}

void *Buffer::bind() {
	return memory;
}

void Buffer::unbind(size_t usedsize) {

}

void Buffer::nextFrame() {

}

bool Buffer::create() {
	return false;
}

const void *Buffer::getHandle() const {
	return nullptr;
}

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
