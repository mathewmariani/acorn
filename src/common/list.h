#ifndef ACORN_LIST_H
#define ACORN_LIST_H

// STL
#include <string>
#include <vector>

// acorn
#include "common/squirrel.h"
#include "common/variant.h"

namespace acorn {

class List {
public:
	List(const std::string &key, const std::vector<Variant> &vargs = {});
	~List();

	int toSquirrel(SQVM *v) const noexcept;

private:
	std::string key;
	std::vector<Variant> vargs;
};	// list

}	// acorn

#endif	// ACORN_LIST_H
