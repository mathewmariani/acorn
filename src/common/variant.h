#ifndef ACORN_VARIANT_H
#define ACORN_VARIANT_H

// STL
#include <string>

// squirrel
#include "common/object.h"
#include "common/squirrel.h"

namespace acorn {

class Variant {
public:
	enum {
		T_NULL = (1 << 0),
		T_BOOL = (1 << 1),
		T_INTEGER = (1 << 2),
		T_FLOAT = (1 << 3),
		T_STRING = (1 << 4),
		T_INSTANCE = (1 << 5),
		T_USERPOINTER = (1 << 6),
	};

	union Data {
		bool t_bool;
		int t_int;
		float t_float;
		std::string *t_string;
		//acorn::ForeignObject *t_instance;
		void *t_userptr;
	};

public:
	Variant();
	Variant(bool boolean);
	Variant(int number);
	Variant(float number);
	Variant(const std::string &string);
	//Variant(acorn::Type &type, std::shared_ptr<acorn::Object> instance);
	Variant(void *userptr);
	~Variant();

public:
	Variant &operator = (const Variant &v);

public:
	int getType() const noexcept { return type; }

	// inline int getInt() const noexcept { return (type == T_INTEGER) ? data.t_int : 0; }
	// inline bool getBool() const noexcept { return (type == T_BOOL) ? data.t_bool : false; }
	// inline float getFloat() const noexcept { return (type == T_FLOAT) ? data.t_float : 0.0f; }
	// inline void *getUserPointer() const noexcept { return (type == T_USERPOINTER) ? data.t_userptr : nullptr; }

	inline bool isInt() const noexcept { return (type == T_INTEGER); }
	inline bool isBool() const noexcept { return (type == T_BOOL); }
	inline bool isFloat() const noexcept { return (type == T_FLOAT); }
	inline bool isString() const { return (type == T_STRING); }
	inline bool isInstance() const { return (type == T_INSTANCE); }
	inline bool isUserPointer() const { return (type == T_USERPOINTER); }

	void toSquirrel(SQVM *v) const noexcept;

private:
	int type;
	Data data;
};	// variant

}	// acorn

#endif	// ACORN_VARIANT_H
