// C/C++
#include <stdio.h>
#include <stdarg.h>

// acorn
#include "common/log.h"

namespace acorn {

void _log(int level, const char *file, int line, const char *str, ...) {
  va_list args;

  switch (level) {
		case 0: fprintf(stdout, "TRACE: "); break;
    case 1: fprintf(stdout, "INFO: "); break;
    case 2: fprintf(stdout, "ERROR: "); break;
    case 3: fprintf(stdout, "WARNING: "); break;
    case 4: fprintf(stdout, "DEBUG: "); break;
    default: break;
  }

#ifdef _DEBUG
  // print file and line
  fprintf(stdout, "%s:%d: ", file, line);
#endif

  va_start(args, str);
  vfprintf(stdout, str, args);
  va_end(args);

  fprintf(stdout, "\n");
}

}	// acorn
