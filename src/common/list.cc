// acorn
#include "common/list.h"

namespace acorn {

List::List(const std::string &key, const std::vector<Variant> &vargs) :
	key(key),
	vargs(vargs) {

}

List::~List() {

}

int List::toSquirrel(SQVM *v) const noexcept {
	sq_newarray(v, 0);

	sq_pushstring(v, key.c_str(), key.length());
	sq_arrayappend(v, -2);

	for (const auto &var : vargs) {
		var.toSquirrel(v);
		sq_arrayappend(v, -2);
	}
	
	return (int) vargs.size() + 1;
}

}	// acorn
