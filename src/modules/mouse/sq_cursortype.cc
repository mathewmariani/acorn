// acorn
#include "mouse/sq_cursortype.h"
#include "mouse/cursor.h"

namespace acorn {
namespace mouse {

EnumRegistry reg[] = {
	{ "Arrow", (int) Cursor::SystemCursorType::ARROW },
	{ "IBeam", (int) Cursor::SystemCursorType::IBEAM },
	{ "Wait", (int) Cursor::SystemCursorType::WAIT },
	{ "Crosshair", (int) Cursor::SystemCursorType::CROSSHAIR },
	{ "WaitArrow", (int) Cursor::SystemCursorType::WAITARROW },
	{ "SizeNWSE", (int) Cursor::SystemCursorType::SIZENWSE },
	{ "SizeNESW", (int) Cursor::SystemCursorType::SIZENESW },
	{ "SizeWE", (int) Cursor::SystemCursorType::SIZEWE },
	{ "SizeNS", (int) Cursor::SystemCursorType::SIZENS },
	{ "SizeAll", (int) Cursor::SystemCursorType::SIZEALL },
	{ "No", (int) Cursor::SystemCursorType::NO },
	{ "Hand", (int) Cursor::SystemCursorType::HAND },
};

extern "C" int sq_acorn_cursortype(SQVM *v) {
	return sqx_registerenum(v, "CursorType", reg);
}

}	// mouse
}	// acorn
