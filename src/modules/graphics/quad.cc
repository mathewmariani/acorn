// acorn
#include "graphics/quad.h"

namespace acorn {
namespace graphics {

acorn::Type Quad::type = std::type_index(typeid(graphics::Quad));

Quad::Quad(float x, float y, float w, float h, float sw, float sh) :
	width(w),
	height(h),
	textureWidth(sw),
	textureHeight(sh) {

	// FIXME: should this include colour?

	// NOTE: custom vertex implementation
	//vertices[0] = { 0.0f, 0.0f, 0.0f,		(x / sw), (y / sh) };
	//vertices[1] = { 0.0f, h, 0.0f,		(x / sw), ((y + h) / sh) };
	//vertices[2] = { w, 0.0f, 0.0f,		((x + w) / sw), (y / sh) };
	//vertices[3] = { w, h, 0.0f,			((x + w) / sw),  ((y + h) / sh) };

	vertices[0] = { 0.0f, 0.0f,		(x / sw), (y / sh) };
	vertices[1] = { 0.0f, h,		(x / sw), ((y + h) / sh) };
	vertices[2] = { w, 0.0f,		((x + w) / sw), (y / sh) };
	vertices[3] = { w, h,			((x + w) / sw),  ((y + h) / sh) };

	// NOTE: basic vertex implementation
	//vertices[0] = { -1.0f,  1.0f,	0.0f, 1.0f };
	//vertices[1] = { -1.0f, -1.0f,	0.0f, 0.0f };
	//vertices[2] = {  1.0f,  1.0f,	1.0f, 1.0f };
	//vertices[3] = {  1.0f, -1.0f,	1.0f, 0.0f };

	// NOTE: glm vertex implementation
	vertexPositions[0] = { 0.0f, 0.0f };
	vertexPositions[1] = { 0.0f, h };
	vertexPositions[2] = { w, 0.0f };
	vertexPositions[3] = { w, h };

	vertexTexCoords[0] = { (x / sw), (y / sh) };
	vertexTexCoords[1] = { (x / sw), ((y + h) / sh) };
	vertexTexCoords[2] = { ((x + w) / sw), (y / sh) };
	vertexTexCoords[3] = { ((x + w) / sw),  ((y + h) / sh) };
}

Quad::~Quad() {

}

const float Quad::getWidth() const noexcept {
	return width;
}

const float Quad::getHeight() const noexcept {
	return height;
}

const float Quad::getTextureWidth() const noexcept {
	return textureWidth;
}

const float Quad::getTextureHeight() const noexcept {
	return textureHeight;
}

const Vertex *Quad::getVertices() const noexcept {
	return vertices;
}

const glm::vec2 *Quad::getVertexPositions() const noexcept {
	return vertexPositions;
}

const glm::vec2 *Quad::getVertexTexCoords() const noexcept {
	return vertexTexCoords;
}

}	// graphics
}	// acorn
