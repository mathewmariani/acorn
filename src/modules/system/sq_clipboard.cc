// acorn
#include "system/sq_clipboard.h"
#include "system/system.h"

namespace acorn {
namespace system {

#define instance() (Module::getInstance<system::System>())

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Clipboard cannot be constructed directly.");
}

static SQInteger wrap_setText(SQVM *v) {
	const char *ctext;
	sq_getstring(v, 2, &ctext);
	sq_pushbool(v, instance()->setClipboardText(ctext));
	return 1;
}

static SQInteger wrap_getText(SQVM *v) {
	sq_pushstring(v, instance()->getClipboardText().c_str(), -1);
	return 1;
}

static SQInteger wrap_hasText(SQVM *v) {
	sq_pushbool(v, instance()->hasClipboardText());
	return 1;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),
	_DECL_FUNC(setText, 1, "ys", true),
	_DECL_FUNC(getText, 1, "y", true),
	_DECL_FUNC(hasText, 1, "y", true),
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_acorn_clipboard(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Clipboard";
	reg.functions = functions;
	
	return sqx_registerclass(v, System::type, reg);
}

}	// system
}	// acorn
