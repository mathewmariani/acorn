// acorn
#include "system/sq_platform.h"
#include "system/system.h"

namespace acorn {
namespace system {

#define instance() (Module::getInstance<system::System>())

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Platform cannot be constructed directly.");
}

static SQInteger wrap_getName(SQVM *v) {
	sq_pushstring(v, instance()->getOS().c_str(), -1);
	return 1;
}

static SQInteger wrap_getCores(SQVM *v) {
	sq_pushinteger(v, instance()->getCores());
	return 1;
}

static SQInteger wrap_getMemory(SQVM *v) {
	sq_pushinteger(v, instance()->getMemory());
	return 1;
}

static SQInteger wrap_getPowerState(SQVM *v) {
	int seconds, percent;
	int state = (int) instance()->getPowerInfo(&seconds, &percent);

	sq_newtableex(v, 3);

	sq_pushstring(v, "state", -1);
	sq_pushinteger(v, state);
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "seconds", -1);
	sq_pushinteger(v, seconds);
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "percent", -1);
	sq_pushinteger(v, percent);
	sq_newslot(v, -3, SQFalse);

	return 1;
}

static SQInteger wrap_isWindows(SQVM *v) {
	sq_pushbool(v, instance()->isWindows());
	return 1;
}

static SQInteger wrap_isMacOS(SQVM *v) {
	sq_pushbool(v, instance()->isMacOS());
	return 1;
}

static SQInteger wrap_isLinux(SQVM *v) {
	sq_pushbool(v, instance()->isLinux());
	return 1;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),
	_DECL_FUNC(getName, 1, "y", true),
	_DECL_FUNC(getCores, 1, "y", true),
	_DECL_FUNC(getMemory, 1, "y", true),
	_DECL_FUNC(getPowerState, 1, "y", true),
	_DECL_FUNC(isWindows, 1, "y", true),
	_DECL_FUNC(isMacOS, 1, "y", true),
	_DECL_FUNC(isLinux, 1, "y", true),
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_acorn_platform(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Platform";
	reg.functions = functions;

	return sqx_registerclass(v, System::type, reg);
}

}	// system
}	// acorn
