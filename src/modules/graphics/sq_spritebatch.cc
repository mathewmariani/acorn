// acorn
#include "graphics/sq_spritebatch.h"
#include "graphics/spritebatch.h"
#include "graphics/texture.h"
#include "graphics/quad.h"

#include "graphics/graphics.h"

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace acorn {
namespace graphics {

#define instance() (Module::getInstance<graphics::Graphics>())

static SQInteger wrap_constructor(SQVM *v) {
	std::shared_ptr<Texture> texture{
		sqx_checkinstance<Texture>(v, 2)
	};
	int size = sqx_getnumber(v, 3, 0);
	try {
		std::shared_ptr<SpriteBatch> spritebatch{
			std::make_shared<SpriteBatch>(texture, size)
		};
		sqx_pushinstance(v, SpriteBatch::type, spritebatch);
	} catch (const std::exception &e) {
		return sq_throwerror(v, "could not create spritebatch");
	}
	return 1;
}

static SQInteger wrap_add(SQVM *v) {
	std::weak_ptr<SpriteBatch> spritebatch{
		sqx_checkinstance<SpriteBatch>(v, 1)
	};
	if (auto spt = spritebatch.lock()) {
		std::shared_ptr<Quad> quad{
			sqx_checkinstance<Quad>(v, 2)
		};

		auto ox = (0.5f * quad->getWidth());
		auto oy = (0.5f * quad->getWidth());

		float x = sqx_getnumber(v, 3, 0.0f);
		float y = sqx_getnumber(v, 4, 0.0f);
		float a = sqx_getnumber(v, 5, 0.0f);
		float sx = sqx_getnumber(v, 6, 0.0f);
		float sy = sqx_getnumber(v, 7, 0.0f);

		glm::mat4 t(1.0f);
		t = glm::translate(t, glm::vec3(x, y, 0.0f));
		t = glm::translate(t, glm::vec3(ox, oy, 0.0f));
		t = glm::rotate(t, a, glm::vec3(0.0f, 0.0f, 1.0f));
		t = glm::scale(t, glm::vec3(sx, sy, 0.0f));
		t = glm::translate(t, glm::vec3(-ox, -oy, 0.0f));

		sq_pushinteger(v, spt->add(quad.get(), t));
		return 1;
	}

	return 0;
}

static SQInteger wrap_set(SQVM *v) {
	std::weak_ptr<SpriteBatch> spritebatch{
		sqx_checkinstance<SpriteBatch>(v, 1)
	};
	if (auto spt = spritebatch.lock()) {
		std::shared_ptr<Quad> quad{
			sqx_checkinstance<Quad>(v, 2)
		};

		auto ox = (0.5f * quad->getWidth());
		auto oy = (0.5f * quad->getWidth());

		float x = sqx_getnumber(v, 3, 0.0f);
		float y = sqx_getnumber(v, 4, 0.0f);
		float a = sqx_getnumber(v, 5, 0.0f);
		float sx = sqx_getnumber(v, 6, 0.0f);
		float sy = sqx_getnumber(v, 7, 0.0f);
		int idx = sqx_getnumber(v, 8, 0);

		glm::mat4 t(1.0f);
		t = glm::translate(t, glm::vec3(x, y, 0.0f));
		t = glm::translate(t, glm::vec3(ox, oy, 0.0f));
		t = glm::rotate(t, a, glm::vec3(0.0f, 0.0f, 1.0f));
		t = glm::scale(t, glm::vec3(sx, sy, 0.0f));
		t = glm::translate(t, glm::vec3(-ox, -oy, 0.0f));

		sq_pushinteger(v, spt->add(quad.get(), t, idx));
		return 1;
	}
	return 0;
}

static SQInteger wrap_clear(SQVM *v) {
	std::weak_ptr<SpriteBatch> spritebatch{
		sqx_checkinstance<SpriteBatch>(v, 1)
	};
	if (auto spt = spritebatch.lock()) {
        spt->clear();
	}
	return 0;
}

static SQInteger wrap_draw(SQVM *v) {
	std::weak_ptr<SpriteBatch> spritebatch{
		sqx_checkinstance<SpriteBatch>(v, 1)
	};
	if (auto spt = spritebatch.lock()) {
		std::shared_ptr<Quad> quad{
			sqx_checkinstance<Quad>(v, 2)
		};

		// auto ox = (0.5f * quad->getWidth());
		// auto oy = (0.5f * quad->getWidth());

		float x = sqx_getnumber(v, 3, 0.0f);
		float y = sqx_getnumber(v, 4, 0.0f);
		float a = sqx_getnumber(v, 5, 0.0f);
		float sx = sqx_getnumber(v, 6, 0.0f);
		float sy = sqx_getnumber(v, 7, 0.0f);

		glm::mat4 t(1.0f);
		t = glm::translate(t, glm::vec3(x, y, 0.0f));
		// t = glm::translate(t, glm::vec3(ox, oy, 0.0f));
		t = glm::rotate(t, a, glm::vec3(0.0f, 0.0f, 1.0f));
		t = glm::scale(t, glm::vec3(sx, sy, 0.0f));
		// t = glm::translate(t, glm::vec3(-ox, -oy, 0.0f));

		spt->draw(instance(), t);
	}
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 3, "xxi", false),
	_DECL_FUNC(add, 7, "xxnnnnn", false),
	_DECL_FUNC(set, 8, "xxnnnnni", false),
	_DECL_FUNC(clear, 1, "x", false),
	_DECL_FUNC(draw, 6, "xnnnnn", false),
	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_spritebatch(SQVM *v) {
	ClassRegistry reg;
	reg.name = "SpriteBatch";
	reg.functions = functions;

	return sqx_registerclass(v, SpriteBatch::type, reg);
}

}	// graphics
}	// acorn
