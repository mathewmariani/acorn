#ifndef ACORN_SQ_GRAPHICS_PARTICLESYSTEM_H
#define ACORN_SQ_GRAPHICS_PARTICLESYSTEM_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace graphics {

extern "C" int sq_register_particlesystem(SQVM *v);

}	// graphics
}	// acorn

#endif	// ACORN_SQ_GRAPHICS_PARTICLESYSTEM_H
