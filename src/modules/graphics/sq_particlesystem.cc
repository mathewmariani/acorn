// C/C++
#include <memory>

// acorn
#include "graphics/graphics.h"
#include "graphics/particlesystem.h"
#include "graphics/sq_particlesystem.h"

namespace acorn {
namespace graphics {

#define instance() (Module::getInstance<graphics::Graphics>())

static SQInteger wrap_constructor(SQVM *v) {
	STUBBED("not implemented yet.");
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_particlesystem(SQVM *v) {
	ClassRegistry reg;
	reg.name = "ParticleSystem";
	reg.functions = functions;

	return sqx_registerclass(v, ParticleSystem::type, reg);
}

}	// graphics
}	// acorn
