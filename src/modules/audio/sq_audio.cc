// acorn
#include "audio/sq_audio.h"
#include "audio/sq_source.h"
#include "audio/audio.h"

namespace acorn {
namespace audio {

// NOTE: types for acorn.audio
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for bisous.audio
const static SQFunction types[] = {
	sq_register_source,
	nullptr
};

extern "C" int sq_acorn_audio(SQVM *v) {
	auto instance = std::make_shared<audio::Audio>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize audio module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "audio";
	reg.classes = nullptr;

	return acorn::sqx_registermodule_ext(v, reg);
}

}	// audio
}	// acorn
