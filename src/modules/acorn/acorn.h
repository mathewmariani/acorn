#ifndef ACRON_ACORN_H
#define ACRON_ACORN_H

// acorn
#include "common/platform.h"

// forward declare SQVM
struct SQVM;

#ifdef __cplusplus
extern "C" {
#endif

ACORN_API int sq_register_acorn(SQVM*);
ACORN_API int sq_acorn_boot(SQVM*);

#ifdef __cplusplus
}
#endif

#endif	// ACRON_ACORN_H
