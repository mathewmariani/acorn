// STL
#include <iostream>

// acorn
#include "common/log.h"
#include "common/platform.h"
#include "joystick/joystickmodule.h"

namespace acorn {
namespace joystick {

acorn::Type JoystickModule::type = std::type_index(typeid(joystick::JoystickModule));

JoystickModule::JoystickModule() {
	log_debug("Joystick module initialized.");

	// initialize sdl subsystem
	if (SDL_InitSubSystem(SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER) < 0) {
		log_error("Could not initialize SDL subsystem: (%s)\n", SDL_GetError());
	}

	// there may be joysticks that were plugged in before we initialized
	for (int i = 0; i < SDL_NumJoysticks(); ++i) {
		addJoystick(i);
	}

	SDL_JoystickEventState(SDL_ENABLE);
	SDL_GameControllerEventState(SDL_ENABLE);
}

JoystickModule::~JoystickModule() {
	log_debug("Joystick module deinitialized.");

	// close any open joysticks
	for (auto joy : joysticks) {
		joy->close();
	}

	joysticks.clear();

	// deinitialize subsystem
	SDL_QuitSubSystem(SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER);
}

std::shared_ptr<joystick::Joystick> JoystickModule::addJoystick(int index) {
	if (!isValidDeviceIndex(index)) {
		return nullptr;
	}

	auto joystick = std::make_shared<Joystick>((int) joysticks.size());
	if (!joystick->open(index)) {
		return nullptr;
	}

	// check for duplications
	for (auto joy : joysticks) {
		if (joystick->getHandle() == joy->getHandle()) {
			joystick->close();
			return joy;
		}
	}

	joysticks.push_back(std::move(joystick));
	return joystick;
}

std::shared_ptr<joystick::Joystick> JoystickModule::removeJoystick(int index) {
	if (!isValidDeviceIndex(index)) {
		return nullptr;
	}

	std::shared_ptr<Joystick> joystick = nullptr;
	for (auto joy : joysticks) {
		if (joy->getInstanceID() == index) {
			joy->close();
		}
	}

	printf("%s : %d\n", __FUNCTION__, index);
	return joystick;
}

void JoystickModule::removeJoystick(std::shared_ptr<joystick::Joystick>) {
	printf("%s\n", __FUNCTION__);
}

std::shared_ptr<joystick::Joystick> JoystickModule::getJoystick(int index) const noexcept {
	return joysticks[index];
}

int JoystickModule::getJoystickCount() const noexcept {
	return SDL_NumJoysticks();
}

std::string JoystickModule::getDeviceName(int index) const noexcept {
	return SDL_JoystickNameForIndex(index);
}

bool JoystickModule::isValidDeviceIndex(int index) const noexcept {
	return ((index >= 0) && (index <= getJoystickCount()));
}

}	// joystick
}	// acorn
