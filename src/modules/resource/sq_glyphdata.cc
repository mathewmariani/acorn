// acorn
#include "resource/sq_glyphdata.h"
#include "resource/glyphdata.h"

namespace acorn {
namespace resource {

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "GlyphData cannot be constructed directly.");
}

static SQInteger wrap_getMetrics(SQVM *v) {
	GlyphData *self = nullptr;
	if (SQ_FAILED(sq_getinstanceup(v, 1, (SQUserPointer *)&self, nullptr))) {
		return sq_throwerror(v, "GlyphData expected");
	}

	// FIXME: make something in common/runtime to avoid lots of code like this
	sq_newtableex(v, 5);

	sq_pushstring(v, "advance", -1);
	sq_pushinteger(v, (SQInteger)self->getAdvance());
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "ascebearingx", -1);
	sq_pushinteger(v, (SQInteger)self->getBearingX());
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "bearingy", -1);
	sq_pushinteger(v, (SQInteger)self->getBearingY());
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "height", -1);
	sq_pushinteger(v, (SQInteger)self->getHeight());
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "width", -1);
	sq_pushinteger(v, (SQInteger)self->getWidth());
	sq_newslot(v, -3, SQFalse);

	sq_newslot(v, -3, SQFalse);

	return 1;
}

static SQInteger wrap_getAdvance(SQVM *v) {
	GlyphData *self = nullptr;
	if (SQ_FAILED(sq_getinstanceup(v, 1, (SQUserPointer *)&self, nullptr))) {
		return sq_throwerror(v, "GlyphData expected");
	}

	sq_pushinteger(v, (SQInteger)self->getAdvance());
	return 1;
}

static SQInteger wrap_getBearingX(SQVM *v) {
	GlyphData *self = nullptr;
	if (SQ_FAILED(sq_getinstanceup(v, 1, (SQUserPointer *)&self, nullptr))) {
		return sq_throwerror(v, "GlyphData expected");
	}

	sq_pushinteger(v, (SQInteger)self->getBearingX());
	return 1;
}

static SQInteger wrap_getBearingY(SQVM *v) {
	GlyphData *self = nullptr;
	if (SQ_FAILED(sq_getinstanceup(v, 1, (SQUserPointer *)&self, nullptr))) {
		return sq_throwerror(v, "GlyphData expected");
	}

	sq_pushinteger(v, (SQInteger)self->getBearingY());
	return 1;
}

static SQInteger wrap_getHeight(SQVM *v) {
	GlyphData *self = nullptr;
	if (SQ_FAILED(sq_getinstanceup(v, 1, (SQUserPointer *)&self, nullptr))) {
		return sq_throwerror(v, "GlyphData expected");
	}

	sq_pushinteger(v, (SQInteger)self->getHeight());
	return 1;
}

static SQInteger wrap_getWidth(SQVM *v) {
	GlyphData *self = nullptr;
	if (SQ_FAILED(sq_getinstanceup(v, 1, (SQUserPointer *)&self, nullptr))) {
		return sq_throwerror(v, "GlyphData expected");
	}

	sq_pushinteger(v, (SQInteger)self->getWidth());
	return 1;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	// constructor
	_DECL_FUNC(constructor, NULL, NULL, false),
	_DECL_FUNC(getMetrics, 1, "x", false),

	// metrics
	_DECL_FUNC(getAdvance, 1, "x", false),
	_DECL_FUNC(getBearingX, 1, "x", false),
	_DECL_FUNC(getBearingY, 1, "x", false),
	_DECL_FUNC(getHeight, 1, "x", false),
	_DECL_FUNC(getWidth, 1, "x", false),

	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_glyphdata(SQVM *v) {
	ClassRegistry reg;
	reg.name = "GlyphData";
	reg.functions = functions;

	return sqx_registerclass(v, GlyphData::type, reg);
}

}	// resource
}	// acorn
