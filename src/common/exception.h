#ifndef ACORN_EXCEPTION_H
#define ACORN_EXCEPTION_H

// C/C++
#include <cstdarg>

// STL
#include <stdexcept>
#include <string>
#include <sstream>
#include <iostream>

namespace acorn {

class Exception : public std::exception {
public:
	Exception(const char* format, ...) {
		va_list args;
		va_start(args, format);
		char *buffer = new char[2048];
		vsnprintf(buffer, sizeof(buffer), format, args);
		va_end(args);

		message = std::string(buffer);
		delete[] buffer;
	}

	virtual ~Exception() throw() {

	}

	inline virtual const char *what() const throw() {
		return message.c_str();
	}

private:
	std::string message;
};	// exception

}	// acorn

#endif	// ACORN_EXCEPTION_H
