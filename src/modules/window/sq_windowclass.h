#ifndef ACORN_SQ_WINDOW_CLASS_H
#define ACORN_SQ_WINDOW_CLASS_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace window {

extern "C" int sq_register_window(SQVM *v);

}	// window
}	// acorn

#endif	// ACORN_SQ_WINDOW_CLASS_H
