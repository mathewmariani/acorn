R"squirrel"#(

# timer
describe("acorn.timer", function() {
	# typeof
	it("is a typeof of table", function() {
		assert.is_typeof(acorn.timer, "table")
	})

	# functions
	it("contains a function `step()`", function() {
		assert.is_in("step", acorn.timer)
		assert.is_typeof(acorn.timer.step, "function")
	})
	it("contains a function `getDeltaTime()`", function() {
		assert.is_in("getDeltaTime", acorn.timer)
		assert.is_typeof(acorn.timer.getDeltaTime, "function")
		# assert.is_float(acorn.timer.getDeltaTime())
	})
	it("contains a function `getTime()`", function() {
		assert.is_in("getTime", acorn.timer)
		assert.is_typeof(acorn.timer.getTime, "function")
		# assert.is_float(acorn.timer.getTime())
	})
	it("contains a function `sleep()`", function() {
		assert.is_in("sleep", acorn.timer)
		assert.is_typeof(acorn.timer.sleep, "function")
		# assert.is_float(acorn.timer.sleep(0))
	})
})

#)squirrel"#"
