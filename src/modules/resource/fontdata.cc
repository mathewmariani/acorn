// C/C++
#include <iostream>
#include <memory>
#include <stdexcept>

// acorn
#include "resource/fontdata.h"

namespace acorn {
namespace resource {

acorn::Type FontData::type = std::type_index(typeid(resource::FontData));

FontData::FontData(FT_Library library, std::shared_ptr<acorn::Data> data, int ptsize) :
	data(data) {

	if (ptsize <= 0) {
		throw std::runtime_error("invalid point size");
	}

#if (FREETYPE_IMPL)
	// https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Text_Rendering_01
	FT_Error err = FT_Err_Ok;
	err = FT_New_Memory_Face(
		library,
		(const FT_Byte *)data->getData(),		/* first byte in memory */
		data->getSize(),						/* size in bytes        */
		0,										/* face_index           */
		&face);

	if (err != FT_Err_Ok) {
		// FIXME:
		throw std::runtime_error("could not load font (read more docs)");
	}

	err = FT_Set_Pixel_Sizes(face, ptsize, ptsize);

	if (err != FT_Err_Ok) {
    // FIXME:
		throw std::runtime_error("could set pixel size...? (read more docs)");
	}

	// https://www.freetype.org/freetype2/docs/reference/ft2-base_interface.html#FT_Size_Metrics
	FT_Size_Metrics sm = face->size->metrics;
	metrics.advance = (int)(sm.max_advance >> 6);
	metrics.ascent = (int)(sm.ascender >> 6);
	metrics.descent = (int)(sm.descender >> 6);
	metrics.height = (int)(sm.height >> 6);
#else
	// init font
	if (!stbtt_InitFont(&font, (unsigned char *)data->getData(), 0)) {
		throw std::runtime_error("could not load font");
	}

	// calculate font metrics
	//scale = stbtt_ScaleForPixelHeight(&font, ptsize);
	scale = stbtt_ScaleForMappingEmToPixels(&font, ptsize);

	int ascent, descent, lineGap, baseline, height;
	stbtt_GetFontVMetrics(&font, &ascent, &descent, &lineGap);
	baseline = (int) (ascent * scale);
	height = (ascent - descent + lineGap) * scale;

	// TODO: clean me up, let's not use temp variables
	metrics.advance = 0;	// this value can be calculated
	metrics.ascent = ascent;
	metrics.descent = descent;
	metrics.height = height;
#endif
}

FontData::~FontData() {
#if (FREETYPE_IMPL)
	FT_Done_Face(face);
#else
  // FIXME: release font back to stb
#endif
}

int FontData::getAdvance() const {
	return metrics.advance;
}

int FontData::getAscent() const {
	return metrics.ascent;
}

int FontData::getDescent() const {
	return metrics.descent;
}

int FontData::getGlyphCount() const {
#if (FREETYPE_IMPL)
	return (int) face->num_glyphs;
#else
	return (int) font.numGlyphs;
#endif
}

int FontData::getHeight() const {
	return metrics.height;
}

bool FontData::hasGlyph(int codepoint) const {
#if (FREETYPE_IMPL)
	return (FT_Get_Char_Index(face, codepoint) != 0);
#else
	return (stbtt_FindGlyphIndex(&font, codepoint) != 0);
#endif
}

GlyphData *FontData::getGlyphData(uint32_t codepoint) {
	acorn::resource::GlyphData::GlyphMetrics glyphMetrics = {};

#if (FREETYPE_IMPL)
	// https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Text_Rendering_01#Fonts_and_Glyphs

	FT_Error err = FT_Err_Ok;

	// NOTE: ...
	err = FT_Load_Glyph(face, codepoint, FT_LOAD_DEFAULT);
	if (err != FT_Err_Ok) {
		throw std::runtime_error("Could not load glyph");
	}

  // https://www.freetype.org/freetype2/docs/reference/ft2-glyph_management.html#FT_Glyph
  // NOTE: use FT_Done_Glyph to release glyph

	FT_Glyph glyph;

	// NOTE: ...
	err = FT_Get_Glyph(face->glyph, &glyph);
	if (err != FT_Err_Ok) {
		throw std::runtime_error("Could not load glyph character 'X'");
	}

	// NOTE: ...
	err = FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, 0, 1);
	if (err != FT_Err_Ok) {
		throw std::runtime_error("Could not load glyph character 'X'");
	}

	FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph) glyph;

	// NOTE: ...
	glyphMetrics.advance = (int)(face->glyph->advance.x >> 6);
	glyphMetrics.bearingX = bitmap_glyph->left;
	glyphMetrics.bearingY = bitmap_glyph->top;
	glyphMetrics.width = bitmap_glyph->bitmap.width;
	glyphMetrics.height = bitmap_glyph->bitmap.rows;

	// NOTE: cleanup
	FT_Done_Glyph(glyph);
#else
	int advance, width, height, lsb, x0, y0, x1, y1;
	stbtt_GetCodepointHMetrics(&font, codepoint, &advance, &lsb);
	stbtt_GetCodepointBitmapBox(&font, codepoint, scale, scale, &x0, &y0 ,&x1, &y1);
	width = x1-x0;
	height = y1-y0;

	glyphMetrics.advance = (int)(advance >> 6);
	glyphMetrics.bearingX = x0;
	glyphMetrics.bearingY = -y0;
	glyphMetrics.width = width;
	glyphMetrics.height = height;
#endif

	printf("Get Glyph Data\n");
	printf("advance: %d\n", glyphMetrics.advance);
	printf("bearingX: %d\n", glyphMetrics.bearingX);
	printf("bearingY: %d\n", glyphMetrics.bearingY);
	printf("width: %d\n", glyphMetrics.width);
	printf("height: %d\n", glyphMetrics.height);

	// FIXME:
	std::unique_ptr<GlyphData> glyphData(new GlyphData(codepoint, glyphMetrics));
	return glyphData.get();
}

}	// resource
}	// acorn
