// acorn
#include "graphics/graphicsdevice.h"

namespace acorn {
namespace graphics {

acorn::Type GraphicsDevice::type = std::type_index(typeid(graphics::GraphicsDevice));

GraphicsDevice::GraphicsDevice() {

}

GraphicsDevice::~GraphicsDevice() {

}

}	// graphics
}	// acorn
