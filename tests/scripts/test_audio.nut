R"squirrel"#(

# audio
describe("acorn.audio", function() {
	# typeof
	it("is a typeof of table", function() {
		assert.is_typeof(acorn.audio, "table")
	})

	# source
	describe("acorn.audio.Source", function() {
		it("is an index of acorn.audio", function() {
			assert.is_in("Source", acorn.audio)
		})
	})
})

#)squirrel"#"
