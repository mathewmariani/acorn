// C/C++
#include <memory>

// acorn
#include "graphics/opengl/shader.h"
#include "graphics/graphics.h"
#include "graphics/sq_shader.h"

namespace acorn {
namespace graphics {

#define instance() (Module::getInstance<graphics::Graphics>())

static SQInteger wrap_constructor(SQVM *v) {
	const char *vertex, *fragment;
	sq_getstring(v, 2, &vertex);
	sq_getstring(v, 3, &fragment);

	try {
		auto shader = instance()->newShader(vertex, fragment);
		sqx_pushinstance(v, Shader::type, shader);
		return 1;
	} catch (const std::exception &e) {
		// FIXME: this can throw some errors, we need 'better' a way to catch them for squirrel
		return sq_throwerror(v, "some sort of error was thrown, and we handeld it baldy.");
	}
}

static SQInteger wrap_getWarnings(SQVM *v) {
	sqx_printstack(v);
	std::weak_ptr<opengl::Shader> shader{ sqx_checkinstance<opengl::Shader>(v, 1) };
	if (auto spt = shader.lock()) {
		STUBBED("not implemented yet.");
	}
	return 0;
}

static SQInteger wrap_hasUniform(SQVM *v) {
	sqx_printstack(v);
	std::weak_ptr<opengl::Shader> shader{ sqx_checkinstance<opengl::Shader>(v, 1) };
	if (auto spt = shader.lock()) {
		STUBBED("not implemented yet.");
	}
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 3, "xss", false),

	// taken from love2d API
	_DECL_FUNC(getWarnings, 1, "x", false),
	_DECL_FUNC(hasUniform, 2, "xs", false),

	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_shader(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Shader";
	reg.functions = functions;

	return sqx_registerclass(v, Shader::type, reg);
}

}	// graphics
}	// acorn
