// acorn
#include "window/window.h"
#include "window/sq_display.h"

namespace acorn {
namespace window {

#define instance() (Module::getInstance<window::Window>())

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Display cannot be constructed directly.");
}

static SQInteger wrap_count(SQVM *v) {
	sq_pushinteger(v, instance()->getDisplayCount());
	return 1;
}

static SQInteger wrap_countModes(SQVM *v) {
	//int index = instance()->getDisplayIndex();
	sq_pushinteger(v, instance()->countDisplayModes(0));
	return 1;
}

static SQInteger wrap_getName(SQVM *v) {
	//int index = instance()->getDisplayIndex();
	sq_pushstring(v, instance()->getDisplayName(0).c_str(), -1);
	return 1;
}

static SQInteger wrap_listModes(SQVM *v) {
    int index = instance()->getDisplayIndex();
	std::vector<Window::DisplayMode> list;
	instance()->enumerateDisplayModes(0, list);

	// FIXME: should be an array of tables
	sq_newtableex(v, list.size());
	for (int i = 0; i < (int) list.size(); ++i) {
		sq_pushinteger(v, i);
		sq_newtableex(v, 3);	// width, height, refresh_rate

		sq_pushstring(v, "width", -1);
		sq_pushinteger(v, list[i].width);
		sq_newslot(v, -3, SQFalse);

		sq_pushstring(v, "height", -1);
		sq_pushinteger(v, list[i].height);
		sq_newslot(v, -3, SQFalse);

		sq_pushstring(v, "refresh_rate", -1);
		sq_pushinteger(v, list[i].refresh_rate);
		sq_newslot(v, -3, SQFalse);

		sq_newslot(v, -3, SQFalse);
	}

	return 1;
}

static SQInteger wrap_setScreenSaverEnabled(SQVM *v) {
	SQBool enabled;
	sq_getbool(v, 2, &enabled);
	instance()->setScreenSaverEnabled(enabled);
	return 1;
}

static SQInteger wrap_isScreenSaverEnabled(SQVM *v) {
	sq_pushbool(v, instance()->isScreenSaverEnabled());
	return 1;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),

	// static
	_DECL_FUNC(count, 1, "y", true),
	_DECL_FUNC(countModes, 1, "y", true),
	_DECL_FUNC(getName, 1, "y", true),
	_DECL_FUNC(listModes, 1, "y", true),

	_DECL_FUNC(setScreenSaverEnabled, 2, "yb", true),
	_DECL_FUNC(isScreenSaverEnabled, 1, "y", true),

	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_display(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Display";
	reg.functions = functions;
	return sqx_registerclass(v, Window::type, reg);
}

}	// window
}	// acorn
