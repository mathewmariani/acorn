#ifndef ACORN_GRAPHICS_FRAMEBUFFER_H
#define ACORN_GRAPHICS_FRAMEBUFFER_H

// acorn
#include "common/object.h"
#include "graphics/resource.h"
#include "graphics/texture.h"

// gl3w
#include "libraries/gl3w/gl3w.h"

namespace acorn {
namespace graphics {

class Framebuffer : public graphics::Texture {
public:
	static acorn::Type type;

public:
	Framebuffer(TextureType type, int width, int height);
	virtual ~Framebuffer();

public:
	void bind();
	void unbind();

	void draw(std::shared_ptr<Graphics> gfx, const glm::mat4 &m);

public:
	// managed opengl resource
	const void *getHandle() const override;

private:
	GLuint fbo;
	GLuint texture;
	GLuint rbo;
};	// framebuffer

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_FRAMEBUFFER_H
