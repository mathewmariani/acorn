#ifndef ACORN_GRAPHICS_METAL_FRAMEBUFFER_H
#define ACORN_GRAPHICS_METAL_FRAMEBUFFER_H

// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// acorn
#include "graphics/metal/metal.h"
#include "graphics/framebuffer.h"

namespace acorn {
namespace graphics {
namespace metal {

class Framebuffer : public acorn::graphics::Framebuffer {
public:
	Framebuffer(Texture::TextureType type, int width, int height);
	virtual ~Framebuffer();

public:
	// managed metal resource
	const void *getHandle() const override;
};	// framebuffer

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
#endif	// ACORN_GRAPHICS_METAL_FRAMEBUFFER_H
