#ifndef ACORN_SQ_MOUSE_H
#define ACORN_SQ_MOUSE_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace mouse {

extern "C" int sq_acorn_mouse(SQVM *v);

}	// mouse
}	// acorn

#endif	// ACORN_SQ_MOUSE_H
