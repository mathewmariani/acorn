#ifndef ACORN_GRAPHICS_TEXTURE_H
#define ACORN_GRAPHICS_TEXTURE_H

// C/C++
#include <memory>

// acorn
#include "common/object.h"
#include "graphics/quad.h"
#include "graphics/resource.h"

namespace acorn {
namespace graphics {

// forward declaration
class Graphics;

// NOTE: everything that represents an opengl texture...
// Filter, Wrap, Mipmaps, Width, Height, Quad

class Texture : public acorn::Object, public Resource {
public:
	static acorn::Type type;

	enum class TextureType {
		Texture2D
	};	// texturetype

	enum class FilterMode {
		Nearest,
		Linear
	}; // filtermode

	enum class WrapMode {
		Repeat,
		Mirror,
		Clamp,
		Border
	}; // wrapmode

public:
	Texture(TextureType type);
	virtual ~Texture();

protected:
	void initializeQuad() noexcept;

public:
	int getWidth() const noexcept;
	int getHeight() const noexcept;

	virtual const FilterMode getFilter() const noexcept;
	virtual void setFilter(const FilterMode mode) noexcept;

	virtual const WrapMode getWrap() const noexcept;
	virtual void setWrap(const WrapMode mode) noexcept;

	std::shared_ptr<Quad> getQuad() const noexcept;

	void draw(std::shared_ptr<Graphics> gfx, const glm::mat4 &m);
	void draw(std::shared_ptr<Graphics> gfx, std::shared_ptr<Quad> quad, const glm::mat4 &m);

protected:
	int width;
	int height;

	TextureType textureType;
	FilterMode filter;
	WrapMode wrap;

	// strong reference
	std::shared_ptr<Quad> quad;
};	// texture

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_TEXTURE_H
