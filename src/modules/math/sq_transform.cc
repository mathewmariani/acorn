// acorn
#include "math/transform.h"
#include "math/sq_transform.h"

namespace acorn {
namespace math {

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Transform could not be constructed directly");
}

static SQInteger wrap_translate(SQVM *v) {
	std::weak_ptr<Transform> transform{
		sqx_checkinstance<Transform>(v, 1)
	};
	if (auto spt = transform.lock()) {
		float x, y;
		sq_getfloat(v, 2, (SQFloat *) &x);
		sq_getfloat(v, 3, (SQFloat *) &y);
		spt->translate(x, y);
	}
	return 0;
}

static SQInteger wrap_rotate(SQVM *v) {
	std::weak_ptr<Transform> transform{
		sqx_checkinstance<Transform>(v, 1)
	};
	if (auto spt = transform.lock()) {
		float a;
		sq_getfloat(v, 2, (SQFloat *) &a);
		spt->rotate(a);
	}
	return 0;
}

static SQInteger wrap_scale(SQVM *v) {
	std::weak_ptr<Transform> transform{
		sqx_checkinstance<Transform>(v, 1)
	};
	if (auto spt = transform.lock()) {
		float sx, sy;
		sq_getfloat(v, 2, (SQFloat *) &sx);
		sq_getfloat(v, 3, (SQFloat *) &sy);
		spt->scale(sx, sy);
	}
	return 0;
}

static SQInteger wrap_setIndetity(SQVM *v) {
	std::weak_ptr<Transform> transform{
		sqx_checkinstance<Transform>(v, 1)
	};
	if (auto spt = transform.lock()) {
		spt->setIndetity();
	}
	return 0;
}

static SQInteger wrap_setTransformation(SQVM *v) {
	std::weak_ptr<Transform> transform{
		sqx_checkinstance<Transform>(v, 1)
	};
	if (auto spt = transform.lock()) {
		float x, y, a, sx, sy, ox, oy;
		sq_getfloat(v, 2, (SQFloat *) &x);
		sq_getfloat(v, 3, (SQFloat *) &y);
		sq_getfloat(v, 4, (SQFloat *) &a);
		sq_getfloat(v, 5, (SQFloat *) &sx);
		sq_getfloat(v, 6, (SQFloat *) &sy);
		sq_getfloat(v, 7, (SQFloat *) &ox);
		sq_getfloat(v, 8, (SQFloat *) &oy);
		spt->setTransformation(x, y, a, sx, sy, ox, oy);
	}
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),

	_DECL_FUNC(translate, 3, "xnn", false),
	_DECL_FUNC(rotate, 2, "xn", false),
	_DECL_FUNC(scale, 3, "xnn", false),

	_DECL_FUNC(setIndetity, 1, "x", false),
	_DECL_FUNC(setTransformation, 7, "xnnnnnn", false),

{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_acorn_transform(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Transform";
	reg.functions = functions;

	return sqx_registerclass(v, Transform::type, reg);
}

}	// math
}	// acorn
