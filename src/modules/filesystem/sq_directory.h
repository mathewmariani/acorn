#ifndef ACORN_SQ_FILESYSTEM_DIRECTORY_H
#define ACORN_SQ_FILESYSTEM_DIRECTORY_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace filesystem {

extern "C" int sq_register_directory(SQVM *v);

}	// filesystem
}	// acorn

#endif	// ACORN_SQ_FILESYSTEM_DIRECTORY_H
