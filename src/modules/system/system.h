#ifndef ACORN_SYSTEM_H
#define ACORN_SYSTEM_H

// C/C++
#include <string>
#include <unordered_map>
#include <type_traits>

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"

// SDL
#include <SDL.h>

namespace acorn {
namespace system {

class System : public acorn::Module {
public:
	static acorn::Type type;

public:
	enum class PowerState {
		UKNOWN = 0,
		ON_BATTERY = 1,
		NO_BATTERY = 2,
		CHARGING = 3,
		CHARGED = 4,

		// always last.
		POWERSTATE_MAX_ENUM
	};	// powerstate

	struct Notification {
		std::string title;
		std::string subtitle;
		std::string message;
	};	// notification

public:
	System();
	virtual ~System();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:
	std::string getOS() const noexcept;
	bool isWindows() const noexcept;
	bool isMacOS() const noexcept;
	bool isLinux() const noexcept;

	int getCores() const noexcept;
	int getMemory() const noexcept;

	PowerState getPowerInfo(int *secs, int *pct) const noexcept;

	void postNotification(Notification &notification);
  void requestUserAttention() const noexcept;
	void beep() const noexcept;

	void showApplicationBadge(bool show) const noexcept;
	void setBadgeCount(int count) const noexcept;

public:
	// clipboard

	// use this function to put UTF-8 text into the clipboard.
	bool setClipboardText(const std::string &text) const noexcept;

	// use this function to get UTF-8 text from the clipboard, which must be freed with SDL_free().
	std::string getClipboardText() const noexcept;

	// use this function to get a flag indicating whether the clipboard exists and contains a text string that is non-empty.
	bool hasClipboardText() const noexcept;

private:
	// FIXME: I don't know if this is needed
	//static std::unordered_map<std::string, PowerState> powerstateTable;
};	// system

}	// system
}	// acorn

#endif	// ACORN_SYSTEM_H
