// acorn
#include "keyboard/sq_keyboard.h"
#include "keyboard/sq_modifier.h"
#include "keyboard/sq_scancodes.h"
#include "keyboard/keyboard.h"

namespace acorn {
namespace keyboard {

#define instance() (Module::getInstance<keyboard::Keyboard>())

static SQInteger wrap_isPressed(SQVM *v) {
	bool index;
	sq_getinteger(v, 2, (SQInteger *) &index);
	sq_pushbool(v, instance()->isPressed((Keyboard::Scancode) index));
	return 1;
}

static SQInteger wrap_isModifierPressed(SQVM *v) {
	int index;
	sq_getinteger(v, 2, (SQInteger *) &index);
	sq_pushbool(v, instance()->isModifierPressed((Keyboard::Modifier) index));
	return 1;
}

static SQInteger wrap_hasScreenKeyboard(SQVM *v) {
	sq_pushbool(v, instance()->hasScreenKeyboard());
	return 1;
}

static SQInteger wrap_isScreenKeyboardShown(SQVM *v) {
	sq_pushbool(v, instance()->isScreenKeyboardShown());
	return 1;
}

static SQInteger wrap_isTextInputActive(SQVM *v) {
	sq_pushbool(v, instance()->isTextInputActive());
	return 1;
}

static SQInteger wrap_setTextInput(SQVM *v) {
	bool toggle;
	sq_getbool(v, 2, (SQBool *) &toggle);

	if (toggle) {
		instance()->startTextInput();
	} else {
		instance()->stopTextInput();
	}

	return 1;
}

// NOTE: functions for acorn.keyboard
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	_DECL_FUNC(isPressed, 2, "tn"),
	_DECL_FUNC(isModifierPressed, 2, "tn"),
	_DECL_FUNC(isScreenKeyboardShown, 1, "t"),
	_DECL_FUNC(isTextInputActive, 1, "t"),
	_DECL_FUNC(setTextInput, 2, "tb"),
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.keyboard
const static SQFunction types[] = {
	nullptr
};

const static SQFunction enums[] = {
	sq_register_modifier,
	sq_register_scancodes,
	nullptr
};

extern "C" int sq_acorn_keyboard(SQVM *v) {
	auto instance = std::make_shared<Keyboard>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize keyboard module");
	}

	SQRegModule reg;
	reg.module = std::move(instance);
	reg.name = "keyboard";
	reg.functions = functions;
	reg.enums = enums;

	return acorn::sqx_registermodule(v, reg);
}

}	// keyboard
}	// acorn
