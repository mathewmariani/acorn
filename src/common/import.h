#ifndef ACORN_IMPORT_H
#define ACORN_IMPORT_H

// forward declare SQVM
struct SQVM;

#if defined(WIN32) || defined(_WIN32)
	#define SQX_API __declspec(dllexport)
#else
	#define SQX_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

SQX_API int sqx_register_import(SQVM *v);

#ifdef __cplusplus
}
#endif

#endif // ACORN_IMPORT_H
