---
layout: docs
title: Index
description:
---

##### Introduction

---

Acorn is an open source, multi-platform framework for developing 2D games in the Squirrel programming language.

- [Getting Started]({{ site.baseurl }}/docs/{{ site.docs_version }}/getting-started/)
- [Third Party]({{ site.baseurl }}/docs/{{ site.docs_version }}/third-parties/)
- [Alternatives]({{ site.baseurl }}/docs/{{ site.docs_version }}/alternatives/)
- [acorn]({{ site.baseurl }}/docs/{{ site.docs_version }}/acorn/) (module)

##### Squirrel

---

Squirrel is a high level imperative, object-oriented programming language, designed to be a light-weight scripting language that fits in the size, memory bandwidth, and real-time requirements of applications like video games.

- [Squirrel 3.1 Reference Manual (online)](http://squirrel-lang.org/squirreldoc/)
- [Squirrel 3.1 Reference Manual (offline)](http://squirrel-lang.org/squirreldoc/squirrel3.pdf)
- [Squirrel 3.1 Standard Libraries Manual (offline)](http://squirrel-lang.org/squirreldoc/sqstdlib3.pdf)

##### Tools

---

Games can easily be made using [Atom](https://atom.io/), [Sublime Text](https://www.sublimetext.com/) or any other preferred text editor. But, be sure to pickup a [syntax highlighter](https://atom.io/packages/squirrel-language) for Squirrel as it is not commonly supported.
