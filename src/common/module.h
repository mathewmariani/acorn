#ifndef ACORN_MODULE_H
#define ACORN_MODULE_H

// STL
#include <map>
#include <memory>
#include <typeinfo>
#include <typeindex>

// acorn
#include "common/object.h"

namespace acorn {

// NOTE: modules don't exist in squirrel, they are native C/C++ objects
// that are used by squirrel objects; like a kernel module.
class Module : public acorn::Object {
public:
	static acorn::Type type;

public:
	Module();
	virtual ~Module();

	virtual size_t getHash() const = 0;
	virtual const char *getName() const = 0;

public:
	static void registerInstance(std::shared_ptr<Module> instance);

	template <typename T>
	static std::shared_ptr<T> getInstance() {
		// FIXME: 
		//static_assert(std::is_convertible<T, acorn::Object>::value, "Trying to use getInstance with non acorn::Module type.");

		auto &registry = getRegistry();
		auto itr = registry.find(T::type.hash_code());
		if (itr != registry.end()) {
			if (auto spt = (*itr).second.lock()) {
				return std::static_pointer_cast<T>(spt);
			}
		}

		return nullptr;
	}

private:
	// cache weak references to modules for easier use within C/C++.
	// we use a map since we often iterate over this cache.
	static std::map<size_t, std::weak_ptr<Module>>& getRegistry();
};	// module

}	// acorn

#endif	// ACORN_MODULE_H
