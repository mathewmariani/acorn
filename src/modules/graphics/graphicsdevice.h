#ifndef ACORN_GRAPHICS_DEVICE_H
#define ACORN_GRAPHICS_DEVICE_H

// acorn
#include "common/object.h"

namespace acorn {
namespace graphics {

class GraphicsDevice : public acorn::Object {
public:
	static acorn::Type type;

public:
	GraphicsDevice();
	virtual ~GraphicsDevice();

private:

};	// graphicsdevice

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_DEVICE_H
