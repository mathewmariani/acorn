#ifndef ACORN_RESOURCE_IMAGEDATA_H
#define ACORN_RESOURCE_IMAGEDATA_H

// C/C++
#include <memory>

// acorn
#include "common/data.h"

namespace acorn {
namespace resource {

class ImageData : public acorn::Data {
public:
	static acorn::Type type;

public:
	ImageData(void *data, size_t size);
	virtual ~ImageData();

public:
	int getWidth() const;
	int getHeight() const;
	int getChannels() const;

public:
	void *getData() const override;
	size_t getSize() const override;

private:
	// FIXME: pimpl
	unsigned char *pixels = nullptr;

	int width;
	int height;
	int channels;
};	// imagedata

}	// resource
}	// acorn

#endif	// ACORN_RESOURCE_IMAGEDATA_H
