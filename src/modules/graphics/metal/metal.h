#ifndef ACORN_GRAPHICS_METAL_METAL_H
#define ACORN_GRAPHICS_METAL_METAL_H

// acorn
#include "common/platform.h"

#if defined(OS_MACOS)

// acorn
#include "graphics/buffer.h"
#include "graphics/texture.h"

// metal
#import <Metal/Metal.h>

namespace acorn {
namespace graphics {
namespace metal {

// https://developer.apple.com/documentation/metalkit?language=objc

MTLTextureType map(const graphics::Texture::TextureType textureType);
MTLSamplerMinMagFilter map(const graphics::Texture::FilterMode filterMode);
MTLSamplerAddressMode map(const graphics::Texture::WrapMode wrapMode);

}	// metal
}	// graphics
}	// acorn

#endif  // OS_MACOS

#endif // ACORN_GRAPHICS_METAL_METAL_H
