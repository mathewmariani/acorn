#ifndef ACORN_RESOURCE_GLYPHDATA_H
#define ACORN_RESOURCE_GLYPHDATA_H

// acorn
#include "common/object.h"

namespace acorn {
namespace resource {

class GlyphData : public acorn::Object {
public:
	static acorn::Type type;

public:
	struct GlyphMetrics {
		int height;
		int width;
		int advance;
		int bearingX;
		int bearingY;
	};	// glyphmetrics

public:
	GlyphData(int codepoint, GlyphMetrics metrics);
	virtual ~GlyphData();

public:
	int getCodepoint() const;
	int getAdvance() const;
	int getBearingX() const;
	int getBearingY() const;
	int getHeight() const;
	int getWidth() const;

private:
	int codepoint;
	GlyphMetrics metrics;
};	// glyphdata

}	// resource
}	// acorn

#endif	// ACORN_RESOURCE_GLYPHDATA_H
