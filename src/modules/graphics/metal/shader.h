#ifndef ACORN_GRAPHICS_METAL_SHADER_H
#define ACORN_GRAPHICS_METAL_SHADER_H

// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// acorn
#include "graphics/metal/metal.h"
#include "graphics/shader.h"

namespace acorn {
namespace graphics {
namespace metal {

class Shader final : public graphics::Shader {
public:
	Shader(
		std::shared_ptr<acorn::graphics::ShaderStage> vertex,
		std::shared_ptr<acorn::graphics::ShaderStage> fragment
	);
	virtual ~Shader();

public:
	void bind() override;
	void unbind() override;

	const int getAttributeLocation(const std::string &name) override;
	const int getGetUniformLocation(const std::string &name) override;

	void sendFloats(int location, int count, const float *value) override;
	void sendInts(int location, int count, const int *value) override;
	void sendMatrices(int location, int count, const float *value) override;

private:
	bool createShader();

public:
	const void *getHandle() const override;

private:
	// managed metal resource
};	// shader

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
#endif	// ACORN_GRAPHICS_METAL_SHADER_H
