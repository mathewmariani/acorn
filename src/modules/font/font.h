#ifndef ACORN_FONT_H
#define ACORN_FONT_H

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"

// freetype2
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

namespace acorn {
namespace font {

class Font : public acorn::Module {
public:
	static acorn::Type type;

public:
	Font();
	virtual ~Font();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:
	const void *getHandle() const noexcept;

private:
	FT_Library library;
};	// font

}	// font
}	// acorn

#endif	// ACORN_FONT_H
