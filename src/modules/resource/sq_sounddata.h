#ifndef ACORN_SQ_RESOURCE_SOUNDDATA_H
#define ACORN_SQ_RESOURCE_SOUNDDATA_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace resource {

extern "C" int sq_register_sounddata(SQVM *v);

}	// resource
}	// acorn

#endif	// ACORN_SQ_RESOURCE_SOUNDDATA_H
