#ifndef ACORN_DATA_H
#define ACORN_DATA_H

// acorn
#include "common/object.h"

namespace acorn {

class Data : public acorn::Object {
public:
	static acorn::Type type;

public:
	virtual ~Data();

public:
	virtual void *getData() const = 0;
	virtual size_t getSize() const = 0;
};	// data

}	// acorn

#endif	// ACORN_DATA_H
