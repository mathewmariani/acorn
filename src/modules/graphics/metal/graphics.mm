// acorn
#include "common/platform.h"

#if defined(OS_MACOS)

#include "graphics/metal/graphics.h"

namespace acorn {
namespace graphics {
namespace metal {

Graphics::Graphics() {

}

Graphics::~Graphics() {

}

const char *Graphics::getName() const {
	return type.name();
}

std::shared_ptr<Image> Graphics::newImage() noexcept {
	return nullptr;
}

std::shared_ptr<Shader> Graphics::newShader() noexcept {
	return nullptr;
}

}	// metal
}	// graphics
}	// acorn

#endif  // OS_MACOS
