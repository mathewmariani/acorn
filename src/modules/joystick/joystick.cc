// C/C++
#include <iostream>

// acorn
#include "common/log.h"
#include "common/platform.h"
#include "joystick/joystick.h"

namespace acorn {
namespace joystick {

acorn::Type Joystick::type = std::type_index(typeid(joystick::Joystick));

float Joystick::clampval(float x) {
	if (fabsf(x) < 0.01)
		return 0.0f;

	if (x < -0.99f) return -1.0f;
	if (x > 0.99f) return 1.0f;

	return x;
}

Joystick::Joystick(int deviceid) :
	joyhandle(nullptr),
	controller(nullptr),
	haptic(nullptr),
	deviceid(deviceid),
	instanceid(-1) {

}

Joystick::~Joystick() {
	if (haptic) {
		SDL_HapticClose(haptic);
		haptic = nullptr;
	}

	if (controller) {
		SDL_GameControllerClose(controller);
		controller = nullptr;
	}

	if (joyhandle) {
		SDL_JoystickClose(joyhandle);
		joyhandle = nullptr;
	}

	instanceid = -1;
}

bool Joystick::open(int index) {
	joyhandle = SDL_JoystickOpen(index);
	if (joyhandle) {
		printf("Opened Joystick %d\n", index);

		// we only need to do this once
		instanceid = SDL_JoystickInstanceID(joyhandle);

		// use this function to open a gamecontroller for use.
		const auto openGamepad = [&](int index) -> bool {
			if (controller != nullptr) {
				SDL_GameControllerClose(controller);
				controller = nullptr;
			}
			controller = SDL_GameControllerOpen(index);
			return true;
		};

		// use this function to check if the given joystick is supported by the game controller interface.
		if (SDL_IsGameController(index)) {
			openGamepad(index);
		}

	} else {
		printf("Couldn't open Joystick %d\n", index);
	}

	return isAttached();
}

void Joystick::close() {
	if (haptic) {
		SDL_HapticClose(haptic);
		haptic = nullptr;
	}

	if (controller) {
		SDL_GameControllerClose(controller);
		controller = nullptr;
	}

	if (joyhandle) {
		SDL_JoystickClose(joyhandle);
		joyhandle = nullptr;
	}

	instanceid = -1;
}

const std::string Joystick::getName() noexcept {
	if (name.empty()) {
		const char *joyname = SDL_JoystickName(joyhandle);
		if (joyname == nullptr && controller) {
			joyname = SDL_GameControllerName(controller);
		}

		if (joyname) {
			name = joyname;
		}
	}

	return name;
}

const std::string Joystick::getGUID() noexcept {
	if (guid.empty()) {
		char cstr[33];
		SDL_JoystickGUID sdlguid = SDL_JoystickGetGUID(joyhandle);
		SDL_JoystickGetGUIDString(sdlguid, cstr, (int) sizeof(cstr));
		guid = std::string(cstr);
	}

	return guid;
}

bool Joystick::isAttached() const noexcept {
	return (joyhandle != nullptr && SDL_JoystickGetAttached(joyhandle));
}

int Joystick::getAxisCount() const noexcept {
	return (isAttached() ? SDL_JoystickNumAxes(joyhandle) : 0);
}

int Joystick::getBallCount() const noexcept {
	return (isAttached() ? SDL_JoystickNumBalls(joyhandle) : 0);
}

int Joystick::getHatCount() const noexcept {
	return (isAttached() ? SDL_JoystickNumHats(joyhandle) : 0);
}

int Joystick::getButtonCount() const noexcept {
	return (isAttached() ? SDL_JoystickNumButtons(joyhandle) : 0);
}

int Joystick::getInstanceID() const noexcept {
	return instanceid;
}

Joystick::PowerState Joystick::getPowerState() const noexcept {
	return (PowerState) (isAttached() ? SDL_JoystickCurrentPowerLevel(joyhandle) : -1);
}

bool Joystick::isGameController() const noexcept {
	return (controller != nullptr);
}

Joystick::JoystickHat Joystick::getHat(int index) const noexcept {
	return (JoystickHat) SDL_JoystickGetHat(joyhandle, index);
}

bool Joystick::getButton(int index) const noexcept {
	if (SDL_JoystickGetButton(joyhandle, index) == 1) {
		return true;
	}

	return false;
}


const void *Joystick::getHandle() const noexcept {
	return joyhandle;
}

// FIXME: I don't know if this is needed
//std::unordered_map<std::string, Joystick::PowerState> Joystick::powerstateTable = {
//	{ "unknown", Joystick::PowerState::UNKNOWN },
//	{ "empty", Joystick::PowerState::EMPTY },
//	{ "low", Joystick::PowerState::LOW },
//	{ "medium", Joystick::PowerState::MEDIUM },
//	{ "full", Joystick::PowerState::FULL },
//	{ "wired", Joystick::PowerState::WIRED },
//};

// FIXME: I don't know if this is needed
//std::unordered_map<std::string, Joystick::JoystickHat> Joystick::joystickhatTable = {
//	{ "centered", Joystick::JoystickHat::centered },
//	{ "up", Joystick::JoystickHat::up },
//	{ "right", Joystick::JoystickHat::right },
//	{ "down", Joystick::JoystickHat::down },
//	{ "left", Joystick::JoystickHat::left },
//	{ "right_up", Joystick::JoystickHat::right_up },
//	{ "right_down", Joystick::JoystickHat::right_down },
//	{ "left_up", Joystick::JoystickHat::left_up },
//	{ "left_down", Joystick::JoystickHat::left_down },
//};

// FIXME: I don't know if this is needed
//std::unordered_map<std::string, Joystick::ControllerAxis> Joystick::controlleraxisTable = {
//	{ "invalid", Joystick::ControllerAxis::INVALID },
//	{ "left_x", Joystick::ControllerAxis::LEFTX },
//	{ "left_y", Joystick::ControllerAxis::LEFTY },
//	{ "right_x", Joystick::ControllerAxis::RIGHTX },
//	{ "right_y", Joystick::ControllerAxis::RIGHTY },
//	{ "trigger_left", Joystick::ControllerAxis::TRIGGERLEFT },
//	{ "trigger_right", Joystick::ControllerAxis::TRIGGERRIGHT },
//};

// FIXME: I don't know if this is needed
//std::unordered_map<std::string, Joystick::InputType> Joystick::inputtypeTable = {
//	{ "none", Joystick::InputType::NONE },
//	{ "button", Joystick::InputType::BUTTON },
//	{ "axis", Joystick::InputType::AXIS },
//	{ "hat", Joystick::InputType::HAT },
//};

// FIXME: I don't know if this is needed
//std::unordered_map<std::string, Joystick::ControllerButton> Joystick::controllerbuttonTable = {
//	{ "invalid", Joystick::ControllerButton::INVALID },
//	{ "a", Joystick::ControllerButton::A },
//	{ "b", Joystick::ControllerButton::B },
//	{ "x", Joystick::ControllerButton::X },
//	{ "y", Joystick::ControllerButton::Y },
//	{ "back", Joystick::ControllerButton::BACK },
//	{ "guide", Joystick::ControllerButton::GUIDE },
//	{ "start", Joystick::ControllerButton::START },
//	{ "leftstick", Joystick::ControllerButton::LEFTSTICK },
//	{ "rightstick", Joystick::ControllerButton::RIGHTSTICK },
//	{ "leftshoulder", Joystick::ControllerButton::LEFTSHOULDER },
//	{ "rightshoulder", Joystick::ControllerButton::RIGHTSHOULDER },
//	{ "dpad_up", Joystick::ControllerButton::DPAD_UP },
//	{ "dpad_down", Joystick::ControllerButton::DPAD_DOWN },
//	{ "dpad_left", Joystick::ControllerButton::DPAD_LEFT },
//	{ "dpad_right", Joystick::ControllerButton::DPAD_RIGHT },
//};

}	// joystick
}	// acorn
