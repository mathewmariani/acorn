// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// acorn
#include "common/exception.h"
#include "graphics/metal/shaderstage.h"

namespace acorn {
namespace graphics {
namespace metal {

ShaderStage::ShaderStage(ShaderStage::StageType glstage, const std::string &glsl) :
	acorn::graphics::ShaderStage(glstage, glsl) {
	compileShader();
}

ShaderStage::~ShaderStage() {

}

bool ShaderStage::compileShader() {
	return true;
}

const void *ShaderStage::getHandle() const {
	return nullptr;
}

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
