#ifndef ACORN_SQ_AUDIO_H
#define ACORN_SQ_AUDIO_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace audio {

extern "C" int sq_acorn_audio(SQVM *v);

}	// audio
}	// acorn

#endif	// ACORN_SQ_AUDIO_H
