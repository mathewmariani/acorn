// acorn
#include "common/exception.h"
#include "common/module.h"

namespace acorn {

acorn::Type Module::type = std::type_index(typeid(acorn::Module));

std::map<size_t, std::weak_ptr<Module>> &Module::getRegistry() {
	static auto* ret = new std::map<size_t, std::weak_ptr<Module>>();
	return *ret;
}

Module::Module() {

}

Module::~Module() {
	auto &registry = getRegistry();
	for (auto it = registry.begin(); it != registry.end(); ++it) {
		if (it->second.expired()) {
			registry.erase(it);
			break;
		}
	}

	if (registry.empty()) {
		delete &registry;
	}
}

void Module::registerInstance(std::shared_ptr<Module> instance) {
	auto &registry = getRegistry();
	auto itr = registry.find(instance->getHash());
	if (itr != registry.end()) {
		if (auto spt = itr->second.lock()) {
			if (spt.get() == instance.get()) {
				return;
			}
		}
		printf("Module (%s) already registered; overwritting.", instance->getName());
	}

	registry.insert({
		instance->getHash(), instance
	});
}

}	// acorn
