// acorn
#include "common/exception.h"
#include "graphics/image.h"

namespace acorn {
namespace graphics {

acorn::Type Image::type = std::type_index(typeid(graphics::Image));

Image::Image(TextureType type, std::shared_ptr<image::ImageData> imagedata) :
	acorn::graphics::Texture(type),
	imagedata(imagedata) {

	width = imagedata->getWidth();
	height = imagedata->getHeight();

	initializeQuad();
}

Image::~Image() {

}

}	// graphics
}	// acorn
