// acorn
#include "mouse/cursor.h"
#include "mouse/sq_cursor.h"

namespace acorn {
namespace mouse {

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Cursor could not be constructed directly");
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),
	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_acorn_cursor(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Cursor";
	reg.functions = functions;

	return sqx_registerclass(v, mouse::Cursor::type, reg);
}

}	// mouse
}	// acorn
