#ifndef ACORN_SQ_FILESYSTEM_FILE_H
#define ACORN_SQ_FILESYSTEM_FILE_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace filesystem {

extern "C" int sq_register_file(SQVM *v);

}	// filesystem
}	// acorn

#endif	// ACORN_SQ_FILESYSTEM_FILE_H
