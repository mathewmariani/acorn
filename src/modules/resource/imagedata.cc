// C/C++
#include <stdexcept>

// acorn
#include "resource/imagedata.h"

// stb
#include "libraries/stb/stb_image.h"

namespace acorn {
namespace resource {

acorn::Type ImageData::type = std::type_index(typeid(resource::ImageData));

ImageData::ImageData(void *data, size_t size) {
	if (!data) {
		throw std::runtime_error("could not create image");
	}

	pixels = stbi_load_from_memory(
		(stbi_uc*) data, size, &width, &height, &channels, STBI_rgb_alpha
	);

	if (!pixels) {
		throw std::runtime_error("could not load image from memory");
	}

	printf("image loaded successfully (%ix%i)\n", getWidth(), getHeight());
}

ImageData::~ImageData() {
	delete[] pixels;
}

int ImageData::getWidth() const {
	return width;
}

int ImageData::getHeight() const {
	return height;
}

int ImageData::getChannels() const {
	return channels;
}

void *ImageData::getData() const {
	return pixels;
}

size_t ImageData::getSize() const {
	return 0;
}

}	// resource
}	// acorn
