// C/C++
#include <memory>

// acorn
#include "graphics/framebuffer.h"
#include "graphics/graphics.h"
#include "graphics/sq_framebuffer.h"

namespace acorn {
namespace graphics {

#define instance() (Module::getInstance<graphics::Graphics>())

static SQInteger wrap_constructor(SQVM *v) {
	SQInteger width, height;
	sq_getinteger(v, 2, &width);
	sq_getinteger(v, 3, &height);
	std::shared_ptr<graphics::Framebuffer> framebuffer{
		instance()->newFramebuffer(width, height)
	};
	sqx_pushinstance(v, Framebuffer::type, framebuffer);
	return 1;
}

static SQInteger wrap_bind(SQVM *v) {
	std::weak_ptr<graphics::Framebuffer> framebuffer{
		sqx_checkinstance<graphics::Framebuffer>(v, 1)
	};
	if (auto spt = framebuffer.lock()) {
		spt->bind();
	}
	return 0;
}

static SQInteger wrap_unbind(SQVM *v) {
	std::weak_ptr<graphics::Framebuffer> framebuffer{
		sqx_checkinstance<graphics::Framebuffer>(v, 1)
	};
	if (auto spt = framebuffer.lock()) {
		spt->unbind();
	}
	return 0;
}

static SQInteger wrap_renderTo(SQVM *v) {
	std::weak_ptr<graphics::Framebuffer> framebuffer{
		sqx_checkinstance<graphics::Framebuffer>(v, 1)
	};

	//SQUnsignedInteger nparams, nfreevars;
	//sq_getclosureinfo(v, 2, &nparams, &nfreevars);
	//printf("nparams: %d\n", nparams);
	//printf("nfreevars: %d\n", nfreevars);

	if (SQ_SUCCEEDED(sq_call(v, 1, SQTrue, SQTrue))) {
		printf("SUCCEEDED\n");
	}

	return 0;
}

static SQInteger wrap_draw(SQVM *v) {
	std::weak_ptr<graphics::Framebuffer> framebuffer{
		sqx_checkinstance<graphics::Framebuffer>(v, 1)
	};

	float x, y, a, sx, sy, ox, oy;
	sq_getfloat(v, 2, (SQFloat *) &x);
	sq_getfloat(v, 3, (SQFloat *) &y);
	sq_getfloat(v, 4, (SQFloat *) &a);
	sq_getfloat(v, 5, (SQFloat *) &sx);
	sq_getfloat(v, 6, (SQFloat *) &sy);
	sq_getfloat(v, 7, (SQFloat *) &ox);
	sq_getfloat(v, 8, (SQFloat *) &oy);

	glm::mat4 t(1.0f);
	t = glm::translate(t, glm::vec3(ox, oy, 0.0f));
	t = glm::scale(t, glm::vec3(sx, sy, 0.0f));
	t = glm::rotate(t, a, glm::vec3(0.0, 0.0, 1.0f));
	t = glm::translate(t, glm::vec3(x, y, 0.0f));

	if (auto spt = framebuffer.lock()) {
		spt->draw(instance(), t);
	}
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 3, "xnn", false),

	// FIXME: not final
	_DECL_FUNC(bind, 1, "x", false),
	_DECL_FUNC(unbind, 1, "x", false),
	_DECL_FUNC(renderTo, 2, "xc", false),
	_DECL_FUNC(draw, 8, "x nn n nn nn", false),

	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_framebuffer(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Framebuffer";
	reg.functions = functions;

	return sqx_registerclass(v, Framebuffer::type, reg);
}

}	// graphics
}	// acorn
