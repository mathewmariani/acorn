// acorn
#include "filesystem/sq_directory.h"
#include "filesystem/sq_file.h"
#include "filesystem/sq_filedata.h"
#include "filesystem/sq_filesystem.h"
#include "filesystem/sq_filetype.h"
#include "filesystem/filesystem.h"

namespace acorn {
namespace filesystem {

#define instance() (Module::getInstance<filesystem::Filesystem>())

std::shared_ptr<File> sqx_getfile(SQVM *v, int idx) {
	std::shared_ptr<File> file{ nullptr };
	if (sq_gettype(v, idx) == OT_STRING) {
		const char *filename;
		sq_getstring(v, idx, &filename);
		file = std::make_shared<File>(filename);
	}
	return file;
}

std::shared_ptr<FileData> sqx_getfiledata(SQVM *v, int idx) {
	std::shared_ptr<File> file{ nullptr };
	if (sq_gettype(v, idx) == OT_STRING) {
		file = sqx_getfile(v, idx);
	}
	std::shared_ptr<FileData> data{ nullptr };
	if (file) {
		data = file->read();
		file->close();
		file.reset();
	}
	return data;
}

// NOTE: functions for acorn.filesystem
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.filesystem
static SQFunction classes[] = {
	// sq_register_filedata,
	sq_register_directory,
	sq_register_file,
	nullptr
};

static SQFunction enums[] = {
	sq_acorn_filetype,
	nullptr
};

extern "C" int sq_acorn_filesystem(SQVM *v) {
	auto instance = std::make_shared<filesystem::Filesystem>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize system module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "filesystem";
	reg.classes = classes;
	reg.enums = enums;

	return acorn::sqx_registermodule_ext(v, reg);
}

}	// filesystem
}	// acorn
