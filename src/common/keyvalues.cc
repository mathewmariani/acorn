// acorn
#include "common/keyvalues.h"

namespace acorn {

KeyValues::KeyValues() :
	name(nullptr) {

}

KeyValues::KeyValues(const std::string &name) :
	name(name),
	map({}) {

}

KeyValues::~KeyValues() {

}

const std::string &KeyValues::getName() const noexcept {
	return name;
}

int KeyValues::toSquirrel(SQVM *v) const noexcept {
	// open table
	sq_newtable(v);

	sq_pushstring(v, "name", -1);
	sq_pushstring(v, name.c_str(), -1);
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "args", -1);
	sq_newarray(v, 0);

	for (const auto &pair : map) {
		pair.second.toSquirrel(v);
		sq_arrayappend(v, -2);
	}

	// create slot for array
	sq_newslot(v, -3, SQFalse);

	// close table
	sq_newslot(v, -3, SQFalse);

	return (int) map.size() + 1;
}

}	// acorn
