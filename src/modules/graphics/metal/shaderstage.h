#ifndef ACORN_GRAPHICS_METAL_SHADERSTAGE_H
#define ACORN_GRAPHICS_METAL_SHADERSTAGE_H

// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// acorn
#include "graphics/metal/metal.h"
#include "graphics/shaderstage.h"

namespace acorn {
namespace graphics {
namespace metal {

class ShaderStage final : public graphics::ShaderStage {
public:
	ShaderStage(ShaderStage::StageType stage, const std::string &glsl);
	virtual ~ShaderStage();

private:
	bool compileShader();

public:
	// managed opengl resource
	const void *getHandle() const override;

private:
	// metal shaderstage identifier
};	// shaderstage

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
#endif	// ACORN_GRAPHICS_METAL_SHADERSTAGE_H
