R"squirrel"#(

# event
describe("acorn.event", function() {
	# typeof
	it("is a typeof of table", function() {
		assert.is_typeof(acorn.event, "table")
	})

	# functions
	it("contains a function `push()`", function() {
		assert.is_in("push", acorn.event)
		assert.is_typeof(acorn.event.push, "function")

		# functionality
		acorn.event.push("test")
		acorn.event.pump()
		local event = acorn.event.poll()

		assert.is_not_null(event, acorn.event)
		assert.is_table(event)
		assert.equals(event.name, "test")
	})
	it("contains a function `poll()`", function() {
		assert.is_in("poll", acorn.event)
		assert.is_typeof(acorn.event.poll, "function")
	})
	it("contains a function `pump()`", function() {
		assert.is_in("pump", acorn.event)
		assert.is_typeof(acorn.event.pump, "function")
	})
})

#)squirrel"#"
