#ifndef ACORN_GRAPHICS_SPRITEBATCH_H
#define ACORN_GRAPHICS_SPRITEBATCH_H

// C/C++
#include <memory>

// acorn
#include "common/object.h"
#include "graphics/buffer.h"
#include "graphics/quad.h"
#include "graphics/texture.h"

// gl3w
#include "libraries/gl3w/gl3w.h"

// glm
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace acorn {
namespace graphics {

// forward declaration
class Graphics;

class SpriteBatch : public acorn::Object {
public:
	static acorn::Type type;

public:
	SpriteBatch(std::shared_ptr<Texture>, size_t);
	virtual ~SpriteBatch();

public:
	int add(Quad *q, const glm::mat4 &t, int idx = -1);
	void draw(std::shared_ptr<Graphics> gfx, const glm::mat4 &m);
	void clear();

private:
	std::shared_ptr<Texture> texture;

	// size of the batch
	size_t size;

	// OpenGL buffers
	Buffer *buffer;
	Buffer *indices;

	int nidx;
};	// spritebatch

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_SPRITEBATCH_H
