#ifndef ACORN_SQ_SYSTEM_PLATFORM_H
#define ACORN_SQ_SYSTEM_PLATFORM_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace system {

extern "C" int sq_acorn_platform(SQVM *v);

}	// system
}	// acorn

#endif	// ACORN_SQ_SYSTEM_PLATFORM_H
