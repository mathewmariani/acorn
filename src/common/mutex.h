#ifndef ACORN_MUTEX_H
#define ACORN_MUTEX_H

// SDL2
#if defined (OS_WINDOWS)
	#include <SDL_mutex.h>
#else
	#include <SDL2/SDL_mutex.h>
#endif


namespace acorn {

// NOTE: SDL
// https://wiki.libsdl.org/SDL_CreateMutex
// https://wiki.libsdl.org/SDL_LockMutex
// https://wiki.libsdl.org/SDL_UnlockMutex
// https://wiki.libsdl.org/SDL_DestroyMutex

}	// acorn

#endif	// ACORN_MUTEX_H
