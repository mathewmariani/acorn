// acorn
#include "common/keyvalues.h"
#include "event/sq_event.h"
#include "event/event.h"

namespace acorn {
namespace event {

#define instance() (Module::getInstance<event::Event>())

static SQInteger wrap_push(SQVM *v) {
	// FIXME: terrible..., it doesn't work outside testing
	const char *name;
	sq_getstring(v, 2, &name);

	std::shared_ptr<List> e = std::make_shared<List>(name);
	sq_pushbool(v, (e.get() != nullptr));

	if (e.get() == nullptr) {
		return 1;
	}

	instance()->push(e);
	return 1;
}

static SQInteger wrap_poll(SQVM *v) {
	std::shared_ptr<List> e = nullptr;
	if (instance()->poll(e)) {
		// FIXME: returns undefined values
		int args = e->toSquirrel(v);
		e.reset();
		return args;
	}
	return 0;
}

static SQInteger wrap_pump(SQVM *v) {
	instance()->pump();
	return 0;
}

// NOTE: types for acorn.event
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	_DECL_FUNC(push, 2, "ts"),
	_DECL_FUNC(poll, 1, "t"),
	_DECL_FUNC(pump, 1, "t"),
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.event
const static SQFunction types[] = {
	nullptr
};

extern "C" int sq_acorn_event(SQVM *v) {
	auto instance = std::make_shared<event::Event>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize event module");
	}

	SQRegModule reg;
	reg.module = std::move(instance);
	reg.name = "event";
	reg.functions = functions;

	return acorn::sqx_registermodule(v, reg);
}

}	// event
}	// acorn
