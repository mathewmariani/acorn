#ifndef ACORN_EVENT_H
#define ACORN_EVENT_H

// STL
#include <memory>
#include <mutex>
#include <queue>

// acorn
#include "common/keyvalues.h"
#include "common/list.h"
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"

// SDL
#include <SDL.h>

namespace acorn {
namespace event {

class Event : public acorn::Module {
public:
	static acorn::Type type;

public:
	Event();
	virtual ~Event();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:

	// use this function push events onto the queue
	void push(std::shared_ptr<List> event) noexcept;

	// use this function to poll for currently pending events and add it to the queue
	void pump() noexcept;

	// use this function for an iterator to an event handler from the queue
	bool poll(std::shared_ptr<List> &event) noexcept;

	// use this function to wait until the specified timeout (in milliseconds) for the next available event.
	// SDL_WaitEventTimeout
	// Use this function to wait indefinitely for the next available event.
	// SDL_WaitEvent
	//void wait();

private:
	// an enumeration of the types of events that can be delivered.
	std::shared_ptr<List> convert(const SDL_Event &e);

	// an enumeration of keyboard events that can be delivered.
	std::shared_ptr<List> handleKeyboardEvent(const SDL_Event &e) const noexcept;

	// an enumeration of mouse events that can be delivered.
	std::shared_ptr<List> handleMouseEvent(const SDL_Event &e) const noexcept;

	// an enumeration of joystick events that can be delivered.
	std::shared_ptr<List> handleJoystickEvent(const SDL_Event &e) const noexcept;

	// an enumeration of touch events that can be delivered.
	std::shared_ptr<List> handleTouchEvent(const SDL_Event &e) const noexcept;

	// an enumeration of window events that can be delivered.
	std::shared_ptr<List> handleWindowEvent(const SDL_Event &e) const noexcept;

private:
	std::mutex mutex;
	std::queue<std::shared_ptr<List>> queue;
};	// event

}	// event
}	// acorn

#endif	// ACORN_EVENT_H
