// acorn
#include "timer/sq_timer.h"
#include "timer/timer.h"

namespace acorn {
namespace timer {

#define instance() (Module::getInstance<timer::Timer>())

static SQInteger wrap_step(SQVM *v) {
	sq_pushfloat(v, instance()->step());
	return 1;
}

static SQInteger wrap_getDeltaTime(SQVM *v) {
	sq_pushfloat(v, instance()->getDeltaTime());
	return 1;
}

static SQInteger wrap_getTime(SQVM *v) {
	sq_pushfloat(v, instance()->getTime());
	return 1;
}

static SQInteger wrap_sleep(SQVM *v) {
	SQInteger ms;
	sq_getinteger(v, -1, &ms);
	instance()->sleep(ms);
	return 0;
}

// NOTE: functions for acorn.timer
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	_DECL_FUNC(step, 1, "t"),
	_DECL_FUNC(getDeltaTime, 1, "t"),
	_DECL_FUNC(getTime, 1, "t"),
	_DECL_FUNC(sleep, 2, "ti"),

	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.timer
const static SQFunction types[] = {
    nullptr
};

extern "C" int sq_acorn_timer(SQVM *v) {
	auto instance = std::make_shared<Timer>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize timer module");
	}

	SQRegModule reg;
	reg.module = std::move(instance);
	reg.name = "timer";
	reg.functions = functions;

	return acorn::sqx_registermodule(v, reg);
}

}	// timer
}	// acorn
