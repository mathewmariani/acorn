#ifndef ACORN_TYPE_H
#define ACORN_TYPE_H

// STL
#include <typeinfo>
#include <typeindex>

namespace acorn {

using Type = std::type_index;

}	// acorn

#endif	// ACORN_TYPE_H
