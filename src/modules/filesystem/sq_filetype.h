#ifndef ACORN_SQ_FILESYSTEM_FILTYPE_H
#define ACORN_SQ_FILESYSTEM_FILTYPE_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace filesystem {

extern "C" int sq_acorn_filetype(SQVM *v);

}	// filesystem
}	// acorn

#endif	// ACORN_SQ_FILESYSTEM_FILTYPE_H
