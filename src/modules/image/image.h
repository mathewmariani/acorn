#ifndef ACORN_IMAGE_H
#define ACORN_IMAGE_H

// STL
#include <memory>

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"
#include "image/imagedata.h"

namespace acorn {
namespace image {

class Image : public acorn::Module {
public:
	static acorn::Type type;

public:
	Image();
	virtual ~Image();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:
	std::shared_ptr<image::ImageData> newImageData(int width, int height);
};	// image

}	// image
}	// acorn

#endif	// ACORN_IMAGE_H
