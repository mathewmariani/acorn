// acorn
#include "common/log.h"
#include "image/image.h"

namespace acorn {
namespace image {

acorn::Type Image::type = std::type_index(typeid(image::Image));

Image::Image() {
	log_debug("Image module initialized.");
}

Image::~Image() {
	log_debug("Image module deinitialized.");
}

std::shared_ptr<image::ImageData> newImageData(int width, int height) {
	return std::make_shared<ImageData>(width, height);
}

}	// image
}	// acorn
