// acorn
#include "graphics/graphics.h"
#include "graphics/graphicsdevice.h"
#include "graphics/sq_graphicsdevice.h"

#include "math/mathmodule.h"

#include <math.h>

namespace acorn {
namespace graphics {

#define instance() (Module::getInstance<graphics::Graphics>())

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "GraphicsDevice cannot be constructed directly.");
}

static SQInteger wrap_getInfo(SQVM *v) {
	auto info = instance()->getRendererInfo();

	// create table
	sq_newtableex(v, 4);

	sq_pushstring(v, "name", -1);
	sq_pushstring(v, info.name.c_str(), info.name.length());
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "version", -1);
	sq_pushstring(v, info.version.c_str(), info.version.length());
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "vendor", -1);
	sq_pushstring(v, info.vendor.c_str(), info.vendor.length());
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "device", -1);
	sq_pushstring(v, info.device.c_str(), info.device.length());
	sq_newslot(v, -3, SQFalse);

	sq_newslot(v, -3, SQFalse);

	return 1;
}

static SQInteger wrap_getDimensions(SQVM *v) {
	// create table
	sq_newtableex(v, 2);

	sq_pushstring(v, "width", -1);
	sq_pushinteger(v, instance()->getWidth());
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "height", -1);
	sq_pushinteger(v, instance()->getHeight());
	sq_newslot(v, -3, SQFalse);

	sq_newslot(v, -3, SQFalse);
	return 1;
}

static SQInteger wrap_getWidth(SQVM *v) {
	sq_pushinteger(v, instance()->getWidth());
	return 1;
}

static SQInteger wrap_getHeight(SQVM *v) {
	sq_pushinteger(v, instance()->getHeight());
	return 1;
}

static SQInteger wrap_clear(SQVM *v) {
	float r, g, b, a;
	sq_getfloat(v, 4, (SQFloat *)&r);
	sq_getfloat(v, 3, (SQFloat *)&g);
	sq_getfloat(v, 2, (SQFloat *)&b);
	sq_getfloat(v, 1, (SQFloat *)&a);

	instance()->clear(r, g, b, a);
	return 0;
}

static SQInteger wrap_present(SQVM *v) {
	instance()->present();
	return 0;
}

static SQInteger wrap_reset(SQVM *v) {
	STUBBED("wrap_reset");
	return 0;
}

static SQInteger wrap_setShader(SQVM *v) {
	std::weak_ptr<Shader> shader{ sqx_checkinstance<Shader>(v, 2) };
	if (auto spt = shader.lock()) {
		instance()->setShader(spt.get());
	}
	return 0;
}

static SQInteger wrap_getShader(SQVM *v) {
	STUBBED("wrap_getShader");
	return 0;
}

static SQInteger wrap_setPointSize(SQVM *v) {
	float size = sqx_getnumber(v, 2, 1.0f);
	instance()->setPointSize(size);
	return 0;
}

static SQInteger wrap_getPointSize(SQVM *v) {
	STUBBED("wrap_getPointSize");
	return 0;
}

static SQInteger wrap_setPointMode(SQVM *v) {
	STUBBED("wrap_setPointMode");
	return 0;
}

static SQInteger wrap_setLineMode(SQVM *v) {
	STUBBED("wrap_setLineMode");
	return 0;
}

static SQInteger wrap_setFillMode(SQVM *v) {
	STUBBED("wrap_setFillMode");
	return 0;
}

static SQInteger wrap_isPointMode(SQVM *v) {
	STUBBED("wrap_isPointMode");
	return 0;
}

static SQInteger wrap_isLineMode(SQVM *v) {
	STUBBED("wrap_isLineMode");
	return 0;
}

static SQInteger wrap_isFillMode(SQVM *v) {
	STUBBED("wrap_isFillMode");
	return 0;
}

static SQInteger wrap_getDrawableDimensions(SQVM *v) {
	STUBBED("wrap_getDrawableDimensions");
	return 0;
}

static SQInteger wrap_getDrawableWidth(SQVM *v) {
	STUBBED("wrap_getDrawableWidth");
	return 0;
}

static SQInteger wrap_getDrawableHeight(SQVM *v) {
	STUBBED("wrap_getDrawableHeight");
	return 0;
}

static SQInteger wrap_points(SQVM *v) {
	std::vector<float> points{
		sqx_getnumber(v, 2, 0.0f),
		sqx_getnumber(v, 3, 0.0f)
	};

	instance()->points(points);
	return 0;
}

static SQInteger wrap_triangle(SQVM *v) {
	std::vector<float> points{
		sqx_getnumber(v, 2, 0.0f),
		sqx_getnumber(v, 3, 0.0f),

		sqx_getnumber(v, 4, 0.0f),
		sqx_getnumber(v, 5, 0.0f),

		sqx_getnumber(v, 6, 0.0f),
		sqx_getnumber(v, 7, 0.0f)
	};

	instance()->triangle(points);
	return 0;
}

static SQInteger wrap_circle(SQVM *v) {
	auto x = sqx_getnumber(v, 2, 0.0f);
	auto y = sqx_getnumber(v, 3, 0.0f);
	auto r = sqx_getnumber(v, 4, 0.0f);
	auto n = sqx_getnumber(v, 5, 0.0f);

	std::vector<float> arr;
	arr.reserve(n);

	arr.push_back(x);
	arr.push_back(y);
	for (auto i = 0; i <= n; i++) {
		arr.push_back(
			x + (r * sin(i * ACORN_M_PI * 2 / n))
		);
		arr.push_back(
			y + (r * cos(i * ACORN_M_PI * 2 / n))
		);

	}
	instance()->polygon(
		arr
	);
	return 0;
}

static SQInteger wrap_polygon(SQVM *v) {
	sqx_printstack(v);
	auto readarray = [](SQVM *v, int idx) -> std::vector<float> {
		std::vector<float> arr;
		for (auto i = idx; i <= sq_gettop(v); ++i) {
			arr.push_back(
				sqx_getnumber(v, i, 0.0f)
			);
			auto num = sqx_getnumber(v, i, 0.0f);
			printf("reading idx: %d (%f)\n", i, num);
		}
		return arr;
	};
	instance()->polygon(
		readarray(v, 2)
	);
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),

	// system
	_DECL_FUNC(getInfo, 1, "y", true),

	// window
	_DECL_FUNC(getDimensions, 1, "y", true),
	_DECL_FUNC(getWidth, 1, "y", true),
	_DECL_FUNC(getHeight, 1, "y", true),

	_DECL_FUNC(clear, 5, "ynnnn", true),
	_DECL_FUNC(present, 1, "y", true),
	_DECL_FUNC(reset, 1, "y", true),

	// ect
	_DECL_FUNC(setShader, 2, "yx", true),
	_DECL_FUNC(getShader, 1, "y", true),

	_DECL_FUNC(setPointSize, 2, "yn", true),
	_DECL_FUNC(getPointSize, 1, "y", true),

	// polygon modes
	_DECL_FUNC(setPointMode, 2, "yb", true),
	_DECL_FUNC(setLineMode, 2, "yb", true),
	_DECL_FUNC(setFillMode, 2, "yb", true),
	_DECL_FUNC(isLineMode, 1, "y", true),
	_DECL_FUNC(isPointMode, 1, "y", true),
	_DECL_FUNC(isFillMode, 1, "y", true),

	// dimensions
	_DECL_FUNC(getDrawableDimensions, 1, "y", true),
	_DECL_FUNC(getDrawableWidth, 1, "y", true),
	_DECL_FUNC(getDrawableHeight, 1, "y", true),

	// primitives
	_DECL_FUNC(points, 3, "ynn", true),
	_DECL_FUNC(triangle, 7, "ynnnnnn", true),
	_DECL_FUNC(circle, 5, "ynnnn", true),
	_DECL_FUNC(polygon, -1, "y", true),

	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_graphicsdevice(SQVM *v) {
	ClassRegistry reg;
	reg.name = "GraphicsDevice";
	reg.functions = functions;
	return sqx_registerclass(v, GraphicsDevice::type, reg);
}

}	// graphics
}	// acorn
