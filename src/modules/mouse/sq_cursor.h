#ifndef ACORN_SQ_CURSOR_H
#define ACORN_SQ_CURSOR_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace mouse {

extern "C" int sq_acorn_cursor(SQVM *v);

}	// mouse
}	// acorn

#endif	// ACORN_SQ_CURSOR_H
