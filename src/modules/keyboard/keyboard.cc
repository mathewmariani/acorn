// C/C++
#include <iostream>

// acorn
#include "common/log.h"
#include "common/platform.h"
#include "keyboard/keyboard.h"
#include "window/window.h"

// SDL
#include <SDL.h>

namespace acorn {
namespace keyboard {

acorn::Type Keyboard::type = std::type_index(typeid(keyboard::Keyboard));

Keyboard::Keyboard() {
	log_debug("Keyboard module initialized.");
}

Keyboard::~Keyboard() {
	log_debug("Keyboard module deinitialized.");
}

bool Keyboard::isPressed(Scancode code) const noexcept {
	const Uint8 *state = SDL_GetKeyboardState(nullptr);
	return (state[(uint8) code] != 0);
}

bool Keyboard::isPressed(const std::vector<Scancode> &scancode) const noexcept {
	// return true if any of the buttons are down
	STUBBED("std::vector of Scancode")
	return false;
}

bool Keyboard::isModifierPressed(Modifier mod) const noexcept {
	return (((Uint32) SDL_GetModState() & (Uint32) mod) != 0);
}

bool Keyboard::isModifierPressed(const std::vector<Modifier> &mod) const noexcept {
	// return true if any of the buttons are down
	STUBBED("std::vector of Modifier")
	return false;
}

bool Keyboard::hasScreenKeyboard() const noexcept {
	return (SDL_HasScreenKeyboardSupport() == SDL_FALSE);
}

bool Keyboard::isScreenKeyboardShown() const noexcept {
	auto window = Module::getInstance<window::Window>();
	if (window == nullptr) {
		return false;
	}

	return (SDL_IsScreenKeyboardShown((SDL_Window *) window->getHandle()) == SDL_TRUE);
}

bool Keyboard::isTextInputActive() const noexcept {
	return (SDL_IsTextInputActive() == SDL_TRUE);
}

void Keyboard::startTextInput() const noexcept {
	SDL_StartTextInput();
}

void Keyboard::stopTextInput() const noexcept {
	SDL_StopTextInput();
}

// FIXME: these may not be super useful...
//std::unordered_map<std::string, Keyboard::Scancode> Keyboard::scancodesTable = {
//	{ "UNKNOWN", Keyboard::SCANCODE_UNKNOWN },
//
//	{ "A", Keyboard::SCANCODE_A },
//	{ "B", Keyboard::SCANCODE_B },
//	{ "C", Keyboard::SCANCODE_C },
//	{ "D", Keyboard::SCANCODE_D },
//	{ "E", Keyboard::SCANCODE_E },
//	{ "F", Keyboard::SCANCODE_F },
//	{ "G", Keyboard::SCANCODE_G },
//	{ "H", Keyboard::SCANCODE_H },
//	{ "I", Keyboard::SCANCODE_I },
//	{ "J", Keyboard::SCANCODE_J },
//	{ "K", Keyboard::SCANCODE_K },
//	{ "L", Keyboard::SCANCODE_L },
//	{ "M", Keyboard::SCANCODE_M },
//	{ "N", Keyboard::SCANCODE_N },
//	{ "O", Keyboard::SCANCODE_O },
//	{ "P", Keyboard::SCANCODE_P },
//	{ "Q", Keyboard::SCANCODE_Q },
//	{ "R", Keyboard::SCANCODE_R },
//	{ "S", Keyboard::SCANCODE_S },
//	{ "T", Keyboard::SCANCODE_T },
//	{ "U", Keyboard::SCANCODE_U },
//	{ "V", Keyboard::SCANCODE_V },
//	{ "W", Keyboard::SCANCODE_W },
//	{ "X", Keyboard::SCANCODE_X },
//	{ "Y", Keyboard::SCANCODE_Y },
//	{ "Z", Keyboard::SCANCODE_Z },
//
//	{ "Alpha1", Keyboard::SCANCODE_1 },
//	{ "Alpha2", Keyboard::SCANCODE_2 },
//	{ "Alpha3", Keyboard::SCANCODE_3 },
//	{ "Alpha4", Keyboard::SCANCODE_4 },
//	{ "Alpha5", Keyboard::SCANCODE_5 },
//	{ "Alpha6", Keyboard::SCANCODE_6 },
//	{ "Alpha7", Keyboard::SCANCODE_7 },
//	{ "Alpha8", Keyboard::SCANCODE_8 },
//	{ "Alpha9", Keyboard::SCANCODE_9 },
//	{ "Alpha0", Keyboard::SCANCODE_0 },
//
//	{ "RETURN", Keyboard::SCANCODE_RETURN },
//	{ "ESCAPE", Keyboard::SCANCODE_ESCAPE },
//	{ "BACKSPACE", Keyboard::SCANCODE_BACKSPACE },
//	{ "TAB", Keyboard::SCANCODE_TAB },
//	{ "SPACE", Keyboard::SCANCODE_SPACE },
//
//	{ "MINUS", Keyboard::SCANCODE_MINUS },
//	{ "EQUALS", Keyboard::SCANCODE_EQUALS },
//	{ "LEFTBRACKET", Keyboard::SCANCODE_LEFTBRACKET },
//	{ "RIGHTBRACKET", Keyboard::SCANCODE_RIGHTBRACKET },
//	{ "BACKSLASH", Keyboard::SCANCODE_BACKSLASH },
//	{ "NONUSHASH", Keyboard::SCANCODE_NONUSHASH },
//	{ "SEMICOLON", Keyboard::SCANCODE_SEMICOLON },
//	{ "APOSTROPHE", Keyboard::SCANCODE_APOSTROPHE },
//	{ "GRAVE", Keyboard::SCANCODE_GRAVE },
//	{ "COMMA", Keyboard::SCANCODE_COMMA },
//	{ "PERIOD", Keyboard::SCANCODE_PERIOD },
//	{ "SLASH", Keyboard::SCANCODE_SLASH },
//
//	{ "CAPSLOCK", Keyboard::SCANCODE_CAPSLOCK },
//
//	{ "F1", Keyboard::SCANCODE_F1 },
//	{ "F2", Keyboard::SCANCODE_F2 },
//	{ "F3", Keyboard::SCANCODE_F3 },
//	{ "F4", Keyboard::SCANCODE_F4 },
//	{ "F5", Keyboard::SCANCODE_F5 },
//	{ "F6", Keyboard::SCANCODE_F6 },
//	{ "F7", Keyboard::SCANCODE_F7 },
//	{ "F8", Keyboard::SCANCODE_F8 },
//	{ "F9", Keyboard::SCANCODE_F9 },
//	{ "F10", Keyboard::SCANCODE_F10 },
//	{ "F11", Keyboard::SCANCODE_F11 },
//	{ "F12", Keyboard::SCANCODE_F12 },
//
//	{ "PRINTSCREEN", Keyboard::SCANCODE_PRINTSCREEN },
//	{ "SCROLLLOCK", Keyboard::SCANCODE_SCROLLLOCK },
//	{ "PAUSE", Keyboard::SCANCODE_PAUSE },
//	{ "INSERT", Keyboard::SCANCODE_INSERT },
//	{ "HOME", Keyboard::SCANCODE_HOME },
//	{ "PAGEUP", Keyboard::SCANCODE_PAGEUP },
//	{ "DELETE", Keyboard::SCANCODE_DELETE },
//	{ "END", Keyboard::SCANCODE_END },
//	{ "PAGEDOWN", Keyboard::SCANCODE_PAGEDOWN },
//	{ "RIGHT", Keyboard::SCANCODE_RIGHT },
//	{ "LEFT", Keyboard::SCANCODE_LEFT },
//	{ "DOWN", Keyboard::SCANCODE_DOWN },
//	{ "UP", Keyboard::SCANCODE_UP },
//
//	{ "NUMLOCKCLEAR", Keyboard::SCANCODE_NUMLOCKCLEAR },
//	{ "KP_DIVIDE", Keyboard::SCANCODE_KP_DIVIDE },
//	{ "KP_MULTIPLY", Keyboard::SCANCODE_KP_MULTIPLY },
//	{ "KP_MINUS", Keyboard::SCANCODE_KP_MINUS },
//	{ "KP_PLUS", Keyboard::SCANCODE_KP_PLUS },
//	{ "KP_ENTER", Keyboard::SCANCODE_KP_ENTER },
//	{ "KP_1", Keyboard::SCANCODE_KP_1 },
//	{ "KP_2", Keyboard::SCANCODE_KP_2 },
//	{ "KP_3", Keyboard::SCANCODE_KP_3 },
//	{ "KP_4", Keyboard::SCANCODE_KP_4 },
//	{ "KP_5", Keyboard::SCANCODE_KP_5 },
//	{ "KP_6", Keyboard::SCANCODE_KP_6 },
//	{ "KP_7", Keyboard::SCANCODE_KP_7 },
//	{ "KP_8", Keyboard::SCANCODE_KP_8 },
//	{ "KP_9", Keyboard::SCANCODE_KP_9 },
//	{ "KP_0", Keyboard::SCANCODE_KP_0 },
//	{ "KP_PERIOD", Keyboard::SCANCODE_KP_PERIOD },
//
//	{ "NONUSBACKSLASH", Keyboard::SCANCODE_NONUSBACKSLASH },
//	{ "APPLICATION", Keyboard::SCANCODE_APPLICATION },
//	{ "POWER", Keyboard::SCANCODE_POWER },
//	{ "KP_EQUALS", Keyboard::SCANCODE_KP_EQUALS },
//	{ "F13", Keyboard::SCANCODE_F13 },
//	{ "F14", Keyboard::SCANCODE_F14 },
//	{ "F15", Keyboard::SCANCODE_F15 },
//	{ "F16", Keyboard::SCANCODE_F16 },
//	{ "F17", Keyboard::SCANCODE_F17 },
//	{ "F18", Keyboard::SCANCODE_F18 },
//	{ "F19", Keyboard::SCANCODE_F19 },
//	{ "F20", Keyboard::SCANCODE_F20 },
//	{ "F21", Keyboard::SCANCODE_F21 },
//	{ "F22", Keyboard::SCANCODE_F22 },
//	{ "F23", Keyboard::SCANCODE_F23 },
//	{ "F24", Keyboard::SCANCODE_F24 },
//	{ "EXECUTE", Keyboard::SCANCODE_EXECUTE },
//	{ "HELP", Keyboard::SCANCODE_HELP },
//	{ "MENU", Keyboard::SCANCODE_MENU },
//	{ "SELECT", Keyboard::SCANCODE_SELECT },
//	{ "STOP", Keyboard::SCANCODE_STOP },
//	{ "AGAIN", Keyboard::SCANCODE_AGAIN },
//	{ "UNDO", Keyboard::SCANCODE_UNDO },
//	{ "CUT", Keyboard::SCANCODE_CUT },
//	{ "COPY", Keyboard::SCANCODE_COPY },
//	{ "PASTE", Keyboard::SCANCODE_PASTE },
//	{ "FIND", Keyboard::SCANCODE_FIND },
//	{ "MUTE", Keyboard::SCANCODE_MUTE },
//	{ "VOLUMEUP", Keyboard::SCANCODE_VOLUMEUP },
//	{ "VOLUMEDOWN", Keyboard::SCANCODE_VOLUMEDOWN },
//	{ "KP_COMMA", Keyboard::SCANCODE_KP_COMMA },
//	{ "KP_EQUALSAS400", Keyboard::SCANCODE_KP_EQUALSAS400 },
//
//	{ "INTERNATIONAL1", Keyboard::SCANCODE_INTERNATIONAL1 },
//	{ "INTERNATIONAL2", Keyboard::SCANCODE_INTERNATIONAL2 },
//	{ "INTERNATIONAL3", Keyboard::SCANCODE_INTERNATIONAL3 },
//	{ "INTERNATIONAL4", Keyboard::SCANCODE_INTERNATIONAL4 },
//	{ "INTERNATIONAL5", Keyboard::SCANCODE_INTERNATIONAL5 },
//	{ "INTERNATIONAL6", Keyboard::SCANCODE_INTERNATIONAL6 },
//	{ "INTERNATIONAL7", Keyboard::SCANCODE_INTERNATIONAL7 },
//	{ "INTERNATIONAL8", Keyboard::SCANCODE_INTERNATIONAL8 },
//	{ "INTERNATIONAL9", Keyboard::SCANCODE_INTERNATIONAL9 },
//	{ "LANG1", Keyboard::SCANCODE_LANG1 },
//	{ "LANG2", Keyboard::SCANCODE_LANG2 },
//	{ "LANG3", Keyboard::SCANCODE_LANG3 },
//	{ "LANG4", Keyboard::SCANCODE_LANG4 },
//	{ "LANG5", Keyboard::SCANCODE_LANG5 },
//	{ "LANG6", Keyboard::SCANCODE_LANG6 },
//	{ "LANG7", Keyboard::SCANCODE_LANG7 },
//	{ "LANG8", Keyboard::SCANCODE_LANG8 },
//	{ "LANG9", Keyboard::SCANCODE_LANG9 },
//
//	{ "ALTERASE", Keyboard::SCANCODE_ALTERASE },
//	{ "SYSREQ", Keyboard::SCANCODE_SYSREQ },
//	{ "CANCEL", Keyboard::SCANCODE_CANCEL },
//	{ "CLEAR", Keyboard::SCANCODE_CLEAR },
//	{ "PRIOR", Keyboard::SCANCODE_PRIOR },
//	{ "RETURN2", Keyboard::SCANCODE_RETURN2 },
//	{ "SEPARATOR", Keyboard::SCANCODE_SEPARATOR },
//	{ "OUT", Keyboard::SCANCODE_OUT },
//	{ "OPER", Keyboard::SCANCODE_OPER },
//	{ "CLEARAGAIN", Keyboard::SCANCODE_CLEARAGAIN },
//	{ "CRSEL", Keyboard::SCANCODE_CRSEL },
//	{ "EXSEL", Keyboard::SCANCODE_EXSEL },
//
//	{ "KP_00", Keyboard::SCANCODE_KP_00 },
//	{ "KP_000", Keyboard::SCANCODE_KP_000 },
//	{ "THOUSANDSSEPARATOR", Keyboard::SCANCODE_THOUSANDSSEPARATOR },
//	{ "DECIMALSEPARATOR", Keyboard::SCANCODE_DECIMALSEPARATOR },
//	{ "CURRENCYUNIT", Keyboard::SCANCODE_CURRENCYUNIT },
//	{ "CURRENCYSUBUNIT", Keyboard::SCANCODE_CURRENCYSUBUNIT },
//	{ "KP_LEFTPAREN", Keyboard::SCANCODE_KP_LEFTPAREN },
//	{ "KP_RIGHTPAREN", Keyboard::SCANCODE_KP_RIGHTPAREN },
//	{ "KP_LEFTBRACE", Keyboard::SCANCODE_KP_LEFTBRACE },
//	{ "KP_RIGHTBRACE", Keyboard::SCANCODE_KP_RIGHTBRACE },
//	{ "KP_TAB", Keyboard::SCANCODE_KP_TAB },
//	{ "KP_BACKSPACE", Keyboard::SCANCODE_KP_BACKSPACE },
//	{ "KP_A", Keyboard::SCANCODE_KP_A },
//	{ "KP_B", Keyboard::SCANCODE_KP_B },
//	{ "KP_C", Keyboard::SCANCODE_KP_C },
//	{ "KP_D", Keyboard::SCANCODE_KP_D },
//	{ "KP_E", Keyboard::SCANCODE_KP_E },
//	{ "KP_F", Keyboard::SCANCODE_KP_F },
//	{ "KP_XOR", Keyboard::SCANCODE_KP_XOR },
//	{ "KP_POWER", Keyboard::SCANCODE_KP_POWER },
//	{ "KP_PERCENT", Keyboard::SCANCODE_KP_PERCENT },
//	{ "KP_LESS", Keyboard::SCANCODE_KP_LESS },
//	{ "KP_GREATER", Keyboard::SCANCODE_KP_GREATER },
//	{ "KP_AMPERSAND", Keyboard::SCANCODE_KP_AMPERSAND },
//	{ "KP_DBLAMPERSAND", Keyboard::SCANCODE_KP_DBLAMPERSAND },
//	{ "KP_VERTICALBAR", Keyboard::SCANCODE_KP_VERTICALBAR },
//	{ "KP_DBLVERTICALBAR", Keyboard::SCANCODE_KP_DBLVERTICALBAR },
//	{ "KP_COLON", Keyboard::SCANCODE_KP_COLON },
//	{ "KP_HASH", Keyboard::SCANCODE_KP_HASH },
//	{ "KP_SPACE", Keyboard::SCANCODE_KP_SPACE },
//	{ "KP_AT", Keyboard::SCANCODE_KP_AT },
//	{ "KP_EXCLAM", Keyboard::SCANCODE_KP_EXCLAM },
//	{ "KP_MEMSTORE", Keyboard::SCANCODE_KP_MEMSTORE },
//	{ "KP_MEMRECALL", Keyboard::SCANCODE_KP_MEMRECALL },
//	{ "KP_MEMCLEAR", Keyboard::SCANCODE_KP_MEMCLEAR },
//	{ "KP_MEMADD", Keyboard::SCANCODE_KP_MEMADD },
//	{ "KP_MEMSUBTRACT", Keyboard::SCANCODE_KP_MEMSUBTRACT },
//	{ "KP_MEMMULTIPLY", Keyboard::SCANCODE_KP_MEMMULTIPLY },
//	{ "KP_MEMDIVIDE", Keyboard::SCANCODE_KP_MEMDIVIDE },
//	{ "KP_PLUSMINUS", Keyboard::SCANCODE_KP_PLUSMINUS },
//	{ "KP_CLEAR", Keyboard::SCANCODE_KP_CLEAR },
//	{ "KP_CLEARENTRY", Keyboard::SCANCODE_KP_CLEARENTRY },
//	{ "KP_BINARY", Keyboard::SCANCODE_KP_BINARY },
//	{ "KP_OCTAL", Keyboard::SCANCODE_KP_OCTAL },
//	{ "KP_DECIMAL", Keyboard::SCANCODE_KP_DECIMAL },
//	{ "KP_HEXADECIMAL", Keyboard::SCANCODE_KP_HEXADECIMAL },
//
//	{ "LCTRL", Keyboard::SCANCODE_LCTRL },
//	{ "LSHIFT", Keyboard::SCANCODE_LSHIFT },
//	{ "LALT", Keyboard::SCANCODE_LALT },
//	{ "LGUI", Keyboard::SCANCODE_LGUI },
//	{ "RCTRL", Keyboard::SCANCODE_RCTRL },
//	{ "RSHIFT", Keyboard::SCANCODE_RSHIFT },
//	{ "RALT", Keyboard::SCANCODE_RALT },
//	{ "RGUI", Keyboard::SCANCODE_RGUI },
//
//	{ "MODE", Keyboard::SCANCODE_MODE },
//
//	{ "AUDIONEXT", Keyboard::SCANCODE_AUDIONEXT },
//	{ "AUDIOPREV", Keyboard::SCANCODE_AUDIOPREV },
//	{ "AUDIOSTOP", Keyboard::SCANCODE_AUDIOSTOP },
//	{ "AUDIOPLAY", Keyboard::SCANCODE_AUDIOPLAY },
//	{ "AUDIOMUTE", Keyboard::SCANCODE_AUDIOMUTE },
//	{ "MEDIASELECT", Keyboard::SCANCODE_MEDIASELECT },
//	{ "WWW", Keyboard::SCANCODE_WWW },
//	{ "MAIL", Keyboard::SCANCODE_MAIL },
//	{ "CALCULATOR", Keyboard::SCANCODE_CALCULATOR },
//	{ "COMPUTER", Keyboard::SCANCODE_COMPUTER },
//	{ "AC_SEARCH", Keyboard::SCANCODE_AC_SEARCH },
//	{ "AC_HOME", Keyboard::SCANCODE_AC_HOME },
//	{ "AC_BACK", Keyboard::SCANCODE_AC_BACK },
//	{ "AC_FORWARD", Keyboard::SCANCODE_AC_FORWARD },
//	{ "AC_STOP", Keyboard::SCANCODE_AC_STOP },
//	{ "AC_REFRESH", Keyboard::SCANCODE_AC_REFRESH },
//	{ "AC_BOOKMARKS", Keyboard::SCANCODE_AC_BOOKMARKS },
//
//	{ "BRIGHTNESSDOWN", Keyboard::SCANCODE_BRIGHTNESSDOWN },
//	{ "BRIGHTNESSUP", Keyboard::SCANCODE_BRIGHTNESSUP },
//	{ "DISPLAYSWITCH", Keyboard::SCANCODE_DISPLAYSWITCH },
//	{ "KBDILLUMTOGGLE", Keyboard::SCANCODE_KBDILLUMTOGGLE },
//	{ "KBDILLUMDOWN", Keyboard::SCANCODE_KBDILLUMDOWN },
//	{ "KBDILLUMUP", Keyboard::SCANCODE_KBDILLUMUP },
//	{ "EJECT", Keyboard::SCANCODE_EJECT },
//	{ "SLEEP", Keyboard::SCANCODE_SLEEP },
//
//	{ "APP1", Keyboard::SCANCODE_APP1 },
//	{ "APP2", Keyboard::SCANCODE_APP2 },
//
//	{ "AUDIOREWIND", Keyboard::SCANCODE_AUDIOREWIND },
//	{ "AUDIOFASTFORWARD", Keyboard::SCANCODE_AUDIOFASTFORWARD },
//};

// FIXME: these may not be super useful...
//std::unordered_map<std::string, Keyboard::Modifier> Keyboard::modifierTable = {
//	{ "NONE", Keyboard::Modifier::NONE },
//	{ "LSHIFT", Keyboard::Modifier::LSHIFT },
//	{ "RSHIFT", Keyboard::Modifier::RSHIFT },
//	{ "LCTRL", Keyboard::Modifier::LCTRL },
//	{ "RCTRL", Keyboard::Modifier::RCTRL },
//	{ "LALT", Keyboard::Modifier::LALT },
//	{ "RALT", Keyboard::Modifier::RALT },
//	{ "LGUI", Keyboard::Modifier::LGUI },
//	{ "RGUI", Keyboard::Modifier::RGUI },
//	{ "NUM", Keyboard::Modifier::NUM },
//	{ "CAPS", Keyboard::Modifier::CAPS },
//	{ "MODE", Keyboard::Modifier::MODE },
//	{ "CTRL", Keyboard::Modifier::CTRL },
//	{ "SHIFT", Keyboard::Modifier::SHIFT },
//	{ "ALT", Keyboard::Modifier::ALT },
//	{ "GUI", Keyboard::Modifier::GUI },
//};

}	// keyboard
}	// acorn
