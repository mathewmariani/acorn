// C/C++
#include <cstring>
#include <cmath>
#include <math.h>

// acorn
#include "common/_la.h"

namespace acorn {

Vector4f::Vector4f() {
	setZero();
}

void Vector4f::setZero() {
	memset(e, 0, sizeof(float) * 4);
}

float Vector4f::angle(const Vector4f &a, const Vector4f &b) {
	return (std::acos(dot(a, b) / (magnitude(a) * magnitude(b))) / 0.0174533f);
}


float Vector4f::distance(const Vector4f &a, const Vector4f &b) {
	float sum = 0.0f;
	for (auto i = 0; i < 4; i++) {
		sum += ((a[i] - b[i])*(a[i] - b[i]));
	}
	return std::sqrt(sum);
}

float Vector4f::dot(const Vector4f &a, const Vector4f &b) {
	float sum = 0.0f;
	for (auto i = 0; i < 4; i++) {
		sum += (a[i] * b[i]);
	}
	return sum;
}

float Vector4f::magnitude(const Vector4f &v) {
	float sum = 0.0f;
	for (auto i = 0; i < 4; i++) {
		sum += (v[i] * v[i]);
	}
	return std::sqrt(sum);
}

Vector4f Vector4f::max(const Vector4f &a, const Vector4f &b) {
	Vector4f ret;
	for (auto i = 0; i < 4; i++) {
        ret[i] = std::fmax(a[i], b[i]);
	}
	return ret;
}

Vector4f Vector4f::min(const Vector4f &a, const Vector4f &b) {
	Vector4f ret;
	for (auto i = 0; i < 4; i++) {
        ret[i] = std::fmin(a[i], b[i]);
	}
	return ret;
}

Vector4f Vector4f::normalize(const Vector4f &v) {
	return (v / Vector4f::magnitude(v));
}

inline Vector4f::operator float* () {
	return &e[0];
}

inline Vector4f::operator const float* () const {
	return &e[0];
}

inline float& Vector4f::operator [] (int i) {
	return e[i];
}

inline const float& Vector4f::operator [] (int i) const {
	return e[i];
}

inline Vector4f &Vector4f::operator = (const Vector4f &v) {
	memcpy(&e, v.e, sizeof(float) * 4);
	return (*this);
}

inline Vector4f Vector4f::operator + (const Vector4f &v) const {
	Vector4f ret;
	for (auto i = 0; i < 4; i++) {
		ret.e[i] = e[i] + v.e[i];
	}
	return ret;
}

inline void Vector4f::operator += (const Vector4f &v) {
	(*this) = (*this) + v;
}

inline Vector4f Vector4f::operator - (const Vector4f &v) const {
	Vector4f ret;
	for (auto i = 0; i < 4; i++) {
		ret.e[i] = e[i] - v.e[i];
	}
	return ret;
}

inline void Vector4f::operator -= (const Vector4f &v) {
	(*this) = (*this) - v;
}

inline Vector4f Vector4f::operator * (const Vector4f &v) const {
	Vector4f ret;
	for (auto i = 0; i < 4; i++) {
		ret.e[i] = e[i] * v.e[i];
	}
	return ret;
}

inline void Vector4f::operator *= (const Vector4f &v) {
	(*this) = (*this) * v;
}

inline Vector4f Vector4f::operator / (const Vector4f &v) const {
	Vector4f ret;
	for (auto i = 0; i < 4; i++) {
		ret.e[i] = e[i] / v.e[i];
	}

	return ret;
}

inline void Vector4f::operator /= (const Vector4f &v) {
	(*this) = (*this) / v;
}

inline Vector4f Vector4f::operator * (const float &s) const {
	Vector4f ret;
	for (auto i = 0; i < 4; i++) {
		ret.e[i] = e[i] * s;
	}
	return ret;
}

inline void Vector4f::operator *= (const float &s) {
	(*this) = (*this) * s;
}

inline Vector4f Vector4f::operator / (const float &s) const {
	Vector4f ret;
	for (auto i = 0; i < 4; i++) {
		ret.e[i] = e[i] / s;
	}
	return ret;
}

inline void Vector4f::operator /= (const float &s) {
	(*this) = (*this) / s;
}

Matrix4::Matrix4() {
	setIndentity();
}

Matrix4 Matrix4::ortho(float left, float right, float bottom, float top, float near, float far) {
  Matrix4 m;
  m.v[0][0] = (2.0f / (right - left));
  m.v[1][1] = (2.0f / (top - bottom));
  m.v[2][2] = (-2.0f / (far - near));
  m.v[3][0] = (-(right + left) / (right - left));
  m.v[3][1] = (-(top + bottom) / (top - bottom));
  m.v[3][2] = (-(far + near) / (far - near));
  return m;
}

void Matrix4::setIndentity() {
	memset(v, 0, sizeof(v));
	for (int i = 0; i < 4; i++) {
		v[i][i] = 1;
	}
}

void Matrix4::setTranslation(float x, float y) {
	setIndentity();
	v[3][0] = x;
	v[3][1] = y;
}

void Matrix4::setRotation(float angle) {
	setIndentity();
	float c = cosf(angle);
	float s = sinf(angle);
	v[0][0] = c;
	v[0][1] = s;
	v[1][0] = -s;
	v[1][1] = c;
}

void Matrix4::setScale(float sx, float sy) {
	setIndentity();
	v[0][0] = sx;
	v[1][1] = sy;
}

void Matrix4::translate(float x, float y) {
	Matrix4 t;
	t.setTranslation(x, y);
	*this = (*this * t);
}

void Matrix4::rotate(float angle) {
	Matrix4 t;
	t.setRotation(angle);
	*this = (*this * t);
}

void Matrix4::scale(float sx, float sy) {
	Matrix4 t;
	t.setScale(sx, sy);
	*this = (*this * t);
}

inline Matrix4::operator float* () {
	return &v[0][0];
}

inline Matrix4::operator const float* () const {
	return &v[0][0];
}

inline Vector4f &Matrix4::operator [] (int i) {
	return v[i];
}

inline const Vector4f &Matrix4::operator [] (int i) const {
	return v[i];
}

inline Matrix4 Matrix4::operator * (const Matrix4 &m) const {
	Matrix4 t;
	for (auto i = 0; i < 4; i++) {
		for (auto j = 0; j < 4; j++) {
			auto sum = 0.0f;
			for (auto k = 0; k < 4; k++) {
				sum += v[k][j] * m.v[i][k];
			}
			t[i][j] = sum;
		}
	}
	return t;
}

inline void Matrix4::operator *= (const Matrix4 &m) {
	*this = (*this) * m;
}

}	// acorn
