// C/C++
#include <algorithm>
#include <memory>

// acorn
#include "filesystem/sq_filesystem.h"
#include "resource/sq_sounddata.h"
#include "resource/sounddata.h"

namespace acorn {
namespace resource {

static SQInteger wrap_constructor(SQVM *v) {
	// get file data from file
	std::shared_ptr<filesystem::FileData> filedata = filesystem::sqx_getfiledata(v, 2);

	auto sounddata = std::make_shared<SoundData>(filedata);
	sqx_pushinstance(v, SoundData::type, sounddata);
	return 1;
}

static SQInteger wrap_getChannels(SQVM *v) {
	std::weak_ptr<SoundData> instance(sqx_checkinstance<SoundData>(v, 1));
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getChannels());
		return 1;
	}

	return 0;
}

static SQInteger wrap_getSampleRate(SQVM *v) {
	std::weak_ptr<SoundData> instance(sqx_checkinstance<SoundData>(v, 1));
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getSampleRate());
		return 1;
	}

	return 0;
}

static SQInteger wrap_getDuration(SQVM *v) {
	std::weak_ptr<SoundData> instance(sqx_checkinstance<SoundData>(v, 1));
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getDuration());
		return 1;
	}

	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 2, "xs", false),
	_DECL_FUNC(getChannels, 1, "x", false),
	_DECL_FUNC(getSampleRate, 1, "x", false),
	_DECL_FUNC(getDuration, 1, "x", false),
	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_sounddata(SQVM *v) {
	ClassRegistry reg;
	reg.name = "SoundData";
	reg.functions = functions;

	return sqx_registerclass(v, SoundData::type, reg);
}

}	// resource
}	// acorn
