// acorn
#include "resource/sounddata.h"

// FIXME:
#include <iostream>

namespace acorn {
namespace resource {

acorn::Type SoundData::type = std::type_index(typeid(resource::SoundData));

SoundData::SoundData(std::shared_ptr<acorn::Data> data) : data(data) {
	stream = stb_vorbis_open_memory((unsigned char*)data->getData(), data->getSize(), NULL, NULL);

	if (!stream) {
		throw std::runtime_error("Could not load sound from memory.");
	}

	// NOTE: OGG bit rate defaults to 16 bit
	auto info = stb_vorbis_get_info(stream);
	sampleRate = info.sample_rate;
	sampleSize = 16;
	sampleCount = stb_vorbis_stream_length_in_samples(stream);
	channels = info.channels;
	duration = (double)stb_vorbis_stream_length_in_seconds(stream);

	// try {
	// 	buffer = new char[size];
	// } catch (std::bad_alloc&) {
	// 	throw std::runtime_error("out of memory");
	// }

	buffer = (short *) malloc(sampleCount * channels * sizeof(short));
	stb_vorbis_get_samples_short_interleaved(stream, info.channels, (short *)buffer, sampleCount * channels);

	stb_vorbis_close(stream);
}

SoundData::~SoundData() {
	if (buffer != 0) {
	  delete[] (char *)buffer;
	}
}

int SoundData::getChannels() const {
	return channels;
}

double SoundData::getDuration() const {
	return duration;
}

int SoundData::getSampleCount() const {
	return sampleCount;
}

int SoundData::getSampleRate() const {
	return sampleRate;
}

int SoundData::getSampleSize() const {
	return sampleSize;
}

}	// resource
}	// acorn
