// acorn
#include "common/exception.h"
#include "graphics/opengl/image.h"

namespace acorn {
namespace graphics {
namespace opengl {

Image::Image(std::shared_ptr<image::ImageData> imagedata) :
    acorn::graphics::Image(Texture::TextureType::Texture2D, imagedata),
	texture(0) {

	// NOTE: order of operations
	// 1. Generate Texture (glGenTextures)			[x]
	// 2. Bind Texture to unit (glBindTexture)		[x]
	// 3. Set Filter (glTexParameterf)				[x]
	// 4. Set Wrap (glTexParameterf)				[x]
	// 5. Set Mipmaps								[-]
	// 6. Clear/Check errors						[X]
	// 7. Store texture in VRAM (glTexImage2D)		[x]

	// create and bind texture
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	// set the texture wrapping parameters
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// set the texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glCheckError();

	// generate texture image on currently bown texture object
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, getWidth(), getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, imagedata->getData());
	//glGenerateMipmap(GL_TEXTURE_2D);

	GLenum glerr = glGetError();
	if (glerr != GL_NO_ERROR) {
		printf("OpenGL error %08x\n", glerr);
		throw acorn::Exception("Cannot create image.");
	}

	// finally, unbind the texture
	glBindTexture(GL_TEXTURE_2D, 0);
}

Image::~Image() {
	if (texture != 0) {
		glDeleteTextures(1, &texture);
	}

	texture = 0;
}

void Image::setFilter(const Texture::FilterMode mode) noexcept {
	Texture::setFilter(mode);

	GLuint f = 0;
	switch (mode) {
	case Texture::FilterMode::Nearest: f = GL_NEAREST; break;
	case Texture::FilterMode::Linear: f = GL_LINEAR; break;
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, f);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, f);
}

void Image::setWrap(const Texture::WrapMode mode) noexcept {
	Texture::setWrap(mode);

	GLuint w = 0;
	switch (mode) {
	case Texture::WrapMode::Repeat: w = GL_REPEAT; break;
	case Texture::WrapMode::Mirror: w = GL_MIRRORED_REPEAT; break;
	case Texture::WrapMode::Clamp: w = GL_CLAMP_TO_EDGE; break;
	case Texture::WrapMode::Border: w = GL_CLAMP_TO_BORDER; break;
	}

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, w);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, w);
}

const void *Image::getHandle() const {
	return &texture;
}

}	// opengl
}	// graphics
}	// acorn
