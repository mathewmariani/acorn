#ifndef ACORN_GRAPHICS_BUFFER_H
#define ACORN_GRAPHICS_BUFFER_H

// acorn
#include "graphics/resource.h"

namespace acorn {
namespace graphics {

class Buffer : public Resource {
public:
	enum class BufferType {
		Vertex,
		Index
	};	// buffertype

	enum class BufferUsage {
		Stream,
		Dynamic,
		Static
	};	// bufferusage

public:
	Buffer(size_t size, BufferType type, BufferUsage usage);
	virtual ~Buffer();

public:
	size_t getSize() const noexcept;
	BufferType getType() const noexcept;
	BufferUsage getUsage() const noexcept;

	virtual void *bind() = 0;
	virtual size_t unbind(size_t usedsize) = 0;
	virtual void nextFrame() = 0;

protected:
	size_t size;
	BufferType type;
	BufferUsage usage;
};	// buffer

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_BUFFER_H
