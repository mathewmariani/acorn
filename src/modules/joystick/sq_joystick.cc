// acorn
#include "joystick/sq_joystick.h"
#include "joystick/joystick.h"
#include "joystick/joystickmodule.h"

namespace acorn {
namespace joystick {

#define instance() (Module::getInstance<joystick::JoystickModule>())

// static functions

static SQInteger wrap_getDevice(SQVM *v) {
	int index;
	sq_getinteger(v, 2, (SQInteger *) &index);
	sq_createinstance(v, 1);

	sq_remove(v, 2);
	sq_remove(v, 1);

	printf("%s\n", __FUNCTION__);
	sqx_printstack(v);

	sqx_pushinstance(v, Joystick::type, instance()->getJoystick(index));
	return 1;
}

static SQInteger wrap_getDevices(SQVM *v) {
	sq_createinstance(v, 1);
	sq_remove(v, 1);

	sq_newarray(v, 0);
	for (auto i = 0; i < instance()->getJoystickCount(); i++) {
		sqx_pushinstance(v, Joystick::type, instance()->getJoystick(i));
		sq_push(v, 1);
		sq_arrayappend(v, -2);
	}
	sq_remove(v, 1);
	return 1;
}

static SQInteger wrap_getJoystickCount(SQVM *v) {
	sq_pushinteger(v, instance()->getJoystickCount());
	return 1;
}

// non-static functions

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Joystick cannot be constructed directly.");
}

static SQInteger wrap_getName(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushstring(v, spt->getName().c_str(), -1);
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_getGUID(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushstring(v, spt->getGUID().c_str(), -1);
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_getInstanceID(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushinteger(v, spt->getInstanceID());
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_isAttached(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushbool(v, spt->isAttached());
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_isGameController(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushbool(v, spt->isGameController());
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_getAxisCount(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushinteger(v, spt->getAxisCount());
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_getBallCount(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushinteger(v, spt->getBallCount());
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_getHatCount(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushinteger(v, spt->getHatCount());
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_getButtonCount(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushinteger(v, spt->getButtonCount());
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_getPowerState(SQVM *v) {
	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushinteger(v, (int) spt->getPowerState());
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_getHat(SQVM *v) {
	int index;
	sq_getinteger(v, 2, (SQInteger *) &index);

	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushinteger(v, (int) spt->getHat(index));
	} else {
		return SQ_ERROR;
	}
	return 1;
}

static SQInteger wrap_getButton(SQVM *v) {
	int index;
	sq_getinteger(v, 2, (SQInteger *) &index);

	std::weak_ptr<Joystick> joystick(sqx_checkinstance<Joystick>(v, 1));
	if (auto spt = joystick.lock()) {
		sq_pushinteger(v, (int) spt->getButton(index));
	} else {
		return SQ_ERROR;
	}
	return 1;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),

	// static
	_DECL_FUNC(getDevice, 2, "yn", true),
	_DECL_FUNC(getDevices, 1, "y", true),
	_DECL_FUNC(getJoystickCount, 1, "y", true),

	// non-static
	_DECL_FUNC(getName, 1, "x", false),
	_DECL_FUNC(getGUID, 1, "x", false),
	_DECL_FUNC(getInstanceID, 1, "x", false),

	_DECL_FUNC(isAttached, 1, "x", false),
	_DECL_FUNC(isGameController, 1, "x", false),

	_DECL_FUNC(getAxisCount, 1, "x", false),
	_DECL_FUNC(getBallCount, 1, "x", false),
	_DECL_FUNC(getHatCount, 1, "x", false),
	_DECL_FUNC(getButtonCount, 1, "x", false),
	_DECL_FUNC(getPowerState, 1, "x", false),

	_DECL_FUNC(getHat, 2, "xn", false),
	_DECL_FUNC(getButton, 2, "xn", false),

	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_acorn_joystick(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Joystick";
	reg.functions = functions;

	return sqx_registerclass(v, Joystick::type, reg);
}

}	// joystick
}	// acorn
