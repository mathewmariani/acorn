#ifndef ACORN_PLATFORM_H
#define ACORN_PLATFORM_H

// stubbed
#define STUBBED(x) \
	fprintf(stderr, "STUBBED: %s at %s (%s:%d)\n", x, __FUNCTION__, __FILE__, __LINE__);

// platform
#if defined(WIN32) || defined(_WIN32)
	#define OS_WINDOWS 1
#elif defined(__APPLE__)
	#include <TargetConditionals.h>
	#if defined(TARGET_OS_MAC)
		#define OS_MACOS 1
	#elif defined(TARGET_OS_IPHONE)
		#define OS_IOS 1
	#endif
#elif defined(__linux__)
	#define OS_LINUX 1
#elif defined(__FreeBSD__)
	#define OS_FREEBSD 1
#else
	#error Could not deduce platform
#endif

// dll import, exports
#if defined(OS_WINDOWS)
	#define ACORN_API __declspec(dllexport)
#else
	#define ACORN_API
#endif

// windows specific
#if defined(OS_WINDOWS)
	#ifndef NOMINMAX
		#define NOMINMAX
	#endif
#endif

// filesystem
#define ACORN_APPDATA_FOLDER "acorn"
#if defined(OS_WINDOWS)
	#define ACORN_PATH_SEPARATOR "\\"
#else
	#define ACORN_PATH_SEPARATOR "/"
#endif

// debug
#if defined(_DEBUG)
	#define DEBUG 1
#endif

// integer types
#include <stdint.h>
namespace acorn {
	typedef int8_t int8;
	typedef uint8_t uint8;
	typedef int16_t int16;
	typedef uint16_t uint16;
	typedef int32_t int32;
	typedef uint32_t uint32;
	typedef int64_t int64;
	typedef uint64_t uint64;
}

// modules

#endif	// ACORN_PLATFORM_H
