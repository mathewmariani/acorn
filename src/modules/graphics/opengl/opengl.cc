// STL
#include <iostream>
#include <string>

// acorn
#include "graphics/opengl/opengl.h"

// NOTE: used for debugging
GLenum glCheckError_(const char *file, int line) {
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR) {
		std::string error;
		switch (errorCode) {
		case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
		case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
		case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
		case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
		case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
		case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}

namespace acorn {
namespace graphics {
namespace opengl {

GLenum map(const Buffer::BufferType bufferType) {
	switch (bufferType) {
	case Buffer::BufferType::Vertex: return GL_ARRAY_BUFFER;
	case Buffer::BufferType::Index: return GL_ELEMENT_ARRAY_BUFFER;
	}
}

GLenum map(const Buffer::BufferUsage bufferUsage) {
	switch (bufferUsage) {
	case Buffer::BufferUsage::Stream: return GL_STREAM_DRAW;
	case Buffer::BufferUsage::Dynamic: return GL_DYNAMIC_DRAW;
	case Buffer::BufferUsage::Static: return GL_STATIC_DRAW;
	}
}

GLenum map(const Texture::TextureType textureType) {
	switch (textureType) {
	case Texture::TextureType::Texture2D: return GL_TEXTURE_2D;
	}
}

GLenum map(const Texture::FilterMode filterMode) {
	switch (filterMode) {
	case Texture::FilterMode::Nearest: return GL_NEAREST;
	case Texture::FilterMode::Linear: return GL_LINEAR;
	}
}

GLenum map(const Texture::WrapMode wrapMode) {
	switch (wrapMode) {
	case Texture::WrapMode::Repeat: return GL_REPEAT;
	case Texture::WrapMode::Mirror: return GL_MIRRORED_REPEAT;
	case Texture::WrapMode::Clamp: return GL_CLAMP_TO_EDGE;
	case Texture::WrapMode::Border: return GL_CLAMP_TO_BORDER;
	}
}

} // opengl
} // graphics
} // acorn
