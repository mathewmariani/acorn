R"squirrel"#(

function boot() {
	# initialize filesystem (required for PHYSFS)
	local arg0 = acorn.args[1]	# FIXME: args[0] is null
	acorn.filesystem.init(arg0)

	local exepath = acorn.filesystem.getCurrentWorkingDirectory()
	acorn.filesystem.setSource(exepath)

  # ...
  local os = acorn.system.getOS()
	print(os+"\n")

	# ...
	local powerstate = acorn.system.getPowerInfo()
	# print(powerstate.seconds+"\n")	# FIXME: seconds does not exist

  # ...

	if (acorn.filesystem.exists("main.nut")) {
		require("main.nut")
	} else {
		print("no game found!\n")
	}
}

# FIXME: use call, acall, pcall, or xcall
function createHandlers() {
  acorn.handlers <- {
		# mouse
		mousepressed = function(x, y, b) {
			if ("mousepressed" in acorn) acorn.mousepressed(x, y, b)
		},
		mousereleased = function(x, y, b) {
			if ("mousereleased" in acorn) acorn.mousereleased(x, y, b)
		},
		mousemotion = function(x, y, xr, yr) {
			if ("mousemotion" in acorn) acorn.mousemotion(x, y, xr, yr)
		},
		mousewheel = function(x, y) {
			if ("mousewheel" in acorn) acorn.mousewheel(x, y)
		},

		# keyboard
		keypressed = function(k) {
			if ("keypressed" in acorn) acorn.keypressed(k)
		},
		keyreleased = function(k) {
			if ("keyreleased" in acorn) acorn.keyreleased(k)
		},

		# joystick
		joystickadded = function(w) {
			if ("joystickadded" in acorn) acorn.joystickadded(w)
		},
		joystickremoved = function(w) {
			if ("joystickremoved" in acorn) acorn.joystickremoved(w)
		},
		joystickbuttonpressed = function(w, b) {
			if ("joystickpressed" in acorn) acorn.joystickpressed(w, b)
		},
		joystickbuttonreleased = function(w, b) {
			if ("joystickreleased" in acorn) acorn.joystickreleased(w, b)
		},

		# controller
		controllerbuttonup = function(w, b) {
			if ("controllerpressed" in acorn) acorn.controllerpressed(w, b)
		},
		controllerbuttondown = function(w, b) {
			if ("controllerreleased" in acorn) acorn.controllerreleased(w, b)
		},
	}
}

function init() {
	local config = {
		fullscreen = false,
		borderless = false,
		resizable = true,
		highdpi = true,
		centered = true,
		vsync = false
	}

	if ("window" in acorn) {
		if (!acorn.window.setWindow(300, 280, config)) {
			throw "could not create window."
		}
		acorn.window.setTitle("acorn2d")
	}

	# FIXME: atom extension for floating point numbers.
	# num = 1.e-2
	# num = 1.e2
	# print(num+"\n")

	# create event handlers
	createHandlers();
}

function run() {

	if ("load" in acorn) {
		acorn.load()
	}

	if ("timer" in acorn) {
		acorn.timer.step()
	}

	local dt = 0.0
	while (true) {

		if ("event" in acorn) {
	    acorn.event.pump()
	    local event = acorn.event.poll()
	    if (event != null) {
				event.args.insert(0, this)
				acorn.handlers[event.name].acall(event.args)
	    }
	  }

		if ("timer" in acorn) {
			acorn.timer.step()
			dt = acorn.timer.getDeltaTime()
		}

		if ("update" in acorn) {
			acorn.update(dt)
		}

		if ("graphics" in acorn) {
			acorn.graphics.clear(0, 0, 0, 0)
			if ("draw" in acorn) { acorn.draw() }
			acorn.graphics.present()
		}

		if ("timer" in acorn) {
			acorn.timer.sleep(1)
		}
	}
}

# NOTE: error handler
local errhandler = function(err) {
	local level = 1
	local output = ""
	local stackInfo = getstackinfos(level)
	while (stackInfo) {
		output += stackInfo.func + "() at " + stackInfo.src + " line [" + stackInfo.line + "]: "+err+"\n"
		stackInfo = getstackinfos(level++)
	}
	print(output)
}

local retval = function() {
  local result = 0
  try { boot() } catch (e) { errhandler(e); return 1 }
  try { init() } catch (e) { errhandler(e); return 1 }
  try { run() } catch (e) { errhandler(e); return 1 }
  return result
}

seterrorhandler(errhandler)

return retval()

#)squirrel"#"
