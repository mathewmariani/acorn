// acorn
#include "common/log.h"
#include "audio/audio.h"

// stb
#include "libraries/stb/stb_vorbis.h"

namespace acorn {
namespace audio {

acorn::Type Audio::type = std::type_index(typeid(audio::Audio));

Audio::Audio() {
	log_debug("Audio module initialized.");

	// initialize a device with default settings
	device = alcOpenDevice(nullptr);

	if (device == nullptr) {
		log_error("Audio device could not be opened");
		return;
	}

	context = alcCreateContext(device, nullptr);

	if (context == nullptr) {
		log_error("Could not initialize audio context");
	}

	if (alcMakeContextCurrent(context) == ALC_FALSE || alcGetError(device) != ALC_NO_ERROR) {
		if (context != NULL) {
		  alcDestroyContext(context);
		}

		log_error("Could not make audio context current");
	}
}

Audio::~Audio() {
	log_debug("Audio module deinitialized.");

	alcMakeContextCurrent(nullptr);
	alcDestroyContext(context);
	alcCloseDevice(device);
}

void Audio::play(const Source &source) {

}

void Audio::pause(const Source &source) {

}

void Audio::resume(const Source &source) {

}

void Audio::stop(const Source &source) {

}

//void play(Source* source) {
//    alSourcePlay(source->source);
//}
//
//void pause(Source* source) {
//    alSourcePause(source->source);
//}
//
//void resume(Source* source) {
//    ALenum state;
//    alGetSourcei(source->source, AL_SOURCE_STATE, &state);
//    if (state == AL_PAUSED) alSourcePlay(source->source);
//}
//
//void stop(Source* source) {
//    alSourceStop(source->source);
//}

}	// audio
}	// acorn
