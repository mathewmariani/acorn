#ifndef ACORN_SQ_GRAPHICS_FRAMEBUFFER_H
#define ACORN_SQ_GRAPHICS_FRAMEBUFFER_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace graphics {

extern "C" int sq_register_framebuffer(SQVM *v);

}	// graphics
}	// acorn

#endif	// ACORN_SQ_GRAPHICS_FRAMEBUFFER_H
