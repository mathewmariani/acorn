#ifndef ACORN_CURSOR_H
#define ACORN_CURSOR_H

// C/C++
#include <string>
#include <unordered_map>
#include <type_traits>

// acorn
#include "common/object.h"
#include "common/platform.h"

// SDL
#include <SDL.h>

namespace acorn {
namespace mouse {

class Cursor : public acorn::Object {
public:
	static acorn::Type type;

public:
  enum class SystemCursorType {
		ARROW = 0,
		IBEAM = 1,
		WAIT = 2,
		CROSSHAIR = 3,
		WAITARROW = 4,
		SIZENWSE = 5,
		SIZENESW = 6,
		SIZEWE = 7,
		SIZENS = 8,
		SIZEALL = 9,
		NO = 10,
		HAND = 11,

		// always last.
		SYSTEMCURSORTYPE_MAX_ENUM
	};	// systemcursortype

public:
	Cursor(SystemCursorType type);
	virtual ~Cursor();

public:
	// use this function to get the pointer to the underlying joystic object.
	// NOTE: cast as SDL_Cursor*
	const void *getHandle() const noexcept;

private:
	// managed sdl resources
	SDL_Cursor *cursor;

public:
	// FIXME: I don't know if this is needed
	//static std::unordered_map<std::string, SystemCursorType> systemcursorTable;
};	// cursor

}	// mouse
}	// acorn

#endif	// ACORN_CURSOR_H
