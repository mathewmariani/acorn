#ifndef ACORN_GRAPHICS_SHADER_H
#define ACORN_GRAPHICS_SHADER_H

// C/C++
#include <memory>
#include <string>
#include <unordered_map>

// acorn
#include "common/object.h"
#include "graphics/shaderstage.h"
#include "graphics/resource.h"

namespace acorn {
namespace graphics {

class Shader : public acorn::Object, public Resource {
public:
	static acorn::Type type;

public:
	// strong reference
	static std::shared_ptr<Shader> current;

public:
	Shader(std::shared_ptr<ShaderStage> vertex, std::shared_ptr<ShaderStage> fragment);
	virtual ~Shader();

public:
	virtual void bind() = 0;
	virtual void unbind() = 0;

	// NOTE: to be implemented
	// 1. uniforms
	// 2. warnings

	virtual const int getAttributeLocation(const std::string &name) = 0;
	virtual const int getGetUniformLocation(const std::string &name) = 0;

	//void updateUniforms();

	virtual void sendFloats(int location, int count, const float *value) = 0;
	virtual void sendInts(int location, int count, const int *value) = 0;
	virtual void sendMatrices(int location, int count, const float *value) = 0;

private:
	bool createShader();

protected:
	// strong reference
	std::shared_ptr<ShaderStage> vertexStage;
	std::shared_ptr<ShaderStage> fragmentStage;

	std::unordered_map<std::string, int> uniforms;
	std::unordered_map<std::string, int> attributes;
};	// shader

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_SHADER_H
