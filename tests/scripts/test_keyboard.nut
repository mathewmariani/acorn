R"squirrel"#(

# keyboard
describe("acorn.keyboard", function() {
	# typeof
	it("is a typeof of table", function() {
		assert.is_typeof(acorn.keyboard, "table")
	})

	# enums
	it("is a typeof of class", function() {
		assert.is_typeof(Scancode, "table")
	})

	# functions
	it("contains a function `isPressed()`", function() {
		assert.is_in("isPressed", acorn.keyboard)
		assert.is_typeof(acorn.keyboard.isPressed, "function")
	})
	it("contains a function `isScreenKeyboardShown()`", function() {
		assert.is_in("isScreenKeyboardShown", acorn.keyboard)
		assert.is_typeof(acorn.keyboard.isScreenKeyboardShown, "function")
	})
	it("contains a function `isTextInputActive()`", function() {
		assert.is_in("isTextInputActive", acorn.keyboard)
		assert.is_typeof(acorn.keyboard.isTextInputActive, "function")
	})
	it("contains a function `setTextInput()`", function() {
		assert.is_in("setTextInput", acorn.keyboard)
		assert.is_typeof(acorn.keyboard.setTextInput, "function")
	})
})

#)squirrel"#"
