#ifndef ACORN_SQ_TRANSFORM_H
#define ACORN_SQ_TRANSFORM_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace math {

extern "C" int sq_acorn_transform(SQVM *v);

}	// math
}	// acorn

#endif	// ACORN_SQ_TRANSFORM_H
