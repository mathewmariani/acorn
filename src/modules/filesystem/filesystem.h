#ifndef ACORN_FILESYSTEM_H
#define ACORN_FILESYSTEM_H

// STL
#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"

// acorn.filesystem
#include "filesystem/filedata.h"
#include "filesystem/file.h"

namespace acorn {
namespace filesystem {

class Filesystem : public acorn::Module {
public:
	static acorn::Type type;

	enum class FileType {
		FILE,
		DIRECTORY,
		SYMLINK,
		OTHER,

		// always last.
		FILETYPE_MAX_ENUM
	};	// filetype

	enum class Mode {
		READ,
		WRITE,
		APPEND,

		// always last.
		MODE_MAX_ENUM
	};	// mode

	struct Stat {
		int64 filesize;
		int64 modtime;
		int64 createtime;
		FileType type;
	};	// stat


public:
	Filesystem();
	virtual ~Filesystem();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:
	// use this function to initialize the PhysicsFS library.
	// this must be called before any other PhysicsFS function.
	void init(const char *arg0);

	// use this function to set the identity (organization and application) used for creating a write directory.
	bool setIdentity(const std::string &org, const std::string &app) noexcept;

	// use this function to tell PhysicsFS where it may write files.
	bool setSaveIdentity(const std::string &identity) noexcept;

	// NOTE: may not be useful
	bool setSource(const std::string &path) const noexcept;

	// use this function to determine if a file exists in the search path.
	bool exists(const std::string &path) const noexcept;

	// use this function to determine if a file in the search path is really a directory.
	bool isDirectory(const std::string &filename) const noexcept;

	// use this function to determine if a file in the search path is really a symbolic link.
	bool isSymbolicLink(const std::string &filename) const noexcept;

	// use this function to determine if a file in the search path is really a file.
	bool isFile(const std::string &filename) const noexcept;

	// use this function to get meta data for a file or directory.
	bool getStat(const std::string &path, Stat &stat) const noexcept;

	// use this function to figure out where in the search path a file resides.
	const std::string getRealDirectory(const std::string &filename) const;

	// use this function to get the path where application data is written.
	const std::string getAppdataDirectory() noexcept;

	// use this function to get the path where the application resides.
	const std::string getBaseDirectory() noexcept;

	// use this function to get the current path of the application.
	const std::string getCurrentWorkingDirectory() noexcept;

	// use this function to get the path where the executable resides.
	const std::string getExecutablePath() noexcept;

	// use this function to get the user-and-app-specific path where files can be written.
	const std::string getPrefDirectory() noexcept;

	// use this function to get the path where user's home directory resides.
	const std::string getUserDirectory() const;

	// use this function to get the path where PhysicsFS will allow file writing.
	const std::string getWriteDirectory() const;

	// use this function to add an archive or directory to the search path.
	bool mount(const std::string &path) const noexcept;

	// use this function to remove a directory or archive from the search path.
	bool unmount(const std::string &path) const noexcept;

	// use this function to get a listing of the search path.
	void getSearchPath(std::vector<std::string> &list);

	// use this function to create a directory.
	bool makeDirectory(const std::string &dirpath) const noexcept;

	// use this function to delete a file or directory.
	bool remove(const std::string &filename) const noexcept;

	// use this function to get a file listing of a search path's directory.
	void enumerateFiles(const std::string &path, std::vector<std::string> &list);

public:
	std::shared_ptr<File> read(const std::string &filename, int64 size) const;

public:
	// factory functions
	std::shared_ptr<File> newFile() const;
	std::shared_ptr<FileData> newFileData() const;

private:
	std::string appdatadir;
	std::string basedir;
	std::string currentdir;
	std::string exepath;
	std::string prefdir;

	std::string saveidentity;

public:
	// FIXME: I don't know if this is needed
	//static std::unordered_map<std::string, FileType> filetypeTable;
};	// filesystem

}	// filesystem
}	// acorn

#endif	// ACORN_FILESYSTEM_H
