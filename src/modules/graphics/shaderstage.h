#ifndef ACORN_GRAPHICS_SHADERSTAGE_H
#define ACORN_GRAPHICS_SHADERSTAGE_H

// STL
#include <string>

// acorn
#include "graphics/resource.h"

namespace acorn {
namespace graphics {

class ShaderStage : public Resource {
public:
	enum class StageType {
		Vertex,
		Fragment
	};  // shadertype

public:
	ShaderStage(StageType glstage, const std::string &glsl);
	~ShaderStage();

protected:
	StageType stageType;
	std::string source;
};	// shaderstage

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_SHADERSTAGE_H
