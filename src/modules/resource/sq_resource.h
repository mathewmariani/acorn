#ifndef ACORN_SQ_RESOURCE_H
#define ACORN_SQ_RESOURCE_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace resource {

extern "C" int sq_acorn_resource(SQVM *v);

}	// resource
}	// acorn

#endif	// ACORN_SQ_RESOURCE_H
