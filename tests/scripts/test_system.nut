R"squirrel"#(

# system
describe("acorn.system", function() {
	# clipboard
	describe("acorn.system.Clipboard", function() {
		# typeof
		it("is a typeof of class", function() {
			assert.is_typeof(Clipboard, "class")
		})

		# functions
		it("contains a function `constructor()`", function() {
			assert.is_in("constructor", Clipboard)
			assert.is_typeof(Clipboard.constructor, "function")
		})
		it("contains a function `setText()`", function() {
			assert.is_in("setText", Clipboard)
			assert.is_typeof(Clipboard.setText, "function")
		})
		it("contains a function `getText()`", function() {
			assert.is_in("getText", Clipboard)
			assert.is_typeof(Clipboard.getText, "function")
		})
		it("contains a function `hasText()`", function() {
			assert.is_in("hasText", Clipboard)
			assert.is_typeof(Clipboard.hasText, "function")
		})
	})

	# platform
	describe("acorn.system.Platform", function() {
		# typeof
		it("is a typeof of class", function() {
			assert.is_typeof(Platform, "class")
		})

		# functions
		it("contains a function `constructor()`", function() {
			assert.is_in("constructor", Platform)
			assert.is_typeof(Platform.constructor, "function")
		})
		it("contains a function `getName()`", function() {
			assert.is_in("getName", Platform)
			assert.is_typeof(Platform.getName, "function")
			assert.is_string(Platform.getName())
		})
		it("contains a function `getCores()`", function() {
			assert.is_in("getCores", Platform)
			assert.is_typeof(Platform.getCores, "function")
			assert.is_integer(Platform.getMemory())
		})
		it("contains a function `getMemory()`", function() {
			assert.is_in("getMemory", Platform)
			assert.is_typeof(Platform.getMemory, "function")
			assert.is_integer(Platform.getMemory())
		})
		it("contains a function `getPowerState()`", function() {
			assert.is_in("getPowerState", Platform)
			assert.is_typeof(Platform.getPowerState, "function")

			local _powerstate = Platform.getPowerState()
			assert.is_typeof(_powerstate, "table")

			assert.is_in("state", _powerstate)
			assert.is_integer(_powerstate.state)

			assert.is_in("seconds", _powerstate)
			assert.is_integer(_powerstate.seconds)

			assert.is_in("percent", _powerstate)
			assert.is_integer(_powerstate.percent)
		})
		it("contains a function `isWindows()`", function() {
			assert.is_in("isWindows", Platform)
			assert.is_typeof(Platform.isWindows, "function")
			assert.is_bool(Platform.isWindows())
		})
		it("contains a function `isMacOS()`", function() {
			assert.is_in("isMacOS", Platform)
			assert.is_typeof(Platform.isMacOS, "function")
			assert.is_bool(Platform.isMacOS())
		})
		it("contains a function `isLinux()`", function() {
			assert.is_in("isLinux", Platform)
			assert.is_typeof(Platform.isLinux, "function")
			assert.is_bool(Platform.isLinux())
		})
	})

	# powerstate
	describe("acorn.system.PowerState", function() {
		# typeof
		# FIXME: why does this not work?
		# it("is a typeof of table", function() {
		# 	assert.is_typeof(PowerState, "table")
		# })

		# index
		it("contains an index `unknown`", function() {
			assert.equals(PowerState.unknown, 0)
		})
		it("contains an index `on_battery`", function() {
			assert.equals(PowerState.on_battery, 1)
		})
		it("contains an index `no_battery`", function() {
			assert.equals(PowerState.no_battery, 2)
		})
		it("contains an index `charged`", function() {
			assert.equals(PowerState.charged, 3)
		})
		it("contains an index `charging`", function() {
			assert.equals(PowerState.charging, 4)
		})
	})
})

#)squirrel"#"
