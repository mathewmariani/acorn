#ifndef ACORN_SQ_IMAGE_IMAGEDATA_H
#define ACORN_SQ_IMAGE_IMAGEDATA_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace image {

extern "C" int sq_register_imagedata(SQVM *v);

}	// image
}	// acorn

#endif	// ACORN_SQ_IMAGE_IMAGEDATA_H
