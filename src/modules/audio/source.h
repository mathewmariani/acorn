#ifndef ACORN_AUDIO_SOURCE_H
#define ACORN_AUDIO_SOURCE_H

// C/C++
#include <memory>
#include <string>

// acorn
#include "common/object.h"
#include "resource/sounddata.h"

// OpenAL
#if defined(OS_APPLE)
	#include "OpenAL/al.h"
	#include "OpenAL/alc.h"
#else
	#include <al.h>
	#include <alc.h>
#endif

namespace acorn {
namespace audio {

class Source : public acorn::Object {
public:
	static acorn::Type type;

public:
	Source(std::shared_ptr<resource::SoundData> sounddata);
	virtual ~Source();

private:
	bool create();
	ALenum getFormat();
	
private:
	std::shared_ptr<resource::SoundData> sounddata;

	// OpenAL
	ALuint source;
	ALuint buffer;
	ALenum format;
};	// file

}	// audio
}	// acorn

#endif	// ACORN_AUDIO_SOURCE_H
