---
layout: docs
title: Third Parties
description:
---

Acorn is currently built using these libraries.

- [Squirrel](http://squirrel-lang.org/) - Scripting language.
- [OpenGL](https://www.opengl.org/) - Hardware accelerated graphics.
- [SDL2](https://www.libsdl.org) - OS abstraction and provides low level access to audio, keyboard, mouse, joystick.
- [OpenAL Soft](http://kcat.strangesoft.net/openal.html) - Audio playback.
- [GLM](https://glm.g-truc.net/) - Mathematics library for graphics software.
- [STB](https://github.com/nothings/stb) - OGG support, image loading/decoding, and parse, decode, and rasterize characters from truetype fonts.
