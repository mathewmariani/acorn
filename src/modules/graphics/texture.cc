// acorn
#include "common/exception.h"

// OpenGL
#include "graphics/graphics.h"
#include "graphics/texture.h"

namespace acorn {
namespace graphics {

acorn::Type Texture::type = std::type_index(typeid(graphics::Texture));

Texture::Texture(TextureType type) :
	width(0),
	height(0),
	textureType(type),
	filter(),
	wrap() {

}

Texture::~Texture() {

}

void Texture::initializeQuad() noexcept {
	quad = std::make_shared<Quad>(
		0.0f, 0.0f, width, height, width, height
		);
}

int Texture::getWidth() const noexcept {
	return width;
}

int Texture::getHeight() const noexcept {
	return height;
}

const Texture::FilterMode Texture::getFilter() const noexcept {
	return filter;
}

void Texture::setFilter(const FilterMode mode) noexcept {
	filter = mode;
}

const Texture::WrapMode Texture::getWrap() const noexcept {
	return wrap;
}

void Texture::setWrap(const WrapMode mode) noexcept {
	wrap = mode;
}

std::shared_ptr<Quad> Texture::getQuad() const noexcept {
	return quad;
}

void Texture::draw(std::shared_ptr<Graphics> gfx, const glm::mat4 &m) {
	draw(gfx, quad, m);
}

void Texture::draw(std::shared_ptr<Graphics> gfx, std::shared_ptr<Quad> quad, const glm::mat4 &m) {

	// TODO: use the matrix (m) for something...
	gfx->push(m);

	// FIXME: remove all opengl variables
	Graphics::DrawCommand cmd;
	cmd.mode = GL_TRIANGLE_STRIP;
	cmd.first = 0;
	cmd.count = 4;
	cmd.texture = this;

	auto buffer = gfx->ext_requestDraw(cmd);

	//const Vertex *_vertices = getQuad()->getVertices();
	//auto vertices = (float *) buffer->bind();
	//memcpy(vertices, _vertices, sizeof(Vertex) * 4);

	const auto *v = getQuad()->getVertices();
	auto vertices = (Vertex *) buffer->bind();

	for (int i = 0; i < 4; ++i) {
		// vertex position
		glm::vec4 temp{ v[i].x, v[i].y, 0.0f, 1.0f };
		temp = m * temp;

		// vertex position
		vertices[i].x = temp.x;
		vertices[i].y = temp.y;

		// vertex texcoord
		vertices[i].u = v[i].u;
		vertices[i].v = v[i].v;
	}
}

}	// graphics
}	// acorn
