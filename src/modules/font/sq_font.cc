// acorn
#include "font/sq_font.h"
#include "font/font.h"

namespace acorn {
namespace font {

#define instance() (Module::getInstance<font::Font>())

// NOTE: functions for acorn.font
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.font
const static SQFunction types[] = {
    nullptr
};

extern "C" int sq_acorn_font(SQVM *v) {
	auto instance = std::make_shared<font::Font>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize font module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "font";

	return acorn::sqx_registermodule_ext(v, reg);
}

}	// font
}	// acorn
