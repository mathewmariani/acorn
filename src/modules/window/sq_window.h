#ifndef ACORN_SQ_WINDOW_H
#define ACORN_SQ_WINDOW_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace window {

extern "C" int sq_acorn_window(SQVM *v);

}	// window
}	// acorn

#endif	// ACORN_SQ_WINDOW_H
