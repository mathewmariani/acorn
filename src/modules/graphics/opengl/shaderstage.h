#ifndef ACORN_GRAPHICS_OPENGL_SHADERSTAGE_H
#define ACORN_GRAPHICS_OPENGL_SHADERSTAGE_H

// acorn
#include "graphics/opengl/opengl.h"
#include "graphics/shaderstage.h"

namespace acorn {
namespace graphics {
namespace opengl {

class ShaderStage final : public graphics::ShaderStage {
public:
	ShaderStage(ShaderStage::StageType stage, const std::string &glsl);
	virtual ~ShaderStage();

private:
	bool compileShader();

public:
	// managed opengl resource
	const void *getHandle() const override;

private:
	GLuint glshader;
};	// shaderstage

}	// opengl
}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_OPENGL_SHADERSTAGE_H
