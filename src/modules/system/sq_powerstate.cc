// acorn
#include "system/sq_powerstate.h"
#include "system/system.h"

namespace acorn {
namespace system {

EnumRegistry reg[] = {
	{ "unknown", (int) System::PowerState::UKNOWN },
	{ "on_battery", (int) System::PowerState::ON_BATTERY },
	{ "no_battery", (int) System::PowerState::NO_BATTERY },
	{ "charged", (int) System::PowerState::CHARGING },
	{ "charging", (int) System::PowerState::CHARGED },
};

extern "C" int sq_acorn_powerstate(SQVM *v) {
	return sqx_registerenum(v, "PowerState", reg);
}

}	// system
}	// acorn
