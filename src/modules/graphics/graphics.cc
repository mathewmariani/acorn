// acorn
#include "common/exception.h"
#include "common/log.h"
#include "graphics/graphics.h"
#include "window/window.h"
#include "graphics/opengl/opengl.h"

// glm
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// NOTE: not needed
#include <iostream>

// TODO: implement better error handling
// #define GL_CHECK() { \
// 	GLenum err; while ((err = glGetError()) != GL_NO_ERROR) { \
// 	printf("OpenGL error %08x, at %s:%i\n", err, __FILE__, __LINE__); \
// 	}}

namespace acorn {
namespace graphics {

acorn::Type Graphics::type = std::type_index(typeid(graphics::Graphics));

Graphics::Graphics() :
	width(0),
	height(0),
	vao(0) {
	log_debug("Graphics module initialized.");

	color = { 1.0f, 1.0f, 1.0f, 1.0f };

	matrices.projection.push(glm::mat4(1.0f));
	matrices.transform.push(glm::mat4(1.0f));

	std::shared_ptr<window::Window> window = Module::getInstance<window::Window>();
	if (window.get() != nullptr) {
		window->setGraphicsDevice(shared_from_this());
	}
}

Graphics::~Graphics() {
	log_debug("Graphics module deinitialized.");

	if (vao > 0) {
		glDeleteVertexArrays(1, &vao);
	}

	delete buffer;
}

Graphics::RendererInfo Graphics::getRendererInfo() const {
	RendererInfo info;
	info.device = (const char *) glGetString(GL_RENDERER);
	info.name = (const char *) glGetString(GL_SHADING_LANGUAGE_VERSION);
	info.vendor = (const char *) glGetString(GL_VENDOR);
	info.version = (const char *) glGetString(GL_VERSION);

	return info;
}

int Graphics::getWidth() const noexcept {
	return width;
}

int Graphics::getHeight() const noexcept {
	return height;
}

bool Graphics::setMode(int width, int height) {
	if (gl3wInit()) {
		log_error("failed to initialize OpenGL");
		return false;
	}

	glViewport(0, 0, width, height);
	this->width = width;
	this->height = height;

	// create a new orthogonic projection for the viewport.
	auto& t = matrices.projection.top();
	t = glm::ortho(0.0f, (float) width, (float) height, 0.0f, -1.0f, 1.0f);
	//t = Matrix4::ortho(0.0f, (float) width, (float) height, 0.0f, -1.0f, 1.0f);

	// create vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// bind default framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glEnable(GL_SCISSOR_TEST);
	glEnable(GL_CULL_FACE);

	// stuff
	//glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);
	//glEnableVertexAttribArray(0);

	// renderer info
	//log_info("GPU: Vendor: %s", glGetString(GL_VENDOR));
	//log_info("GPU: Renderer: %s", glGetString(GL_RENDERER));
	//log_info("GPU: Version: %s", glGetString(GL_VERSION));
	//log_info("GPU: GLSL: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));

	// // depth test
	// glDepthFunc(GL_ALWAYS);
	// glDisable(GL_DEPTH_TEST);
	//
	// // blending
	// glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// glEnable(GL_BLEND);

	// FIXME: quads need to respect this
	// culling
	//glCullFace(GL_BACK);
	//glFrontFace(GL_CCW);
	//glEnable(GL_CULL_FACE);

	// NOTE: wireframe
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);

	// color and depth buffers
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// buffer
	buffer = new opengl::Buffer(
		1024 * 1024,
		Buffer::BufferType::Vertex,
		Buffer::BufferUsage::Stream
	);

	return (glGetError() == GL_NO_ERROR);
}

void Graphics::clear(float r, float g, float b, float a) {
	glClearColor((r / 255.0f), (g / 255.0f), (b / 255.0f), (a / 255.0f));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glCheckError();
}

void Graphics::present() {
	ext_flushDraw();
	buffer->nextFrame();

	auto window = Module::getInstance<window::Window>();
	if (window.get()) {
		window->swapBuffers();
	}
}

void Graphics::setColor(float r, float g, float b, float a) {
	color.r = (r / 255.0f);
	color.g = (g / 255.0f);
	color.b = (b / 255.0f);
	color.a = (a / 255.0f);
}

// FIXME: should we be a strongref or a weakref?
void Graphics::setShader(acorn::graphics::Shader *shader) {
	if (shader != nullptr) {
		shader->bind();
	}
}

void Graphics::setPointSize(float size) {
	glPointSize(size);
}

// FIXME: send data to the shader
void Graphics::prepareDraw() {
	auto shader{
		Shader::current
	};

	// NOTE: update shader uniforms (should be done in the shader)

	const glm::mat4 &project = matrices.projection.top();
	const glm::mat4 &transform = matrices.transform.top();

	int p_mat = shader->getGetUniformLocation("ProjectionMatrix");
	shader->sendMatrices(p_mat, 4, glm::value_ptr(project));

	int t_mat = shader->getGetUniformLocation("TransformMatrix");
	shader->sendMatrices(t_mat, 4, glm::value_ptr(transform));

	glm::mat4 projtrans = project * transform;
	int pt_mat = shader->getGetUniformLocation("ProjTransMatrix");
	shader->sendMatrices(pt_mat, 4, glm::value_ptr(projtrans));

	glCheckError();
}

void Graphics::push() {
	matrices.transform.push(glm::mat4(1.0f));
}

void Graphics::push(const glm::mat4 &m) {
	matrices.transform.push(m);
}

void Graphics::pop() {
	matrices.transform.pop();
}

void Graphics::identity() {
	auto& t = matrices.transform.top();
	t = glm::mat4(1.0f);
	//t.setIndentity();
}

void Graphics::translate(float x, float y) {
	auto& t = matrices.transform.top();
	t = glm::translate(t, glm::vec3(x, y, 0.0f));
	//t.translate(x, y);
}

void Graphics::rotate(float angle) {
	auto& t = matrices.transform.top();
	//t = glm::rotate(t, glm::vec3(x, y, 0.0f));
	//t.rotate(angle);
}

void Graphics::scale(float x, float y) {
	auto& t = matrices.transform.top();
	t = glm::scale(t, glm::vec3(x, y, 1.0f));
	//t.scale(x, y);
}

const glm::mat4 &Graphics::getTransform() const {
	return matrices.transform.top();
}

const glm::mat4 &Graphics::getProjection() const {
	return matrices.projection.top();
}

void Graphics::bindFramebuffer(std::shared_ptr<Framebuffer> framebuffer) {

}

void Graphics::points(std::vector<float> &points) {
	if (points_vbo == 0) {
		glGenBuffers(1, &points_vbo);

		glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * points.size(), NULL, GL_STREAM_DRAW);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void *) 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glCheckError();
	}

	glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
	float *vertices = (float *) (glMapBufferRange(
		GL_ARRAY_BUFFER, 0, sizeof(float) * points.size(),
		(GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT | GL_MAP_UNSYNCHRONIZED_BIT)
	));

	if (vertices == nullptr) {
		throw std::runtime_error("Failed to map stream buffer.");
	}

	memcpy(vertices, &points[0], sizeof(float) * points.size());

	// unmap buffer
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_POINTS, 0, 1);
	glCheckError();
}

void Graphics::triangle(std::vector<float> &points) {
	if (triangle_vbo == 0) {
		glGenBuffers(1, &triangle_vbo);

		glBindBuffer(GL_ARRAY_BUFFER, triangle_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * points.size(), NULL, GL_STREAM_DRAW);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void *) 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glCheckError();
	}

	glBindBuffer(GL_ARRAY_BUFFER, triangle_vbo);
	float *vertices = (float *) (glMapBufferRange(
		GL_ARRAY_BUFFER, 0, sizeof(float) * points.size(),
		(GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT | GL_MAP_UNSYNCHRONIZED_BIT)
	));

	if (vertices == nullptr) {
		throw std::runtime_error("Failed to map stream buffer.");
	}

	memcpy(vertices, &points[0], sizeof(float) * points.size());

	// unmap buffer
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_TRIANGLES, 0, 3);
	glCheckError();
}

void Graphics::polygon(std::vector<float> &points) {
	if (polygon_vbo == 0) {
		glGenBuffers(1, &polygon_vbo);

		glBindBuffer(GL_ARRAY_BUFFER, polygon_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * points.size(), NULL, GL_STREAM_DRAW);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void *) 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glCheckError();
	}

	glBindBuffer(GL_ARRAY_BUFFER, polygon_vbo);
	float *vertices = (float *) (glMapBufferRange(
		GL_ARRAY_BUFFER, 0, sizeof(float) * points.size(),
		(GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT | GL_MAP_UNSYNCHRONIZED_BIT)
	));

	if (vertices == nullptr) {
		throw std::runtime_error("Failed to map stream buffer.");
	}

	memcpy(vertices, &points[0], sizeof(float) * points.size());

	// unmap buffer
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, (points.size() / 2));
	glCheckError();
}

void Graphics::draw(std::shared_ptr<Texture> texture, const glm::mat4 &m) {
	if (image_vbo == 0) {

		float vertices[] = {
			// positions	// texture coords
			0.5f,  0.5f,	1.0f, 1.0f, // top right
			0.5f, -0.5f,	1.0f, 0.0f, // bottom right
			-0.5f, -0.5f,	0.0f, 0.0f, // bottom left
			-0.5f,  0.5f,	0.0f, 1.0f  // top left
		};

		unsigned int indices[] = {
			0, 1, 3, // first triangle
			1, 2, 3  // second triangle
		};

		glGenBuffers(1, &image_vbo);
		glGenBuffers(1, &image_ebo);

		glBindBuffer(GL_ARRAY_BUFFER, image_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, image_ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void *) 0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*) (2 * sizeof(float)));
		glEnableVertexAttribArray(1);

		glCheckError();
	}

	// bind texture
	glBindTexture(GL_TEXTURE_2D, *(GLuint *) texture->getHandle());

	//glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	matrices.transform.push(m);
	drawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glCheckError();
}

void Graphics::draw(GLenum mode, GLsizei count, GLenum type, const GLvoid *indices, const void *ptr) {
	if (image_vbo == 0) {

		float vertices[] = {
			// positions	// texture coords
			-0.5f,  0.5f,	0.0f, 1.0f, // bottom left
			-0.5f, -0.5f,	0.0f, 0.0f, // top left
			 0.5f,	0.5f,	1.0f, 1.0f, // bottom right
			 0.5f, -0.5f,	1.0f, 0.0f  // top right
		};

		glGenBuffers(1, &image_vbo);

		glBindBuffer(GL_ARRAY_BUFFER, image_vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void *) 0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*) (2 * sizeof(float)));
		glEnableVertexAttribArray(1);

		glCheckError();
	}

	// bind texture
	glBindTexture(GL_TEXTURE_2D, *(GLuint *) ptr);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glCheckError();
}

void Graphics::flush() {
	DrawCommand cmd;
	cmd.mode = state_mode;
	cmd.first = 0;
	cmd.count = state_count;
	cmd.texture = state_texture;

	flush(cmd);

	// cleanup state
	if (matrices.transform.size() > 1) {
		matrices.transform.pop();
	}
	state_count = 0;
}

void Graphics::flush(const DrawCommand &cmd) {
	prepareDraw();

	// bind texture
	glBindTexture(GL_TEXTURE_2D, *(GLuint *) cmd.texture->getHandle());

	// draw array
	glDrawArrays(cmd.mode, cmd.first, cmd.count);
}

void Graphics::draw(const DrawCommand &cmd) {

	if (cmd.mode != state_mode || cmd.texture != state_texture) {
		state_texture = cmd.texture;
		flush();
	}

	int totalvertices = state_count + cmd.count;

	const Vertex *_vertices = cmd.texture->getQuad()->getVertices();
	auto vertices = (float *) buffer->bind();
	memcpy(vertices, _vertices, sizeof(Vertex) * cmd.count);

	buffer->unbind(sizeof(Vertex) * cmd.count);


	state_count = state_count + cmd.count;

	//if (state_texture != cmd.texture) {
	//	flush(cmd);
	//}

	//const Vertex *_vertices = cmd.texture->getQuad()->getVertices();
	//auto vertices = (float *) buffer->bind();
	//memcpy(vertices, _vertices, sizeof(Vertex) * cmd.count);

	//size_t offset = 0;
	//offset = buffer->unbind(sizeof(Vertex) * cmd.count);

	//if (matrices.transform.size() > 1) {
	//	matrices.transform.pop();
	//}

	//glCheckError();
}

void Graphics::draw(const DrawIndexedCommand &cmd) {
	prepareDraw();

	const Vertex *_vertices = cmd.texture->getQuad()->getVertices();
	auto vertices = (float *) buffer->bind();
	memcpy(vertices, _vertices, sizeof(Vertex) * cmd.count);

	buffer->unbind(sizeof(Vertex) * cmd.count);

	// bind texture
	glBindTexture(GL_TEXTURE_2D, *(GLuint *) cmd.texture->getHandle());

	// draw array
	glDrawElements(cmd.mode, cmd.count, GL_UNSIGNED_INT, cmd.indices);

	if (matrices.transform.size() > 1) {
		matrices.transform.pop();
	}

	glCheckError();
}

Buffer *Graphics::ext_requestDraw(const DrawCommand &cmd) {
	// NOTE: 
	// 1. check opengl state
	// 2. count total vertices
	// 3. check buffers
	// 4. flush (if needed)
	//	  update state
	// 5. stream the vertex data
	// 6. update state

	// check opengl state to see if we should flush
	if (cmd.mode != state_mode || cmd.texture != state_texture) {
		printf("we should flush\n");
	}

	// count total vertices
	int totalvertices = state_count + cmd.count;

	// TODO: check buffers for size

	// flush if needed
	if (cmd.mode != state_mode || cmd.texture != state_texture) {
		ext_flushDraw();

		// update sate
		state_mode = cmd.mode;
		state_texture = cmd.texture;
	}

	//// stream vertex data
	//const Vertex *_vertices = cmd.texture->getQuad()->getVertices();
	//auto vertices = (float *) buffer->bind();
	//memcpy(vertices, _vertices, sizeof(Vertex) * cmd.count);

	// update state
	state_count = state_count + cmd.count;
	return buffer;
}

void Graphics::ext_flushDraw() {
	if (state_count == 0) return;

	buffer->unbind(sizeof(Vertex) * state_count);

	DrawCommand cmd;
	cmd.mode = state_mode;
	cmd.first = 0;
	cmd.count = state_count;
	cmd.texture = state_texture;
	ext_draw(cmd);

	state_count = 0;
}

void Graphics::ext_draw(const DrawCommand &cmd) {
	prepareDraw();

	// bind texture
	glBindTexture(GL_TEXTURE_2D, *(GLuint *) cmd.texture->getHandle());

	glDrawArrays(cmd.mode, cmd.first, cmd.count);
}

//void Graphics::draw(GLenum mode, GLsizei count, GLenum type, const GLvoid *indices, const void *ptr) {
//	if (image_vbo == 0) {
//
//		float vertices[] = {
//			// positions	// texture coords
//			0.5f,  0.5f,	1.0f, 1.0f, // top right
//			0.5f, -0.5f,	1.0f, 0.0f, // bottom right
//			-0.5f, -0.5f,	0.0f, 0.0f, // bottom left
//			-0.5f,  0.5f,	0.0f, 1.0f  // top left
//		};
//
//		unsigned int indices[] = {
//			0, 1, 3, // first triangle
//			1, 2, 3  // second triangle
//		};
//
//		glGenBuffers(1, &image_vbo);
//		glGenBuffers(1, &image_ebo);
//
//		glBindBuffer(GL_ARRAY_BUFFER, image_vbo);
//		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
//
//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, image_ebo);
//		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
//
//		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void *) 0);
//		glEnableVertexAttribArray(0);
//
//		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*) (2 * sizeof(float)));
//		glEnableVertexAttribArray(1);
//
//		GL_CHECK();
//	}
//
//	// bind texture
//	glBindTexture(GL_TEXTURE_2D, *(GLuint *) ptr);
//
//	glDrawElements(mode, count, type, indices);
//	GL_CHECK();
//}

//void Graphics::draw(Texture *texture, Buffer *buffer, GLenum mode, int first, int count) {
//	prepareDraw();
//
//	// position
//	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, x));
//	glEnableVertexAttribArray(0);
//
//	// texcoord
//	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, u));
//	glEnableVertexAttribArray(1);
//
//	// bind texture
//	glBindTexture(GL_TEXTURE_2D, *(GLuint *)texture->getHandle());
//
//	buffer->unbind();
//
//	// draw.
//	glDrawArrays(mode, first, count);
//
//	GL_CHECK();
//}
//
//void Graphics::drawIndexed(Texture *texture, Buffer *buffer, GLenum mode, int count, GLenum type, const GLvoid *indices) {
//	prepareDraw();
//
//	// position
//	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, x));
//	glEnableVertexAttribArray(0);
//
//	// texcoord
//	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, u));
//	glEnableVertexAttribArray(1);
//
//	// bind texture
//	glBindTexture(GL_TEXTURE_2D, *(GLuint *)texture->getHandle());
//
//	buffer->unbind();
//
//	// draw.
//	glDrawElements(mode, count, type, indices);
//}
//
//void Graphics::draw(Image *image, Buffer *buffer, GLenum mode, int first, int count) {
//	prepareDraw();
//
//	// position
//	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, x));
//	glEnableVertexAttribArray(0);
//
//	// texcoord
//	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, u));
//	glEnableVertexAttribArray(1);
//
//	// bind texture
//	glActiveTexture(GL_TEXTURE0);
//	glBindTexture(GL_TEXTURE_2D, *(GLuint *) image->getHandle());
//
//	buffer->unbind();
//
//	// draw.
//	glDrawArrays(mode, first, count);
//
//	GL_CHECK();
//}

void Graphics::drawArrays(GLenum mode, GLint first, GLsizei count) {
	prepareDraw();
	glDrawArrays(mode, first, count);
}

void Graphics::drawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid *indices) {
	prepareDraw();
	glDrawElements(mode, count, type, indices);

	if (matrices.transform.size() > 1) {
		matrices.transform.pop();
	}
}

std::shared_ptr<Framebuffer> Graphics::newFramebuffer(int width, int height) {
	return std::make_shared<Framebuffer>(Texture::TextureType::Texture2D, width, height);
}

std::shared_ptr<Quad> Graphics::newQuad(int x, int y, int w, int h, int sw, int sh) {
	return std::make_shared<Quad>(x, y, w, h, sw, sh);
}

std::shared_ptr<opengl::ShaderStage> Graphics::newShaderStage(ShaderStage::StageType stageType, const std::string &source) {
	if (stageType != ShaderStage::StageType::Vertex && stageType != ShaderStage::StageType::Fragment) {
		throw acorn::Exception("unknown glstage.");
	}

	if (source.empty()) {
		throw acorn::Exception("Error creating shader stage.");
	}

    return std::make_shared<opengl::ShaderStage>(stageType, source);
}

std::shared_ptr<opengl::Shader> Graphics::newShader(const std::string &vertex, const std::string &fragment) {
	if (vertex.empty() && fragment.empty()) {
		throw acorn::Exception("Error creating shader.");
	}

    auto vertexstage = newShaderStage(ShaderStage::StageType::Vertex, vertex);
	auto fragmentstage = newShaderStage(ShaderStage::StageType::Fragment, fragment);

	return std::make_shared<opengl::Shader>(vertexstage, fragmentstage);
}

}   // graphics
}	// acorn
