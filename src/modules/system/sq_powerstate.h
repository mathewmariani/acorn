#ifndef ACORN_SQ_SYSTEM_POWERSTATE_H
#define ACORN_SQ_SYSTEM_POWERSTATE_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace system {

extern "C" int sq_acorn_powerstate(SQVM *v);

}	// system
}	// acorn

#endif	// ACORN_SQ_SYSTEM_POWERSTATE_H
