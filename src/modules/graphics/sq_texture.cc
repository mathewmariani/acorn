// acorn
#include "graphics/sq_texture.h"
#include "graphics/texture.h"

namespace acorn {
namespace graphics {

#define instance() (Module::getInstance<graphics::Graphics>())

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Texture cannot be constructed directly.");
}

static SQInteger wrap_getWidth(SQVM *v) {
	std::weak_ptr<Texture> texture(sqx_checkinstance<Texture>(v, 1));
	if (auto spt = texture.lock()) {
		sq_pushinteger(v, (SQInteger) spt->getWidth());
		return 1;
	}
	return 0;
}

static SQInteger wrap_getHeight(SQVM *v) {
	std::weak_ptr<Texture> texture(sqx_checkinstance<Texture>(v, 1));
	if (auto spt = texture.lock()) {
		sq_pushinteger(v, (SQInteger) spt->getHeight());
		return 1;
	}
	return 0;
}

static SQInteger wrap_setFilter(SQVM *v) {
	std::weak_ptr<Texture> texture(sqx_checkinstance<Texture>(v, 1));
	if (auto spt = texture.lock()) {
		//int filter;
		//sq_getinteger(v, 2, (SQInteger *) &filter);
		//spt->setWrap(filter);
		STUBBED("Missing FilterMode enum");
	}
	return 0;
}

static SQInteger wrap_getFilter(SQVM *v) {
	std::weak_ptr<Texture> texture(sqx_checkinstance<Texture>(v, 1));
	if (auto spt = texture.lock()) {
		//sq_pushinteger(v, (SQInteger) spt->getFilter());
		//return 1;
		STUBBED("Missing FilterMode enum");
	}
	return 0;
}

static SQInteger wrap_setWrap(SQVM *v) {
	std::weak_ptr<Texture> texture(sqx_checkinstance<Texture>(v, 1));
	if (auto spt = texture.lock()) {
		//int wrap;
		//sq_getinteger(v, 2, (SQInteger *) &wrap);
		//spt->setWrap(wrap);
		STUBBED("Missing WrapMode enum");
	}
	return 0;
}

static SQInteger wrap_getWrap(SQVM *v) {
	std::weak_ptr<Texture> texture(sqx_checkinstance<Texture>(v, 1));
	if (auto spt = texture.lock()) {
		//sq_pushinteger(v, (SQInteger) spt->getWrap());
		//return 1;
		STUBBED("Missing WrapMode enum");
	}
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),
	_DECL_FUNC(getWidth, 1, "x", false),
	_DECL_FUNC(getHeight, 1, "x", false),

	_DECL_FUNC(setFilter, 1, "x", false),
	_DECL_FUNC(getFilter, 1, "x", false),

	_DECL_FUNC(setWrap, 1, "x", false),
	_DECL_FUNC(getWrap, 1, "x", false),

{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_texture2d(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Texture";
	reg.functions = functions;

	return sqx_registerclass(v, Texture::type, reg);
}

}	// graphics
}	// acorn
