// acorn
#include "window/sq_windowclass.h"
#include "window/window.h"

namespace acorn {
namespace window {

#define instance() (Module::getInstance<window::Window>())

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Window cannot be constructed directly.");
}

static SQInteger wrap_setWindow(SQVM* v) {
	const auto sqx_getfield = [&](SQVM *v, int idx, const std::string &k, bool _default = false) -> bool {
		sq_pushstring(v, k.c_str(), -1);
		SQBool flag = _default;
		if (SQ_SUCCEEDED(sq_get(v, idx))) {
			sq_getbool(v, -1, &flag);
		}
		sq_pop(v, 1);
		return (bool) flag;
	};

	SQInteger width, height;
	sq_getinteger(v, 2, &width);
	sq_getinteger(v, 3, &height);

	Window::Settings settings = {};
	settings.fullscreen = sqx_getfield(v, 4, "fullscreen", false);
	settings.borderless = sqx_getfield(v, 4, "borderless", false);
	settings.resizable = sqx_getfield(v, 4, "resizable", false);
	settings.highdpi = sqx_getfield(v, 4, "highdpi", false);
	settings.centered = sqx_getfield(v, 4, "centered", false);
	settings.vsync = sqx_getfield(v, 4, "vsync", false);

	sq_pushbool(v, instance()->setWindow((int) width, (int) height, &settings));

	return 1;
}


static SQInteger wrap_getDisplayIndex(SQVM* v) {
	sq_pushinteger(v, instance()->getDisplayIndex());
	return 1;
}

static SQInteger wrap_getTitle(SQVM* v) {
	sq_pushstring(v, instance()->getTitle().c_str(), -1);
	return 1;
}

static SQInteger wrap_setTitle(SQVM* v) {
	const char *title;
	sq_getstring(v, -1, &title);
	instance()->setTitle(title);
	return 0;
}

static SQInteger wrap_setPosition(SQVM* v) {
	STUBBED(__FUNCTION__);
	return 0;
}

static SQInteger wrap_getPosition(SQVM* v) {
	int x, y;
	instance()->getPosition(x, y);

	// create table
	sq_newtableex(v, 2);

	sq_pushstring(v, "x", -1);
	sq_pushinteger(v, x);
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "y", -1);
	sq_pushinteger(v, y);
	sq_newslot(v, -3, SQFalse);

	sq_newslot(v, -3, SQFalse);

	return 1;
}

static SQInteger wrap_getX(SQVM* v) {
	int x, y;
	instance()->getPosition(x, y);
	sq_pushinteger(v, x);
	return 1;
}

static SQInteger wrap_getY(SQVM* v) {
	int x, y;
	instance()->getPosition(x, y);
	sq_pushinteger(v, y);
	return 1;
}

static SQInteger wrap_setDimensions(SQVM* v) {
	STUBBED(__FUNCTION__);
	return 0;
}

static SQInteger wrap_getDimensions(SQVM *v) {
	int x, y;
	instance()->getDimensions(x, y);

	// create table
	sq_newtableex(v, 2);

	sq_pushstring(v, "x", -1);
	sq_pushinteger(v, x);
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "y", -1);
	sq_pushinteger(v, y);
	sq_newslot(v, -3, SQFalse);

	sq_newslot(v, -3, SQFalse);

	return 1;
}

static SQInteger wrap_getWidth(SQVM* v) {
	int x, y;
	instance()->getDimensions(x, y);
	sq_pushinteger(v, x);
	return 1;
}

static SQInteger wrap_getHeight(SQVM* v) {
	int x, y;
	instance()->getDimensions(x, y);
	sq_pushinteger(v, y);
	return 1;
}

static SQInteger wrap_getDrawableDimensions(SQVM *v) {
	int x, y;
	instance()->getDrawableDimensions(x, y);

	// create table
	sq_newtableex(v, 2);

	sq_pushstring(v, "x", -1);
	sq_pushinteger(v, x);
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "y", -1);
	sq_pushinteger(v, y);
	sq_newslot(v, -3, SQFalse);

	sq_newslot(v, -3, SQFalse);

	return 1;
}

static SQInteger wrap_getDrawableWidth(SQVM *v) {
	int x, y;
	instance()->getDrawableDimensions(x, y);
	sq_pushinteger(v, x);
	return 1;
}

static SQInteger wrap_getDrawableHeight(SQVM *v) {
	int x, y;
	instance()->getDrawableDimensions(x, y);
	sq_pushinteger(v, y);
	return 1;
}

static SQInteger wrap_setFullscreen(SQVM *v) {
	SQBool fullscreen;
	sq_getbool(v, 2, &fullscreen);
	instance()->setFullscreen(fullscreen, Window::FullscreenMode::DESKTOP);
	return 1;
}

static SQInteger wrap_hasKeyboardFocus(SQVM *v) {
	sq_pushbool(v, instance()->hasKeyboardFocus());
	return 1;
}

static SQInteger wrap_hasMouseFocus(SQVM *v) {
	sq_pushbool(v, instance()->hasMouseFocus());
	return 1;
}

static SQInteger wrap_maximize(SQVM *v) {
	instance()->maximize();
	return 0;
}

static SQInteger wrap_minimize(SQVM *v) {
	instance()->minimize();
	return 0;
}

static SQInteger wrap_hide(SQVM *v) {
	instance()->hide();
	return 0;
}

static SQInteger wrap_restore(SQVM *v) {
	instance()->restore();
	return 0;
}

static SQInteger wrap_raise(SQVM *v) {
	instance()->raise();
	return 0;
}

static SQInteger wrap_show(SQVM *v) {
	instance()->show();
	return 0;
}

static SQInteger wrap_close(SQVM *v) {
	instance()->close();
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 1, "x", false),

	_DECL_FUNC(setWindow, 4, "ynnt", true),
	_DECL_FUNC(getDisplayIndex, 1, "y", true),

	_DECL_FUNC(getTitle, 1, "y", true),
	_DECL_FUNC(setTitle, 2, "ys", true),

	_DECL_FUNC(setPosition, 1, "y", true),
	_DECL_FUNC(getPosition, 1, "y", true),
	_DECL_FUNC(getX, 1, "y", true),
	_DECL_FUNC(getY, 1, "y", true),

	_DECL_FUNC(setDimensions, 1, "y", true),
	_DECL_FUNC(getDimensions, 1, "y", true),
	_DECL_FUNC(getWidth, 1, "y", true),
	_DECL_FUNC(getHeight, 1, "y", true),

	_DECL_FUNC(getDrawableDimensions, 1, "y", true),
	_DECL_FUNC(getDrawableWidth, 1, "y", true),
	_DECL_FUNC(getDrawableHeight, 1, "y", true),

	_DECL_FUNC(setFullscreen, 2, "yb", true),

	_DECL_FUNC(hasKeyboardFocus, 1, "y", true),
	_DECL_FUNC(hasMouseFocus, 1, "y", true),

	_DECL_FUNC(maximize, 1, "y", true),
	_DECL_FUNC(minimize, 1, "y", true),
	_DECL_FUNC(hide, 1, "y", true),
	_DECL_FUNC(restore, 1, "y", true),
	_DECL_FUNC(raise, 1, "y", true),
	_DECL_FUNC(show, 1, "y", true),
	_DECL_FUNC(close, 1, "y", true),

	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_window(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Window";
	reg.functions = functions;
	return sqx_registerclass(v, Window::type, reg);
}

}	// window
}	// acorn
