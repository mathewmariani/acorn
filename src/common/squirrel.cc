// C/C++
#include <cstdarg>

// acorn
#include "common/object.h"
#include "common/module.h"
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {

const char *sqx_printstackobject(SQVM *v, SQInteger idx) {
	switch (sq_gettype(v, idx)) {
	case OT_NULL: return "null";
	case OT_BOOL: return "boolean";
	case OT_INTEGER: return "integer";
	case OT_FLOAT: return "float";
	case OT_STRING: return "string";
	case OT_TABLE: return "table";
	case OT_ARRAY: return "array";
	case OT_USERDATA: return "userdata";
	case OT_CLOSURE: return "closure";
	case OT_NATIVECLOSURE: return "native c closure";
	case OT_GENERATOR: return "generator";
	case OT_USERPOINTER: return "userpointer";
	case OT_THREAD: return "thread";
	case OT_CLASS: return "class";
	case OT_INSTANCE: return "instance";
	case OT_WEAKREF: return "weak reference";
	default: return "unkown";
	}
}

void sqx_printstack(SQVM *v) {
	printf("-------------------------------------------\n");
	for (int idx = 1; idx <= sq_gettop(v); ++idx) {
		int nidx = (idx - sq_gettop(v) - 1);
		printf("| %d | %d | %s\n", idx, nidx, sqx_printstackobject(v, idx));
	}
	printf("-------------------------------------------\n");
}

void sqx_printfunc(SQVM*, const char* str, ...) {
	char buffer[2048];
	va_list arglist;
	va_start(arglist, str);
	vsprintf(buffer, str, arglist);
	va_end(arglist);

	printf("%s", buffer);
}

void sqx_errorfunc(SQVM*, const char* str, ...) {
	char buffer[2048];
	va_list arglist;
	va_start(arglist, str);
	vsprintf(buffer, str, arglist);
	va_end(arglist);

	printf("%s", buffer);
}

// NOTE: release shared ownership objects from squirrel
static SQInteger wrap_release(SQUserPointer ptr, SQInteger /* size */) {
	auto t = static_cast<std::shared_ptr<Object>*>(ptr);
	if (t->get()) {
		std::cout << __FUNCTION__ << std::endl
			<< "Releasing shared ownership object" << std::endl
			<< "  context.get() = " << t->get() << std::endl
			<< "  context.use_count() = " << t->use_count() << std::endl;
	}

	return 0;
}


static SQInteger delegate_get(SQVM *v) {
	printf("%s\n", __FUNCTION__);
	sq_push(v, 2);
	if (!SQ_SUCCEEDED(sq_get(v, -2))) {
		const char* s;
		sq_getstring(v, 2, &s);
		return sq_throwerror(v, "member variable not found");
	}

	sq_push(v, 1);

	sq_call(v, 1, SQTrue, SQTrue);
	return 1;
}

static SQInteger delegate_set(SQVM *v) {
	printf("%s\n", __FUNCTION__);
	sqx_printstack(v);
	sq_push(v, 2);
	if (SQ_FAILED(sq_get(v, -2))) {
		const char* s;
		sq_getstring(v, 2, &s);
		return sq_throwerror(v, "member variable not found");
	}

	sq_push(v, 1);	// instance
	sq_push(v, 3);	// variable

	sq_call(v, 2, SQTrue, SQTrue);
	return 0;
}

bool sqx_isnull(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_NULL);
}

bool sqx_isboolean(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_BOOL);
}

bool sqx_isinteger(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_INTEGER);
}

bool sqx_isfloat(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_FLOAT);
}

bool sqx_isstring(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_STRING);
}

bool sqx_istable(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_TABLE);
}

bool sqx_isarray(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_ARRAY);
}

bool sqx_isuserdata(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_USERDATA);
}

bool sqx_isclosure(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_CLOSURE);
}

bool sqx_isnative(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_NATIVECLOSURE);
}

bool sqx_isgenerator(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_GENERATOR);
}

bool sqx_isuserpointer(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_USERPOINTER);
}

bool sqx_isthread(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_THREAD);
}

bool sqx_isclass(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_CLASS);
}

bool sqx_isinstance(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_INSTANCE);
}

bool sqx_isweakref(SQVM *v, int idx) {
	return (sq_gettype(v, idx) == OT_WEAKREF);
}

void sqx_pushclassestable(SQVM *v) {
	// get classes table
	sq_pushregistrytable(v);
	sq_pushstring(v, "_classes", -1);
	if (SQ_FAILED(sq_rawget(v, -2))) {
		sq_newtable(v);
		sq_pushstring(v, "_classes", -1);
		// leave table on stack
		sq_push(v, -2);
		sq_rawset(v, -4);
	}
	// remove registry table
	sq_remove(v, -2);
}

void sqx_pushobjectstable(SQVM *v) {
	// get classes table
	sq_pushregistrytable(v);
	sq_pushstring(v, "_objects", -1);
	if (SQ_FAILED(sq_rawget(v, -2))) {
		sq_newtable(v);
		sq_pushstring(v, "_objects", -1);
		// leave table on stack
		sq_push(v, -2);
		sq_rawset(v, -4);
	}
	// remove registry table
	sq_remove(v, -2);
}

void sqx_pushmodulestable(SQVM *v) {
	// get classes table
	sq_pushregistrytable(v);
	sq_pushstring(v, "_modules", -1);
	if (SQ_FAILED(sq_rawget(v, -2))) {
		sq_newtable(v);
		sq_pushstring(v, "_modules", -1);
		// leave table on stack
		sq_push(v, -2);
		sq_rawset(v, -4);
	}
	// remove registry table
	sq_remove(v, -2);
}

void sqx_pushacorntable(SQVM *v) {
	// get acorn table
	sq_pushroottable(v);
	sq_pushstring(v, "acorn", -1);
	if (SQ_FAILED(sq_rawget(v, -2))) {
		sq_newtable(v);
		sq_pushstring(v, "acorn", -1);
		// leave table on stack
		sq_push(v, -2);
		sq_rawset(v, -4);
	}
	// remove root table
	sq_remove(v, -2);
}


/*
 * NOTE: stack during execution
 * FIXME:
 * -------------------------------------------
 * | 1 | -5 | table		(root)		} not needed, please remove
 * | 2 | -4 | table		(acorn)		}
 * | 3 | -3 | table		(_modules)
 * | 4 | -2 | string	(key)
 * | 5 | -1 | userdata	(foreing object)
 * -------------------------------------------
 */
int sqx_registermodule(SQVM *v, const SQRegModule &reg) {
	printf("%s : %zx : %s\n", __FUNCTION__, reg.module->getHash(), reg.module->getName());

	// push reference to squirrel registry table; index: _modules
	sqx_pushmodulestable(v);
	sq_pushstring(v, reg.name, -1);

	void *ud = sq_newuserdata(v, sizeof(std::shared_ptr<Object>));
	new(ud) std::shared_ptr<Object>(reg.module);

	sq_setreleasehook(v, -1, &wrap_release);

	sq_newslot(v, -3, SQFalse);
	sq_pop(v, 1);

	// bind to squirrel root table; index: acorn
	sqx_pushacorntable(v);

	sq_pushstring(v, reg.name, -1);
	sq_newtable(v);

	// register all functions
	if (reg.functions != nullptr) {
		sqx_setfunctions(v, reg.functions);
	}

	// register all classes
	if (reg.classes != nullptr) {
		for (const SQFunction *t = reg.classes; *t != nullptr; t++) {
			(*t)(v);
		}
	}

	// register all enums
	if (reg.enums != nullptr) {
		for (const SQFunction *t = reg.enums; *t != nullptr; t++) {
			(*t)(v);
		}
	}

	sq_newslot(v, -3, SQFalse);
	sq_pop(v, 1);

	// register module instance
	Module::registerInstance(reg.module);

	return 1;
}

int sqx_registerclass(SQVM *v, acorn::Type &type, const ClassRegistry &reg) {
	sq_pushstring(v, reg.name, -1);
	sq_newclass(v, SQFalse);

	// typetag
	sq_settypetag(v, -1, (SQUserPointer *)type.hash_code());

	// register all functions
	if (reg.functions != nullptr) {
		sqx_setfunctions(v, reg.functions);
	}

	sq_newslot(v, -3, SQFalse);
	return 1;
}

int sqx_registerenum(SQVM *v, const std::string &name, const EnumRegistry *reg) {
	sq_pushconsttable(v);

	sq_pushstring(v, name.c_str(), -1);
	sq_newtable(v);

	for (auto i = 0; reg[i].key; ++i) {
		sq_pushstring(v, reg[i].key, -1);
		sq_pushinteger(v, (SQInteger) reg[i].value);
		sq_newslot(v, -3, SQTrue);
	}

	sq_newslot(v, -3, SQFalse);
	sq_pop(v, 1);

	return 1;
}

void sqx_setfunctions(SQVM *v, const MethodRegistry *functions) {
	if (functions == nullptr) {
		// no functions, return early
		return;
	}

	for (int i = 0; functions[i].name; i++) {
		const MethodRegistry& f = functions[i];
		sq_pushstring(v, f.name, -1);
		sq_newclosure(v, f.f, 0);
		sq_setparamscheck(v, f.nparamscheck, f.typemask);
		sq_setnativeclosurename(v, -1, f.name);
		sq_newslot(v, -3, f.isStatic);
	}
}

void sqx_setfunctions(SQVM *v, const SQRegFunction *functions) {
	if (functions == nullptr) {
		// no functions, return early
		return;
	}

	for (int i = 0; functions[i].name; i++) {
		const SQRegFunction& f = functions[i];
		sq_pushstring(v, f.name, -1);
		sq_newclosure(v, f.f, 0);
		sq_setparamscheck(v, f.nparamscheck, f.typemask);
		sq_setnativeclosurename(v, -1, f.name);
		sq_newslot(v, -3, SQFalse);
	}
}

int sqx_registermodule_ext(SQVM *v, const ModuleRegistry &reg) {
	printf("%s\n", __FUNCTION__);

	// push reference to squirrel registry table; index: _modules
	sqx_pushmodulestable(v);
	sq_pushstring(v, reg.name, -1);

	void *ud = sq_newuserdata(v, sizeof(std::shared_ptr<Object>));
	new(ud) std::shared_ptr<Object>(reg.module);

	sq_setreleasehook(v, -1, &wrap_release);

	sq_newslot(v, -3, SQFalse);
	sq_pop(v, 1);

	// bind to the squirrel root table
	sq_pushroottable(v);

	// register all classes
	if (reg.classes != nullptr) {
		for (const SQFunction *t = reg.classes; *t != nullptr; t++) {
			(*t)(v);
		}
	}

	// register all enums
	if (reg.enums != nullptr) {
		for (const SQFunction *t = reg.enums; *t != nullptr; t++) {
			(*t)(v);
		}
	}

	sq_pop(v, 1);

	// register module instance
	Module::registerInstance(reg.module);

	return 1;
}

void sqx_pushinstance(SQVM *v, acorn::Type &type, std::shared_ptr<Object> object) {
	if (object == nullptr) {
		sq_pushnull(v);
		return;
	}

	sqx_pushobjectstable(v);

	sq_pushuserpointer(v, object.get());
	if (SQ_FAILED(sq_rawget(v, -2))) {
		void *ud = sq_newuserdata(v, sizeof(std::shared_ptr<Object>));
		new(ud) std::shared_ptr<Object>(object);

		// typetag
		sq_settypetag(v, -1, (SQUserPointer *) type.hash_code());

		sq_pushuserpointer(v, object.get());
		sq_push(v, -2);
		sq_newslot(v, -4, false);
	}

	sq_remove(v, -2);

	SQUserPointer ptr;
	sq_getuserdata(v, -1, &ptr, nullptr);
	if (SQ_FAILED(sq_setinstanceup(v, 1, ptr))) {
		printf("sqx_pushinstance failed!!!\n");
		sq_pushstring(v, "sqx_pushinstance failed", -1);
		sq_throwobject(v);
	}

	sq_setreleasehook(v, 1, &wrap_release);

	sq_pop(v, 1);
}

}	// acorn
