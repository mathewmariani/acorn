R"squirrel"#(

# mouse
describe("acorn.mouse", function() {
	# typeof
	it("is a typeof of table", function() {
		assert.is_typeof(acorn.mouse, "table")
	})

	# functions
	it("contains a function `getX()`", function() {
		assert.is_in("getX", acorn.mouse)
		assert.is_typeof(acorn.mouse.getX, "function")
	})
	it("contains a function `getY()`", function() {
		assert.is_in("getY", acorn.mouse)
		assert.is_typeof(acorn.mouse.getY, "function")
	})
	it("contains a function `getPosition()`", function() {
		assert.is_in("getPosition", acorn.mouse)
		assert.is_typeof(acorn.mouse.getPosition, "function")
	})
	it("contains a function `setPosition()`", function() {
		assert.is_in("setPosition", acorn.mouse)
		assert.is_typeof(acorn.mouse.setPosition, "function")
	})
	it("contains a function `getVisible()`", function() {
		assert.is_in("getVisible", acorn.mouse)
		assert.is_typeof(acorn.mouse.getVisible, "function")
	})
	it("contains a function `setVisible()`", function() {
		assert.is_in("setVisible", acorn.mouse)
		assert.is_typeof(acorn.mouse.setVisible, "function")
	})
	it("contains a function `getRelativeMode()`", function() {
		assert.is_in("getRelativeMode", acorn.mouse)
		assert.is_typeof(acorn.mouse.getRelativeMode, "function")
	})
	it("contains a function `setRelativeMode()`", function() {
		assert.is_in("setRelativeMode", acorn.mouse)
		assert.is_typeof(acorn.mouse.setRelativeMode, "function")
	})
	it("contains a function `isDown()`", function() {
		assert.is_in("isDown", acorn.mouse)
		assert.is_typeof(acorn.mouse.isDown, "function")
	})
	it("contains a function `getSystemCursor()`", function() {
		assert.is_in("getSystemCursor", acorn.mouse)
		assert.is_typeof(acorn.mouse.getSystemCursor, "function")
	})

	# cursortype
	describe("acorn.mouse.CursorType", function() {
		# typeof
		# FIXME: why does this not work?
		# it("is a typeof of table", function() {
		# 	assert.is_typeof(PowerState, "table")
		# })

		# index
		it("contains an index `Arrow`", function() {
			assert.equals(CursorType.Arrow, 0)
		})
		it("contains an index `IBeam`", function() {
			assert.equals(CursorType.IBeam, 1)
		})
		it("contains an index `Wait`", function() {
			assert.equals(CursorType.Wait, 2)
		})
		it("contains an index `Crosshair`", function() {
			assert.equals(CursorType.Crosshair, 3)
		})
		it("contains an index `WaitArrow`", function() {
			assert.equals(CursorType.WaitArrow, 4)
		})
		it("contains an index `SizeNWSE`", function() {
			assert.equals(CursorType.SizeNWSE, 5)
		})
		it("contains an index `SizeNESW`", function() {
			assert.equals(CursorType.SizeNESW, 6)
		})
		it("contains an index `SizeWE`", function() {
			assert.equals(CursorType.SizeWE, 7)
		})
		it("contains an index `SizeNS`", function() {
			assert.equals(CursorType.SizeNS, 8)
		})
		it("contains an index `SizeAll`", function() {
			assert.equals(CursorType.SizeAll, 9)
		})
		it("contains an index `No`", function() {
			assert.equals(CursorType.No, 10)
		})
		it("contains an index `Hand`", function() {
			assert.equals(CursorType.Hand, 11)
		})
	})

	# cursor
	describe("acorn.mouse.Cursor", function() {
		# typeof
		it("is a typeof of class", function() {
			assert.is_typeof(acorn.mouse.Cursor, "class")
		})
	})
})

#)squirrel"#"
