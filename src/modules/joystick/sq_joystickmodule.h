#ifndef ACORN_SQ_JOYSTICK_H
#define ACORN_SQ_JOYSTICK_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace joystick {

extern "C" int sq_acorn_joystickmodule(SQVM *v);

}	// joystick
}	// acorn

#endif	// ACORN_SQ_JOYSTICK_H
