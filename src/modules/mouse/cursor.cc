// acorn
#include "common/log.h"
#include "mouse/cursor.h"

namespace acorn {
namespace mouse {

acorn::Type Cursor::type = std::type_index(typeid(mouse::Cursor));

Cursor::Cursor(SystemCursorType type) :
	cursor(nullptr) {
	cursor = SDL_CreateSystemCursor((SDL_SystemCursor) type);

	if (cursor == nullptr) {
		printf("cannot create cursor!");
	}
}

Cursor::~Cursor() {
	if (cursor) {
		SDL_FreeCursor(cursor);
	}
}

const void *Cursor::getHandle() const noexcept {
	return cursor;
}

// FIXME: I don't know if this is needed
//std::unordered_map<std::string, Cursor::SystemCursorType> Cursor::systemcursorTable = {
//	{ "Arrow", Cursor::SystemCursorType::ARROW },
//	{ "IBeam", Cursor::SystemCursorType::IBEAM },
//	{ "Wait", Cursor::SystemCursorType::WAIT },
//	{ "Crosshair", Cursor::SystemCursorType::CROSSHAIR },
//	{ "WaitArrow", Cursor::SystemCursorType::WAITARROW },
//	{ "SizeNWSE", Cursor::SystemCursorType::SIZENWSE },
//	{ "SizeNESW", Cursor::SystemCursorType::SIZENESW },
//	{ "SizeWE", Cursor::SystemCursorType::SIZEWE },
//	{ "SizeNS", Cursor::SystemCursorType::SIZENS },
//	{ "SizeAll", Cursor::SystemCursorType::SIZEALL },
//	{ "No", Cursor::SystemCursorType::NO },
//	{ "Hand", Cursor::SystemCursorType::HAND },
//};

}	// mouse
}	// acorn
