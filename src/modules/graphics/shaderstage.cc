// acorn
#include "common/exception.h"
#include "graphics/shaderstage.h"

namespace acorn {
namespace graphics {

ShaderStage::ShaderStage(StageType glstage, const std::string &glsl) :
	stageType(glstage),
	source(glsl) {

}

ShaderStage::~ShaderStage() {

}

}	// graphics
}	// acorn
