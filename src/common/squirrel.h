#ifndef ACORN_SQUIRREL_OBJECT_H
#define ACORN_SQUIRREL_OBJECT_H

// STL
#include <iostream>
#include <memory>
#include <string>
#include <typeinfo>
#include <unordered_map>

// squirrel
#include <squirrel.h>

// acorn
#include "common/type.h"

namespace acorn {

// forward declarations
class Object;
class Module;

// type definitions
typedef int(*SQFunction)(SQVM*);

void sqx_printfunc(SQVM *v, const char *str, ...);
void sqx_errorfunc(SQVM *v, const char *str, ...);

void sqx_printstack(SQVM *v);

struct SQRegClass {
	const SQChar *name;
	const SQRegFunction *functions = nullptr;
};	// sqregclass

struct SQRegModule {
	const SQChar *name;
	const SQRegFunction *functions = nullptr;
	const SQFunction *classes = nullptr;
	const SQFunction *enums = nullptr;
	std::shared_ptr<Module> module;
	acorn::Type *type;
};	// sqregmodule

struct ModuleRegistry {
	const SQChar *name;
	const SQFunction *classes = nullptr;
	const SQFunction *enums = nullptr;
	std::shared_ptr<Module> module;
	acorn::Type *type;
};	// moduleregistry

struct MethodRegistry {
	const SQChar *name;
	SQFUNCTION f;
	SQInteger nparamscheck;
	const SQChar *typemask;
	bool isStatic;
};	// methodregistry

struct ClassRegistry {
	const SQChar *name;
	const MethodRegistry *functions = nullptr;
};	// classregistry

struct EnumRegistry {
	const SQChar *key;
	const SQInteger value;
};	// enumregistry

bool sqx_isnull(SQVM *v, int idx);
bool sqx_isboolean(SQVM *v, int idx);
bool sqx_isinteger(SQVM *v, int idx);
bool sqx_isfloat(SQVM *v, int idx);
bool sqx_isstring(SQVM *v, int idx);
bool sqx_istable(SQVM *v, int idx);
bool sqx_isarray(SQVM *v, int idx);
bool sqx_isuserdata(SQVM *v, int idx);
bool sqx_isclosure(SQVM *v, int idx);
bool sqx_isnative(SQVM *v, int idx);
bool sqx_isgenerator(SQVM *v, int idx);
bool sqx_isuserpointer(SQVM *v, int idx);
bool sqx_isthread(SQVM *v, int idx);
bool sqx_isclass(SQVM *v, int idx);
bool sqx_isinstance(SQVM *v, int idx);
bool sqx_isweakref(SQVM *v, int idx);

void sqx_pushclassestable(SQVM *v);
void sqx_pushobjectstable(SQVM *v);
void sqx_pushmodulestable(SQVM *v);
void sqx_pushacorntable(SQVM *v);

int sqx_registermodule(SQVM *v, const SQRegModule &reg);
int sqx_registerclass(SQVM *v, acorn::Type &type, const ClassRegistry &reg);
int sqx_registerenum(SQVM *v, const std::string &name, const EnumRegistry *reg);

void sqx_setfunctions(SQVM *v, const MethodRegistry *functions);
void sqx_setfunctions(SQVM *v, const SQRegFunction *functions);

int sqx_registermodule_ext(SQVM *v, const ModuleRegistry &reg);

void sqx_pushinstance(SQVM *v, acorn::Type &type, std::shared_ptr<Object> object);

// FIXME: need a method to create classes
// this method would need to access a registry table that stores
// information on how to create classes

template <typename T>
void sqx_pushinstance(SQVM *v, std::shared_ptr<Object> object) {
	sqx_pushinstance(v, T::type, std::move(object));
}

// NOTE: useful to pass const shared_ptr & if the function creates a weak pointer from the shared pointer.
template <typename T>
std::shared_ptr<T> sqx_checkinstance(SQVM *v, int idx, const acorn::Type &type) {
	// FIXME:
	//static_assert(std::is_convertible<T, Object>(), "Trying to use sqx_checktype with disallowed type.");

	if (sq_gettype(v, idx) != OT_INSTANCE) {
		sq_pushstring(v, "Unexpected type; Expected a class instance.", -1);
		sq_throwobject(v);
		return nullptr;
	}

	std::shared_ptr<T> *t = nullptr;
	if (SQ_FAILED(sq_getinstanceup(v, idx, (SQUserPointer *) &t, (SQUserPointer *) type.hash_code()))) {
		printf("%s\n", type.name());
		sqx_printstack(v);
		sq_pushstring(v, "Unexpected type. (2)", -1);
		sq_throwobject(v);
		return nullptr;
	}

	if (t->get() == nullptr) {
		sq_pushstring(v, "Unexpected type. (2)", -1);
		sq_throwobject(v);
		return nullptr;
	}

	// static_pointer_cast to go up class hierarchy
	// dynamic_pointer_cast to go down/across class hierarchy
	return std::static_pointer_cast<T>(*t);
}

// NOTE: these functions are cleaner than the squirrel api
//int sqx_pushinteger(SQVM *v, int value);
//int sqx_pushfloat(SQVM *v, float value);
//int sqx_pushbool(SQVM *v, float value);


// NOTE: useful to pass const shared_ptr & if the function creates a weak pointer from the shared pointer.
template <typename T>
std::shared_ptr<T> sqx_checkinstance(SQVM *v, int idx) {
	// FIXME:
	//static_assert(std::is_convertible<T, acorn::Object>::value, "Trying to use sqx_checktype with disallowed type.");
	return sqx_checkinstance<T>(v, idx, T::type);
}

// NOTE: useful to pass const shared_ptr & if the function creates a weak pointer from the shared pointer.
template <typename T>
std::shared_ptr<T> sqx_getmodule(SQVM *v, const acorn::Type &type) {
	// FIXME:
	//static_assert(std::is_convertible<T, acorn::Module>::value, "Trying to use sqx_getmodule with disallowed type.");
	return nullptr;
}

// NOTE: useful to pass const shared_ptr & if the function creates a weak pointer from the shared pointer.
template <typename T>
std::shared_ptr<T> sqx_getmodule(SQVM *v) {
	// FIXME:
	//static_assert(std::is_convertible<T, acorn::Module>::value, "Trying to use sqx_getmodule with disallowed type.");
	return nullptr;
}

template <class T>
inline typename std::enable_if<std::is_integral<T>::value, T>::type sqx_getnumber(SQVM *v, int idx, T defval) {
	SQInteger i;
	return (SQ_FAILED(sq_getinteger(v, idx, &i))) ? defval : static_cast<T>(i);
}

template <class T>
inline typename std::enable_if<std::is_floating_point<T>::value, T>::type sqx_getnumber(SQVM *v, int idx, T defval) {
	SQFloat f;
	return (SQ_FAILED(sq_getfloat(v, idx, &f))) ? defval : static_cast<T>(f);
}

template <class T>
inline typename std::enable_if<std::is_integral<T>::value, T>::type sqx_pushnumber(SQVM *v, T t) {
	sq_pushinteger(v, (SQInteger) t);
}

template <class T>
inline typename std::enable_if<std::is_floating_point<T>::value, T>::type sqx_pushnumber(SQVM *v, T t) {
	sq_pushfloat(v, (SQFloat) t);
}

}	// acorn

#endif	// ACORN_SQUIRREL_OBJECT_H
