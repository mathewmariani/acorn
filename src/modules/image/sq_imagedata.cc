// C/C++
#include <memory>

// acorn
#include "filesystem/sq_filesystem.h"
#include "image/sq_imagedata.h"
#include "image/imagedata.h"
#include "image/image.h"

// C/C++
#include <iostream>

namespace acorn {
namespace image {

//#define instance() (Module::getInstance<image::Image>())

static SQInteger wrap_constructor(SQVM *v) {

	if (sq_gettype(v, 2) == OT_STRING) {
		std::shared_ptr<Data> data{
			filesystem::sqx_getfiledata(v, 2)
		};
		std::shared_ptr<ImageData> imagedata{
			std::make_shared<ImageData>(data)
		};
		sqx_pushinstance(v, ImageData::type, imagedata);	
	} else {
		int width, height;
		sq_getinteger(v, 2, (SQInteger *) &width);
		sq_getinteger(v, 3, (SQInteger *) &height);
		auto imagedata{
			std::make_shared<ImageData>(width, height)
		};
		sqx_pushinstance(v, ImageData::type, imagedata);
	}

	return 1;
}

static SQInteger wrap_getDimensions(SQVM *v) {
	std::weak_ptr<ImageData> instance{
		sqx_checkinstance<ImageData>(v, 1)
	};
	if (auto spt = instance.lock()) {
		int width, height;
		width = spt->getWidth();
		height = spt->getHeight();

		// create table
		sq_newtableex(v, 2);

		sq_pushstring(v, "width", -1);
		sq_pushinteger(v, width);
		sq_newslot(v, -3, SQFalse);

		sq_pushstring(v, "height", -1);
		sq_pushinteger(v, height);
		sq_newslot(v, -3, SQFalse);

		sq_newslot(v, -3, SQFalse);
		return 1;
	}
	return 0;
}

static SQInteger wrap_getWidth(SQVM *v) {
	std::weak_ptr<ImageData> instance{
		sqx_checkinstance<ImageData>(v, 1)
	};
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getWidth());
		return 1;
	}
	return 0;
}

static SQInteger wrap_getHeight(SQVM *v) {
	std::weak_ptr<ImageData> instance{
		sqx_checkinstance<ImageData>(v, 1)
	};
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger)spt->getHeight());
		return 1;
	}
	return 0;
}

static SQInteger wrap_getPixel(SQVM *v) {
	std::weak_ptr<ImageData> instance{
		sqx_checkinstance<ImageData>(v, 1)
	};
	if (auto spt = instance.lock()) {
		int x, y;
		sq_getinteger(v, 2, (SQInteger *) &x);
		sq_getinteger(v, 3, (SQInteger *) &y);

		// FIXME: this can throw an error
		std::array<uint8_t, 4> pixel;
		spt->getPixel(x, y, pixel);

		sq_newarray(v, 0);
		for (const auto& p : pixel) {
			sq_pushinteger(v, p);
			sq_arrayappend(v, -2);
		}
		return 1;
	}
	return 0;
}

static SQInteger wrap_setPixel(SQVM *v) {
	std::weak_ptr<ImageData> instance{
		sqx_checkinstance<ImageData>(v, 1)
	};
	if (auto spt = instance.lock()) {
		int x, y, r, g, b, a;
		std::array<uint8_t, 4> pixel;

		sq_getinteger(v, 2, (SQInteger *) &x);
		sq_getinteger(v, 3, (SQInteger *) &y);
		sq_getinteger(v, 4, (SQInteger *) &r);
		sq_getinteger(v, 5, (SQInteger *) &g);
		sq_getinteger(v, 6, (SQInteger *) &b);
		sq_getinteger(v, 7, (SQInteger *) &a);

		// copy color values
		pixel[0] = r;
		pixel[1] = g;
		pixel[2] = b;
		pixel[3] = a;

		// FIXME: this can throw an error
		spt->setPixel(x, y, pixel);
	}
	return 0;
}

static SQInteger wrap_getSize(SQVM *v) {
	std::weak_ptr<ImageData> instance{
		sqx_checkinstance<ImageData>(v, 1)
	};
	if (auto spt = instance.lock()) {
		sq_pushinteger(v, (SQInteger) spt->getSize());
		return 1;
	}
	return 0;
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, -2, "xs|nn", false),
	_DECL_FUNC(getDimensions, 1, "x", false),
	_DECL_FUNC(getWidth, 1, "x", false),
	_DECL_FUNC(getHeight, 1, "x", false),
	_DECL_FUNC(getPixel, 3, "xnn", false),
	_DECL_FUNC(setPixel, 7, "xnnnnnn", false),

	// implements data
	_DECL_FUNC(getSize, 1, "x", false),

	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_imagedata(SQVM *v) {
	ClassRegistry reg;
	reg.name = "ImageData";
	reg.functions = functions;

	return sqx_registerclass(v, ImageData::type, reg);
}

}	// image
}	// acorn
