// acorn
#include "audio/sq_source.h"
#include "audio/source.h"

namespace acorn {
namespace audio {

static SQInteger wrap_constructor(SQVM *v) {
	return sq_throwerror(v, "Source cannot be constructed directly.");
}

#define _DECL_FUNC(name, nparams, pmask, isstatic) { _SC(#name), wrap_##name, nparams, _SC(pmask), isstatic }
static const MethodRegistry functions[] = {
	_DECL_FUNC(constructor, 2, "xs", false),
	{ NULL, NULL, NULL }
};
#undef _DECL_FUNC

extern "C" int sq_register_source(SQVM *v) {
	ClassRegistry reg;
	reg.name = "Source";
	reg.functions = functions;

	return sqx_registerclass(v, Source::type, reg);
}

}	// audio
}	// acorn
