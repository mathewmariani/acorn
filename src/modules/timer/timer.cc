// acorn
#include "common/log.h"
#include "timer/timer.h"

// SDL2
#if defined (OS_WINDOWS)
	#include <SDL_timer.h>
#else
	#include <SDL2/SDL_timer.h>
#endif

namespace acorn {
namespace timer {

acorn::Type Timer::type = std::type_index(typeid(timer::Timer));

Timer::Timer() :
	prev(0), curr(0), dt(0) {
	log_debug("Timer module initialized.");

	// NOTE:
	// initialize sdl subsystem
	// if (SDL_InitSubSystem(SDL_INIT_TIMER) < 0) {
	// 	log_error("Could not initialize SDL subsystem: (%s)\n", SDL_GetError());
	// }

	beg_ = clock_::now();
}

Timer::~Timer() {
	log_debug("Timer module deinitialized.");

	//SDL_QuitSubSystem(SDL_INIT_TIMER);
}

double Timer::step() noexcept {
	prev = curr;
	curr = getTime();
	dt = curr - prev;
	return dt;
}

double Timer::getDeltaTime() const noexcept {
	return dt;
}

double Timer::getTime() const noexcept {
	return (double) std::chrono::duration_cast<std::chrono::nanoseconds>
		(clock_::now() - beg_).count() / 1000000000.0;
}

void Timer::sleep(unsigned int ms) const noexcept {
	if (ms > 0) {
		SDL_Delay(ms);
	}
}

}	// timer
}	// acorn
