// STL
#include <algorithm>
#include <iostream>

// acorn
#include "common/log.h"
#include "graphics/graphics.h"
#include "window/window.h"

namespace acorn {
namespace window {

acorn::Type Window::type = std::type_index(typeid(window::Window));

Window::Window() :
	window(nullptr),
	context(nullptr) {

	log_debug("Window module initialized.");

	// initialize sdl subsystem
	if (SDL_InitSubSystem(SDL_INIT_VIDEO) < 0) {
		log_error("Could not initialize SDL subsystem: (%s)\n", SDL_GetError());
	}
}

Window::~Window() {
	close();

	// release reference to graphics module
	graphics.reset();

	// deinitialize subsystem
	SDL_QuitSubSystem(SDL_INIT_VIDEO);

  log_debug("Window module deinitialized.");
}

void Window::setGraphicsDevice(std::shared_ptr<graphics::Graphics> gfx) {
	if (graphics.get()) {
		graphics = gfx;

		std::cout << __FUNCTION__ << "\n"
			<< "  gfx.get() = " << gfx.get()
			<< "  gfx.use_count() = " << gfx.use_count() << '\n';
	}
}

bool Window::setWindow(int width, int height, Settings *settings) {
	if (graphics.get() == nullptr) {
		graphics = Module::getInstance<graphics::Graphics>();
	}

	Settings s;
	if (settings) {
		s = *settings;
	}

	// set sdl flags based on preferences
	uint32_t sdlflags = SDL_WINDOW_OPENGL;

	// window position
	int x = 0, y = 0;

	// window dimensions
	windowWidth = s.width;
	windowHeight = s.height;

	// fullscreen settings
	if (s.fullscreen) {
		if (s.fmode == FullscreenMode::FULLSCREEN) {
			// "real" fullscreen with a videomode change
			sdlflags |= SDL_WINDOW_FULLSCREEN;
		} else {
			// "fake" fullscreen that takes the size of the desktop
			sdlflags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		}
	} else {
		// if were not fullscreen, we can be centered
		if (s.centered) {
			x = SDL_WINDOWPOS_CENTERED_DISPLAY(0);
			y = SDL_WINDOWPOS_CENTERED_DISPLAY(0);
		}
	}

	// borderless
	if (s.borderless) {
		sdlflags |= SDL_WINDOW_BORDERLESS;
	}

	// resizable
	if (s.resizable) {
		sdlflags |= SDL_WINDOW_RESIZABLE;
	}

	// highdpi
	if (s.highdpi) {
		sdlflags |= SDL_WINDOW_ALLOW_HIGHDPI;
	}

	if (!createWindowAndContext(x, y, width, height, sdlflags)) {
		return false;
	}

	// raise window above other windows and set the input focus
	SDL_RaiseWindow(window);

	// vsync
	SDL_GL_SetSwapInterval(s.vsync ? 1 : 0);

	if (graphics.get() != nullptr) {
		SDL_GL_GetDrawableSize(window, &pixelWidth, &pixelHeight);
		graphics->setMode(pixelWidth, pixelHeight);
	}

	return true;
}

bool Window::createWindowAndContext(int x, int y, int w, int h, uint32_t flags) {

	// use OpenGL 3.0 core
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	const auto create = [&]() -> bool {
		if (context) {
			SDL_GL_DeleteContext(context);
			context = nullptr;
		}

		if (window) {
			SDL_DestroyWindow(window);
			SDL_FlushEvent(SDL_WINDOWEVENT);
			window = nullptr;
		}

		window = SDL_CreateWindow("Acorn2D", x, y, w, h, flags);

		if (!window) {
			log_error("Could not create window: %s\n", SDL_GetError());
			return false;
		}

		context = SDL_GL_CreateContext(window);

		if (!context) {
			log_error("Could not create OpenGL context: %s\n", SDL_GetError());
		}

		if (!context) {
			SDL_DestroyWindow(window);
			window = nullptr;
			return false;
		}

		return true;
	};

	create();

	if (!window || !context) {
		log_error("Could not create an OpenGL window.\n");
		return false;
	}

	return true;
}

bool Window::getDisplayDPI(int index, float *ddpi, float *hdpi, float *vdpi) const noexcept {
	return (SDL_GetDisplayDPI(index, ddpi, hdpi, vdpi) == 0);
}

int Window::getDisplayCount() const noexcept {
	return SDL_GetNumVideoDisplays();
}

int	Window::countDisplayModes(int index) const noexcept {
	return SDL_GetNumDisplayModes(index);
}

void Window::getDesktopDimensions(int index, int &width, int &height) const noexcept {
	SDL_DisplayMode mode{};
	SDL_GetDesktopDisplayMode(index, &mode);
	width = mode.w;
	height = mode.h;
}

std::string Window::getDisplayName(int index) const noexcept {
	return SDL_GetDisplayName(index);
}

void Window::enumerateDisplayModes(int index, std::vector<DisplayMode> &list) const noexcept {
  SDL_DisplayMode mode{ SDL_PIXELFORMAT_UNKNOWN, 0, 0, 0, 0 };
  auto count = countDisplayModes(index);

	for (auto i = 0; i < count; ++i) {
		SDL_GetDisplayMode(index, i, &mode);
		list.push_back({ mode.w, mode.h, mode.refresh_rate });
	}
}

int Window::getDisplayIndex() const noexcept {
	return SDL_GetWindowDisplayIndex(window);
}

uint32 Window::getId() const noexcept {
	return SDL_GetWindowID(window);
}

void Window::setPosition(int x, int y) {
	if (window != nullptr) {
		SDL_SetWindowPosition(window, x, y);
	}
}

void Window::getPosition(int &x, int &y) const noexcept {
	if (!window) {
		x = 0; y = 0;
		return;
	}

	SDL_GetWindowPosition(window, &x, &y);
}

void Window::setDimensions(int width, int height) {
	if (window != nullptr) {
		SDL_SetWindowSize(window, width, height);
	}
}

void Window::getDimensions(int &width, int &height) const noexcept {
	if (!window) {
		width = 0; height = 0;
		return;
	}

	SDL_GetWindowSize(window, &width, &height);
}

int Window::getWidth() const {
	int width = 0;
	SDL_GetWindowSize(window, &width, nullptr);
	return width;
}

int Window::getHeight() const {
	int height = 0;
	SDL_GetWindowSize(window, nullptr, &height);
	return height;
}

void Window::getDrawableDimensions(int &width, int &height) const noexcept {
	if (!window) {
		width = 0; height = 0;
		return;
	}

	SDL_GL_GetDrawableSize(window, &width, &height);
}

int Window::getDrawableWidth() const {
	int width;
	SDL_GL_GetDrawableSize(window, &width, nullptr);
	return width;
}

int Window::getDrawableHeight() const {
	int height;
	SDL_GL_GetDrawableSize(window, nullptr, &height);
	return height;
}

void Window::setTitle(const std::string &title) noexcept {
    this->title = title;
    if (window != nullptr) {
        SDL_SetWindowTitle(window, title.c_str());
    }
}

const std::string &Window::getTitle() const noexcept {
  return title;
}

void Window::setScreenSaverEnabled(bool enable) const noexcept {
	enable ? SDL_EnableScreenSaver() : SDL_DisableScreenSaver();
}

bool Window::isScreenSaverEnabled() const noexcept {
	return (SDL_IsScreenSaverEnabled() == SDL_TRUE);
}

bool Window::isMaximized() const noexcept {
	return (window && (SDL_GetWindowFlags(window) & SDL_WINDOW_MAXIMIZED));
}

bool Window::isMinimized() const noexcept {
	return (window && (SDL_GetWindowFlags(window) & SDL_WINDOW_MINIMIZED));
}

bool Window::isHidden() const noexcept {
	return (window && (SDL_GetWindowFlags(window) & SDL_WINDOW_SHOWN));
}

bool Window::isShown() const noexcept {
	return (window && (SDL_GetWindowFlags(window) & SDL_WINDOW_HIDDEN));
}

bool Window::isFullscreen() const noexcept {
	return (window && (SDL_GetWindowFlags(window) & SDL_WINDOW_FULLSCREEN));
}

bool Window::isFullscreenDesktop() const noexcept {
    return (window && (SDL_GetWindowFlags(window) & SDL_WINDOW_FULLSCREEN_DESKTOP));
}

bool Window::setFullscreen(bool fullscreen, FullscreenMode mode) {
	if (window == nullptr) {
		return false;
	}

	uint32 sdlflags = (mode == FullscreenMode::FULLSCREEN) ? SDL_WINDOW_FULLSCREEN : SDL_WINDOW_FULLSCREEN_DESKTOP;

	if (fullscreen) {
		if (mode == FullscreenMode::FULLSCREEN) {
			SDL_DisplayMode mode{};
			mode.w = windowWidth;
			mode.w = windowHeight;

			// use this function to get the closest match to the requested display mode.
			SDL_GetClosestDisplayMode(
				SDL_GetWindowDisplayIndex(window), &mode, &mode
			);

			// use this function to set the display mode to use when a window is visible at fullscreen.
			SDL_SetWindowDisplayMode(window, &mode);
		}
	} else {
		sdlflags = 0;
	}

	// use this function to set a window's fullscreen state.
	if (SDL_SetWindowFullscreen(window, sdlflags) == 0) {
		SDL_GL_MakeCurrent(window, context);
		return true;
	}

	return false;
}

bool Window::hasKeyboardFocus() const noexcept {
	return (window && (SDL_GetKeyboardFocus() == window));
}

bool Window::hasMouseFocus() const noexcept {
	return (window && (SDL_GetKeyboardFocus() == window));
}

void Window::maximize() const noexcept {
	if (window != nullptr) {
		SDL_MaximizeWindow(window);
	}
}

void Window::minimize() const noexcept {
	if (window != nullptr) {
		SDL_MinimizeWindow(window);
	}
}

void Window::hide() const noexcept {
	if (window != nullptr) {
		SDL_HideWindow(window);
	}
}

void Window::restore() const noexcept {
	if (window != nullptr) {
		SDL_RestoreWindow(window);
	}
}

void Window::raise() const noexcept {
	if (window != nullptr) {
		SDL_RaiseWindow(window);
	}
}

void Window::show() const noexcept {
	if (window != nullptr) {
		SDL_ShowWindow(window);
	}
}

void Window::close() noexcept {
	// delete and clean up the OpenGL context
	if (context) {
		SDL_GL_DeleteContext(context);
	}

	// destroy and cleanup the window
	if (window) {
		SDL_DestroyWindow(window);
	}

	context = nullptr;
	window = nullptr;
}

void Window::swapBuffers() const noexcept {
	SDL_GL_SwapWindow(window);
}

const void *Window::getHandle() const noexcept {
	return window;
}

bool Window::isValidDisplayIndex(int index) const noexcept {
	return (index >= 0 && index <= SDL_GetNumVideoDisplays());
}

}	// window
}	// acorn
