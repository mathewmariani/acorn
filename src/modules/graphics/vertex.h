#ifndef ACORN_GRAPHICS_VERTEX_H
#define ACORN_GRAPHICS_VERTEX_H

namespace acorn {
namespace graphics {

struct Colorf {
	float r;
	float g;
	float b;
	float a;
};	// colorf

struct Vertex {
	float x, y;
	float u, v;
};	// vertex

// NOTE: possible implementation
// GLM
//#include <glm/glm.hpp>
//struct Vertex_glm {
//	glm::vec3 position;
//	glm::vec2 texcoord;
//};	// vertex

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_VERTEX_H
