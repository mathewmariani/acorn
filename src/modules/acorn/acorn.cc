// STL
#include <iostream>

// acorn
#include "common/squirrel.h"
#include "common/platform.h"
#include "acorn/acorn.h"

extern "C" {
	extern int sq_acorn_audio(SQVM*);
	extern int sq_acorn_event(SQVM*);
	extern int sq_acorn_filesystem(SQVM*);
	extern int sq_acorn_font(SQVM*);
	extern int sq_acorn_graphics(SQVM*);
	extern int sq_acorn_image(SQVM*);
	// extern int sq_acorn_input(SQVM*);
	extern int sq_acorn_joystickmodule(SQVM*);
	extern int sq_acorn_keyboard(SQVM*);
	extern int sq_acorn_mathmodule(SQVM*);
	extern int sq_acorn_mouse(SQVM*);
	//extern int sq_acorn_resource(SQVM*);
	extern int sq_acorn_system(SQVM*);
	extern int sq_acorn_timer(SQVM*);
	extern int sq_acorn_window(SQVM*);
}

static struct { const char* name; int(*fn)(SQVM *v); } modules[] = {
	{ "audio", sq_acorn_audio },
	{ "event", sq_acorn_event },
	{ "filesystem", sq_acorn_filesystem },
	{ "font", sq_acorn_font },
	{ "graphics", sq_acorn_graphics },
	{ "image", sq_acorn_image },
	{ "joystick", sq_acorn_joystickmodule },
	{ "keyboard", sq_acorn_keyboard },
	{ "math", sq_acorn_mathmodule },
	{ "mouse", sq_acorn_mouse },
	//{ "resource", sq_acorn_resource },
	{ "system", sq_acorn_system },
	{ "timer", sq_acorn_timer },
	{ "window", sq_acorn_window },
	{ nullptr, nullptr }
};

int sq_register_acorn(SQVM *v) {
	// set print functions
	sq_setprintfunc(v, acorn::sqx_printfunc, acorn::sqx_errorfunc);

	// create acorn module
	//sq_pushroottable(v);
	//sq_newtable(v);
	//sq_pushstring(v, "acorn", -1);
	//sq_push(v, -2);
	//sq_rawset(v, -4);

	// NOTE: embed the modules last
	for (int i = 0; modules[i].name; i++) {
		modules[i].fn(v);
	}

	//sq_pop(v, 1);
	return 0;
}

int sq_acorn_boot(SQVM *v) {
	// init embedded scripts
	const char boot_nut[] =
		#include "scripts/boot.nut"
		;

	int retval = 0;
	if (SQ_SUCCEEDED(sq_compilebuffer(v, (const char *) boot_nut, sizeof(boot_nut), "boot.nut", SQTrue))) {
		sq_pushroottable(v);
		if (SQ_SUCCEEDED(sq_call(v, 1, SQTrue, SQTrue))) {
			// get the return value from squirrel
			sq_getinteger(v, -1, (SQInteger*) &retval);
			// pops the root, function, return value
			//sq_pop(v, 3);
		}
	}

	return retval;
}
