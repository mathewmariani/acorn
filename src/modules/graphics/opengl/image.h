#ifndef ACORN_GRAPHICS_OPENGL_IMAGE_H
#define ACORN_GRAPHICS_OPENGL_IMAGE_H

// acorn
#include "graphics/opengl/opengl.h"
#include "graphics/image.h"

namespace acorn {
namespace graphics {
namespace opengl {

// forward declaration
class Graphics;

class Image final : public acorn::graphics::Image {
public:
	Image(std::shared_ptr<image::ImageData> imagedata);
	virtual ~Image();

public:
	void setFilter(const Texture::FilterMode mode) noexcept override;
	void setWrap(const Texture::WrapMode mode) noexcept override;

public:
	// managed opengl resource
	const void *getHandle() const override;

private:
	// openGL texture identifier
	GLuint texture;
};	// image

}	// opengl
}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_OPENGL_IMAGE_H
