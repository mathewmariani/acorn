// acorn
#include "graphics/sq_framebuffer.h"
#include "graphics/sq_graphics.h"
#include "graphics/sq_graphicsdevice.h"
#include "graphics/sq_image.h"
#include "graphics/sq_particlesystem.h"
#include "graphics/sq_quad.h"
#include "graphics/sq_shader.h"
#include "graphics/sq_spritebatch.h"
#include "graphics/sq_texture.h"
#include "graphics/graphics.h"
#include "graphics/shader.h"
#include "window/window.h"

namespace acorn {
namespace graphics {

#define instance() (Module::getInstance<graphics::Graphics>())

static SQInteger wrap_clear(SQVM *v) {
	float r = sqx_getnumber(v, 2, 0.0f);
	float g = sqx_getnumber(v, 3, 0.0f);
	float b = sqx_getnumber(v, 4, 0.0f);
	float a = sqx_getnumber(v, 5, 0.0f);

	instance()->clear(r, g, b, a);
	return 0;
}

static SQInteger wrap_present(SQVM *v) {
	instance()->present();
	return 0;
}

static SQInteger wrap_setColor(SQVM *v) {
	float r = sqx_getnumber(v, 2, 0.0f);
	float g = sqx_getnumber(v, 3, 0.0f);
	float b = sqx_getnumber(v, 4, 0.0f);
	float a = sqx_getnumber(v, 5, 0.0f);

	instance()->setColor(r, g, b, a);
	return 0;
}

static SQInteger wrap_setShader(SQVM *v) {
	if (sq_gettype(v, 2) == OT_NULL) {
		return 0;
	}

	std::shared_ptr<Shader> shader(sqx_checkinstance<Shader>(v, 2));
	instance()->setShader(shader.get());
	return 0;
}

static SQInteger wrap_push(SQVM *v) {
	instance()->push();
	return 0;
}

static SQInteger wrap_pop(SQVM *v) {
	instance()->pop();
	return 0;
}

static SQInteger wrap_identity(SQVM *v) {
	instance()->identity();
	return 0;
}

static SQInteger wrap_translate(SQVM *v) {
	float x = sqx_getnumber(v, 2, 0.0f);
	float y = sqx_getnumber(v, 3, 0.0f);

	instance()->translate(x, y);
	return 0;
}

static SQInteger wrap_rotate(SQVM *v) {
	float a = sqx_getnumber(v, 2, 0.0f);

	instance()->rotate(a);
	return 0;
}

static SQInteger wrap_scale(SQVM *v) {
	float sx = sqx_getnumber(v, 2, 0.0f);
	float sy = sqx_getnumber(v, 3, 0.0f);

	instance()->scale(sx, sy);
	return 0;
}

static SQInteger wrap_points(SQVM *v) {
	STUBBED("wrap_points");
	return 0;
}

// NOTE: functions for acorn.graphics
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	_DECL_FUNC(clear, 5, "tnnnn"),
	_DECL_FUNC(present, 1, "t"),
	_DECL_FUNC(setColor, 5, "tnnnn"),

	// shaders
	_DECL_FUNC(setShader, 2, "tx|o"),

	// matrix transformations
	_DECL_FUNC(push, 1, "t"),
	_DECL_FUNC(pop, 1, "t"),
	_DECL_FUNC(identity, 1, "t"),
	_DECL_FUNC(translate, 3, "tnn"),
	_DECL_FUNC(rotate, 2, "tn"),
	_DECL_FUNC(scale, 3, "tnn"),

	// primitives
	_DECL_FUNC(points, 3, "tnn"),

	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.graphics
const static SQFunction classes[] = {
	sq_register_framebuffer,
	sq_register_graphicsdevice,
	sq_register_image,
	sq_register_particlesystem,
	sq_register_quad,
	sq_register_shader,
	// sq_register_spritebatch,
	// sq_register_texture2d,
	nullptr
};

extern "C" int sq_acorn_graphics(SQVM *v) {
	auto instance = std::make_shared<graphics::Graphics>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize graphics module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "graphics";
	reg.classes = classes;

	return acorn::sqx_registermodule_ext(v, reg);
}

}	// graphics
}	// acorn
