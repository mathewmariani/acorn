#ifndef ACORN_SQ_AUDIO_SOURCE_H
#define ACORN_SQ_AUDIO_SOURCE_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace audio {

extern "C" int sq_register_source(SQVM *v);

}	// audio
}	// acorn

#endif	// ACORN_SQ_AUDIO_SOURCE_H
