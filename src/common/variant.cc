// acorn
#include "common/variant.h"

namespace acorn {

Variant::Variant() :
	type(T_NULL) {

}

Variant::Variant(bool boolean) :
	type(T_BOOL) {
	data.t_bool = boolean;
}

Variant::Variant(int number) :
	type(T_INTEGER) { 
	data.t_int = number;
}

Variant::Variant(float number) :
	type(T_FLOAT) {
	data.t_float = number;
}

Variant::Variant(const std::string &string) :
	type(T_STRING) {
	data.t_string = new std::string(string);
}

//Variant::Variant(acorn::Type &type, std::shared_ptr<acorn::Object> instance) :
//	type(T_INSTANCE) {
//	data.t_instance->object = instance;
//	data.t_instance->type = &type;
//}

Variant::Variant(void *userptr) :
	type(T_USERPOINTER) {
	data.t_userptr = userptr;
}

Variant::~Variant() {
	if (type == T_STRING) {
		delete data.t_string;
	} else if (type == T_INSTANCE) {
		//data.t_instance->object.reset();
	}
}

Variant &Variant::operator = (const Variant &v) {
	type = v.type;
	data = v.data;
	return *this;
}

void Variant::toSquirrel(SQVM *v) const noexcept {
	switch (type) {
	case T_BOOL:
		sq_pushbool(v, data.t_bool);
		break;
	case T_INTEGER:
		sq_pushinteger(v, data.t_int);
		break;
	case T_FLOAT:
		sq_pushfloat(v, data.t_float);
		break;
	case T_STRING:
		sq_pushstring(v, data.t_string->c_str(), data.t_string->length());
		break;
	case T_INSTANCE:
		//sqx_pushinstance(v, *data.t_instance->type, data.t_instance->object);
		sq_pushnull(v);
		break;
	case T_USERPOINTER:
		sq_pushuserpointer(v, data.t_userptr);
		break;
	case T_NULL:
	default:
		sq_pushnull(v);
		break;
	}
}

}	// acorn
