---
layout: doc
title: Alternatives
---

Acorn isn't an original idea; many other frameworks have come before it, and many of them are the inspiration for what Acorn is. Each alternative provides its own benefits, and may be better suited for your project. The following alternatives share similar ideals.

- [LÖVE](https://love2d.org)
- [MonoGame](http://www.monogame.net/)
- [Phaser](https://phaser.io/)

###### Others

Some people may want a little more for their projects. The following list of alternatives provides more than just a simple framework for making games.

- [Unity3D](https://unity3d.com/)
- [GameMaker Studio](https://www.yoyogames.com/gamemaker)
- [Construct 2](https://www.scirra.com/)
