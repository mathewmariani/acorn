// acorn
#include "common/log.h"
#include "math/mathmodule.h"

namespace acorn {
namespace math {

acorn::Type MathModule::type = std::type_index(typeid(math::MathModule));

MathModule::MathModule() {
	log_debug("Math module initialized.");
}

MathModule::~MathModule() {
	log_debug("Math module deinitialized.");
}

}	// math
}	// acorn
