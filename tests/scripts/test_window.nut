R"squirrel"#(

# window
describe("acorn.window", function() {
	# display
	describe("acorn.window.Display", function() {
		# typeof
		it("is a typeof of class", function() {
			assert.is_typeof(Display, "class")
		})

		# functions
		it("contains a function `constructor()`", function() {
			assert.is_in("constructor", Display)
			assert.is_typeof(Display.constructor, "function")
		})
		it("contains a function `count()`", function() {
			assert.is_in("count", Display)
			assert.is_typeof(Display.count, "function")
		})
		it("contains a function `countModes()`", function() {
			assert.is_in("countModes", Display)
			assert.is_typeof(Display.countModes, "function")
		})
		it("contains a function `getName()`", function() {
			assert.is_in("getName", Display)
			assert.is_typeof(Display.getName, "function")
		})
		it("contains a function `listModes()`", function() {
			assert.is_in("listModes", Display)
			assert.is_typeof(Display.listModes, "function")
		})
		it("contains a function `setScreenSaverEnabled()`", function() {
			assert.is_in("setScreenSaverEnabled", Display)
			assert.is_typeof(Display.setScreenSaverEnabled, "function")
		})
		it("contains a function `isScreenSaverEnabled()`", function() {
			assert.is_in("isScreenSaverEnabled", Display)
			assert.is_typeof(Display.isScreenSaverEnabled, "function")
		})
	})

	# window
	describe("acorn.window.Window", function() {
		# typeof
		it("is a typeof of class", function() {
			assert.is_typeof(Window, "class")
		})

		# functions
		it("contains a function `constructor()`", function() {
			assert.is_in("constructor", Window)
			assert.is_typeof(Window.constructor, "function")
		})
		it("contains a function `setWindow()`", function() {
			assert.is_in("setWindow", Window)
			assert.is_typeof(Window.setWindow, "function")
		})
		it("contains a function `getDisplayIndex()`", function() {
			assert.is_in("getDisplayIndex", Window)
			assert.is_typeof(Window.getDisplayIndex, "function")
		})
		it("contains a function `getTitle()`", function() {
			assert.is_in("getTitle", Window)
			assert.is_typeof(Window.getTitle, "function")
		})
		it("contains a function `setTitle()`", function() {
			assert.is_in("setTitle", Window)
			assert.is_typeof(Window.setTitle, "function")
		})
		it("contains a function `setPosition()`", function() {
			assert.is_in("setPosition", Window)
			assert.is_typeof(Window.setPosition, "function")
		})
		it("contains a function `getPosition()`", function() {
			assert.is_in("getPosition", Window)
			assert.is_typeof(Window.getPosition, "function")
		})
		it("contains a function `getX()`", function() {
			assert.is_in("getX", Window)
			assert.is_typeof(Window.getX, "function")
		})
		it("contains a function `getY()`", function() {
			assert.is_in("getY", Window)
			assert.is_typeof(Window.getY, "function")
		})
		it("contains a function `setDimensions()`", function() {
			assert.is_in("setDimensions", Window)
			assert.is_typeof(Window.setDimensions, "function")
		})
		it("contains a function `getDimensions()`", function() {
			assert.is_in("getDimensions", Window)
			assert.is_typeof(Window.getDimensions, "function")
		})
		it("contains a function `getWidth()`", function() {
			assert.is_in("getWidth", Window)
			assert.is_typeof(Window.getWidth, "function")
		})
		it("contains a function `getHeight()`", function() {
			assert.is_in("getHeight", Window)
			assert.is_typeof(Window.getHeight, "function")
		})
		it("contains a function `getDrawableDimensions()`", function() {
			assert.is_in("getDrawableDimensions", Window)
			assert.is_typeof(Window.getDrawableDimensions, "function")
		})
		it("contains a function `getDrawableWidth()`", function() {
			assert.is_in("getDrawableWidth", Window)
			assert.is_typeof(Window.getDrawableWidth, "function")
		})
		it("contains a function `getDrawableHeight()`", function() {
			assert.is_in("getDrawableHeight", Window)
			assert.is_typeof(Window.getDrawableHeight, "function")
		})
		it("contains a function `hasKeyboardFocus()`", function() {
			assert.is_in("hasKeyboardFocus", Window)
			assert.is_typeof(Window.hasKeyboardFocus, "function")
		})
		it("contains a function `hasKeyboardFocus()`", function() {
			assert.is_in("hasKeyboardFocus", Window)
			assert.is_typeof(Window.hasKeyboardFocus, "function")
		})
		it("contains a function `hasMouseFocus()`", function() {
			assert.is_in("hasMouseFocus", Window)
			assert.is_typeof(Window.hasMouseFocus, "function")
		})
		it("contains a function `maximize()`", function() {
			assert.is_in("maximize", Window)
			assert.is_typeof(Window.maximize, "function")
		})
		it("contains a function `minimize()`", function() {
			assert.is_in("minimize", Window)
			assert.is_typeof(Window.minimize, "function")
		})
		it("contains a function `hide()`", function() {
			assert.is_in("hide", Window)
			assert.is_typeof(Window.hide, "function")
		})
		it("contains a function `restore()`", function() {
			assert.is_in("restore", Window)
			assert.is_typeof(Window.restore, "function")
		})
		it("contains a function `raise()`", function() {
			assert.is_in("raise", Window)
			assert.is_typeof(Window.raise, "function")
		})
		it("contains a function `show()`", function() {
			assert.is_in("show", Window)
			assert.is_typeof(Window.show, "function")
		})
		it("contains a function `close()`", function() {
			assert.is_in("close", Window)
			assert.is_typeof(Window.close, "function")
		})
	})
})

#)squirrel"#"
