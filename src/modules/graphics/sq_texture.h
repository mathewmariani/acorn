#ifndef ACORN_SQ_GRAPHICS_TEXTURE2D_H
#define ACORN_SQ_GRAPHICS_TEXTURE2D_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace graphics {

extern "C" int sq_register_texture2d(SQVM *v);

}	// graphics
}	// acorn

#endif	// ACORN_SQ_GRAPHICS_TEXTURE2D_H
