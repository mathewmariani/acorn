#ifndef ACORN_SQ_GLYPHDATA_H
#define ACORN_SQ_GLYPHDATA_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace resource {

extern "C" int sq_register_glyphdata(SQVM *v);

}	// resource
}	// acorn

#endif	// ACORN_SQ_GLYPHDATA_H
