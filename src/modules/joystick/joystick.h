#ifndef ACORN_JOYSTICK_H
#define ACORN_JOYSTICK_H

// STL
#include <string>
#include <unordered_map>

// acorn
#include "common/object.h"
#include "common/platform.h"

// SDL
#include <SDL.h>

namespace acorn {
namespace joystick {

class Joystick final : public acorn::Object {
public:
	static acorn::Type type;

public:
	enum class PowerState {
		UNKNOWN = -1,
		EMPTY = 0,
		LOW = 1,
		MEDIUM = 2,
		FULL = 3,
		WIRED = 4,

		// always last.
		POWERSTATE_MAX_ENUM
	};	// powerstate

	enum class JoystickHat : Uint8 {
		centered = SDL_HAT_CENTERED,
		up = SDL_HAT_UP,
		right = SDL_HAT_RIGHT,
		down = SDL_HAT_DOWN,
		left = SDL_HAT_LEFT,
		right_up = SDL_HAT_RIGHTUP,
		right_down = SDL_HAT_RIGHTDOWN,
		left_up = SDL_HAT_LEFTUP,
		left_down = SDL_HAT_LEFTDOWN,
	};	// joystickhat

	enum class ControllerAxis {
		INVALID = -1,
		LEFTX = 0,
		LEFTY = 1,
		RIGHTX = 2,
		RIGHTY = 3,
		TRIGGERLEFT = 4,
		TRIGGERRIGHT = 5,

		// always last.
		CONTROLLERAXIS_MAX_ENUM
	};	// controlleraxis

	enum class InputType {
		NONE = 0,
		BUTTON = 1,
		AXIS = 2,
		HAT = 3,

		// always last.
		INPUTTYPE_MAX_ENUM
	};	// inputtype

	enum class ControllerButton {
		INVALID = -1,
		A = 0,
		B = 1,
		X = 2,
		Y = 3,
		BACK = 4,
		GUIDE = 5,
		START = 6,
		LEFTSTICK = 7,
		RIGHTSTICK = 8,
		LEFTSHOULDER = 9,
		RIGHTSHOULDER = 10,
		DPAD_UP = 11,
		DPAD_DOWN = 12,
		DPAD_LEFT = 13,
		DPAD_RIGHT = 14,

		// always last.
		CONTROLLERAXIS_MAX_ENUM
	};	// controllerbutton

public:
	Joystick(int deviceid);
	virtual ~Joystick();

public:
	// use this function to open a joystick for use.
	bool open(int index);

	// use this function to close a joystick previously opened.
	void close();

public:
	// use this function to get the implementation dependent name of a joystick.
	const std::string getName() noexcept;

	// use this function to get the implementation-dependent GUID for the joystick.
	const std::string getGUID() noexcept;

	// use this function to get the status of a specified joystick.
	bool isAttached() const noexcept;

	// use this function to get the number of general axis controls on a joystick.
	int getAxisCount() const noexcept;

	// use this function to get the number of trackballs on a joystick.
	int getBallCount() const noexcept;

	// use this function to get the number of POV hats on a joystick.
	int getHatCount() const noexcept;

	// use this function to get the number of buttons on a joystick.
	int getButtonCount() const noexcept;

	// use this function to get the battery level of a joystick.
	PowerState getPowerState() const noexcept;

	// use this function to check if the given joystick is supported by the game controller interface.
	bool isGameController() const noexcept;

	// use this function to get the current state of a hat on a joystick.
	JoystickHat getHat(int index) const noexcept;

	// use this function to get the current state of a button on a joystick.
	bool getButton(int index) const noexcept;

public:
	// use this function to get the instance ID of an opened joystick.
	int getInstanceID() const noexcept;

public:
	// use this function to get the pointer to the underlying joystic object.
	// NOTE: cast as SDL_Joystick*
	const void *getHandle() const noexcept;

public:
	static float clampval(float x);

private:
	SDL_JoystickID instanceid;
	int deviceid;
	std::string name;
	std::string guid;

private:
	// managed sdl resources
	SDL_Joystick *joyhandle;
	SDL_GameController *controller;
	SDL_Haptic *haptic;

public:
	// FIXME: I don't know if this is needed
	//static std::unordered_map<std::string, PowerState> powerstateTable;
	//static std::unordered_map<std::string, JoystickHat> joystickhatTable;
	//static std::unordered_map<std::string, ControllerAxis> controlleraxisTable;
	//static std::unordered_map<std::string, InputType> inputtypeTable;
	//static std::unordered_map<std::string, ControllerButton> controllerbuttonTable;
};	// joystick

}	// joystick
}	// acorn

#endif	// ACORN_JOYSTICK_H
