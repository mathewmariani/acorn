#ifndef ACORN_SQ_SYSTEM_APPLICATIOJ_H
#define ACORN_SQ_SYSTEM_APPLICATIOJ_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace system {

extern "C" int sq_acorn_application(SQVM *v);

}	// system
}	// acorn

#endif	// ACORN_SQ_SYSTEM_APPLICATIOJ_H
