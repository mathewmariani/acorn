#ifndef ACORN_GRAPHICS_RESOURCE_H
#define ACORN_GRAPHICS_RESOURCE_H

namespace acorn {
namespace graphics {

class Resource {
public:
	Resource();
	virtual ~Resource();

	// managed resource
	virtual const void *getHandle() const = 0;
};	// resource

}	// graphics
}	// acorn

#endif	// ACORN_GRAPHICS_RESOURCE_H
