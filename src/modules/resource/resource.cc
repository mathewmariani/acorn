// acorn
#include "common/log.h"
#include "resource/resource.h"

namespace acorn {
namespace resource {

acorn::Type Resource::type = std::type_index(typeid(resource::Resource));

Resource::Resource() {
	log_debug("Resource module initialized.");
}

Resource::~Resource() {
	log_debug("Resource module deinitialized.");
}

}	// resource
}	// acorn
