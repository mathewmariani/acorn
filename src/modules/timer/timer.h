#ifndef ACORN_TIMER_H
#define ACORN_TIMER_H

// STL
#include <chrono>

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"

namespace acorn {
namespace timer {

class Timer : public acorn::Module {
public:
	static acorn::Type type;

private:
	using clock_ = std::chrono::high_resolution_clock;
	using nsec_ = std::chrono::duration<double, std::chrono::nanoseconds>;

public:
	Timer();
	virtual ~Timer();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:
	double step() noexcept;

	double getDeltaTime() const noexcept;
	double getTime() const noexcept;
	void sleep(unsigned int ms) const noexcept;

private:
	double prev;
	double curr;
	double dt;

	std::chrono::time_point<clock_> beg_;
};	// timer

}	// timer
}	// acorn

#endif	// ACORN_TIMER_H
