R"squirrel"#(

# global stack
stack <- []

enum Results {
  PASSED,
  FAILED
}

# helper class for building specifications
class SpecBuilder {
	constructor(_name, _fn) {
  	Spec(_name, _fn, stack.top())
  }
}

# helper class for building specification groups
class GroupBuilder {
  constructor(_name, _fn, skipped = false, only = false) {
  	Group(_name, _fn, stack.top()).parse()
  }
}

# specification class
class Spec {
	name = null
	fn = null
	parent = null

	constructor(_name, _fn, _parent) {
		name = _name
		fn = _fn
		parent = _parent

		parent.queue(this)
	}

	function run() {
		# started
		print("\t")

		try {
			fn();
			# specification passed
			print("[ ] "+ name + "\n")
			return [Results.PASSED]
		} catch (e) {
			# specification failed
			print("[X] "+ name + "\n")
			print("\t"+e+"\n");
			return [Results.FAILED]
		}
	}
}

class Group {
	name = null
	fn = null
	parent = null
	runQueue = null

	# builders
	it = SpecBuilder
	describe = GroupBuilder

	constructor(_name, _fn, _parent) {
		name = _name
		fn = _fn
		parent = _parent
		runQueue = []

		if (_parent) {
			_parent.queue(this)
		}
	}

	function parse() {
		stack.push(this)
		fn()
		stack.pop()
	}

	function queue(runnable) {
	  runQueue.push(runnable)
	}

	function run() {
		# group started
		print(name + "\n")

		local results = []
		foreach(runnable in runQueue) {
			results.extend(runnable.run())
		}

		# group ended
		print("")
		return results
	}
}

# driver class for testing
class Hazel {
	root = null
	constructor() {

	}

	function addTest(name, fn) {
		root = Group(name, fn, null)
		root.parse()
	}

	function runTests() {
		local results = root.run()
		local passed = results.filter(@(index, item) item == Results.PASSED).len()
		local failed = results.filter(@(index, item) item == Results.FAILED).len()

		print("\n")
		print(passed + " passing\n")
		print(failed + " failing\n")
		print("\n")
		print("\n")
	}
}

# global
hazel <- Hazel()

# what exactly does `it()` do?
# can be thought of as a specification.
class it {

}

# what exactly does `describe()` do?
# can be thought of as a specification group
class describe {
  constructor(name, fn) {
		hazel.addTest(name, fn)
		hazel.runTests()
  }
}

# assertions
class assert {
	static function equals(result, expected, message = "Failed to assert that %s == %s") {
		if (expected == result) return;
		throw format(message, expected.tostring(), result.tostring());
	}

	static function not_equals(result, expected, message = "Failed to assert that %s != %s") {
		if (expected != result) return;
		throw format(message, expected.tostring(), result.tostring());
	}

	function greater_than(a, b, message = "Failed to assert that %s > %s") {
		if (a > b) return;
		throw format(message, a.tostring(), b.tostring());
	}

	function less_than(a, b, message = "Failed to assert that %s < %s") {
		if (a < b) return;
		throw format(message, a.tostring(), b.tostring());
	}

	function at_most(a, b, message = "Failed to assert that %s <= %s") {
		if (a <= b) return;
		throw format(message, a.tostring(), b.tostring());
	}

	function at_least(a, b, message = "Failed to assert that %s >= %s") {
		if (a >= b) return;
		throw format(message, a.tostring(), b.tostring());
	}

	static function between(value, min, max, inclusive = false, message = "Expected value to be between %s..%s, got %s") {
		if (inclusive ? min <= value && value <= max : min < value && value < max) return;
		throw format(message, min.tostring(), max.tostring(), value.tostring());
	}

	static function length_of(target, length, message = "Expected %s to be length of %d") {
		if (target.len() == length) return;
		throw format(message, target.tostring(), length);
	}

	static function is_empty(target, message = "Expected %s to be empty") {
		if (target.len() == 0) return;
		throw format(message, target.tostring());
	}

	static function is_not_empty(target, message = "Expected %s to not be empty") {
		if (target.len() != 0) return;
		throw format(message, target.tostring());
	}

	static function is_true(value, message = "Failed to assert that %s is true") {
		if (value) return;
		throw format(message, value);
	}

	static function is_false(value, message = "Failed to assert that %s is false") {
		if (!value) return;
		throw format(message, value.tostring());
	}

	static function is_typeof(value, name, message = "Failled to assert that %s is typeof %s") {
		if (typeof(value) == name) return;
		throw format(message, value.tostring(), name);
	}

	static function is_not_typeof(value, name, message = "Failled to assert that %s is not typeof %s") {
		if (typeof(value) != name) return;
		throw format(message, value.tostring(), name);
	}

	static function is_integer(value, message = "Failed to assert that %s is an integer") {
		if (typeof(value) == "integer") return;
		throw format(message, value.tostring());
	}

	static function is_not_integer(value, message = "Failed to assert that %s is not an integer") {
		if (typeof(value) != "integer") return;
		throw format(message, value.tostring());
	}

	static function is_float(value, message = "Failed to assert that %s is a float") {
		if (typeof(value) == "float") return;
		throw format(message, value.tostring());
	}

	static function is_not_float(value, message = "Failed to assert that %s is not a float") {
		if (typeof(value) != "float") return;
		throw format(message, value.tostring());
	}

	static function is_string(value, message = "Failed to assert that %s is a string") {
		if (typeof(value) == "string") return;
		throw format(message, value.tostring());
	}

	static function is_not_string(value, message = "Failed to assert that %s is not a string") {
		if (typeof(value) != "string") return;
		throw format(message, value.tostring());
	}

	static function is_null(value, message = "Failed to assert that %s is null") {
		if (typeof(value) == "null") return;
		throw format(message, value.tostring());
	}

	static function is_not_null(value, message = "Failed to assert that %s is not null") {
		if (typeof(value) != "null") return;
		throw format(message, value.tostring());
	}

	static function is_bool(value, message = "Failed to assert that %s is a bool") {
		if (typeof(value) == "bool") return;
		throw format(message, value.tostring());
	}

	static function is_not_bool(value, message = "Failed to assert that %s is not a bool") {
		if (typeof(value) != "bool") return;
		throw format(message, value.tostring());
	}

	static function is_table(value, message = "Failed to assert that %s is a table") {
		if (typeof(value) == "table") return;
		throw format(message, value.tostring());
	}

	static function is_not_table(value, message = "Failed to assert that %s is not a table") {
		if (typeof(value) != "table") return;
		throw format(message, value.tostring());
	}

	static function is_array(value, message = "Failed to assert that %s is an array") {
		if (typeof(value) == "array") return;
		throw format(message, value.tostring());
	}

	static function is_not_array(value, message = "Failed to assert that %s is not an array") {
		if (typeof(value) != "array") return;
		throw format(message, value.tostring());
	}

	static function is_function(value, message = "Failed to assert that %s is a function") {
		if (typeof(value) == "function") return;
		throw format(message, value.tostring());
	}

	static function is_not_function(value, message = "Failed to assert that %s is not a function") {
		if (typeof(value) != "function") return;
		throw format(message, value.tostring());
	}

	static function is_class(value, message = "Failed to assert that %s is a class") {
		if (typeof(value) == "class") return;
		throw format(message, value.tostring());
	}

	static function is_not_class(value, message = "Failed to assert that %s is not a class") {
		if (typeof(value) != "class") return;
		throw format(message, value.tostring());
	}

	static function is_instance(value, message = "Failed to assert that %s is an instance") {
		if (typeof(value) == "instance") return;
		throw format(message, value.tostring());
	}

	static function is_not_instance(value, message = "Failed to assert that %s is not an instance") {
		if (typeof(value) != "instance") return;
		throw format(message, value.tostring());
	}

	static function is_instance_of(value, instance, message = "Failed to assert that %s is an instance of %s") {
		if (value instanceof instance) return;
		throw format(message, value.tostring(), instance.tostring());
	}

	static function is_not_instance_of(value, instance, message = "Failed to assert that %s is an instance of %s") {
		if (!(value instanceof instance)) return;
		throw format(message, value.tostring(), instance.tostring());
	}

	static function is_generator(value, message = "Failed to assert that %s is a generator") {
		if (typeof(value) == "generator") return;
		throw format(message, value.tostring());
	}

	static function is_not_generator(value, message = "Failed to assert that %s is not a generator") {
		if (typeof(value) != "generator") return;
		throw format(message, value.tostring());
	}

	static function is_userdata(value, message = "Failed to assert that %s is userdata") {
		if (typeof(value) == "userdata") return;
		throw format(message, value.tostring());
	}

	static function is_not_userdata(value, message = "Failed to assert that %s is not userdata") {
		if (typeof(value) != "userdata") return;
		throw format(message, value.tostring());
	}

	static function is_thread(value, message = "Failed to assert that %s is a thread") {
		if (typeof(value) == "thread") return;
		throw format(message, value.tostring());
	}

	static function is_not_thread(value, message = "Failed to assert that %s is not a thread") {
		if (typeof(value) != "thread") return;
		throw format(message, value.tostring());
	}

	static function is_weakref(value, message = "Failed to assert that %s is a weakref") {
		if (typeof(value) == "weakref") return;
		throw format(message, value.tostring());
	}

	static function is_not_weakref(value, message = "Failed to assert that %s is not a weakref") {
		if (typeof(value) != "weakref") return;
		throw format(message, value.tostring());
	}

	static function is_in(a, b, message = "Failed to assert that %s is in %s") {
		if (a in b) return;
		throw format(message, value.tostring());
	}

	static function is_not_in(a, b, message = "Failed to assert that %s is not in %s") {
		if (!(a in b)) return;
		throw format(message, value.tostring());
	}
}

#)squirrel"#"
