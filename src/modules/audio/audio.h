#ifndef ACORN_AUDIO_H
#define ACORN_AUDIO_H

// acorn
#include "common/module.h"
#include "common/platform.h"
#include "common/type.h"
#include "audio/source.h"

// OpenAL
#if defined(OS_APPLE)
	#include "OpenAL/al.h"
	#include "OpenAL/alc.h"
#else
	#include <al.h>
	#include <alc.h>
#endif

namespace acorn {
namespace audio {

class Audio : public acorn::Module {
public:
	static acorn::Type type;

public:
	Audio();
	virtual ~Audio();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }

public:
	void play(const Source &source);
	void pause(const Source &source);
	void resume(const Source &source);
	void stop(const Source &source);

private:
	// managed openal resources
	ALCdevice* device = nullptr;
	ALCcontext* context = nullptr;
};	// audio

}	// audio
}	// acorn

#endif	// ACORN_AUDIO_H
