#ifndef ACORN_PLATFORM_WINDOWS_H
#define ACORN_PLATFORM_WINDOWS_H

// acorn
#include "common/platform.h"

#if defined(OS_WINDOWS)

// STL
#include <string>

namespace acorn {
namespace windows {

void postNotification();

void requestUserAttention();
void systemBeep();

const std::string getAppdataDirectory();
const std::string getCurrentWorkingDirectory();
const std::string getExecutablePath();

}   // macos
}   // acorn

#endif	// OS_WINDOWS

#endif	// ACORN_PLATFORM_WINDOWS_H
