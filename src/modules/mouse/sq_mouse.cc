// acorn
#include "mouse/sq_cursor.h"
#include "mouse/sq_cursortype.h"
#include "mouse/sq_mouse.h"
#include "mouse/mouse.h"
#include "mouse/cursor.h"

namespace acorn {
namespace mouse {

#define instance() (Module::getInstance<mouse::Mouse>())

static SQInteger wrap_setPosition(SQVM *v) {
	int x, y;
	sq_getinteger(v, 2, (SQInteger *) &x);
	sq_getinteger(v, 3, (SQInteger *) &y);
	instance()->setPosition(x, y);
	return 0;
}

static SQInteger wrap_setPositionInWindow(SQVM *v) {
	int x, y;
	sq_getinteger(v, 2, (SQInteger *) &x);
	sq_getinteger(v, 3, (SQInteger *) &y);
	instance()->setPositionInWindow(x, y);
	return 0;
}

static SQInteger wrap_getPosition(SQVM *v) {
	sq_newtableex(v, 2);

	int mx, my;
	instance()->getPosition(mx, my);

	sq_pushstring(v, "x", -1);
	sq_pushinteger(v, mx);
	sq_newslot(v, -3, SQFalse);

	sq_pushstring(v, "y", -1);
	sq_pushinteger(v, my);
	sq_newslot(v, -3, SQFalse);

	sq_newslot(v, -3, SQFalse);
	return 1;
}

static SQInteger wrap_getX(SQVM *v) {
	sq_pushinteger(v, instance()->getX());
	return 1;
}

static SQInteger wrap_getY(SQVM *v) {
	sq_pushinteger(v, instance()->getY());
	return 1;
}

static SQInteger wrap_setVisible(SQVM *v) {
	bool visible;
	sq_getbool(v, 2, (SQBool *) &visible);
	instance()->setVisible(visible);
	return 0;
}

static SQInteger wrap_getVisible(SQVM *v) {
	sq_pushbool(v, instance()->getVisible());
	return 1;
}

static SQInteger wrap_setRelativeMode(SQVM *v) {
	bool relative;
	sq_getbool(v, 2, (SQBool *) &relative);
	instance()->setRelativeMode(relative);
	return 0;
}

static SQInteger wrap_getRelativeMode(SQVM *v) {
	sq_pushbool(v, instance()->getRelativeMode());
	return 1;
}

static SQInteger wrap_getSystemCursor(SQVM *v) {
	Cursor::SystemCursorType type;
	sq_getinteger(v, 2, (SQInteger *) &type);

	// FIXME: this should be in wrap_setSystemCursor
	auto cursor = instance()->getSystemCursor(type);
	instance()->setSystemCursor(cursor);

	//sqx_pushinstance(v, Cursor::type, instance()->getSystemCursor(type));
	//return 1;

	return 0;
}

static SQInteger wrap_setSystemCursor(SQVM *v) {
	std::shared_ptr<mouse::Cursor> cursor(sqx_checkinstance<mouse::Cursor>(v, 2));
	instance()->setSystemCursor(cursor);
	return 0;
}

// NOTE: functions for acorn.mouse
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	_DECL_FUNC(setPosition, 3, "tnn"),
	_DECL_FUNC(setPositionInWindow, 3, "tnn"),
	_DECL_FUNC(getPosition, 1, "t"),
	_DECL_FUNC(getX, 1, "t"),
	_DECL_FUNC(getY, 1, "t"),

	_DECL_FUNC(setVisible, 2, "tb"),
	_DECL_FUNC(getVisible, 1, "t"),

	_DECL_FUNC(setRelativeMode, 1, "t"),
	_DECL_FUNC(getRelativeMode, 1, "t"),

	_DECL_FUNC(getSystemCursor, 2, "tn"),
	_DECL_FUNC(setSystemCursor, 2, "tx"),

	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.mouse
const static SQFunction classes[] = {
	sq_acorn_cursor,
	nullptr
};

const static SQFunction enums[] = {
	sq_acorn_cursortype,
	nullptr
};

extern "C" int sq_acorn_mouse(SQVM *v) {
	auto instance = std::make_shared<mouse::Mouse>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize mouse module");
	}

	SQRegModule reg;
	reg.module = std::move(instance);
	reg.name = "mouse";
	reg.functions = functions;
	reg.enums = enums;

	return acorn::sqx_registermodule(v, reg);
}

}	// mouse
}	// acorn
