// acorn
#include "keyboard/keyboard.h"
#include "keyboard/sq_modifier.h"

namespace acorn {
namespace keyboard {

EnumRegistry mod_reg[] = {
	{ "none", (int) Keyboard::Modifier::NONE },
	{ "lshift", (int) Keyboard::Modifier::LSHIFT },
	{ "rshift", (int) Keyboard::Modifier::RSHIFT },
	{ "lctrl", (int) Keyboard::Modifier::LCTRL },
	{ "rctrl", (int) Keyboard::Modifier::RCTRL },
	{ "lalt", (int) Keyboard::Modifier::LALT },
	{ "ralt", (int) Keyboard::Modifier::RALT },
	{ "lgui", (int) Keyboard::Modifier::LGUI },
	{ "rgui", (int) Keyboard::Modifier::RGUI },
	{ "num", (int) Keyboard::Modifier::NUM },
	{ "caps", (int) Keyboard::Modifier::CAPS },
	{ "mode", (int) Keyboard::Modifier::MODE },
	{ "ctrl", (int) Keyboard::Modifier::CTRL },
	{ "shift", (int) Keyboard::Modifier::SHIFT },
	{ "alt", (int) Keyboard::Modifier::ALT },
	{ "gui", (int) Keyboard::Modifier::GUI }
};

extern "C" int sq_register_modifier(SQVM *v) {
	return sqx_registerenum(v, "Modifier", mod_reg);
}

}	// keyboard
}	// acorn
