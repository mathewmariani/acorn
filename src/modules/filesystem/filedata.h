#ifndef ACORN_FILESYSTEM_FILEDATA_H
#define ACORN_FILESYSTEM_FILEDATA_H

// C/C++
#include <string>

// acorn
#include "common/data.h"

namespace acorn {
namespace filesystem {

class FileData : public acorn::Data {
public:
	static acorn::Type type;

public:
	FileData(size_t size);
	virtual ~FileData();

public:
	void *getData() const override;
	size_t getSize() const override;

private:
	char *data;
	size_t size;
};	// filedata

}	// filesystem
}	// acorn

#endif	// ACORN_FILESYSTEM_FILEDATA_H
