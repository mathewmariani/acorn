#ifndef ACORN_RESOURCE_SOUNDDATA_H
#define ACORN_RESOURCE_SOUNDDATA_H

// C/C++
#include <memory>

// acorn
#include "common/data.h"
#include "common/object.h"

// stb
#include "libraries/stb/stb_vorbis.h"

namespace acorn {
namespace resource {

class SoundData : public acorn::Object {
public:
	static acorn::Type type;

public:
	SoundData(std::shared_ptr<acorn::Data> data);
	virtual ~SoundData();

public:
	int getChannels() const;
	double getDuration() const;
	int getSampleCount() const;
	int getSampleRate() const;
	int getSampleSize() const;

private:
	stb_vorbis *stream;
	void *buffer;

	int sampleRate;
	int sampleSize;
	int sampleCount;
	int channels;
	double duration;

	std::shared_ptr<acorn::Data> data;
};	// sounddata

}	// resource
}	// acorn

#endif	// ACORN_RESOURCE_SOUNDDATA_H
