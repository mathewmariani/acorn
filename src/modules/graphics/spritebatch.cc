// acorn
#include "common/log.h"
#include "graphics/graphics.h"
#include "graphics/spritebatch.h"
#include "graphics/opengl/buffer.h"

// FIXME:
#include <iostream>
#include <new>
#include <stdexcept>

namespace acorn {
namespace graphics {

acorn::Type SpriteBatch::type = std::type_index(typeid(graphics::SpriteBatch));

SpriteBatch::SpriteBatch(std::shared_ptr<Texture> texture, size_t size) :
	texture(texture),
	size(size),
	nidx(0) {

	if (size <= 0) {
		throw std::runtime_error("invalid spritebatch size.");
	}

	auto buffer_size = sizeof(Vertex) * 4 * size;
	buffer = new opengl::Buffer(buffer_size, Buffer::BufferType::Vertex, Buffer::BufferUsage::Dynamic);

	auto indices_size = sizeof(GLuint) * 6 * size;
	indices = new opengl::Buffer(indices_size, Buffer::BufferType::Index, Buffer::BufferUsage::Static);

	auto elements = (GLuint *)indices->bind();
	for (size_t i = 0; i < size; ++i) {
		elements[i * 6 + 0] = (i * 4 + 0);
		elements[i * 6 + 1] = (i * 4 + 1);
		elements[i * 6 + 2] = (i * 4 + 2);

		elements[i * 6 + 3] = (i * 4 + 2);
		elements[i * 6 + 4] = (i * 4 + 1);
		elements[i * 6 + 5] = (i * 4 + 3);
	}
}

SpriteBatch::~SpriteBatch() {
	delete buffer;
	delete indices;
}

int SpriteBatch::add(Quad *quad, const glm::mat4 &t, int idx) {
	// NOTE: make sure we have a proper index, if any.
	if (idx < -1 || idx >= size) {
		// throw std::runtime_error("invalid sprite index.");
	}

	const auto *v = quad->getVertices();

	// NOTE: this offset will get us either some usefull or useless vertices
	size_t stride = sizeof(Vertex);
	size_t offset = (idx == -1 ? nidx : idx) * stride * 4;

	auto vertices = (Vertex *) ((uint8_t *) buffer->bind() + offset);

	// FIXME: GLM takes to much CPU time
	for (int i = 0; i < 4; ++i) {
		glm::vec4 temp{ v[i].x, v[i].y, 0.0f, 1.0f };
		temp = t * temp;

		// vertex position
		vertices[i].x = temp.x;
		vertices[i].y = temp.y;

		// vertex texcoord
		vertices[i].u = v[i].u;
		vertices[i].v = v[i].v;
	}

	// NOTE: update index
	return (idx == -1) ? (nidx++) : idx;
}

void SpriteBatch::clear() {
	nidx = 0;
}

void SpriteBatch::draw(std::shared_ptr<Graphics> gfx, const glm::mat4 &m) {
	//buffer->unbind();

	// FIXME: remove all opengl variables
	//Graphics::DrawIndexedCommand cmd;
	//cmd.mode = GL_TRIANGLES;
	//cmd.first = 0;
	//cmd.count = nidx * 6;
	//cmd.indices = indices;
	//cmd.texture = texture.get();

	//gfx->draw(cmd);
}

}	// graphics
}	// acorn
