// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// Foundation
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <Foundation/NSUserNotification.h>

namespace acorn {
namespace macos {

void postNotification(const std::string &title, const std::string &message) {
	NSUserNotification* notification = [[NSUserNotification alloc] init];

	// convert arguments to nsstring
	notification.title = [NSString stringWithCString:title.c_str()
		encoding:[NSString defaultCStringEncoding]];
	notification.informativeText = [NSString stringWithCString:message.c_str()
		encoding:[NSString defaultCStringEncoding]];

  // default sound
	notification.soundName = NSUserNotificationDefaultSoundName;

	[[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
	[notification autorelease];
}

void systemBeep() {
	@autoreleasepool {
		NSBeep();
	}
}

void postNotification(const std::string &title, const std::string &subtitle, const std::string &message) {
	// create the notification and setup information
	NSUserNotification* notification = [[NSUserNotification alloc] init];
	notification.title = [NSString stringWithCString:title.c_str()
		encoding:[NSString defaultCStringEncoding]];
	notification.subtitle = [NSString stringWithCString:subtitle.c_str()
		encoding:[NSString defaultCStringEncoding]];
	notification.informativeText = [NSString stringWithCString:message.c_str()
		encoding:[NSString defaultCStringEncoding]];

  // default sound
  notification.soundName = NSUserNotificationDefaultSoundName;

	// manually display the notification
	NSUserNotificationCenter *notificationCenter = [NSUserNotificationCenter defaultUserNotificationCenter];
  [notificationCenter deliverNotification: notification];

	// [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
	// [notification autorelease];
}

void requestUserAttention() {
    @autoreleasepool {
        [NSApp requestUserAttention: NSCriticalRequest];
    }
}

void showApplicationBadge(bool show) {
  NSDockTile *tile = [NSApp dockTile];
  [tile setShowsApplicationBadge: show];
}

void setBadgeCount(int count) {
	// convert arguments to nsstring
	// NSString *badgeLabel = [NSString stringWithCString:label.c_str()
	// 	encoding:[NSString defaultCStringEncoding]];

	NSDockTile *tile = [NSApp dockTile];
	[tile setBadgeLabel: [NSString stringWithFormat:@"%1d", count]];
}


const std::string getExecutablePath() {
  return std::string([NSBundle mainBundle].executablePath.UTF8String);
}

const std::string getBundlePath() {
  return std::string([NSBundle mainBundle].bundlePath.UTF8String);
}

const std::string getResourcePath() {
  return std::string([NSBundle mainBundle].resourcePath.UTF8String);
}

const std::string getApplicationSupportDirectory() {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
	NSString *applicationSupportDirectory = [paths firstObject];
	return std::string(applicationSupportDirectory.UTF8String);
}


}   // macos
}	// acorn

#endif	// OS_MACOS
