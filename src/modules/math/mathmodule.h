#ifndef ACORN_MATH_MODULE_H
#define ACORN_MATH_MODULE_H

// acorn
#include "common/module.h"
#include "common/platform.h"

// NOTE: math constants from GNU
// http://www.gnu.org/software/libc/manual/html_node/Mathematical-Constants.html

// e
#define ACORN_M_E 2.71828182845904523536

// log2(e)
#define ACORN_M_LOG2E 1.44269504088896340736

// log10(e)
#define ACORN_M_LOG10E 0.434294481903251827651

// ln(2)
#define ACORN_M_LN2 0.693147180559945309417

// ln(10)
#define ACORN_M_LN10 2.30258509299404568402

// pi
#define ACORN_M_PI 3.14159265358979323846

// pi/2
#define ACORN_M_PI_2 1.57079632679489661923

// pi/4
#define ACORN_M_PI_4 0.785398163397448309615

// 1/pi
#define ACORN_M_1_PI 0.318309886183790671538

// 2/pi
#define ACORN_M_2_PI 0.636619772367581343076

// tau
#define ACORN_M_TAU 6.28318530717958647692

// 2/sqrt(pi)
#define ACORN_M_2_SQRTPI 1.12837916709551257390

// sqrt(2)
#define ACORN_M_SQRT2 1.41421356237309504880

// 1/sqrt(pi)
#define ACORN_M_SQRT1_2 0.707106781186547524401

#define ACORN_M_DEG2RAD ((ACORN_M_TAU) / 360.0)
#define ACORN_M_RAD2DEG (360.0 / (ACORN_M_TAU))

namespace acorn {
namespace math {

class MathModule : public acorn::Module {
public:
	static acorn::Type type;

public:
	MathModule();
	virtual ~MathModule();

	virtual size_t getHash() const { return type.hash_code(); }
	virtual const char *getName() const { return type.name(); }
};	// math

}	// math
}	// acorn

#endif	// ACORN_MATH_MODULE_H
