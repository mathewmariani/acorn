// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// acorn
#include "common/exception.h"
#include "graphics/metal/framebuffer.h"

namespace acorn {
namespace graphics {
namespace metal {

Framebuffer::Framebuffer(TextureType type, int width, int height) :
	acorn::graphics::Framebuffer(type, width, height) {

}

Framebuffer::~Framebuffer() {

}

const void *Framebuffer::getHandle() const {
	return nullptr;
}

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
