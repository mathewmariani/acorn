R"squirrel"#(

# graphics
describe("acorn.graphics", function() {
	# typeof
	it("is a typeof of table", function() {
		assert.is_typeof(acorn.graphics, "table")
	})

	# functions
	it("contains a function `push()`", function() {
		assert.is_in("push", acorn.graphics)
		assert.is_typeof(acorn.graphics.push, "function")
	})
	it("contains a function `pop()`", function() {
		assert.is_in("pop", acorn.graphics)
		assert.is_typeof(acorn.graphics.pop, "function")
	})
	it("contains a function `identity()`", function() {
		assert.is_in("identity", acorn.graphics)
		assert.is_typeof(acorn.graphics.identity, "function")
	})
	it("contains a function `translate()`", function() {
		assert.is_in("translate", acorn.graphics)
		assert.is_typeof(acorn.graphics.translate, "function")
	})
	it("contains a function `rotate()`", function() {
		assert.is_in("rotate", acorn.graphics)
		assert.is_typeof(acorn.graphics.rotate, "function")
	})
	it("contains a function `scale()`", function() {
		assert.is_in("scale", acorn.graphics)
		assert.is_typeof(acorn.graphics.scale, "function")
	})

	# classes
	it("contains a class `Quad`", function() {
		assert.is_in("Quad", acorn.graphics)
		assert.is_typeof(acorn.graphics.Quad, "class");
	})
	it("contains a class `Shader`", function() {
		assert.is_in("Shader", acorn.graphics)
		assert.is_typeof(acorn.graphics.Shader, "class");
	})
	it("contains a class `SpriteBatch`", function() {
		assert.is_in("SpriteBatch", acorn.graphics)
		assert.is_typeof(acorn.graphics.SpriteBatch, "class");
	})
	it("contains a class `Texture`", function() {
		assert.is_in("Texture", acorn.graphics)
		assert.is_typeof(acorn.graphics.Texture, "class");
	})

	# quad
	describe("acorn.graphics.Quad", function() {
		# typeof
		it("is an index of acorn.graphics", function() {
			assert.is_in("Quad", acorn.graphics)
		})
	})

	# shader
	describe("acorn.graphics.Shader", function() {
		# typeof
		it("is an index of acorn.graphics", function() {
			assert.is_in("Shader", acorn.graphics)
		})
	})

	# spritebatch
	describe("acorn.graphics.SpriteBatch", function() {
		# typeof
		it("is an index of acorn.graphics", function() {
			assert.is_in("SpriteBatch", acorn.graphics)
		})

		# functions
		it("contains a function `add()`", function() {
			assert.is_in("add", acorn.graphics.SpriteBatch)
			assert.is_typeof(acorn.graphics.SpriteBatch.add, "function")
		})
		it("contains a function `set()`", function() {
			assert.is_in("set", acorn.graphics.SpriteBatch)
			assert.is_typeof(acorn.graphics.SpriteBatch.set, "function")
		})
		it("contains a function `clear()`", function() {
			assert.is_in("clear", acorn.graphics.SpriteBatch)
			assert.is_typeof(acorn.graphics.SpriteBatch.clear, "function")
		})
		it("contains a function `draw()`", function() {
			assert.is_in("draw", acorn.graphics.SpriteBatch)
			assert.is_typeof(acorn.graphics.SpriteBatch.draw, "function")
		})
	})

	# texture
	describe("acorn.graphics.Texture", function() {
		# typeof
		it("is an index of acorn.graphics", function() {
			assert.is_in("Texture", acorn.graphics)
		})
	})
})

#)squirrel"#"
