#ifndef ACORN_GRAPHICS_OPENGL_OPENGL_H
#define ACORN_GRAPHICS_OPENGL_OPENGL_H

// acorn
#include "graphics/buffer.h"
#include "graphics/texture.h"

// gl3w
#include "libraries/gl3w/gl3w.h"

GLenum glCheckError_(const char *file, int line);
#define glCheckError() glCheckError_(__FILE__, __LINE__)

namespace acorn {
namespace graphics {
namespace opengl {

// buffer
GLenum map(const graphics::Buffer::BufferType bufferType);
GLenum map(const graphics::Buffer::BufferUsage bufferUsage);

// texture
GLenum map(const graphics::Texture::TextureType textureType);
GLenum map(const graphics::Texture::FilterMode filterMode);
GLenum map(const graphics::Texture::WrapMode wrapMode);

} // opengl
} // graphics
} // acorn

#endif // ACORN_GRAPHICS_OPENGL_OPENGL_H
