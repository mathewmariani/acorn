// C/C++
#include <iostream>

// squirrel
#include <squirrel.h>
#include <sqstdaux.h>
#include <sqstdio.h>
#include <sqstdmath.h>
#include <sqstdstring.h>
#include <sqstdsystem.h>

// acorn
#include "common/log.h"
#include "common/platform.h"
#include "common/import.h"
#include "common/squirrel.h"
#include "acorn/acorn.h"

// SDL
#include <SDL.h>

// acorn
#include "config.h"

// init embedded scripts
const char test_nut[] =
#include "scripts/main.nut"
;

// embedded test scripts
#if defined (ACORN_ENABLE_ACORN)
const char test_acorn[] =
#include "scripts/test_acorn.nut"
;
#endif
#if defined (ACORN_ENABLE_AUDIO)
const char test_audio[] =
#include "scripts/test_audio.nut"
;
#endif
#if defined (ACORN_ENABLE_EVENT)
const char test_event[] =
#include "scripts/test_event.nut"
;
#endif
#if defined (ACORN_ENABLE_FILESYSTEM)
const char test_filesystem[] =
#include "scripts/test_filesystem.nut"
;
#endif
#if defined (ACORN_ENABLE_FONT)
const char test_font[] =
#include "scripts/test_font.nut"
;
#endif
#if defined (ACORN_ENABLE_GRAPHICS)
const char test_graphics[] =
#include "scripts/test_graphics.nut"
;
#endif
#if defined (ACORN_ENABLE_JOYSTICK)
const char test_joystick[] =
#include "scripts/test_joystick.nut"
;
#endif
#if defined (ACORN_ENABLE_KEYBOARD)
const char test_keyboard[] =
#include "scripts/test_keyboard.nut"
;
#endif
#if defined (ACORN_ENABLE_MATH)
const char test_math[] =
#include "scripts/test_math.nut"
;
#endif
#if defined (ACORN_ENABLE_MOUSE)
const char test_mouse[] =
#include "scripts/test_mouse.nut"
;
#endif
#if defined (ACORN_ENABLE_SYSTEM)
const char test_system[] =
#include "scripts/test_system.nut"
;
#endif
#if defined (ACORN_ENABLE_TIMER)
const char test_timer[] =
#include "scripts/test_timer.nut"
;
#endif
#if defined (ACORN_ENABLE_WINDOW)
const char test_window[] =
#include "scripts/test_window.nut"
;
#endif

#define RUN_TEST(_name_)	\
	if (SQ_SUCCEEDED(sq_compilebuffer(v, _name_, sizeof(_name_), #_name_, SQTrue))) { \
		sq_pushroottable(v); \
		if (SQ_SUCCEEDED(sq_call(v, 1, SQTrue, SQTrue))) { \
			sq_getinteger(v, -1, (SQInteger*) &retval); \
		} \
	} \

int sq_acorn_test(SQVM *v) {
	int retval = 0;
	if (SQ_SUCCEEDED(sq_compilebuffer(v, test_nut, sizeof(test_nut), "test.nut", SQTrue))) {
		sq_pushroottable(v);
		if (SQ_SUCCEEDED(sq_call(v, 1, SQTrue, SQTrue))) {
#if defined (ACORN_ENABLE_ACORN)
			RUN_TEST(test_acorn);
#endif
#if defined (ACORN_ENABLE_AUDIO)
			RUN_TEST(test_audio);
#endif
#if defined (ACORN_ENABLE_EVENT)
			RUN_TEST(test_event);
#endif
#if defined (ACORN_ENABLE_FILESYSTEM)
			RUN_TEST(test_filesystem);
#endif
#if defined (ACORN_ENABLE_FONT)
			RUN_TEST(test_font);
#endif
#if defined (ACORN_ENABLE_GRAPHICS)
			RUN_TEST(test_graphics);
#endif
#if defined (ACORN_ENABLE_JOYSTICK)
			RUN_TEST(test_joystick);
#endif
#if defined (ACORN_ENABLE_KEYBOARD)
			RUN_TEST(test_keyboard);
#endif
#if defined (ACORN_ENABLE_MATH)
			RUN_TEST(test_math);
#endif
#if defined (ACORN_ENABLE_MOUSE)
			RUN_TEST(test_mouse);
#endif
#if defined (ACORN_ENABLE_SYSTEM)
			RUN_TEST(test_system);
#endif
#if defined (ACORN_ENABLE_TIMER)
			RUN_TEST(test_timer);
#endif
#if defined (ACORN_ENABLE_WINDOW)
			RUN_TEST(test_window);
#endif
		}
	}

	return retval;
}

static int run_acorn(int argc, char **argv) {
	// create new squirrel virtual machine
	SQVM *v = sq_open(1024);
	if (v == nullptr) {
		printf("Could not initialize Squirrel Virtual Machine\n");
		return 1;
	}

	// required by the sqstd
	sq_pushroottable(v);

	if (sqstd_register_iolib(v) < 0) {
		printf("Could not initialize Squirrel Standard IO Library\n");
		return 1;
	}

	if (sqstd_register_mathlib(v) < 0) {
		printf("Could not initialize Squirrel Standard Math Library\n");
		return 1;
	}

	if (sqstd_register_systemlib(v) < 0) {
		printf("Could not initialize Squirrel Standard System Library\n");
		return 1;
	}

	if (sqstd_register_stringlib(v) < 0) {
		printf("Could not initialize Squirrel Standard String Library\n");
		return 1;
	}

	if (sqx_register_import(v) < 0) {
		printf("Could not initialize Squirrel Extension 'import'\n");
		return 1;
	}

	// set compiler error handler
	sqstd_seterrorhandlers(v);

	sq_pop(v, 1);

	if (sq_register_acorn(v)) {
		printf("Could not initialize Acorn2D\n");
		return 1;
	}

	// boot acorn
	int retval = sq_acorn_test(v);

	// collect garbage in virtual machine
	sq_collectgarbage(v);

	// close our virtual machine
	sq_close(v);

	// clean up SDL
	SDL_Quit();

	return retval;
}

int main(int argc, char **argv) {
	return run_acorn(argc, argv);
}
