// acorn
#include "math/sq_mathmodule.h"
#include "math/sq_transform.h"
#include "math/mathmodule.h"

namespace acorn {
namespace math {

#define instance() (Module::getInstance<math::MathModule>())

// NOTE: functions for acorn.math
#define _DECL_FUNC(name, nparams, pmask) { _SC(#name), wrap_##name, nparams, _SC(pmask) }
static const SQRegFunction functions[] = {
	{ NULL, NULL, NULL, NULL }
};
#undef _DECL_FUNC

// NOTE: types for acorn.window
const static SQFunction types[] = {
	sq_acorn_transform,
	nullptr
};

extern "C" int sq_acorn_mathmodule(SQVM *v) {
	auto instance = std::make_shared<math::MathModule>();
	if (instance == nullptr) {
		return sq_throwerror(v, "Could not initialize math module");
	}

	ModuleRegistry reg;
	reg.module = std::move(instance);
	reg.name = "math";

	return acorn::sqx_registermodule_ext(v, reg);
}

}	// math
}	// acorn
