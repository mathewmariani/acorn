// acorn
#include "platform/macosx.h"

#if defined(OS_MACOS)

// acorn
#include "common/exception.h"
#include "graphics/metal/image.h"

namespace acorn {
namespace graphics {
namespace metal {

Image::Image(std::shared_ptr<image::ImageData> imagedata) :
    acorn::graphics::Image(Texture::TextureType::Texture2D, imagedata) {
}

Image::~Image() {

}

void Image::setFilter(const Texture::FilterMode mode) noexcept {
	Texture::setFilter(mode);
}

void Image::setWrap(const Texture::WrapMode mode) noexcept {

}

const void *Image::getHandle() const {
	return nullptr;
}

}	// metal
}	// graphics
}	// acorn

#endif	// MACOS
