R"squirrel"#(

# acorn
describe("acorn", function() {
	# typeof
	it("is a typeof of table", function() {
		assert.is_typeof(acorn, "table")
	})

	# functions
	describe("functions", function() {
		it("contains a function `getVersion()`", function() {
			assert.is_in("getVersion", acorn)
		})
	})

	# modules
	describe("modules", function() {
		it("contains module `audio`", function() {
			assert.is_in("audio", acorn)
			assert.is_typeof(acorn.audio, "table")
		})
		it("contains module `event`", function() {
			assert.is_in("event", acorn)
			assert.is_typeof(acorn.event, "table")
		})
		it("contains module `filesystem`", function() {
			assert.is_in("filesystem", acorn)
			assert.is_typeof(acorn.filesystem, "table")
		})
		it("contains module `font`", function() {
			assert.is_in("font", acorn)
			assert.is_typeof(acorn.font, "table")
		})
		it("contains module `graphics`", function() {
			assert.is_in("graphics", acorn)
			assert.is_typeof(acorn.graphics, "table")
		})
		it("contains module `joystick`", function() {
			assert.is_in("joystick", acorn)
			assert.is_typeof(acorn.joystick, "table")
		})
		it("contains module `keyboard`", function() {
			assert.is_in("keyboard", acorn)
			assert.is_typeof(acorn.keyboard, "table")
		})
		it("contains module `mouse`", function() {
			assert.is_in("mouse", acorn)
			assert.is_typeof(acorn.mouse, "table")
		})
		it("contains module `system`", function() {
			assert.is_in("system", acorn)
			assert.is_typeof(acorn.system, "table")
		})
		it("contains module `timer`", function() {
			assert.is_in("timer", acorn)
			assert.is_typeof(acorn.timer, "table")
		})
		it("contains module `window`", function() {
			assert.is_in("window", acorn)
			assert.is_typeof(acorn.window, "table")
		})
	})

	# callbacks
	describe("callbacks", function() {
		it("does not contain function `load()`", function() {
			assert.is_not_in("load", acorn)
		})
		it("does not contain function `update()`", function() {
			assert.is_not_in("update", acorn)
		})
		it("does not contain function `draw()`", function() {
			assert.is_not_in("draw", acorn)
		})
		it("does not contain function `init()`", function() {
			assert.is_not_in("init", acorn)
		})

		# mouse callbacks
		it("does not contain function `mousepressed()`", function() {
			assert.is_not_in("mousepressed", acorn)
		})
		it("does not contain function `mousereleased()`", function() {
			assert.is_not_in("mousereleased", acorn)
		})
		it("does not contain function `mousemotion()`", function() {
			assert.is_not_in("mousemotion", acorn)
		})
		it("does not contain function `mousewheel()`", function() {
			assert.is_not_in("mousewheel", acorn)
		})

		# keyboard callbacks
		it("does not contain function `keypressed()`", function() {
			assert.is_not_in("keypressed", acorn)
		})
		it("does not contain function `keyreleased()`", function() {
			assert.is_not_in("keyreleased", acorn)
		})

		# joystick callbacks
		it("does not contain function `joystickadded()`", function() {
			assert.is_not_in("joystickadded", acorn)
		})
		it("does not contain function `joystickremoved()`", function() {
			assert.is_not_in("joystickremoved", acorn)
		})
		it("does not contain function `joystickpressed()`", function() {
			assert.is_not_in("joystickpressed", acorn)
		})
		it("does not contain function `joystickreleased()`", function() {
			assert.is_not_in("joystickreleased", acorn)
		})

		# controller callbacks
		it("does not contain function `controllerpressed()`", function() {
			assert.is_not_in("controllerpressed", acorn)
		})
		it("does not contain function `controllerreleased()`", function() {
			assert.is_not_in("controllerreleased", acorn)
		})
	})
})

#)squirrel"#"
