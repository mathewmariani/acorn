#ifndef ACORN_SQ_MATH_MODULE_H
#define ACORN_SQ_MATH_MODULE_H

// acorn
#include "common/platform.h"
#include "common/squirrel.h"

namespace acorn {
namespace math {

extern "C" int sq_acorn_mathmodule(SQVM *v);

}	// math
}	// acorn

#endif	// ACORN_SQ_MATH_MODULE_H
