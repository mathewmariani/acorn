R"squirrel"#(

# filesystem
describe("acorn.filesystem", function() {
	# directory
	describe("acorn.filesystem.Directory", function() {
		# typeof
		it("is a typeof of class", function() {
			assert.is_typeof(Directory, "class")
		})

		# functions
		it("contains a function `constructor()`", function() {
			assert.is_in("constructor", Directory)
			assert.is_typeof(Directory.constructor, "function")
		})
		it("contains a function `init()`", function() {
			assert.is_in("init", Directory)
			assert.is_typeof(Directory.init, "function")
		})
		it("contains a function `setSource()`", function() {
			assert.is_in("setSource", Directory)
			assert.is_typeof(Directory.setSource, "function")
		})
		it("contains a function `setSaveIdentity()`", function() {
			assert.is_in("setSaveIdentity", Directory)
			assert.is_typeof(Directory.setSaveIdentity, "function")
		})
		it("contains a function `getSearchPath()`", function() {
			assert.is_in("getSearchPath", Directory)
			assert.is_typeof(Directory.getSearchPath, "function")
		})
		it("contains a function `mount()`", function() {
			assert.is_in("mount", Directory)
			assert.is_typeof(Directory.mount, "function")
		})
		it("contains a function `unmount()`", function() {
			assert.is_in("unmount", Directory)
			assert.is_typeof(Directory.unmount, "function")
		})
		it("contains a function `make()`", function() {
			assert.is_in("make", Directory)
			assert.is_typeof(Directory.make, "function")
		})
		it("contains a function `remove()`", function() {
			assert.is_in("remove", Directory)
			assert.is_typeof(Directory.remove, "function")
		})
		it("contains a function `exists()`", function() {
			assert.is_in("exists", Directory)
			assert.is_typeof(Directory.exists, "function")
		})
		it("contains a function `list()`", function() {
			assert.is_in("list", Directory)
			assert.is_typeof(Directory.list, "function")
		})
		it("contains a function `getBase()`", function() {
			assert.is_in("getBase", Directory)
			assert.is_typeof(Directory.getBase, "function")
		})
		it("contains a function `getAppdata()`", function() {
			assert.is_in("getAppdata", Directory)
			assert.is_typeof(Directory.getAppdata, "function")
		})
		it("contains a function `getPref()`", function() {
			assert.is_in("getPref", Directory)
			assert.is_typeof(Directory.getPref, "function")
		})
		it("contains a function `getUser()`", function() {
			assert.is_in("getUser", Directory)
			assert.is_typeof(Directory.getUser, "function")
		})
		it("contains a function `getExecutable()`", function() {
			assert.is_in("getExecutable", Directory)
			assert.is_typeof(Directory.getExecutable, "function")
		})
		it("contains a function `getCurrent()`", function() {
			assert.is_in("getCurrent", Directory)
			assert.is_typeof(Directory.getCurrent, "function")
		})
	})

	# file
	describe("acorn.filesystem.File", function() {
		# functions
		it("contains a function `constructor()`", function() {
			assert.is_in("constructor", File)
			assert.is_typeof(File.constructor, "function")
		})
		it("contains a function `exists()`", function() {
			assert.is_in("exists", File)
			assert.is_typeof(File.exists, "function")
		})
	})

	# filetype
	describe("acorn.filesystem.FileType", function() {
		# typeof
		# FIXME: why does this not work?
		# it("is a typeof of table", function() {
		# 	assert.is_typeof(FileType, "table")
		# })

		# index
		it("contains an index `File`", function() {
			assert.equals(FileType.File, 0)
		})
		it("contains an index `Directory`", function() {
			assert.equals(FileType.Directory, 1)
		})
		it("contains an index `Symlink`", function() {
			assert.equals(FileType.Symlink, 2)
		})
		it("contains an index `Other`", function() {
			assert.equals(FileType.Other, 3)
		})
	})

	# filedata
	describe("acorn.filesystem.FileData", function() {
		# typeof
		it("is a class of acorn.filesystem", function() {
			assert.is_in("FileData", acorn.filesystem)
			assert.is_typeof(acorn.filesystem.FileData, "class")
		})

		# functions
		it("contains a function `constructor()`", function() {
			assert.is_in("constructor", acorn.filesystem.FileData)
			assert.is_typeof(acorn.filesystem.FileData.constructor, "function")
		})
	})
})

#)squirrel"#"
