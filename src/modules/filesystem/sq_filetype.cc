// acorn
#include "filesystem/sq_filetype.h"
#include "filesystem/filesystem.h"

namespace acorn {
namespace filesystem {

EnumRegistry reg[] = {
	{ "File", (int) Filesystem::FileType::FILE },
	{ "Directory", (int) Filesystem::FileType::DIRECTORY },
	{ "Symlink", (int) Filesystem::FileType::SYMLINK },
	{ "Other", (int) Filesystem::FileType::OTHER },
};

extern "C" int sq_acorn_filetype(SQVM *v) {
	return sqx_registerenum(v, "FileType", reg);
}

}	// filesystem
}	// acorn
