// C/C++
#include <iostream>

// squirrel
#include <squirrel.h>
#include <sqstdaux.h>
#include <sqstdio.h>
#include <sqstdmath.h>
#include <sqstdstring.h>
#include <sqstdsystem.h>

// modules
#include "common/import.h"

// acorn
#include "common/platform.h"
#include "common/squirrel.h"
#include "acorn/acorn.h"

// SDL
#include <SDL.h>

static int run_acorn(int argc, char **argv) {
	// create new squirrel virtual machine
	SQVM *v = sq_open(1024);
	if (v == nullptr) {
		printf("Could not initialize Squirrel Virtual Machine\n");
		return 1;
	}

	// required by the sqstd
	sq_pushroottable(v);

	if (sqstd_register_iolib(v) < 0) {
		printf("Could not initialize Squirrel Standard IO Library\n");
		return 1;
	}

	if (sqstd_register_mathlib(v) < 0) {
		printf("Could not initialize Squirrel Standard Math Library\n");
		return 1;
	}

	if (sqstd_register_systemlib(v) < 0) {
		printf("Could not initialize Squirrel Standard System Library\n");
		return 1;
	}

	if (sqstd_register_stringlib(v) < 0) {
		printf("Could not initialize Squirrel Standard String Library\n");
		return 1;
	}

	if (sqx_register_import(v) < 0) {
		printf("Could not initialize Squirrel Extension 'import'\n");
		return 1;
	}

	// set compiler error handler
	sqstd_seterrorhandlers(v);

	sq_pop(v, 1);

	if (sq_register_acorn(v)) {
		printf("Could not initialize Acorn2D\n");
		return 1;
	}

	{	// send cli args to squirrel
		sq_pushroottable(v);
		sq_pushstring(v, "acorn", -1);
		if (SQ_FAILED(sq_rawget(v, -2))) {
			printf("acorn table expected\n");
			return 1;
		}

		sq_pushstring(v, "args", -1);
		sq_newarray(v, argc);

		for (auto i = 0; i < argc; ++i) {
			sq_pushstring(v, argv[i], -1);
			sq_arrayinsert(v, -2, i);
		}

		sq_newslot(v, -3, SQFalse);
		sq_pop(v, 1);
	}

	// boot acorn
	int retval = sq_acorn_boot(v);

	sq_collectgarbage(v);

	// close our virtual machine
	sq_close(v);

	// Clean up
	SDL_Quit();

	return retval;
}

int main(int argc, char **argv) {
	return run_acorn(argc, argv);
}
