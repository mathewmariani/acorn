// This file is just a test file
::print("main!\n");

local mario_texture;
local mario_batch;

local shadow_texture;
local shadow_batch;
local shadow_quad;

local rendertexture;

local kINSTANCES = 1024;

function random() {
	return (1.0 * ::rand() / ::RAND_MAX);
}

local frames = []
for (local y = 0; y < 1; ++y) {
	for (local x = 0; x < 6; ++x) {
		local w = 96.0;
		local h = 84.0;
		local q = acorn.graphics.newQuad(x * 16.0, y * 21.0, 16.0, 21.0, w, h);
		frames.append(q);
	}
}

function sign(value) {
	return ((value < 0) ? -1 : 1);
}

class Animation {

	// all animations
	animations = {};

	// current animation
	current = null;

	// animation from
	frame = 0;
	anim = 0

	// animation timer
	timer = 0.0;

	function constructor() {
		this.animations = { frames = {}, period = 0.0, loop = false };
	}

	function add(name, frames, fps, loop = false) {
  	this.animations[name] <- {
    	frames = frames,
			period = ((fps != 0.0) ? (1.0 / ::abs(fps)) : 1.0),
    	loop = loop,
  	};
	}

	function play(name) {
		if (this.current == this.animations[name]) return;
		this.current = this.animations[name];
		if (true) {
			this.timer = this.current.period;
			this.frame = 0;
			this.anim = this.current.frames[this.frame];
		}
	}

	function stop() {
		current = null;
	}

	function update(dt) {
		if (this.current == null) return;
		this.timer = this.timer - dt;
		if (this.timer <= 0.0) {
			this.frame = this.frame + 1
			if (this.frame > this.current.frames.len() - 1) {
				if (this.current.loop) {
					this.frame = 0;
				} else {
					this.stop();
				}
			}
		this.timer = this.timer + this.current.period;
	    this.anim = this.current.frames[this.frame];
		}
	}
}

class Entity {

	// position
	pos = { x = 0.0, y = 0.0 };

	// velocity
	vel = { x = 0.0, y = 0.0 };
	max_vel = { x = 0.0, y = 0.0 };

	// acceleration
	accel = { x = 0.0, y = 0.0 };

	// drag
	drag = { x = 0.0, y = 0.0 };

	// offset
	offset = { x = 0.0, y = 0.0 };

	// scale
	scale = { x = 0.0, y = 0.0 };

	function constructor() {
		this.pos = { x = 0.0, y = 0.0 };
		this.vel = { x = 0.0, y = 0.0 };
		this.max_vel = { x = 0.0, y = 0.0 };
		this.accel = { x = 0.0, y = 0.0 };
		this.drag = { x = 0.0, y = 0.0 };
		this.offset = { x = 0.0, y = 0.0 };
		this.scale = { x = 0.0, y = 0.0 };
	}

	function update_movement(dt) {
		if (dt == 0.0) { return; }

		// update velocity
		this.vel.x = this.vel.x + (this.accel.x * dt);
		this.vel.y = this.vel.y + (this.accel.y * dt);

		// clamp velocity
		if (::abs(this.vel.x) > this.max_vel.x) {
			this.vel.x = sign(this.vel.x) * this.max_vel.x;
		}
		if (::abs(this.vel.y) > this.max_vel.y) {
			this.vel.y = sign(this.vel.y) * this.max_vel.y;
		}

		// update position
		this.pos.x = this.pos.x + (this.vel.x * dt);
		this.pos.y = this.pos.y + (this.vel.y * dt);

		// update drag
		if (this.accel.x == 0.0 && this.drag.x > 0.0) {
			local sign = sign(this.vel.x);
			this.vel.x = (this.vel.x - (this.drag.x * dt * sign));
			if ((this.vel.x < 0.0) != (sign < 0.0)) {
				this.vel.x = 0.0;
			}
		}
		if (this.accel.y == 0.0 && this.drag.y > 0.0) {
			local sign = sign(this.vel.y);
			this.vel.y = (this.vel.y - (this.drag.y * dt * sign));
			if ((this.vel.y < 0.0) != (sign < 0.0)) {
				this.vel.y = 0.0;
			}
		}
	}

	function update(dt) {
		this.update_movement(dt);
	}

	function draw() {

	}
}

class Mario extends Entity {
	skin = 0;
	flip = false;
	moveTimer = 0.0;
	elapsed = 0.0;
	animSpeed = 0.0;

	animation = null;

	constructor() {
		base.constructor();

		this.pos.x = (random() * acorn.window.getWidth());
		this.pos.y = (random() * acorn.window.getHeight());

		this.max_vel.x = 30.0; this.max_vel.y = 15.0;

		this.drag.x = 30.0; this.drag.y = 15.0;

		this.accel.x = (random() * 2.0 - 1.0) * 30.0;
		this.accel.y = (random() * 2.0 - 1.0) * 15.0;

		this.animation = Animation();
		this.animation.add("mario:idle", [0], 8, true);
		this.animation.add("mario:walk", [3, 4, 5], 12, true);

		flip = random() < 0.5;
		animSpeed = 2.5;
		skin = (1.0 * ::rand() / ::RAND_MAX) * (3 + 1);

		// spritebatch index
		mario_batch.add(frames[0], pos.x, pos.y, 0.0, 1.0, 1.0);
		shadow_batch.add(shadow_quad, pos.x, pos.y, 0.0, 1.0, 1.0);
	}

	function update(dt) {

		// update position
		this.pos.x = (((this.pos.x % acorn.window.getWidth()) + acorn.window.getWidth()) % acorn.window.getWidth());
		this.pos.y = (((this.pos.y % acorn.window.getHeight()) + acorn.window.getHeight()) % acorn.window.getHeight());

		// update timer
		elapsed = elapsed + dt;

		// update move timer
		moveTimer = moveTimer - dt;
		if (moveTimer < 0.0) {
			if (random() < 0.2) {
				accel.x = (random() * 2.0 - 1.0) * 30.0;
				accel.y = (random() * 2.0 - 1.0) * 15.0;

				// flip the sprite
				if (accel.x != 0.0) {
					flip = (accel.x < 0.0);
				}

			} else {
				accel.x = 0.0; accel.y = 0.0;
			}
			moveTimer = 1.0 + random() * 5.0;
		}

		// flip
		local xscale = ((flip) ? -1.0 : 1.0);

		// animation frame
		local e = elapsed * animSpeed;
		local fidx = this.animation.anim;
		//local fidx = (skin.tointeger() * 6) + ::floor(e * 6 % 3);

		/*if (vel.x != 0.0 || vel.y != 0.0) {
			fidx = fidx + 3;
		}*/

		if (this.accel.x == 0.0 || this.accel.y == 0.0) {
			animation.play("mario:idle");
		} else {
			animation.play("mario:walk");
		}

		animation.update(dt);

		mario_batch.add(frames[fidx], this.pos.x, this.pos.y, 0.0, xscale, 1.0);
		shadow_batch.add(shadow_quad, this.pos.x, this.pos.y + 23.0, 0.0, xscale, 2.0);

		base.update(dt);
	}
}

local instances = [];
instances.resize(kINSTANCES);

acorn.load <- function() {
	// create mario texture and spritebatch
	mario_texture = acorn.graphics.newTexture2D("mario.png");
	mario_batch = acorn.graphics.newSpriteBatch(mario_texture, kINSTANCES);

	// create shadow texture and spritebatch
	shadow_texture = acorn.graphics.newTexture2D("shadow.png");
	shadow_batch = acorn.graphics.newSpriteBatch(shadow_texture, kINSTANCES);
	shadow_quad = acorn.graphics.newQuad(0.0, 0.0, 14.0, 4.0, 14.0, 4.0);

	for (local i = 0; i < kINSTANCES; ++i) {
		instances[i] = Mario();
	}
};

acorn.update <- function(dt) {
	// sort instances by y-value
	instances.sort(@(a,b) a.pos.y <=> b.pos.y);

	foreach(i, mario in instances) {
		mario.update(dt);
	}
};

acorn.draw <- function() {
	acorn.graphics.draw(shadow_batch, 0.0, 0.0, 0.0, 0.0, 0.0);
	shadow_batch.clear();

	acorn.graphics.draw(mario_batch, 0.0, 0.0, 0.0, 0.0, 0.0);
	mario_batch.clear();
};

acorn.keypressed <- function(key) {

}
