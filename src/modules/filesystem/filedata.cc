// C/C++
#include <stdexcept>

// acorn
#include "filesystem/filedata.h"

namespace acorn {
namespace filesystem {

acorn::Type FileData::type = std::type_index(typeid(filesystem::FileData));

FileData::FileData(size_t size) :
	data(nullptr), size(size) {
	try {
		data = new char[(size_t)size];
	} catch (std::bad_alloc&) {
		throw std::runtime_error("data");
	}
}

FileData::~FileData() {
	printf("FileData was released\n");
	delete[] data;
}

void *FileData::getData() const {
	return data;
}

size_t FileData::getSize() const {
	return size;
}

}	// filesystem
}	// acorn
