// acorn
#include "common/platform.h"

#if defined(OS_MACOS)

#include "graphics/metal/metal.h"

namespace acorn {
namespace graphics {
namespace metal {

MTLTextureType map(const Texture::TextureType textureType) {
	switch (textureType) {
    case Texture::TextureType::Texture2D: return MTLTextureType2D;
	}
}

MTLSamplerMinMagFilter map(const Texture::FilterMode filterMode) {
	switch (filterMode) {
	case Texture::FilterMode::Nearest: return MTLSamplerMinMagFilterNearest;
	case Texture::FilterMode::Linear: return MTLSamplerMinMagFilterLinear;
	}
}

MTLSamplerAddressMode map(const Texture::WrapMode wrapMode) {
	switch (wrapMode) {
	case Texture::WrapMode::Repeat: return MTLSamplerAddressModeRepeat;
	case Texture::WrapMode::Mirror: return MTLSamplerAddressModeMirrorRepeat;
	case Texture::WrapMode::Clamp: return MTLSamplerAddressModeClampToEdge;
	case Texture::WrapMode::Border: return MTLSamplerAddressModeClampToBorderColor;
	}
}

}	// metal
}	// graphics
}	// acorn

#endif  // OS_MACOS
